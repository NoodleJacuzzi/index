function writeScene(scene) {
	writeHTML(`
		define scientist = sp Scientist; im none;
		define tyler = sp Tyler; im scripts/gamefiles/profiles/tyler1.jpg;
		define amy = sp Amy; im scripts/gamefiles/profiles/amy.jpg;
		define angela = sp Angela; im scripts/gamefiles/profiles/angela.jpg;
		define keith = sp Keith; im scripts/gamefiles/profiles/keith.jpg;
		define lisa = sp Lisa; im scripts/gamefiles/profiles/lisa.jpg;
		define monica = sp Monica; im scripts/gamefiles/profiles/monica.jpg;
		define player = sp player;
	`);
	console.debug(scene);
	if (checkFlag("scarletDark") == true) {
		console.debug(scene);
		scene = scene.replace("scarlet", "dark");
		console.debug(scene);
	}
	if (data.player.time == 3) {
		data.player.time += 1;
		scene = "typhoidMidpointTrigger";
	}
	if (data.player.time == 6) {
		scene = "typhoidEndpointTrigger";
		data.player.time += 1;
	}
	/* Broken emergency-out code for typhoid mary, replaced
	if (scene == data.player.lastLocation) {
		if (data.player.buggedWarning != true) {
			writeSpecial("You quicksaved or refreshed in a place where the return-to-previous-scene button was used, which would cause an infinite loop. You've been returned to the south side of the compound to prevent this. This message will not play again.");
			data.player.buggedWarning = true;
		}
		scene = "typhoidSouth";
	}
	*/
	switch(scene) {
		case "start": {
			document.getElementById('output').innerHTML += `
				<img src="scripts/gamefiles/locations/rain.gif" style="position:absolute; height:100vh; width:100vw; top: 0; left: 0; opacity: 0.3; pointer-events: none;">
				<img src="scripts/gamefiles/logo.png" class = "bigPicture" style="border: none; margin-top: 10vh;">
				<p class="choiceText" onclick="sceneTransition('scenarioSelect')" style="width: 30vw; border:none;">
					Start
				</p>
				`;
			console.log('preparing to rain');
			document.getElementById('wrapperBG').style.backgroundImage = "url(scripts/gamefiles/locations/city.jpg)";
			break;
		}
		case "scenarioSelect": {
			data.items = [];
			data.player.scenario = "";
			document.getElementById('wrapperBG').style.backgroundImage = "url(scripts/gamefiles/locations/streets.jpg)";
			if (data.quicksave != null) {
				writeFunction("quickLoad()", "Load Last Quicksave");
			}
			writeTransition("rainyDayZ", "Scenario 1 - RainyDayZ");
			writeTransition("theFacility", "Scenario 2 - The Facility");
			writeTransition("spreadIsland", "Scenario 3 - Spread Island");
			writeTransition("mansionIntro", "Scenario 4 - Scarlet Mansion");
			writeTransition("typhoidMary", "Scenario 5 - Typhoid Mary");
			//writeTransition("onTheRecord", "Preview - On the Record");
			writeText("...");
			writeTransition("settings", "Game Settings", "#91eba9");
			writeTransition("author", "Author & Credits Information", "#91eba9");
			writeTransition("gallery", "Scene Gallery", "#91eba9");
			writeTransition("cheat", "Enter Cheat Codes", "#91eba9");
			writeText("...");
			writeTransition("start", "Back to Title Screen", "#FF0000");
			writeText("New to the game? Be sure to see the settings menu for save data notes and to disable fetishes you dislike.");
			break;
		}
		case "settings": {
			document.getElementById('wrapperBG').style.backgroundImage = "url(scripts/gamefiles/locations/streets.jpg)";
			if (data.player.beastDisabled == false) {
				writeFunction("toggle('beast')", "Disable beast content", "#FF0000");
			}
			else {
				writeFunction("toggle('beast')", "Enable beast content", "#91eba9");
			}
			if (data.player.rimDisabled == false) {
				writeFunction("toggle('rim')", "Disable rim content", "#FF0000");
			}
			else {
				writeFunction("toggle('rim')", "Enable rim content", "#91eba9");
			}
			if (data.player.wormDisabled == false) {
				writeFunction("toggle('worm')", "Disable parasite content", "#FF0000");
			}
			else {
				writeFunction("toggle('worm')", "Enable parasite content", "#91eba9");
			}
			if (data.player.wsDisabled == false) {
				writeFunction("toggle('ws')", "Disable watersports content", "#FF0000");
			}
			else {
				writeFunction("toggle('ws')", "Enable watersports content", "#91eba9");
			}
			if (data.player.shemaleDisabled != true) {
				writeFunction("toggle('shemale')", "Replace the word 'shemale'", "#FF0000");
			}
			else {
				writeFunction("toggle('shemale')", "Allow the use of the S word again", "#91eba9");
			}
			if (data.player.zombieDisabled != true) {
				writeFunction("toggle('zombie')", "Replace the word 'zombie'", "#FF0000");
			}
			else {
				writeFunction("toggle('zombie')", "Allow the use of the Z word again", "#91eba9");
			}
			writeFunction("saveFile()", "Export save data to string");
			writeFunction("saveTXT()", "Export save data to .noodle file");
			writeText("NOTE: Playing in incognito or with cookies disabled can prevent the game from saving. Use this to save your gallery data if needed.");
			writeFunction("loadFile()", "Import save data from string");
			document.getElementById('output').innerHTML += `
			<p class='rawText'>You can also load external data from .noodle file:</p>
			<input type="file" id="loadFile" onload="fileLoaded()" class = "loadFileButton button" onchange = "loadSave()"></input>
			`;
			writeText("NOTE: This game is incompatible with pre v1.1 Rainy DayZ save files. Trying to load saves from versions before v1.1 will cause the game to break.");
			writeFunction("restartButton()", "Delete all save data", "#FF0000");
			writeTransition("scenarioSelect", "Go back");
			break;
		}
		case "author": {
			writeText("Rainy DayZ is a game created by Noodle Jacuzzi. You can find and check out my other works at my <a href='https://noodlejacuzzi.github.io/index.html'>Master Index</a>.");
			writeText("I'm funded by <a href='https://www.patreon.com/noodlejacuzzi'>Patreon</a>, but Rainy DayZ as a whole doesn't perform too well in monthly polls against Anomaly Vault and Hentai University. If you'd like to support this game's development consider supporting me and letting me know you'd like more. Still, I wouldn't be making this game, or any of my others past Human Alteration App, if not for the people who support me! <br>Thank you to: Robbie, SlackerSavior, Here Not, Confused, Moony, landon sills, Ghost Samurai, A.X.J, Miner49r, Christian Ryan, simon, BlackKnight1945, Eylgar, Jesus Millan, Shaun, Novalis Silveratum, Andre, Stanley Cheong, Ripper, Snargle, Bob, Loui, Rictor86, Simon van Eeden, Dezyego, Zane Dura, Kyle Michael, Na707, Devon McKenzie, Manav, Diederik Toxopeus, Selignite Verine, Christopher Fox, Panda, john, Maybenexttime, Hema Mania, Xazzafrazz, Te Tule, Aleanne, Brian Graham, william lagier, Manny Coutlakis, EniVanella, e3, LostSand, THESLINKER, Ora494, Vikignir, Willy, Zun, Louts, Dylan, notornis, ArtemisAisu, xdrake100, Holden Martin, Sealon, Brandon, that GUY, Elias, 4MinuteWarning, æž—å®¶è±ª, yami, jack spencer, J_C_L, Jinouga, Marco Wassmer, Arthorias28, Colin E, Badaxe, Johngoober, ClockZ -Tar-, cheeriermoss4, Crow, Dominic Pernicone, Lexi Lauton, Ignorant Fool, nicol, Ian Kerris, james aifsgdb, Dan, Joshua Wilmann, beefboy, Evrett Varlan, Shadichaos, GUY premium, Jack Thompson, Josiah gonzales, Akiha Tohno, Timothy Lewis, æ——æœ¨åƒ æ²’æœ‰, Michel, Doryu, AzarathNinja, IDFK, Sean McKeon, None, James Petty, wei huang, Pascal Nitsche, Pandam, ç„¡å çƒé¾œ, Marcus New, kalan willess, cat, axzsd, è½©å®‡ çŽ‹, ThatGuyWithTheFace, Candi-Stryper, INSTSM, Matt Silverman, Bob Sherran, Mason, Liam Connelly, Artheares, Bob, drollest foot892, Daddy Tsume, Hydrq, Blckzck, Bryson, sltcbati, Sniezertas, Ryan Towers, morgan hirst, The Frinky Dink Man, Lex Long, Nick Becker, Jack Masters, Jax Medema, john, Phillip A Brann, philthy, Sonny Coen, Stafford Harris, Blade, Jimboo, Revived Vulture, Zachary Webb, Demonin Koloman, abuse toast, dev, Thomas DeChon, Maxolution, Ville, BeatDem Cheeks, ren hero, Christian Lee, Anarion01, Jacob Atkins, jacob g, meowy2, Ian Whitehead, æ·±æµ·å¸çŽ‹, Void Walker, Cynnau, Mullins JR 74, Malachi Townsley, Mark Laner, Nicman488, Kyverdrade, Jim S, Vault, Marcus Gade, wtrexq, legacy fletcher, Justin Schulterbrandt, unfading89, Konrad Tomanek, dylan makings, Tebachi, Dry_Garen, TCFish, Tayura, Luke Lange, Shadez, Squidy Cool Shoes, Austin, Chima549, Lil boss, Divide, Cameron Chilton, buddy99, earlieman, Dallas Wright, Albin Stenlund, German M Ruiz, goi, Garrett Wade, Roniesis Narvaez, gabriel briones, Joel Humphrey, LIN ZY, Cdev57, Lily Evans, Jesus millan Gonzalez, Trey Gould, Jazz, tyler wyser, fuckboi13, studly787, Lemme learn how to play piano, Nickson Schenk, runge, sebastionLender, Youtube Account, Luis Orellana, Jamie, Fangrove, Asmo, dogsoneup, Michael Stone, Hyunsoo Lee, Big BNB, Mark, Jacob Damoiseaux, RidiculeCat, Damsolo14, asulus diablo, Sean Corley, Hikari, Jaden Clark, Ard Galen, Omar Flores, Tired_Sup, BOK, Jamarion Blair, Snow, Ryker, tito, ReignVI, Shinikami, Jacob Higgs, jaron, Nathaniel Grams, dalvin lopez, Dumcanem, KOOLAID, Skygods, Henry Litten, Forde Wellman, sindre Elshaug, Alex, Shivane, Antoine Panaye, yjzyjz, Draconet547, kyle fenton, Juan Fernandez, Silverstreak1410, qqss, raun q, sakkra83, ASSIRTIVlizard, Tobias Bischoff, Braedon Jasper, Callum Northedge, zafoche, JulikBerlin, Plaaer, asdf123, Kyle Jones, Yongjie Zhao, mahdeennave, è‡´è¿œ èˆ’, Arkanian001, KIVA, Isaiah Sandoval, Neyafi, Cameron Farabee, Venomill, Zachary Schicker, J, Matthew Preston, DigiReave, Johnathen Likens, Maxwell Dedmon, TeLoad, Ary Toussaint, Sid Wolf, Lyko90, Todd Vogel, A Channel, joseph, dawson, Michael Graham, EgoDraconis, Petrichor, Justin, Sean sullivan, D'andre, David Outram, Kient Wong, Ash mash, Calla Smith, Bastl, Rafa Smith, pest, Zach allen, Stan Manson, Carson Goodwin, Dragoon22, mitchellwolbert, undead270, ThatOtherGuy, Ashwinder, Minerve1, Seamus Porter, BINKS, John Nixon, Biblicallyaccuratepitbull, Genxin, Greatsage56 ., sb2017, john sparks, Sera, Bradley Herbert, SomebodyElse, Peter H, é•¿å¹³ é¬£, thesingleguy, Kethis Immortalis, Anton Schmidt, david thompson, Jayesonn leleaux, Bleed1ngthunder, Eric Hslew, Vikteren, theoron, AnotherAccount333, Kaliden, Tyler Ross, FrostyTB, Limitless, Geoff Heimos, Kieron Kow, Slomberg, henry grobins, ddkre, SmolFish, GirmusCz, Harry Brasch, joel larsen, Markus Hansen, Leanerbike1363, ryan, Tanman, Grim2011, Phanes, Scumstango, Wei, Dugelle, Noah675, john smith, Your Husbando, Carlos, Verillia tristari, ItsAllOgreNow, Jesse Greene, joe, Jane, murgatroid99, Roy, Wild Bill, ChronosEdge, and brandon for supporting my work!");
			writeText("In addition, a special thanks to the people who've directly helped my writing and code. Wild Bill for his save to file system, Stiggy752 for the framework for the game's CSS, OrangeMaestro for his work finding hundreds of typos and grammatical errors I've missed, and DaddyTsume for being a great last-minute tester. Thank you all!");
			writeText("As a note, this game also features a dog zombie. To view his scenes you must deliberately wound yourself, go to the forest, and either leave your weapon behind or have 0 stamina when you encounter him. If you dislike this sort of content, you can avoid it easily by doing anything other than what I've just described. To avoid any potential legal troubles, this dog is actually a woman in a very, very convincing dog suit. If you want absolutely no part in this at all, disable some of the more out-there fetishes in the settings menu.");
			writeText("If you have any comments, criticisms, or suggestions for content for this or future chapters, please let me know about them in the <a href='https://docs.google.com/forms/d/1eQ7C1lajlXoJJ8THlQnKwOyMZOU2KWD4-uRX7C7VFew/'>Anonymous Suggestion Box</a>, <a href='https://tfgames.site/phpbb3/viewtopic.php?f=6&t=12575'>TFgames thread</a>, on my patreon, or in the Noodle Jacuzzi <a href='https://discord.gg/pDht5BZ'>Discord</a>.");
			
			
			writeText("As an extra note, no cheat codes aside from <b>oowoo</b>, sorry. There's one you get for unlocking all the game's scenes, but you get that for unlocking all the scenes. If you have any cheat code ideas, please let me know!");
			writeText("Thanks for reading this section. You can check below for some unused scenario ideas / concepts if you'd like. It's just text about the game's design and development though.");
			document.getElementById('wrapperBG').style.backgroundImage = "url(scripts/gamefiles/locations/streets.jpg)";
			writeTransition("unusedIdeas", "Unused Concepts");
			writeTransition("scenarioSelect", "Go back");
			break;
		}
		case "unusedIdeas": {
			writeText("First and foremost, Rainy DayZ was originally a minigame for Human Alteration App, for that reason a lot of the code was kinda spaghetti, and the game was meant to be very short. Please let me know if any traces of that are still present (IE a scene is too short).");
			writeText("The Rainy DayZ scenario was based on the left 4 dead 2 map Hard Rain, I thought the rainy aesthetic and the element of backtracking was really cool, although it didn't add too much from a gameplay standpoint with this being a text-based game. Other left 4 dead maps would be cool to riff off as well, but I wouldn't want to tread the exact same ground with the zombies.");
			writeText("L4d also had really cool special infected, most of which I tried to make some kind of spin on. However ultimately hunters and witches felt too much like regular zombies, spitters boomers and jockies were just not appealing, and the rest were hard to find images that wouldn't completely break immersion. Resident evil was the biggest inspiration for special zombies.");
			writeText("Zombie ideas are hard to come by since they need to be represented by regular people in the images, so nothing like Nemesis from RE3. I tried asking fans of the game for ideas, but no dice. Thus, I'll need to focus on situations in the future. The game already has the 'survivor scavenging for supplies' and 'scientist during a containment breach' stories, but there are a lot more I think could work:");
			writeText("<del>A husband and wife trying to escape a resort island during an outbreak. The wife is infected with a mild strain, so you need to help her while escaping the island. Based on Dead Island, this is the most likely pick for a scenario 3.</del> Used for Spread Island.");
			writeText("A reporter enters a mall during an outbreak to get footage of what's going on. Based on Dead Rising, the biggest draw here is a system where you either help survivors or let them get turned while you record them, super evil!");
			writeText("A man is stranded and seeks help in a seemingly abandoned town in Alaska, during a heavy snowstorm. This was actually the original idea for this game, which is why the 'unlock everything' cheat in v1 was cold mile. One cool idea is that halfway through enemy agents come into the town and try to hunt your down.");
			writeText("Something based on silent hill, probably a pretty straightforwards 'man goes to spoopy town' storyline, but it'll be tough to capture SH's unnerving aesthetic.");
			writeText("A hard-boiled agent investigates a spooky mansion. This one would be straight from Resident Evil 1, and would actually use drawn CGs from Eroquis's work instead of real porn gifs. I ended up scrapping it since it would deviate too far from the shemale zombie concept to straight up use resident evil's monsters, but it could probably end up being its own game.");
			writeText("Similar to the above, a game set in space based on the CG from Eroquis's <i>Dirty Prison Ship</i> game. This one would probably also need to be its own game, as aliens are even farther from the original concept.");
			writeText("Lemme know if you have any ideas to expand on the above, or just suggestions in general! Over the next few updates I hope to add more demos, check out these ones for a preview of what's to come:");
			writeTransition("onTheRecord", "Preview - On the Record");
			document.getElementById('wrapperBG').style.backgroundImage = "url(scripts/gamefiles/locations/streets.jpg)";
			writeTransition("scenarioSelect", "Go back");
			break;
		}
		case "gallery": {
			document.getElementById('wrapperBG').style.backgroundImage = "url(scripts/gamefiles/locations/streets.jpg)";
			generateGalleryNav();
			writeTransition("scenarioSelect", "Go back");
			break;
		}
		case "cheat": {
			document.getElementById('wrapperBG').style.backgroundImage = "url(scripts/gamefiles/locations/streets.jpg)";
			document.getElementById('output').innerHTML += `
				<p class='centeredText'>Enter cheat code: <input type="text" id="cheatSubmission" value=""></p>
				<p class='choiceText' onclick='diagnostic()'>Submit</p>
			`;
			writeTransition("scenarioSelect", "Go back");
			break;
		}
		//Rainy DayZ
		case "rainyDayZ": {
			document.getElementById('wrapperBG').style.backgroundImage = "url(scripts/gamefiles/locations/rainy.jpg)";
			writeMed("scripts/gamefiles/characters/Rainy DayZ.jpg");
			writeText("Scenario 1 - Rainy DayZ");
			writeText("In a world where a zombie virus has spread, transforming people into voracious sex-hungry shemales, you play as a young woman searching for supplies to survive.");
			writeText("Exploration is encouraged since there's no reward for returning to the safehouse with the supplies. Try to get all of the events if you can and don't worry, there's no penalty aside from your time for a game over.");
			data.player.scenario = "Rainy DayZ";
			updateMenu();
			countScenes();
			writeTransition("rainyStart", "Start Rainy DayZ");
			writeTransition("scenarioSelect", "Go back");
			break;
		}
		case "rainyStart": {
			data.player.stamina = 2;
			data.player.wounded = false;
			data.player.infected = false;
			data.player.lockbox = "";
			data.player.townBattle = true;
			data.player.cityBattle = true;
			data.player.factoryBattle = true;
			data.player.flower = false;
			data.quicksave = null;
			data.items = [];
			addItem("Baseball Bat");
			writeText("It's cloudy and overcast today, just like it's been for months now. Your stockpile of food has run dry, so you need to find some more before you get hungry. Not only that, but with the weather getting worse you'll need to find enough to keep you fed for at least a few days.");
			writeText("You remember there was a convenience store in the city. At this point it's your only hope. You carry a large wooden bat in your hands in case you see one of <b>them</b>.");
			writeText("You only have so much room for carrying things. Trying to pick up more than six objects is too much for you, and you'll need to carry food and water back on the return trip.");
			writeSpeech("player", "", "<i>It's time to get moving.</i>");
			writeText("You can get to the city via the highway, it isn't too long of a trek. For now, you'll need to [townStreets|open the door] and head out into town.");
			zombieFooter();
			break;
		}
		case "townStreets": {
			writeText("Worn and partially broken, a [greenHouse|house painted green] stands strong against the wind. If you recall right, it was the home of a handyman. While there's probably no food, there might be something inside that could help you.");
			writeText("More out of the way, a [redHouse|red house] actually has some fortifications intact. If you take the time to break in you might find something useful.");
			if (checkItem("House Key") == true) {
				writeText("On the main street surrounded by an iron fence, a [blueHouse|blue house] is practically calling your name. You've tried to break in before, but this time you have a key.");
			}
			else {
				writeText("On the main street surrounded by an iron fence, a lone blue house stands tall, taunting you. You've tried to break in before, but the entire house is locked down. Every window and back door are barricaded closed, and the front door is deadbolted shut. Maybe you can find a key for the house somewhere?");
			}
			if (data.player.townBattle == true) {
				writeText("You think you hear a [townBattle|zombie] skulking around. Taking it out now might make moving around easier.");
			}
			if (checkItem("Food Supply") == false) {
				writeText("You can see a path to the [highway|highway] from here. You'll need to be careful though, since it's probably crawling with infected.");
			}
			else {
				writeText("You're almost there. A fifteen minute walk is (hopefully) all that's between you and your [safehouse|safehouse]. With exhaustion creeping over you, you wonder if you can make it. Maybe it would be best to find a place to rest for now.");
			}
			zombieFooter();
			break;
		}
		case "townBattle": {
			if (data.player.flower == true) {
				if (data.player.infected == true) {
					writeEvent('flower2');
				}
				else {
					writeEvent('flower1');
				}
			}
			else {
				if (data.player.infected == true) {
					writeText("After making sure there isn't another one around, you approach the zombie from behind. Even despite the weather though she still notices you and turns around, but after starting at you for a moment she just starts walking away.");
					data.player.townBattle = false;
					writeText("The zombie wasn't interested and left. Now you can move around the [townStreets|streets] without worry.");
				}
				else {
					if (data.player.stamina > 0) {
						if (checkItem("Baseball Bat") == true) {
							writeText("After making sure there isn't another one around, you approach the zombie from behind. Even despite the weather though she still notices you and turns around. Time to fight.");
							writeSpecial("You're slightly more tired now, but you've defeated the zombie!");
							data.player.stamina -= 1;
							data.player.townBattle = false;
							writeText("You've defeated the zombie. Now you can move around the [townStreets|streets] without worry.");
						}
						else {
							writeEvent("basic1");
						}
					}
					else {
						writeEvent("basic1");
					}
				}
			}
			break;
		}
		case "blueHouse": {
			writeText("Success! The key works and you're able to break into the home. Unfortunately, the owners are already gone, and they took everything they owned with them. You navigate the rooms one by one but there's barely anything here.");
			writeText("On a large table there's a bowl full of old, moldy fruit. Curiously, only the lemons have been eaten. It almost looks like something tore into each of them and left the rinds in the bowl, ignoring the other fruit.");
			writeText("The place is pretty well secured though. If it weren't for the occasional sound in the walls, probably a rat, you'd be tempted to make this into your new safehouse.");
			if (checkItem("House Key") == true) {
				if (data.player.infected == false) {
					writeText("The master bedroom isn't even all that dusty. The whole room is cozy, and still warm despite the weather. The large, queen-sized bed sits at the west side of the room. You're exhausted from the journey and you still have a ways to go, maybe you should take a event[worms1|rest?]");
				}
				else {
					writeText("The master bedroom isn't even all that dusty. The whole room is cozy, and still warm despite the weather. The large, queen-sized bed sits at the west side of the room. You're exhausted from the journey and you still have a ways to go, maybe you should take a event[worms2|rest?]");
				}
			}
			else {
				writeText("You go to investigate the master bedroom, but your hand stops on the knob as you hear a slithering sound. After a moment of silence, you hear it again. Something's in the walls, and you don't think its a natural animal. You decide that this isn't the best place for a nap.");
			}
			writeText("You can [townStreets|leave] through the front door at any time.");
			zombieFooter();
			break;
		}
		case "greenHouse": {
			writeText("The place is a mess and water is leaking through the ceiling. You've been here a few times before, so you can find your way around here pretty easily. Every room has been picked clean at this point except the owner's studio.");
			writeText("You walk into the studio, the table and equipment covered with dust. A minute of searching is all it should take to be done with this place.");
			if (checkItem("Flashlight") == false) {
				writeText("On the desk underneath a thin layer of dust is a large item[Flashlight|flashlight]. It might still work if it uses long-lasting batteries. It's pretty large though, you should be careful not to take anything you don't need.");
			}
			else {
				writeText("There's an empty spot on the table where you took the flashlight from.");
			}
			writeText("There's also a large empty box on the table. At first you think you might be able to store a snack in there for later, but you can't really think of a good reason to do that.");
			if (data.player.wounded == false) {
				writeText("Over the side of the table you think you can see [greenInjury|something metal]. A spike of sharp metal is in the way and is too heavy to move, but you can probably just grab the thing around it.");
			}
			else {
				writeText("On close inspection, the gleaming object behind the table is a broken screwdriver. The spike blocking the way is still wet with some blood. It isn't rusty though, so no chance of tetanus.");
			}
			writeText("You can [townStreets|leave] through the front door at any time.");
			zombieFooter();
			break;
		}
		case "greenInjury": {
			data.player.wounded = true;
			writeText("You lean on the table and reach for the object, but your arm isn't long enough. You stretch as far as you can, and your fingertips rub against the object.");
			writeText("The push is enough to knock the object over, farther than you could hope to reach. To make matters worse you lose your balance, causing the spike to slash against your arm. 'Fuck!' You exclaim. It really hurts.");
			writeSpecial("You are now wounded!");
			writeText("There's no point in whining about it. You should [greenHouse|move on] and keep searching.");
			break;
		}
		case "redHouse": {
			if (checkItem("Food Supply") == false) {
				writeText("You enter the red house after bypassing some barricades and sliding open a window. There are some footprints in the dust here, and they're recent too. You skulk around for a moment to confirm that the house is empty. Whoever lives here must be out right now.");
				if (checkItem("Snack") == false) {
					writeText("After some quick searching you find a small cache of food. It probably belongs to another survivor, and you don't want to make enemies. That said, taking just one item[Snack|snack] shouldn't hurt.");
				}
				else {
					writeText("More searching reveals a small cache of food. It probably belongs to another survivor. You don't want to make enemies, and you already have something to eat.");
				}
				if (checkItem("Rope") == false) {
					writeText("Hanging on the wall is a length of item[Rope|rope]. Food is precious, but supplies like this are a lot less important. It should be fine to take if you think you need it.");
				}
				else {
					writeText("On the wall is a hook where some rope used to hang.");
				}
				if (checkItem("Bag of Marbles") == false) {
					writeText("Aside from those, you find a item[Bag of Marbles|bag of marbles] sitting on the floor. These are clearly more important than food, and you should definitely take them.");
					writeText("That was a joke, a little bit of post-apocalypse humor for you.");
				}
				else {
					writeText("It doesn't seem like there is anything else here worth taking.");
				}
			}
			else {
				writeText("You sneak into the house very quietly. The window is already open, so you don't think you're alone here. Soon enough you're proven right, and you peer around the corner to find a [fellowSurvivor|fellow survivor]! ");
			}
			writeText("You can [townStreets|leave] through the front door at any time.");
			zombieFooter();
			break;
		}
		case "fellowSurvivor": {
			if (data.player.infected == true) {
				writeEvent("survivor1");
				writeText("You should [townStreets|leave] before she gets back up.");
			}
			else {
				writeText("There's a woman sprawled out on the floor, she's covered in infected cum, and it looks fresh. She's completely out cold, but she hasn't transformed yet.");
				writeText("Her breathing is ragged, and as her chest heaves droplets of fresh jizz leak out of her pussy.");
				writeText("This is extremely dangerous, even a small amount of infected cum can have a disorientating effect. If you event[survivor2|get any closer] you won't be able to control yourself, if you haven't lost control already.");
			}
			break;
		}
		case "highway": {
			writeText("Your journey is exhausting and a half hour of walking leaves you weary, especially since you need to be on high alert.");
			data.player.stamina -= 1;
			writeSpecial("You're slightly more tired now, but you've almost made it to the city!");
			writeText("The trip along the highway is slow and tense, but your stealth pays off once you catch wiff of a scent powerful enough to be smelt through the rain. It's the smell of infected cum.");
			writeText("The deed is already done, the crowd of shambling, purposeless zombies is slowly dispersing. Their former target is laying in the middle of the road, their body caked with semen so thick the rain isn't enough to wash it off.");
			writeText("You don't want to wait around to see them wake up, and you certainly don't want any of the crowd to notice you. It would be best to just try and [cityStreets|sneak past] the horde and make your way into the city. With the rain masking your steps and scent, you should have no problem making it through with your clothes still on.");
			if (checkItem("Bag of Marbles") == true) {
				writeText("But an idea strikes you as you get ready to start sneaking. Your event[horde1|bag of marbles] makes a small click as you hold it in your hand. You could use it as a distraction to make your way through more safely.");
				writeText("Although it doesn't seem like it's necessary, since they have no idea you're here anyway.");
			}
			else {
				writeText("If you had some things to throw, you might be able to cause a distraction to get by. But for now, sneaking is your only option.");
			}
			zombieFooter();
			if (checkItem("Food Supply") == true) {
				writeEvent("horde2");
			}
			break;
		}
		case "cityStreets": {
			writeText("You've reached the outskirts of the city, cars line the road even out here. The rain is getting pretty heavy, so it might be fore the best and hurry on your way.");
			writeText("As you walk down the street, you see an open window on the second story of an apartment building. The place looks closed off, so there aren't any other entrances. If you wanted to, you could scale the wall and [studioApartment|enter the apartment complex].");
			if (data.player.flower != true) {
				writeText("Blooming through a crack in the pavement is a beautiful bright-red flower. You aren't much of a nature gal yourself, but it couldn't hurt to [flower|give it a whiff].");
			}
			else {
				writeText("The bright red flower is here. It's really pretty, but something about the way it smells leaves you a bit put off.");
			}
			if (checkItem("Food Supply") == false) {
				writeText("But at the end of the block is your real goal, the [convenienceStore|convenience store]. The entrance is covered in wooden planks, but you spy a way you could sneak in.");
			}
			else {
				writeText("The rain is getting harder and harder, and your bag isn't getting any lighter. You need to may your way back to the safehouse as soon as you can. The clearest way back you can think of would be to take the highway again, but with the storm going on you'll need to take the road directly instead of walking on the outskirts.");
				writeText("If you feel like you need to get something in the [convenienceStore|convenience store] you won't have another chance to get it.");
				if (data.player.stamina > 1) {
					writeText("You're well fed and prepared for a journey, you feel like you could head through the [highway|highway] even considering the danger.");
				}
				else {
					writeText("You don't feel like you have enough energy to risk taking the highway.");
				}
				if (checkItem("Factory Key") == true) {
					writeText("The factory's gate key is sitting in your pocket. The path to the [factoryGate|factory] should be relatively high and dry, especially compared to the highway.");
				}
				writeText("You could cut directly through the [forest|forest]. You'll be cutting down on distance, and there should be far fewer zombies.");
			}
			zombieFooter();
			break;
		}
		case "flower": {
			data.player.flower = true;
			writeText("You lean in to smell the flower. It's pretty incredible how beautiful nature can be in this hellscape of a town.");
			writeText("It smells pretty bad though. Way too powerful. Saccharine, a sickly sweet smell.");
			writeText("Still, it's nice to look at. As you stand back up the room spins slightly. You're probably dehydrated or something. How pretty the flower is quickly wipes away a nagging sensation that something bad is about to happen.");
			writeText("You should probably [cityStreets|get moving].");
			break;
		}
		case "studioApartment": {
			writeText("You climb through the windowsill and enter the apartment silent as a mouse. The open window has left the room looking pretty damp, but is otherwise in pretty good shape.");
			if (checkItem("Snack") == false) {
				writeText("On the table, amid a pile of trash looks like a sealed can of food you can take as a item[Snack|snack].");
			}
			writeText("Hanging from the wall is a small mangled key, it's so mashed up you can't make out the text on it anymore.");
			writeText("The halls are claustrophobic and dark, each door you pass by has been smashed open, not a good sign. An even worse sign is the smell coming from some of the rooms, the smell of infected semen. Down two flights of stairs is a much heavier door held closed by a simple latch. Inside is some sort of [apartmentBasement|basement room].");
			writeText("Despite how dangerous the situation seems, you still have a way out by [cityStreets|heading back out through the window].");
			zombieFooter();
			break;
		}
		case "apartmentBasement": {
			writeText("The room is small, and there's an odd smell to the room. You think you hear something for a moment, but there isn't a second sound.");
			if (checkItem("Air Freshener") == false) {
				writeText("Resting on a desk are a few objects, the first one you notice is a neon green lemon-scented air freshener. You could use this to mask your scent, although the rain is already doing a pretty good job of that.");
			}
			else {
				writeText("Resting on a desk are a few objects, but you already took the air freshener. You notice that the smell of the room seems a little more powerful now.");
			}
			if (data.player.wounded == false) {
				writeText("In the corner of the room is a wastebasket filled with a variety of objects. If you really feel like you need to, you could [apartmentInjury|reach inside] to search for anything useful.");
			}
			else {
				writeText("In the corner of the room is a wastebasket filled with a variety of objects splattered with a few drops of blood. You don't really feel like you need to search through it right now.");
			}
			if (checkItem("House Key") == false) {
				writeText("There's a pile of clothes in the corner. A quick look through them reveals a single item[House Key|blue key with a lemon-scented air freshener attached].");
			}
			else {
				writeText("There's a pile of clothes in the corner.");
			}
			writeText("Finally, resting on the desk is an [journal|old journal], probably owned by the previous resident of this place.");
			writeText("If you feel you've had enough, you can leave through the [studioApartment|door].");
			zombieFooter();
			break;
		}
		case "apartmentInjury": {
			data.player.wounded = true;
			writeText("You start rummaging through the wastebasket but quickly pull your hand out. You need to hold back your voice as you stem the small flow of blood. <b>You're bleeding!</b> A small knife was left inside the basket.");
			writeText("You don't have a medical kit, so there's nothing to do now but wait. You can clean yourself up a bit more later, but once the bleeding stops you can go back to [apartmentBasement|searching the room].");
			break;
		}
		case "journal": {
			writeText("The journal appears normal until a month ago, when the infections started. Everything after that is a scrawl of notes of varying usefulness. One pretty clean section is devoted to the zombies, and the different types of them.");
			writeText("Read the entry on event[journalBasic|basic zombies].");
			writeText("Read the entry on event[journalHunter|hunter zombies].");
			writeText("Read the entry on event[journalSiren|siren zombies].");
			writeText("Read the entry on event[journalWorms|zombie worms].");
			writeText("There's also an event[journalFlower|extra note] laying on the table.");
			writeText("If you're finished reading, you can [apartmentBasement|close the book].");
			break;
		}
		case "cityBattle": {
			if (data.player.stamina > 0) {
				if (checkItem("Baseball Bat") == true) {
					writeText("You shove the zombie away from you and ready your weapon. Time to fight.");
					writeSpecial("You're slightly more tired now, but you've defeated the zombie!");
					data.player.stamina -= 1;
					data.player.cityBattle = false;
					writeText("You've defeated the zombie. Now you can move around the [apartmentBasement|room] without worry.");
				}
				else {
					
					writeEvent("basic2");
				}
			}
			else {
				
				writeEvent("basic2");
			}
			break;
		}
		case "convenienceStore": {
			data.player.townBattle = true;
			writeText("It takes some work to get inside the convenience store, which is a good sign. The place is still intact and dry, but the glass windows leave you worried about this place's safety. It probably won't hold up against the building storm.");
			writeText("But aside from that the place is everything you had hoped it would be. Some of the shelves have been picked clean, but others are still lined with sealed food. It only takes you a few moments to fill your backpack with a month's worth of food and water. Now you need to carry them back.");
			if (checkItem("Food Supply") == false || checkItem("Water Supply") == false) {
				if (data.items.length > 4 && checkItem("Food Supply") == false && checkItem("Water Supply") == false) {
					writeText("You're carrying too much to take the food and water supply. You'll need to drop some things.");
					writeText("Drop your drop[" + data.items[0].name + "|"+data.items[0].name+"].");
					writeText("Drop your drop[" + data.items[1].name + "|"+data.items[1].name+"].");
					writeText("Drop your drop[" + data.items[2].name + "|"+data.items[2].name+"].");
					writeText("Drop your drop[" + data.items[3].name + "|"+data.items[3].name+"].");
					writeText("Drop your drop[" + data.items[4].name + "|"+data.items[4].name+"].");
					if (data.items.length > 5) {
						writeText("Drop your drop[" + data.items[5].name + "|"+data.items[5].name+"].");
					}
				}
				else {
					if (checkItem("Food Supply") == false) {
						writeText("Take the item[Food Supply|food supply].");
					}
					if (checkItem("Water Supply") == false) {
						writeText("Take the item[Water Supply|water supply].");
					}
				}
			}
			else {
				if (checkItem("Food Supply") == false) {
					writeText("Take the item[Food Supply|food supply].");
				}
				if (checkItem("Water Supply") == false) {
					writeText("Take the item[Water Supply|water supply].");
				}
			}
			if (data.player.infected == false) {
				writeText("The golden rule is to not snack on scavenged stuff, so you spend a little bit longer looking around for something you can eat right now. Behind the counter and inside a sealed container is a single glazed donut. It has a bit of a smell, and is probably stale right now, but in this moment you could be the last person on earth to event[tainted|eat a donut].");
			}
			else {
				if (checkItem("Factory Key") == false) {
					writeText("Underneath the container of the infected food is a item[Factory Key|small silver key] with the words 'Ridgewood Factory' engraved on it.");
				}
			}
			if (checkItem("Food Supply") == true && checkItem("Water Supply") == true) {
				writeText("The wind outside is howling and shaking the windows, it might be time to [cityStreets|leave] soon.");
			}
			zombieFooter();
			break;
		}
		case "factoryGate": {
			writeText("This path is much shorter than the highway, all you'll need to do is cut through the factory itself to get to the back road. From there it's a straight, mostly dry way back to town.");
			writeText("You slide the key into the large lock at the front door and turn it, and are met with a crunching sound. The gate opens, but won't close.");
			writeText("The factory itself is wide and spacious, but is near entirely empty. All that's left are construction materials. From what you can gather, this place wasn't finished yet. You can navigate the place pretty easily and are soon on the second floor.");
			if (checkItem("Baseball Bat") == true) {
				writeText("A large door you walk through strikes you as a risk. If you've been followed here then you could end up getting caught in a dead end. You could drop[Baseball Bat|leave your weapon behind] to bar the door.");
			}
			else {
				writeText("Laying against a door as a makeshift lock is your baseball bat. It's so tightly set in place that you couldn't move it if you wanted to.");
			}
			if (data.player.factoryBattle == true) {
				writeText("You think you hear a noise and dart for cover. Slowly, a zombie walks across a length of scaffolding above you. If you wanted to you could try to [factoryBattle|take it out], or you could just [upperFactory|sneak past it] and go further into the factory.");
			}
			else {
				writeText("With the zombie out of the way, you are free to go [upperFactory|further into the factory].");
			}
			zombieFooter();
			break;
		}
		case "factoryBattle": {
			if (data.player.infected == false) {
				data.player.factoryBattle = false;
				writeText("For whatever reason, the zombie seems to be distracted by something and runs off. Looks like you're safe to [factoryGate|wander the factory] for now.");
			}
			else {
				if (checkItem("Baseball Bat") == true) {
					writeText("After making sure there isn't another one around, you climb up and swing at the zombie from behind. A single swing and she's down for the count.");
					data.player.factoryBattle = false;
					writeText("You've defeated the zombie. Now you can move around the [factoryGate|factory] without worry.");
				}
				else {
					
					writeEvent("basic3");
				}
			}
			break;
		}
		case "upperFactory": {
			writeText("The inner chambers of the factory seem almost like a maze. Winding pathways lead back and forth in circles a few times before you get your bearings together. Not only that, but it's pretty dark in here. It isn't pitch black, but if you aren't careful you could hurt yourself.");
			writeText("A small noise and something moving in the corner of your vision set you on high alert. You crouch and wait patiently, and after a few minutes you can hear a sound that almost sounds like soft snoring. Whatever it is, you're in the home stretch now, you shouldn't take a risk by calling out to it.");
			if (data.player.infected == true) {
				if (checkItem("Flashlight") == true) {
					writeText("Navigating in the darkness with something in the room could be risky. You could use your event[siren1|flashlight] to find your way out of the building.");
				}
				else {
					writeText("It's way too dark to try finding whatever might be in the room with you, so hunting it down isn't an option.");
				}
				if (checkItem("Rope") == true) {
					writeText("You could event[siren2|set a trap with your rope] and make some noise to lure whatever's in the area to it. It probably can't chase you down if it's tied down.");
				}
				else {
					writeText("If you had something to make a trap with, you could snare whatever might be in the room with you and escape.");
				}
			}
			writeText("If you aren't too worried about the snoring sound, you could just quietly [townStreets|sneak out of the factory]. It should be a straight and simple shot back to town.");
			zombieFooter();
			break;
		}
		case "forest": {
			writeText("Hiking through the forest ends up being a lot more exhausting than you thought.");
			if (data.player.stamina > 0) {
				data.player.stamina -= 1;
				writeSpecial("You feel exhausted, but you've almost made it through the forest.");
			}
			writeText("The foliage grows thinner and the wind grows stronger, it should only be another few minutes before you arrive in town. Your elation is put to a quick end when the snap of a twig tells you you're being followed from a good distance away.");
			if (checkItem("Baseball Bat") == true) {
				writeText("Obviously you can't dump the supplies, but you could drop[Baseball Bat|leave your weapon behind] to try and escape faster.");
			}
			else {
				writeText("You've already dropped your weapon to try and save yourself, but you can still hear whatever is following you gaining on you.");
			}
			if (data.player.wounded == true) {
				if (checkItem("Baseball Bat") == false) {
					writeText("Wounded and without a weapon, all you can hope to do is event[hunter2|make this easier on yourself and give up].");
				}
				else {
					if (data.player.stamina < 1) {
						writeText("Wounded and completely exhausted, all you can hope to do is event[hunter1|go down fighting].");
					}
					else {
						writeText("You wait for a moment, cautious and ready. Your wounds keep you from sprinting away from danger. Luckily you have your trusty bat and you have enough energy left to fight. When the zombified dog leaps out from the brush, your quick reaction lets you slam it away with your bat.");
						writeText("Your pursuer defeated you keep moving through the forest. Soon enough you break through the treeline and [townStreets|arrive in town].");
					}
				}
			}
			else {
				writeText("You break out into a sprint, trees rushing past you as you desperately try to outpace whatever's following you. Thankfully you aren't wounded. Soon enough you break through the treeline and [townStreets|arrive in town].");
			}
			zombieFooter();
			break;
		}
		case "safehouse": {
			writeText("You've done it! This young woman will survive another month with the supplies you've gathered, and all without being viciously molested by sex-hungry shemale zombies too! If this was the goal you've been shooting for, then great job!");
			writeText("Check out the 'Author Information' section in the scenario selection menu for a more detailed set of credits, or play the game again to try and get yourself on the business end of the infected.");
			writeTransition("rainyDayZ", "Thanks for playing!");
			if (data.player.infected == true) {
				writeEvent('infected');
			}
			break;
		}
		//The Facility
		case "theFacility": {
			document.getElementById('wrapperBG').style.backgroundImage = "url(scripts/gamefiles/locations/facility.jpg)";
			writeMed("scripts/gamefiles/characters/The Facility.jpg");
			writeText("Scenario 2 - The Facility");
			writeText("Deep in the dark heart of an underground laboratory, experiments are unfolding which will lead the world to ruin.");
			writeText("You, a scientist in the thick of a chaotic containment breach, must escape the facility with your life and mind intact.");
			data.player.scenario = "The Facility";
			updateMenu();
			countScenes();
			//writeText("Note: Only 6 of the scenes are currently unlockable.");
			//writeSpecial("This scenario unfinished. It should be finished in version 2.0 if you'd like to play the full thing. You can explore the facility, but you can't use the keycards to unlock the inoculation lab:");
			//writeFunction("writeEvent('start')", "View the intro");
			writeTransition("facilityStart", "Start The Facility");
			writeTransition("scenarioSelect", "Go back");
			break;
		}
		case "facilityStart": {
			data.player.horny = true;
			data.player.keycards = 0;
			data.player.flags = "";
			data.items = [];
			writeEvent('start');
			data.quicksave = null;
			break;
		}
		case "weaponLab": {
			//writeHTML(`title You're in the weapon lab.`);
			writeText("The weapons lab is empty and dark. From what you can gather you were unconscious for at least a few hours. Most of the work you did here was theoretical, there's nothing you can take to defend yourself. Plus, the idea of hurting your masterpieces makes you feel a little queasy.");
			if (checkItem('Soldier Recording 1') != true) {
				writeText("Laying on a nearby table is a soldier's helmet, the kind used by armed fireteams sent in to evaluate containment breaches. Has a team already been down here? Were you abandoned? In any case, these helmets are equipped with cameras, and have small item[memory cards that keep a backup of their footage|Soldier Recording 1]");
			}
			else {
				writeText("Laying on a nearby table is a soldier's helmet, the kind used by armed fireteams sent in to evaluate containment breaches. You've already taken the memory card inside, but you can't view its contents here.");
			}
			if (data.player.flags.includes("weaponLabDoor") != true) {
				writeText("The room is filled with weapon testing rooms, basically broom closets where new strains are tested on bound infected. All but one are empty, it looks like one is still in use. You could [try to input the passcode for the door|lockedCell].");
			}
			else {
				if (data.player.flags.includes("domination") != true) {
					writeText("The weapon's lab cell door is open. Inside, nearly frothing at the mouth is a woman wearing almost nothing. It looks like her hands are zip-tied to an outcropping of the wall.");
					writeText("She lunges at you, or at least she attempts to. The restraints aren't enough to keep her tied up forever though, you should get moving.");
					if (data.player.horny == true) {
					writeText("Although... Just looking at her, tied up and her cock angry from a lack of release... You're pretty hard yourself. Maybe you should [shut her up|domination] and get some relief at the same time.");
					}
				}
				else {
					writeText("The weapon's lab cell door is open. Inside the soldier formerly known as Jones is laying glassy-eyed in a puddle of your and her cum. Though her restraints look like they're on the verge of breaking, you should be alright for now.");
				}
			}
			writeText("If there's nothing else to do here, you should probably leave and head into the [laboratory hub|labHub].");
			break;
		}
		case "domination": {
			writeEvent(scene);
			data.player.horny == false;
			data.player.flags += "domination";
			break;
		}
		case "lockedCell": {
			document.getElementById('output').innerHTML += `
				<p class='centeredText'>Enter passcode: <input type="text" id="cheatSubmission" value=""></p>
				<p class='choiceText' onclick='passcodeEntry()'>Submit</p>
			`;
			writeText("[Go back|weaponLab].");
			break;
		}
		case "labHub": {
			writeText("You're in the floor's central hub. Multiple doors around you lead to different research labs.");
			writeText("The door to the now empty [Weapons Lab|weaponLab] is behind you.");
			writeText("The door to the [Chemical Lab|chemLab] is to your left. Inside research is conducted mostly on fluids, research like DNA analysis and acid tests. No live testing, so the room should be safe.");
			writeText("The door on your right is the [Containment Lab|containmentLab], where live specimen are studied. The security is extremely tight in case of a containment breach, so it's likely still safe even given the circumstances.");
			writeText("The door down the hall leads to the [Parasite Lab|parasiteLab], where non-human infection vectors are being researched. It's likely to be very dangerous.");
			if (checkItem('White Keycard') == true && checkItem('Blue Keycard') == true && checkItem('Red Keycard') == true) {
				writeText("With three keycards in your possesion, you can freely get through the security checkpoint and enter the [Innoculation Lab|innoculationLab].");
			}
			else {
				writeText("The largest door in front of you leads to the Inoculation Lab, but the door is sealed shut. It's the most likely location of the cure you need, but there's no way in. An ID reader is on the side of the door, in order to gain access despite containment you'd need three keycards to insert here.");
			}
			writeHTML(`
				t Finally, there's a pathway to the [main lobby|lobby]. Inside is the elevator you can use to escape.
			`);
			break;
		}
		case "lobby": {
			writeText("You're in this floor's entrance lobby. There's a desk at the front for a security guard, but there's nobody on duty right now.");
			writeText("At the desk is a computer used for checking records and observing the status of various other rooms. Each room is labeled with a bright red 'COMPROMISED' warning, except for the inoculation lab which is marked with the word 'LOCKDOWN'.");
			writeText("Also on screen is a data file, someone was viewing it before everything went to shit. The file is labeled ['Alternative Siren Strains'|sirenFile].");
			writeText("Attached to the computer is a small external memory card reader. If you have any, you could [view their recordings here|recordingFiles].");
			writeText("The exit elevator appears to be working, but in your condition you won't last long. You need to find the cure before you can escape. For now, you should head back into the [central hub|labHub].");
			break;
		}
		case "recordingFiles": {
			writeText("You open the program needed to view memory card recordings. What will you do?");
			if (checkItem('Soldier Recording 1') == true) {
				writeText("[View the memory card you found in the weapons lab|fireteam1].");
			}
			if (checkItem('Soldier Recording 2') == true) {
				writeText("[View the memory card you found in the parasite lab|fireteam2].");
			}
			if (checkItem('Captain Recording') == true) {
				writeText("[View the captain's memory card|fireteam3].");
			}
			writeText("[Power off the terminal|lobby].");
			break;
		}
		case "fireteam1": {
			writeEvent(scene);
			writeText("[Finish watching|recordingFiles].");
			break;
		}
		case "fireteam2": {
			writeEvent(scene);
			writeText("[Finish watching|recordingFiles].");
			break;
		}
		case "fireteam3": {
			writeEvent(scene);
			writeText("[Finish watching|recordingFiles].");
			break;
		}
		case "sirenFile": {
			writeSpecial("AUTHENTICATION GRANTED");
			writeHTML(`
				t The following are several logs for an alternative to the current Siren-class infection strain. Due to their method of hunting proving to be extremely dangerous for male employees and administrative personnel, large amounts of research have been conducted to finding another method as good at attracting male victims. 
				...
			`);
			if (data.player.wsDisabled != true) {
				writeHTML(`
					im sirenPiss.gif
					t Siren-deviation delta, Water Nymph
					t Sirens possess a valuable ability to draw in targets from secure locations without line of sight. Suggestions were posed that the sense of smell and taste would make good alternatives. 
					t With this deviation the smell and taste of the subject's urine were enhanced with string pheromones. The desired result was obtained as victims would desperately lap up the deviation's piss and greedily beg for more until full infection was guaranteed. 
					t However already infected individuals also showed similar desires for the liquid, and results have shown that nearly every other strain of infected will even ignore humans in the area to find these nymphs and drink from them. 
					t Even if we make the nymphs dislike pissing on or in already infected individuals, other infected quickly realize the nymphs can be forced to urinate if subjected to prostate stimulation. Thus results in a large-form gangrape until the nymph admits total defeat and wets herself before quickly being licked clean. This continues until the nymph can no longer hold back from spraying piss from any kind of anal pleasure, and wetting themselves becomes linked to achieving orgasm. 
					t Result: Partial failure. Some researchers believe the nymph's strain can be repurposed into another unique strain entirely. 
				`);
			}
			else {
				writeHTML(`
					t REDACTED INFORMATION.
					t RESEARCH CONTENT HAS BEEN AUTOMATICALLY PURGED BASED ON USER SETTINGS.
				`);
			}
			writeHTML(`
				...
				im sirenUrethra.gif
				t Siren deviation gamma, Charybdis. 
				t The siren's soft bodytype has proven attractive towards male subjects who would normally resist the allure of common infected. Thus this strain attempts to capitalize on that. The infected's urethra becomes stretchier allowing for better urethral stimulation. The already existing nervous structure means that sensitivity enhancement is not required as the play is already highly pleasurable when combined with the usual endurance improvements of basic strains. 
				t However allowing the urethral to accept an insertion the size of an average male's cock has proven difficult and time-consuming. Not only that but the treatment has had the side-effect of causing the charybdis to become extremely masochistic, often manually preventing herself from cumming by sounding herself deeply just before orgasm, preventing any ejaculation but prolonging the pleasure of masturbation sessions. 
				t Result: Total failure. Creating a strain of masochistic small-dicked girls who want their cocks to be fucked is not getting us closer to global infection. The researchers conducting the tests were revealed to have sounding fetishes, and were promptly infected with the charybdis strain themselves. Videos will be distributed for employee morale and to make an example of the charybdis team. 
				...
			`);
			if (data.player.beastDisabled != true) {
				writeHTML(`
					im sirenBeast.gif
					t Siren deviation epsilon, Echidna. 
					t Result has proven generally positive in using a siren deviation as a support type infected. Their ability to guide and nurture other strains, particularly hound strains, has proven effective so far. By removing their ability to hypnotically attract men, and instead giving them the ability to attract and infect animals, the advantages of the siren remain intact without posing danger to research and combat staff. Like most siren strains they have higher than usual intelligence, though not to the point of speech. 
					t ?fetish rim; Notably, echidnas do not use animals strictly for infection and pleasure, and will often spend long periods of time pampering their "children", referring to their infected pets. While typically this includes bending over and being knotted one-by-one by an entire pack of hounds, this also means slowly pleasuring her children with her hands while passionately rimming their asses and moaning like a normal woman would make out with her husband. Though this seems like a waste of infected sperm, hounds treated this way seem to become even stronger and more virile after these milking situations. 
					t ?fetish ws; New children invited into the pack tend to become very attached to their denmother, and often after their first sexual experience with their echidna they will mark her. This seems to result in extreme sexual pleasure for the echidna, who will quickly achieve orgasm by being sprayed with piss. Echidnas with large packs will sometimes even request that the entire pack spray and mark her all at once, cumming extremely hard from her small, unused penis while attempting to drink as much as she can. Hounds involved in this ritual seem to lose respect for their denmother at this point and begin to treat the echidna roughly, much to the denmother's delight. 
					t Result: Partial success. While they synergize with hound-type infected, that strain was already extremely effective. The echidna strain is being further tested, but has not recieved final approval yet. 
				`);
			}
			else {
				writeHTML(`
					t REDACTED INFORMATION.
					t RESEARCH CONTENT HAS BEEN AUTOMATICALLY PURGED BASED ON USER SETTINGS.
				`);
			}
			writeHTML(`
				t [CLOSE FILE|lobby]
			`);
			break;
		}
		case "chemLab": {
			writeText("The chemical lab smells very powerfully of alcohol, the kind used for cleaning. There aren't any traces of infected in the area, so you should be fine to relax.");
			writeText("The primary research station has a number of chemicals lined out for study. You have a lot of experience here, so if you have the time you could [mix up something useful at the table|chemistry].");
			if (data.player.horny == true) {
				writeText("Insatiable despite how recently you just came, your cock is still standing strong. The chairs here are comfortable, you could [take a moment to pleasure yourself|chemJerk] if you wanted.");
			}
			writeText("There are several research terminals in the room, with your authentication you can access a synopsis of whatever the researchers here were working on. One is on a project labeled '[whizzer|whizzerFile]' another is an [unread email on resistance to the infection|resistanceFile], and another is some kind of [email on the infection process|infectionFile].");
			if (checkItem('Orange Potion') == true) {
				if (checkItem('Blue Keycard') != true) {
					writeText("You spy the blue plastic card you'll need to get into the innoculation lab, but it's down a grate. Now that you have the acidic potion, with careful application you can get into the grate and take the item[access keycard|Blue Keycard] inside.");
				}
				else {
					writeText("You've already grabbed the keycard from the grate.");
				}
			}
			else {
				writeText("Several researchers must've left in a hurry, one of them very high ranked. Researchers of that level keep an access keycard, so after looking around you spy the blue plastic card you'll need to get into the innoculation lab, but it's down a grate. The thing is bolted on, you'll need to break or melt the bolts to get inside.");
			}
			if (checkItem('Captain Recording') != true) {
				writeText("Laying on a nearby table is another soldier's helmet, why are these just laying around? There's an extra marking on it to signify this is a squad leader's helmet. The helmet is intact, and you can item[take the memory card inside|Captain Recording]");
			}
			else {
				writeText("Laying on a nearby table is a squad leader's helmet, you've already taken the memory card from it.");
			}
			writeText("Your throat is starting to feel a little dry, you might've been out for a long time, and spurting from your cock probably isn't helping the matter. Normally you wouldn't chance it, but an open beaker of an off-yellow fluid is here. It seems inviting somehow, and you are really thirsty, so maybe you should [drink it?|whizzerDrink]");
			writeText("If there's nothing else to do here you should probably leave and go back to the [laboratory hub|labHub].");
			break;
		}
		case "chemistry": {
			writeHTML(`
				t You're sitting at the chemistry table. A number of ingredients are available to you.
			`);
			if (data.player.horny == true) {
				writeHTML(`
					t Your cock throbs, distracting you for just a moment and reminding you of how horny you are right now. You'll need to be quick so as not to end up in a jerk-off frenzy, or relieve yourself so you can work at 100%.
				`);
			}
			writeText("First up is the [yellow ingredient, marked with an image of a rock|yellow].");
			writeText("Next is the [red ingredient, marked with an image of an object shattering|red].");
			writeText("Then there's the [blue ingredient, marked with an image of a brain|blue].");
			writeText("Then there's the [white ingredient, marked with an image of a penis|white].");
			writeText("If you don't want to mix anything, you could [step away from the table|chemLab]");
			break;
		}
		case "yellow": {
			writeText("You take the yellow ingredient, marked with an image of a rock. What will you mix it with?");
			writeText("Mix with the [red ingredient, marked with an image of an object shattering|yellowred].");
			writeText("Mix with the [blue ingredient, marked with an image of a brain|yellowblue].");
			if (data.player.horny == true) {
				writeText("Mix with the [white ingredient, marked with an image of a penis|yellowwhite].");
			}
			else {
				writeText("Mix with the [white ingredient, marked with an image of a penis|yellowwhite]. Your mind unclouded, you think this will probably create a dangerous, unstable mixture.");
			}
			writeText("You could always [change your mind|chemistry]");
			break;
		}
		case "red": {
			writeText("You take the red ingredient, marked with an image of an object shattering. What will you mix it with?");
			writeText("Mix with the [yellow ingredient, marked with an image of a rock|yellowred].");
			if (data.player.horny == true) {
				writeText("Mix with the [blue ingredient, marked with an image of a brain|redblue].");
			}
			else {
				writeText("Mix with the [blue ingredient, marked with an image of a brain|redblue]. Your mind unclouded, you think this will probably create a dangerous, unstable mixture.");
			}
			if (data.player.horny == true) {
				writeText("Mix with the [white ingredient, marked with an image of a penis|redwhite].");
			}
			else {
				writeText("Mix with the [white ingredient, marked with an image of a penis|redwhite]. Your mind unclouded, you think this will probably create a dangerous, unstable mixture.");
			}
			writeText("You could always [change your mind|chemistry]");
			break;
		}
		case "blue": {
			writeText("You take the blue ingredient, marked with an image of a brain. What will you mix it with?");
			writeText("Mix with the [yellow ingredient, marked with an image of a rock|yellowblue].");
			if (data.player.horny == true) {
				writeText("Mix with the [red ingredient, marked with an image of an object shattering|redblue].");
			}
			else {
				writeText("Mix with the [red ingredient, marked with an image of an object shattering|redblue]. Your mind unclouded, you think this will probably create a dangerous, unstable mixture.");
			}
			if (data.player.horny == true) {
				writeText("Mix with the [white ingredient, marked with an image of a penis|bluewhite].");
			}
			else {
				writeText("Mix with the [white ingredient, marked with an image of a penis|bluewhite]. Your mind unclouded, you think this will probably create a dangerous, unstable mixture.");
			}
			writeText("You could always [change your mind|chemistry]");
			break;
		}
		case "white": {
			writeText("You take the white ingredient, marked with an image of a penis. What will you mix it with?");
			if (data.player.horny == true) {
				writeText("Mix with the [yellow ingredient, marked with an image of a rock|yellowwhite].");
			}
			else {
				writeText("Mix with the [yellow ingredient, marked with an image of a rock|yellowwhite]. Your mind unclouded, you think this will probably create a dangerous, unstable mixture.");
			}
			if (data.player.horny == true) {
				writeText("Mix with the [red ingredient, marked with an image of an object shattering|redwhite].");
			}
			else {
				writeText("Mix with the [red ingredient, marked with an image of an object shattering|redwhite]. Your mind unclouded, you think this will probably create a dangerous, unstable mixture.");
			}
			if (data.player.horny == true) {
				writeText("Mix with the [blue ingredient, marked with an image of a brain|bluewhite].");
			}
			else {
				writeText("Mix with the [white ingredient, marked with an image of a brain|bluewhite]. Your mind unclouded, you think this will probably create a dangerous, unstable mixture.");
			}
			writeText("You could always [change your mind|chemistry]");
			break;
		}
		case "yellowred": {
			writeText("You mix together the ingredients, and the result is a frothy orange liquid that's quite fizzy. You recognize the reaction, you've just created acid!");
			writeText("It's safe for any kind of organic material, it'll just melt things like metal or stone.");
			addItem("Orange Potion");
			writeText("[Potion in hand, you should finish up|chemLab]");
			break;
		}
		case "yellowblue": {
			writeText("You mix together the ingredients, and the result is a frothy green liquid that's doesn't seem very reactive. You recognize the consistency, you've created a sleeping potion!");
			writeText("Splashing this on someone should knock them clean out, it's quite effective on infected individuals and is the most common type of sedative used here.");
			addItem("Green Potion");
			writeText("[Potion in hand, you should finish up|chemLab]");
			break;
		}
		case "yellowwhite": {
			writeHTML(`
				t The moment the first drip of the mixtures touch, you realize this was a bad idea. The liquids meeting fuzz and bubble, before exploding outwards in a cloud of gas. 
				t You cough and run back, but you definitely inhaled a lot of the stuff. As you clear your lungs and wait for the cloud to dissipate, you feel your cock twitch. 
				t It's... Absolutely larger than before, by at least two inches. At the very least though it doesn't seem to be growing any further. You'll need to hope nothing bad comes from this. 
				t As you step forwards you sigh with pleasure. Just from walking forwards it's clear your cock's sensitivity is even higher. <b>You'll need to jerk off at some point if you want to be clear headed!</b>
			`);
			data.player.horny = true;
			data.player.flags += "rockPotion";
			writeText("[Clean yourself off|chemLab]");
			break;
		}
		case "redblue": {
			writeEvent('mindBreak');
			writeTransition('theFacility', 'GAME OVER', '#FF0000');
			break;
		}
		case "redwhite": {
			writeEvent('cockBreak');
			writeTransition('theFacility', 'GAME OVER', '#FF0000');
			break;
		}
		case "bluewhite": {
			writeEvent('cockMind');
			writeTransition('theFacility', 'GAME OVER', '#FF0000');
			break;
		}
		case "chemJerk": {
			writeEvent('solo1');
			break;
		}
		case "whizzerDrink": {
			writeEvent('whizzer');
			break;
		}
		case "whizzerFile":{
			if (data.player.wsDisabled == false) {
				writeSpecial("AUTHENTICATION GRANTED");
				writeBig("whizzer.jpg");
				writeText("Research synopsis and update on strain XW-tt2, infected pseudoname 'Whizzer'.");
				writeText("The virus is highly effective and contaminates others quickly, even able to infect subjects already infected.");
				writeText("Almost immediately sperm production in men and infected women ceases, replaced with hyperactivity in the bladder. Subject will immediately piss out  remaining sperm and seek to infect others with their now highly contagious urine.");
				writeText("Male subjects and infected display odd signs when in the presence of whizzer fluid, rationalizing reasons to drink it and even deluding themselves into thinking they're dying of thirst or that it's just colored water. Even if the subject is aware of this effect, they still delude themselves or conveniently 'forget'.");
				writeText("No such reaction occurs with women, so open-top experimentation is permitted while the staff remains all female.");
				writeText("[CLOSE FILE|chemLab]");
			}
			else {
				writeText("This scene has been disabled by your fetish settings for containing watersports content.");
				writeText("[CLOSE FILE|chemLab]");
			}
			break;
		}
		case "infectionFile":{
			writeSpecial("AUTHENTICATION GRANTED");
			writeText("Dear Madeline");
			writeText("As per my last email, you'll find an outline of the infection process below.");
			writeText("We've completely removed the airborne infection ability from the virus after the lackluster performance at the Sanctuary Resort, so only direct fluid transmission is the only method now.");
			writeText("Once someone drinks an infected subject's cum, the changes begin immediately. DNA is overwritten, causing their physical appearances to become more attractive and feminine, but the primary changes occur in the reproductive organs.");
			writeText("For women the womb begins to mutate, eggs and fallopian tubes becoming testicles. 'Fat nuts' as you've called them. They'll begin to be pushed out as a new prostate gland begins to develop, eventually the woman's vagina will expel these new nuts. I understand the process is pleasurable, I don't need to hear your collection of wet popping noises.");
			writeText("At the same time the woman's clit begins to lengthen and change shape until it becomes a fully functioning penis. The woman's eggs, having been thoroughly ravaged by the virus, are expelled in a paste similar to a man's spunk. No, we do not keep any for 'tasting'. New highly infectious sperm cells are then generated and the woman's intelligence & sense of self are overpowered with a desire to spread the infection through rape.");
			writeText("For male subjects the process is mostly similar, the changes to appearance are more pronounced as they become much more feminine. They'll grow highly sensitive breasts and anal sensitivity will improve. Old sperm is pumped out and infectious, highly addictive sperm is built up. Highly submissive males have a higher tendency to mutate into the smaller-dicked strains like Sirens. This improves anal sensitivity even higher and actually decreases cock size. Studies have found that they still remain just as enticing as their large-dicked sisters.");
			writeText("Here's an example we took from an earlier virus test showing the results in action. Hopefully this will satisfy you, and you'll keep your fantasies in your room where they belong instead of endangering everyone here with your self-infection fetish.");
			writeBig("infection.gif");
			writeText("[CLOSE FILE|chemLab]");
			break;
		}
		case "resistanceFile": {
			writeHTML(`
				t Re: INFECTION WEAKNESS (URGENT)
				t Recent tests performed on infection vectors show extremely positive results, however test ID #Gamma-1872 resulted in another example of an infected with near full control of mental capacity. 
				t The linking factor between these cases may be sugar consumption. It's possible that taking infected fluids orally along with food or drink high in sugar will result in a situation similar to a mental vaccine. It's possible this is why there were so many cases of mentally-resistant infected during the test case at Sanctuary Hotel Resort, as the infection was first distributed through cocktails. Some of these likely coated the rim with sugar. The aerosolized drug spread through the hotel afterwards may have been the reason the tests were not a complete failure.
				t Team C formally requests a budget increase to study this phenomenon, and hopefully engineer a resistance in the highly dangerous alpha strain. Having <i>her</i> in the facility can be quite nerve-wracking for some members of the staff.nerve-wracking for some members of the staff. 
				t We already have a test subject, some military officer came into a testing site claiming she was looking for shelter. There's a blockade around the city, nobody should come looking for her. Even if she does develop some of the characteristics like enhanced strength, we don't forsee her containment being a problem.
			`);
			writeText("[CLOSE FILE|chemLab]");
			break;
		}
		case "containmentLab": {
			writeText("You're in the containment lab, and you aren't alone. There's at least five other infected in here, but luckily they're locked up. They're stuck behind a full three inches of strong plastic shielding and a set of thick iron bars.");
			writeText("One is agitated by your presence, and is humping the wall with her sizable cock. Others are attempting to get through the wall and attack you, and the rest don't seem to care.");
			writeText("There's a [central research station here|hordeFile], where a stakeholder synopsis of research on infected hordes was being written.");
			if (data.player.horny == true) {
				writeText("Your cock is already acting up again, it seems like the humping infected woman's show is getting to you more than you expected. If you wanted you could take a seat and [jerk off|containmentJerk].");
			}
			if (checkItem('Green Potion') == true) {
				if (checkItem('White Keycard') != true) {
					writeText("One cell in particular is marked as unused, but has a single infected woman inside. On her neck is a lanyard with your prize. Now that you have the sleeping gas potion, with careful application you can get into the cell and take the item[access keycard|White Keycard] she's wearing.");
				}
				else {
					writeText("You put the infected researcher to sleep, and stole their access keycard.");
				}
			}
			else {
				writeText("One cell in particular is marked as unused, but has a single infected woman inside. She's pressing against the plexiglass for freedom, and you notice she's wearing a tattered labcoat.");
				writeText("Hanging from her neck on a lanyard is a white keycard. Grabbing it would probably help get you into the innoculation lab, but you'll need to disable or knock the infected researcher out first.");
			}
			writeText("There's an air vent shaft on the floor here, the cover has been unscrewed and taken off. It'll lead back to the laboratory hub, but it's so small you should only use it in case of an emergency, like if you were being chased.");
			writeText("If there's nothing else to do here you should probably leave and go back to the [laboratory hub|labHub].");
			break;
		}
		case "hordeFile": {
			writeSpecial("AUTHENTICATION GRANTED");
			writeBig("horde.jpg");
			writeText("We've been noticing a trend lately in certain infection methods, so experimentation has started on groups of infected, dubbed 'hordes'. Usually infected tend not to care about grouping up, they scatter throughout a city looking for prey. However, when a large amount of subjects become infected at the same time a sort of bond is formed between them.");
			writeText("These bonded infected will then stick together and even use rudimentary teamwork to spread the infection, and in turn increase the size of their horde. They'll gang-rape survivors, causing the infection to spread extremely rapidly. ");
			writeText("Some members of the horde, usually those with the smallest dick size, are relegated to bottom positions. Rather than becoming pent up, the horde will frequently engage in orgies, taking whatever frustrations they have out on the 'bottom bitches' as we've come to know them. Bottoms will become more submissive overtime, increasing anal sensitivity and decreasing penis length to near Siren levels. ");
			writeText("[CLOSE FILE|containmentLab]");
			break;
		}
		case "containmentJerk": {
			writeEvent('solo2');
			break;
		}
		case "hordeRelease": {
			writeEvent('releaseTheHorde');
			break;
		}
		case "parasiteLab": {
			writeText("You're in the parasite lab. Here, research is conducted into alternate infection vectors, typically animal or insect methods. The place is splattered over the walls and some desks with an off-white goo, a lot stickier and viscous than cum. Worse, some of the research tanks look like they've been smashed from the inside out, and the tanks beneath are leaking fog which covers the floor of the room. Still, nothing attacks, so hopefully you're safe for now.");
			if (checkItem('Red Keycard') != true) {
				writeText("On the table covered in a slimy gunk is a item[access keycard|Red Keycard].");
			}
			else {
				writeText("You already took the keycard, making sure to clean it off thoroughly.");
			}
			if (checkItem('Soldier Recording 2') != true) {
				writeText("Laying on a nearby table is another soldier's helmet, covered in a very thick layer of slime. Though it's a bit sticky, the item[the memory card inside should be intact|Soldier Recording 2]");
			}
			else {
				writeText("Laying on a nearby table is a soldier's helmet caked in slime. The memory card was intact though, so you took it.");
			}
			writeText("There are a pair of still-functional research terminals in the room. A terminal partially covered in the sticky white substance is on a project codenamed '[spider|spiderFile]', and another caked in cum is a log on a project codenamed '[mind worm|wormFile]'. With your high level of security clearance, you could read up on these to get an idea of what might be on the loose.");
			if (data.player.horny == true) {
				writeText("Something about the unnerving environment is making your skin tingle. You know it'd probably be risky, but a dark part of you wants to take a seat and [jerk off|parasiteJerk].");
			}
			writeText("If there's nothing else to do here you should probably leave and go back to the [laboratory hub|labHub].");
			break;
		}
		case "spiderFile": {
			writeSpecial("AUTHENTICATION GRANTED");
			writeBig("spider.jpg");
			writeText("We're on day 43 of our research into the arachnid infection vector project, progress is good in most areas. We've managed to breed out their desire for male and infected subjects, the spiders only prey on entirely biologically female subjects and ignore everyone else. Results are inconclusive on trans women, but looking good. ");
			writeText("One of the subjects attacked an intern, Roxanne, on day 23. Rather than save her we've been letting her serve as a proof of concept for the oviposition methods. They wrapped her up in a cocoon and implanted eggs vigorously and repeatedly. It seems like with only a single human womb they could reproduce quickly enough to fill a town in a week.");
			writeText("Subjects are kept well-cared for and are provided nutrients, the lubrication fluid is extremely addictive and arousing. Roxanne stopped struggling after only a few minutes, and even though she isn't capable of communication anymore it's very clear she does not want to leave.");
			writeText("Luckily the arachnid's natural tendencies not to wander are highly useful. Unless seeking prey they'll stay put, making containment extremely easy. Some security staff members have been lax though, I've submitted a report to let the spiders free to attack a security squad so that they'll be back on guard. ");
			writeText("[CLOSE FILE|parasiteLab]");
			break;
		}
		case "wormFile": {
			writeSpecial("AUTHENTICATION GRANTED");
			writeBig("worm.jpg");
			writeText("Alright, I don't have long. Security will be in soon to transport me, so I need to get this writing done quickly.");
			writeText("These worms are an emergency measure. In less than 1% of the population mental resistance is strong enough for infected to retain their rationality for a time. These worms seek out those resistant infected, inject them with a powerful paralytic, then enter the infected's body through their ears or nose to access the brain.");
			writeText("I understand it sounds gross but it feels so goooooo");
			writeText("I need to focus, losing control already. I'm hosting one now, I can feel it growing acclimated to my brain chemistry. They find the parts of your brain responsible for rational thought and hijack them. If they think you can lead them to other resistant individuals they'll turn you into a sort of sleeper agent to infect others.");
			writeText("But if they think you have nothing to offer they'll just crush those parts of your grey matter, removing what individuality you might have left. After that they start injecting pleasure chemicals directly into your brain. The guards are almost here to escort me to the inoculation chamber so that the worm can be removed before it finds the sections dedicated to rational tho");
			writeText("<i>The log ends here. You probably shouldn't touch the console, as it's coated in an inhuman amount of cum.</i>");
			writeText("[CLOSE FILE|parasiteLab]");
			break;
		}
		case "parasiteJerk": {
			writeEvent('mindWorms');
			break;
		}
		case "innoculationLab": {
			writeText("The sealed vault door to the inoculation lab opens with a loud hiss. The room is cold, nearly freezing. There's a soft buzz from the walls coming from the numerous refrigeration storage devices lining the walls.");
			writeText("The door seals shut once you enter. You can safely use the room as you please, but it's designed to keep you inside until a security team can escort you out. You can't bypass the system, you'll need to completely shut down the system to escape.");
			writeText("that's pretty bad given the extremely well-braced containment cell in the back of the room. It's meant to contain something big, big enough to put every infected on the loose to shame. There's a [research terminal|alphaFile] next to it with an entry about what's inside.");
			writeText("Escape is the most important thing to you right now, even if it means setting a monster loose right on your heels. The infection cure is here, all you need to do is [administer it|infectionCure], disable the security systems, and then escape.");
			break;
		}
		case "alphaFile": {
			writeSpecial("AUTHENTICATION GRANTED");
			writeBig("alpha.jpg");
			writeText("The alpha strain is a uniquely modified version of the base virus with every aspect tuned to be as efficient as possible. It can't be spread from its original infection, when she infects someone they turn into a regular infected shemale. An alpha also maintains a much higher level of intelligence, able to form single word statements.");
			writeText("Immediately after infection the change begins. Unlike traditional strains the growth here is completed over the course of months and is separated into three distinct development phases.");
			writeText("The first of these phases is in cock growth. The alpha strain causes dick growth like the normal virus, only it causes a much greater degree of growth. Pictured above is shortly before her cock reached the fifteen inch mark.");
			writeText("After that the second phase is bodily and muscular growth. At the end of this phase the alpha is nearly seven feet tall and hundreds of pounds of muscle, far from the hundred pound waif she was in the beginning. Her strength is inhuman, she could absolutely break through containment if the sedative flow is broken.");
			writeText("But she's most dangerous after her third phase, where her personality begins to change. The woman we began out experiments on was extremely docile and submissive, but not one ounce of that is still present. We've had seventeen containment breaches thus far because of her. Eye contact is enough to cause complete submissiveness in some cases. Smelling her skin causes a strong desire to lick her sweat, which has a powerful addictive quality. Her sperm cells give off a pheromone so powerful it needs to be treated as dangerous as nuclear material. They have additional body-altering properties as well to ensure women and men will stretch to accommodate her length.");
			writeText("Her dominance may even have a memetic quality. According to surveys of our teams the alpha is present in the masturbation fantasies of nearly every member of staff, even those barely involved with the project.");
			writeText("This project is an extreme danger to everyone in the facility, I recommend we halt the project as soon as possible, and that the alpha be sent to live on a remote island along with everyone on the project team as soon as possible. Luxuries and resources should be provided solely to suit her desires; based on her semen output the rest of us will not be needing food.");
			writeText("[CLOSE FILE|innoculationLab]");
			break;
		}
		case "infectionCure": {
			if (data.player.flags.includes("rockPotion") != true) {
				writeEvent('cure');
				writeText("You don't have long, you've got no chance if whatever's in there catches you. You need to [get out of the lab|hubChase] as soon as possible.");
			}
			else {
				writeEvent('cockRock');
			}
			break;
		}
		case "hubChase": {
			if (data.player.horny == true) {
				writeEvent('failure');
			}
			else {
				writeText("You should be fine, you tell yourself. You've got a big lead and whatever's behind you is held back by strong metal containments.");
				writeText("When you reach the center of the laboratory hub, you hear a large metal wall being thrown aside in the room behind you. You don't have a lot of choices or time to make one.");
				writeText("The [lobby|lobbyChase] is straight ahead.");
				writeText("Otherwise, you could try to hide in the [chemical lab|chemChase], in the [containment lab|containmentChase], or the [parasite lab|parasiteChase].");
				if (data.player.flags.includes("weaponLabDoor") == true) {
					writeText("You remember you left the cell door open in the [weapon lab|weaponChase]. If you hide in there the alpha might be distracted by the chained-up infected woman.");
				}
				writeText("Whatever you decide, you'll need to hurry.");
			}
			break;
		}
		case "lobbyChase": {
			writeText("You make a mad dash into the floor's main lobby. You can hear the alpha behind you.");
			writeText("The [elevator|elevatorEscape] to the surface is within reach. But if it's slow you could be caught before the doors close. Still, it's your only option now.");
			break;
		}
		case "parasiteChase": {
			writeEvent("spider");
			break;
		}
		case "weaponChase": {
			writeEvent("alpha3");
			break;
		}
		case "chemChase": {
			writeEvent("alpha1");
			break;
		}
		case "containmentChase": {
			writeEvent("alpha2");
			break;
		}
		case "elevatorEscape": {
			writeText("Desperately running for a way out you trip over and slam into the elevator doors. You pick yourself up and mash the button as quickly as you can. ");
			writeText("The elevator's power source runs independently of the rest of the facility for exactly this sort of situation. ");
			writeText("The door opens and you rush inside to hit the button for the surface. As the doors close agonizingly slowly, you can see one of the inner laboratory hub doors being thrown off its hinges. ");
			writeText("The elevator grumbles and starts to ascend. ");
			writeSpeech("player", "", "I'm... I'm alive! I did it... ");
			writeText("Tired, naked, and just barely having escaped from an alpha infected, you finally feel yourself starting to relax despite how cold the floor is. ");
			writeText("The forecast predicted a tropical storm would be hitting the state soon, it's probably pouring up on the surface. You'll need to find clothes, shelter, and food as quickly as possible. ");
			writeText("Despite how weary your body is, you stand back up as the elevator comes to a stop. The doors slide open to reveal dozens of armed soldiers and researchers, all wearing the logo of your company. ");
			writeText("One of them is mid-report, talking about how there were 'no survivors, just as simulations predicted', before everyone stops what they're doing to face you with a look of shock. Within moments your hands are up as multiple rifles are pointed right at you. ");
			writeText("You've escaped infection, where will you end up from here? ");
			writeText("...");
			writeText("You've done it! It's unsure what fate will befall this spunky scientist, but she's been saved from sexual assault and infection for now at least. If this was the goal you've been shooting for, then great job!");
			writeText("Check out the 'Author Information' section in the scenario selection menu for a more detailed set of credits, or play the game again to try and get yourself on the business end of the infected.");
			writeTransition("theFacility", "Thanks for playing!");
			break;
		}
		//Spread Island
		case "spreadIsland": {
			document.getElementById('wrapperBG').style.backgroundImage = "url(scripts/gamefiles/locations/spreadIsland.jpg)";
			writeMed("scripts/gamefiles/characters/Spread Island.jpg");
			writeText("Scenario 3 - Spread Island");
			writeText("On a resort island, two newlyweds are enjoying a party-filled honeymoon vacation until all hell breaks loose.");
			writeText("You, a loving husband, must find a way to get yourself and your infected wife to safety.");
			data.player.scenario = "Spread Island";
			updateMenu();
			countScenes();
			writeSpecial("Also, for this campaign you'll need to name your wife:");
			document.getElementById('output').innerHTML += `
				<p class='centeredText'>Your wife's name: <input type="text" id="nameSubmission" value="Kate"></p>
				<p class='choiceText' onclick='renameWife()'>Start Spread Island</p>
			`;
			writeTransition("scenarioSelect", "Go back");
			break;
		}
		case "spreadStart": {
			data.quicksave = null;
			data.player.flags = "";
			data.items = [];
			writeSpeech("DJ", "none", " Alright party people, you ready to turn it up!?");
			writeText("The crowd roars in approval.");
			writeSpeech("DJ", "none", " Then I'll give you all something to sink your teeth into, let's tear this place apart!");
			writeText("The DJ works his magic and a base line vibrates the whole room. You aren't much of a party person these days, but it's nice to get away from daily life and cut loose.");
			writeText("You're mindlessly shuffling along with the repetitive beat, until you feel a familiar weight rest against you. Your wife is back from the bathroom.");
			writeSpeech("wife", "", "I don't feel so good...");
			writeText("...");
			writeSpeech("player", "", "Too much to drink, honey?");
			writeText("The two of you left the party, hopefully an early rest will help her feel better.");
			writeSpeech("wife", "", "I... The bar was offering that free new drink, I wanted to go for some new experiences... That's all I had though...");
			writeText("She nearly falls over, so you pick her up bridal style and carry her to your room.");
			writeSpeech("wife", "", "I feel... Really bloated... Sorry honey, I ruined our last day-");
			writeSpeech("player", "", "None of that, shush. You'll feel better in the morning. You're a lot more important to me that some party, alright?");
			writeText("She gives you a faint smile, before groaning and rubbing her abdomen and drawing a sharp breath. You manage to get the door to your room open and gently lay her down. You leave her over the covers, she's sweating pretty hard. You start slipping off her 'party clothes', they don't look too comfortable.");
			writeSpeech("wife", "", "They s-said it was a test for something new...");
			writeSpeech("player", "", "Just calm down and try to rest. A drink wouldn't do this, maybe you caught a flu.");
			writeSpeech("wife", "", "Squeezing... Tight...");
			writeSpeech("player", "", "Alright, you stay here. I'll go find some help, alright?");
			writeSpeech("wife", "", "No! Please... Please stay, I can't... Gghh...");
			writeText("Now you're starting to get worried. You grab the phone and dial the lobby, and within a single ring it's picked up.");
			writeSpeech("Receptionist", "none", "Merridot lobby, how can I help?");
			writeSpeech("player", "", "It's my wife, I think she's sick. We're in room 106.");
			writeSpeech("Receptionist", "none", "Alright sir, don't panic. We've gotten a few other reports like that so our medic is busy. I'll call in an ambulance for them to take a look-<br>Ma'am? Ma'am I'm sorry, you can't walk around dressed like that, I-");
			writeText("You hear the receptionist drop the phone and the sounds of a scuffle.");
			writeSpeech("Receptionist", "none", "Please, calm- What is- Stop! Stop please! No!!!");
			writeSpeech("player", "", "Hello? What the hell is going on over there? Hello?!");
			writeText("This is going nowhere. You hang up and dial the emergency number, but the line is busy.");
			writeSpeech("player", "", "What the fuck, you can get put on hold? What kind of third world... Honey?");
			writeText("Your wife is writhing on the bed, it looks like she's in pain. She's moaning and shaking from side to side, she looks delirious as she starts slipping her panties down.");
			writeText("Her clit is bright red and swollen, it almost looks like it's growing.");
			writeSpeech("player", "", "Honey, you didn't... <br><i>No, she was only in the bathroom for a few minutes, and an STD wouldn't kick in this fast. <br>And what the hell am I thinking? She wouldn't cheat on me on our honeymoon.</i>");
			writeSpeech("wife", "", "Aaagh! It won't... Hhhg... It won't come out!");
			writeBig("intro-1.gif");
			writeText("Her labia is spread out as she tries to push something, but winks back closed and she groans from heavy exertion. There's a small but visible bulge on her pubic area.");
			writeText("Suddenly there's a thud at the door. Panicked, you rush over and open it, and a young woman stumbles, nearly falls, into your room.");
			writeSpeech("player", "", "Thank god, you must've run all the way here. She's on the bed, quick, help her!");
			writeText("The young woman shambles over to the bed, a strange look in her eyes. As she approaches your writhing wife, you finally notice the bulge in her shorts.");
			writeBig("intro-2.gif");
			writeSpeech("player", "", "What the hell?");
			writeText("She's got a dick, and it looks like she wants to use it. You're [frozen in shock|frozen], trying to find the state of mind to [shove her away|shoveHerAway] from your wife.");
			break;
		}
		case "frozen": {
			writeEvent(scene);
			writeTransition('scenarioSelect', 'GAME OVER', '#FF0000');
			break;
		}
		case "shoveHerAway": {
			writeText("You tackle the woman and push her towards the exit. She's disorientated, but pretty strong. You barely manage to get her out of the door and slam it shut. She bangs on the door a few times but after you hear a man screaming outside the banging stops. She must've found someone else.");
			writeSpeech("player", "", "Alright, we need to get out of here.");
			writeSpeech("wife", "", "Nnnngh!");
			writeText("Your wife lets out a high-pitched scream before a sudden wet *POP* sound fills the air, and her body goes slack.");
			writeText("Where her clit was is now a full-on cock, and a pair of egg sized balls have replaced her cunt. She wraps her hand around her new shaft, eager to finally feel relief.");
			writeBig("shove.gif");
			writeSpeech("wife", "", "Yesss... Cumming...");
			writeSpeech("player", "", "Honey! Honey please, stay with me! We need to get out of here, I won't abandon you! We need to get back home, home to our families!");
			writeSpeech("wife", "", "Home... Where everyone else is... Everyone else...");
			writeText("She's in some kind of delusion as her cock jerks. Someone on the mainland must be able to help, some doctor, some surgery, anything. But first you need to get out of here.");
			writeSpeech("player", "", "Keep the door closed, don't open it for anyone but me, alright? I'll find some help, a wheelchair if I have to, and we're making it out of here alive. You still with me?");
			writeSpeech("wife", "", "Mhmm...");
			writeText("She weakly mumbles out a response before closing her eyes. She needs rest, at least she isn't going psycho rapist like that woman you kicked out.");
			writeText("For now you need to find a safe way out of the resort so that you and your wife can get off the island, and quickly. From the sounds of things, this isn't a localized incident. People all over the resort are turning. [You need to get moving, quickly|hotelRoom].");
			break;
		}
		case "hotelRoom": {
			writeHTML(`
				t You're in room 201 of the Sanctuary Hotel. The structure of the room means there aren't any windows.
				t !flag hotelGassed; Your wife, wifeF, is on the bed getting some rest. Once you've found a safe way out you should wake her and get moving, there's no telling how long until another infected arrives.
				t !flag hotelGassed; ?item Strange Gas; You have the container of strange gas with you. It might be some kind of antidote, but it isn't likely. Still, looking at the flaccid dick between your wife's legs, you're desperate for any kind of hope. Will you [expose her to the strange gas|hotelWifePleasureGas]?
				t !flag hotelGassed; ?item Bedding Rope; You found a safe way out by [climbing down from the window of room 204|hotelEscape]
				t !flag hotelGassed; It seems like the woman from earlier was distracted, it should be safe to [head out into the hallway|hotelHallway].
				
				t ?flag hotelGassed; Your wife, wifeF is by your side. She's wearing a gasmask like you, but these seem to be drawing air from small canisters instead of filtering it. Meaning you don't have very long before the masks are useless.
				t ?flag hotelGassed; A thick pink fog has already begun filling the room. You should [get out of here while you can|hotelHallway].
			`);
			break;
		}
		case "hotelWifePleasureGas": {
			writeEvent(scene);
			break;
		}
		case "hotelHallway": {
			writeHTML(`
				t You're in the second floor hallway of the Sanctuary Hotel. Something's going on with the hotel's systems, every door's electronic lock is registered as 'unlocked'. Almost like someone <i>wants</i> the rooms to be accessible.
				t !flag hotelGassed; [Room 201|hotelRoom] is where you and your wife are staying. With the doors malfunctioning it's not safe to stick around. Once you've found a way out you can safely go back and get her.
				t !flag hotelGassed; [Room 202|hotel202] is across the hallway, you can hear someone sobbing through the door.
				t !flag hotelGassed; [Room 203|hotel203] is a little down the hall, and is unlocked.
				t !flag hotelGassed; [Room 204|hotel204] a little down and across the hall, the door is wide open and nobody's inside.
				t !flag hotelGassed; Down the stairs is the main lobby, maybe the only way out. You could [grab your wife and head downstairs|hotelMainExit] as a last resort, but you can hear the sounds of chaos down there.
				
				t ?flag hotelGassed; Room 201 is behind you, but you don't have time to mess around. You'll need to get out of the hotel while you can still breath.
				t ?flag hotelGassed; Room 202 is across the hallway, you can hear orgasmic moans through the door.
				t ?flag hotelGassed; Room 203 is a little down the hall, and pink gas is billowing out the open door.
				t ?flag hotelGassed; [Room 204|hotel204] a little down and across the hall, the door is wide open and nobody's inside.
				t ?flag hotelGassed; Down the stairs is the main lobby, maybe the only way out. With your wife outside you could [head downstairs|hotelMainExit]. The gas is pretty heavy, so you can actually see it rolling down the steps. The sounds of chaos sound a lot more sensual than they did before.
			`);
			break;
		}
		case "hotelMainExit": {
			if (checkFlag("hotelGassed") != true) {
				writeEvent(scene);
			}
			else {
				writeHTML(`
					t It's a spectacle down here, everyone but you and wifeF is a writhing mess on the floor. Some are clearly already infected and are having their minds blitzed by the gas, but some...
					im hotelGassed.gif
					t One woman is clearly being changed by the gas itself. Her askew swimsuit clearly not built for the meat between her legs. The gas must be causing this, did you do the right thing?
					t wifeF keeps hesitating as you try to pull her through the lobby, her eyes lingering on each freshly birthed cock, on every woman in her situation now content to splurt hands-free from the drugged fog.
					t All you can hear at this point are screams and moans, with the occasional deep sucking breath from someone desperate for more mixed in, but eventually you push through the lobby's main door to freedom.
					t Yet all you see before you is more hell. The quaint little vacation village is being torn apart by survivors desperate to escape and women chasing them down. You feel a small prick on your neck, you hear wifeF scream through her mask, and [you feel the world turn sideways|compoundAwaken].
				`);
			}
			break;
		}
		case "hotel202": {
			writeHTML(`
				t You're in room 202 of the Sanctuary Hotel, or at least you're looking inside.
				t The room is trashed, with no windows or working lamps to illuminate the room. In the shadows you can see someone, it almost looks like her hair is growing. The noises she's making are having some kind of effect on your head, like you've got a headache and her voice is clearing it.
				t [She seems so sad, you can comfort her with your big strong arms|hotelFreshSiren].
				t [Or you could resist and close the door|hotelHallway].
			`);
			break;
		}
		case "hotelFreshSiren": {
			writeEvent(scene);
			break;
		}
		case "hotel203": {
			writeHTML(`
				t You're in room 203 of the Sanctuary Hotel. The room is in pristine condition.
				t Atop the bed is a strange suitcase. It's open, with a pair of canisters of some strange pink gas inside. One of the canisters has a hose attached, and is connected to an air vent. You could probably [break the hose|hotelPleasureGas] if you really tried.
				t !item Strange Gas; The other canister isn't in use. It's definitely got something to do with what's going on, maybe you should item[take the unused canister|Strange Gas]?
				t ?item Strange Gas; You already took the other canister.
				t !item Gas Masks; In a second container underneath the bed is item[a pair of gas masks|Gas Masks]. There's a spot for a third mask but it's already been taken. 
				t ?item Gas Masks; You already took the gas masks. They draw air from small canisters, and don't filter the air, so you're on a time limit once you start using them.
				t !item Bedding; Since the room is untouched, the item[bedding and sheets|Bedding] are still here. They might be useful.
				t ?item Bedding; The beds are bare aside from the suitcase, as you took the bedding and sheets.
				t [Head out into the hallway|hotelHallway].
			`);
			break;
		}
		case "hotelPleasureGas": {
			if (checkItem("Gas Masks") != true) {
				writeEvent(scene);
			}
			else {
				writeHTML(`
					t You slip on the gasmask and tug at the hose attached to the wall. It's got a pretty strong bond but if you just... Pull... Harder...! 
					t There's a sound of rubber breaking before the *KSSSSSSH* of gas as you break the seal. A thick gout of pink gas is spraying from the hose.
					t It's spreading quickly so you run back to your room, wasting no time at all as you slip the second mask onto your dazed wife. 
					t The gas has already started to flow into the room, you should [get moving|hotelRoom].
				`);
				addFlag("hotelGassed");
			}
			break;
		}
		case "hotel204": {
			writeHTML(`
				t You're in room 204 of the Sanctuary Hotel. It's been picked clean. Food from the fridge, towels, even the sheets from the bed have been taken.
				t !item Wire Cutters; Even the TV has been taken, there's a pair of item[wire cutters|Wire Cutters] small enough to hide in the waistband of your clothes in here. Aside from that everything in here is just gone. Maybe this place was robbed before the outbreak?
				t ?item Wire Cutters; You already took the pair of wire cutters. Everything in here has been picked clean. Maybe this place was robbed before the outbreak?
				t !item Onahole; Everything is gone... Except a fleshlight. There's an onahole here, still in the box inside one of the drawers. This probably wasn't a priority to be taken. If you really feel like you'll need it, you could item[take it with you|Onahole].
				t ?item Onahole; The drawers are empty.
				t !flag hotelGassed; !item Bedding; There's a window here, but it's a full story drop to a paved walkway below. 
				t !flag hotelGassed; ?item Bedding; !item Bedding Rope; There's a window here, but it's a full story drop to a paved walkway below. With the bedding you took from the other room, you could item[make a rope to climb down|Bedding Rope].
				t !flag hotelGassed; ?item Bedding; ?item Bedding Rope; There's a window here, but it's a full story drop to a paved walkway below. You've created a rope to [climb down and escape|hotelAbandon], but what about wifeF? You'll need to go back for her if you want to escape together.
				t ?flag hotelGassed; ?item Bedding; ?item Bedding Rope; There's a window here, but it's a full story drop to a paved walkway below. You've created a rope to [climb down and escape|hotelEscape], and there's no time to lose.
				t [Head out into the hallway|hotelHallway].
			`);
			//
			break;
		}
		case "hotelAbandon": {
			writeEvent(scene);
			break;
		}
		case "hotelEscape": {
			writeHTML(`
				t !flag hotelGassed; You gently nudge wifeF awake. She's exhausted from the toll the infection is taking on her body, but she's human. That's all that matters. 
				sp player; !flag hotelGassed; Come on honey, it isn't safe here. I've got a safe way out, let's go.
				t !flag hotelGassed; She nods and slowly climbs out of bed, needing to lean on you as you make your way to room 204.
				t You set up the rope, throw down a pile of pillows, and make sure it's secure before you climb down. She's behind you, although she nearly falls and you need to help her.
				sp wife; Honey... I can't focus...
				sp player; It's fine, just stay in control.
				t Looking around you can see a way out of the hotel towards the less populated expensive villa area. If anyone is getting help right now it'd be the richest folk. 
				...
				t You arrive in the villa area, but there's nobody to be seen. Maybe they've already been evacuated?
				t Suddenly, wifeF can no longer support herself and nearly falls.
				sp player; wifeF? What's wrong?!
				t Her breathing is ragged, so you take her into a thankfully unoccupied building and set her upon the large bed. Almost immediately she begins stroking the very erect member between her legs.
				sp wife; I... I can't!
				t Her balls look full to burst, and she's jacking herself off as furiously as she can. Yet...
				t She can't cum. At least not with just her hand.
				t You gulp dryly as you come to a stunning conclusion. If you and her are going to get off this island, you'll need to [find some way for her to get off|villaKing]
			`);
			break;
		}
		case "villaKing": {
			writeHTML(`
				t You're in the king villa, a large resort villa for the ultra wealthy. It must have been evacuated already, because the place has clearly been looted. The fridge, hanging open, has nothing but partially melted ice inside.
				t There's nothing of value left besides wifeF on the bed, fruitlessly jerking herself off. Tears are forming in her eyes as she arches her back, it wasn't exhaustion slowing you two down before, but mind-consuming lust. It's clear she isn't able to get off herself, despite how frantically she jerks herself.
				t If you can't find anything to help her with... You may need to [help her yourself|wifeHelp].
				t ?item Onahole; [You could give her the onahole you found|wifeOnahole].
				t ?item Dildo; [You could give her the dildo you found|wifeDildo].
				t ?item Chastity Cage; [You could use the ice from the nearby fridge combined with the chastity cage to keep her dick in check|wifeCage].
				t [You can head outside, into the affluent villa district|villaOutside].
			`);
			break;
		}
		case "wifeHelp": {
			writeEvent(scene);
			break;
		}
		case "wifeDildo": {
			writeEvent(scene);
			writeHTML(`
				...
				t The two of you move on, leaving the affluent district behind. There's no helicopters, no tanks, no police of any kind. The island must be devolving into chaos, but there's no trace of government response.
				t As you're wandering around a hiking path looking for any kind of help, you feel a small bite on your neck.
				t You hear wifeF freaking out for some reason as you lift your hand to scratch the bite, only for your hand to brush against something... Metal.
				t The earth goes wobbly, and [sleep suddenly seems like a great idea right now|compoundAwaken].
			`);
			//writeText(".");
			break;
		}
		case "wifeOnahole": {
			writeEvent(scene);
			writeHTML(`
				...
				t The two of you move on, leaving the affluent district behind. There's no helicopters, no tanks, no police of any kind. The island must be devolving into chaos, but there's no trace of government response.
				t As you're wandering around a hiking path looking for any kind of help, you feel a small bite on your neck.
				t You hear wifeF freaking out for some reason as you lift your hand to scratch the bite, only for your hand to brush against something... Metal.
				t The earth goes wobbly, and [sleep suddenly seems like a great idea right now|compoundAwaken].
			`);
			break;
		}
		case "wifeCage": {
			writeEvent(scene);
			addFlag('wifeCage');
			writeHTML(`
				...
				t The two of you move on, leaving the affluent district behind. There's no helicopters, no tanks, no police of any kind. The island must be devolving into chaos, but there's no trace of government response.
				t As you're wandering around a hiking path looking for any kind of help, you feel a small bite on your neck.
				t You hear wifeF freaking out for some reason as you lift your hand to scratch the bite, only for your hand to brush against something... Metal.
				t The earth goes wobbly, and [sleep suddenly seems like a great idea right now|compoundAwaken].
			`);
			break;
		}
		case "villaQueen": {
			writeHTML(`
				t You're in the queen villa, a medium resort villa for the wealthy.
				t And the perverted, apparently. The walls are lined with bondage gear and sex toys of varying shapes and sizes. From ball-gags to zipper outfits...
				t !item Dildo; There's a collection of dildos on the wall, you could item[grab one and take it back to your wife|Dildo].
				t ?item Dildo; There's a collection of dildos on the wall, you don't need more than one. You hope.
				t !item Chastity Cage; There's also a collection of chastity cages in varying sizes. The largest one should fit wifeF. She might not like being caged up, but you might need to item[take one with you|Chastity Cage].
				t ?item Chastity Cage; You already took a chastity cage, there's no reason to take another. It's not like any of the other infected will let you put one on them.
				t [You can head outside, into the rich district|villaOutside].
			`);
			break;
		}
		case "villaSoldier": {
			writeHTML(`
				t You're in the soldier villa, a small resort villa for the wealthy. Despite it's name it seems to be made for high class vacationers just like the other two.
				t Peeking inside, you can see someone's already here. Someone is rummaging through the fridge with a pile of foodstuffs besides her. It's a looter.
				t Aside from her, there doesn't seem to be anything worth taking in here. Nothing that will help wifeF anyways. Unless...
				t You could grab something blunt, drag her across the way, throw her in through the door...
				t You shake your head. You can't be considering [sacrificing her to get your wife off|wifeSacrifice], can you?
				t [You can head outside, into the affluent villa district|villaOutside].
			`);
			break;
		}
		case "wifeSacrifice": {
			writeEvent(scene);
			break;
		}
		case "villaOutside": {
			writeHTML(`
				t You're in the wealthier area of the resort. This place was already advertised as paradise, so this must've been sold as a 'premium paradise'. No hotel rooms here, instead each guest is given an entire villa to relax and enjoy their vacation in.
				t First is the [king villa|villaKing] where wifeF is. You can just barely hear her frantically moaning inside, unable to find relief. You'll need to be quick, otherwise you risk someone or something hearing her.
				t Next is the [queen villa|villaQueen], a little ways off.
				t Finally is the [soldier villa|villaSoldier], slightly smaller than the other two.
			`);
			break;
		}
		case "compoundAwaken": {
			writeHTML(`
				t Reality itself seems... Fuzzy. Visions keep bouncing in and out of your head.
				t You try to hold any of them in your mind but all that results is a lance of pain as the image fades away.
				t Yet one seems to stick in your mind.
				im hotelDream.gif; ?flag hotelGassed;
				t ?flag hotelGassed; The women in the hotel, their bodies and minds ravaged by the gas. If one of them had been someone's wife... If one had been wifeF... Surely they would have been infected anyways... Right?
				im villaDream.gif; !flag hotelGassed;
				t !flag hotelGassed; The image of wifeF on the bed, so desperate for release she would do anthing to get off... Anything... <i>Anyone</i>...
				...
				t You awaken to a pounding headache, and the sound of mysterious voices garbled by what sound like gasmasks.
				sp ???; im none; !flag wifeCage; His wife has turned, but she's clearly a resistant. With so many the second formula must be defective, right? Any sign of infection on him?
				sp ???; im none; !flag wifeCage; No, not yet anyways. It's a shame, watching the guy get turned by his wife would've been pretty hot.
				sp ???; im none; !flag wifeCage; We get a bonus if we turn anyone with an eel. God, what were they thinking when they created a breed that needs hours to prepare?<br>And seriously Tasha, get your head out of the shitpipe. We're here on business.
				sp ???; im none; !flag wifeCage; Aww come on, you're still on that whole global destabilization thing? Why can't you be honest and admit you've got a massive fetish for...
				
				sp ???; im none; ?flag wifeCage; His wife has turned, but she's clearly a resistant. With so many the second formula must be defective, right? Any sign of infection on him?
				sp ???; im none; ?flag wifeCage; No, not yet anyways. Didn't he put his wife in a chastity cage? This is the strain that takes you out of commission when you don't get off enough, right?
				sp ???; im none; ?flag wifeCage; No, that was the last one back in Georgia. This one just causes the need to get off to keep building up. That's why we're keeping it on, no data yet on what'll happen.
				sp ???; im none; ?flag wifeCage; Heh. Need to Breed 2; Too Pent Up, Too Furious.
				sp ???; im none; ?flag wifeCage; Tasha, seriously. We're professionals, not...
				
				t The conversation fades as the two of them walk away. Peering through a small hole into the hallway, you can see more people patrolling around. They're wearing some kind of urban camouflague, and their faces are obscured by thick masks. All of the men are wearing headphones as well.
				t This is a dangerous situation, these people aren't working for any government you know. With wifeF somewhere else and you locked in here, there's no telling how you'll make it out of this alive.
				t Still, you should [plan your escape|compoundRoom].
			`);
			data.player.guardMovement = 0;
			break;
		}
		case "compoundRoom": {
			writeHTML(`
				t You're in some kind of cell, or at least the best one that could be made out of some kind of closet.
				t !flag playerDoorUnlocked; The door is locked, and won't budge. It's some kind of electronic lock that's been fitted onto the door, they must still have power over here. There's no keyhole, but there is a small hole you can peer through.
				t ?flag playerDoorUnlocked; You unlocked the door with the compound's command console, so you can freely [head out into the hallway|compoundHall], so long as you're careful not to be caught.
				t Searching around, you can see an air vent that probably connects to a series of ventilation shafts. Why some prying the loose bolts break off and you can [crawl inside|compoundVent1].
			`);
			calculateMovement();
			break;
		}
		case "compoundVent0": {
			writeHTML(`
				t Through the grate you can see a small storage closet. You can [climb out|compoundCloset] if you'd like.
				t You can head [forwards|compoundVent1] or [wait|compoundVent0].
			`);
			break;
		}
		case "compoundCloset": {
			writeHTML(`
				t You're in a small storage closet. Various cleaning supplies and other goods tell you this building probably wasn't built to be a military stronghold, whoever's running the place must be using this as a temporary base of opperations.
				t !flag closetDoor; The door out is locked tight, and breaking it open would make a ton of noise. Better play it safe.
				t ?flag closetDoor; You unlocked the door from the other side, so you can [safely leave the closet|compoundMain].
				t Aside from that there isn't anything too useful in this place, using a small box you can [climb back into the vent|compoundVent0].
			`);
			break;
		}
		case "compoundVent1": {
			writeHTML(`
				t Through the grate you can see the room you were trapped in. You can [climb out|compoundRoom] if you'd like.
				t You can head [forwards|compoundVent2], [backwards|compoundVent0], or [wait|compoundVent1].
			`);
			calculateMovement();
			break;
		}
		case "compoundVent2": {
			writeHTML(`
				t Through the grate you can see a room filled with transparent containers of pink gas.
				t !flag gasDisturbed; The grate here is loose, you can [climb out into the gas room|compoundGasRoom]. However it's a solid drop, so you won't be able to climb back in.
				t ?flag gasDisturbed; A soldier with a mask is trying to patch up the hose you broke. It's not safe to head in. You thank your lucky stars that the gas seems to be dense enough it can't reach up here.
				t You can head [forwards|compoundVent3], [backwards|compoundVent1], or [wait|compoundVent2].
			`);
			calculateMovement();
			break;
		}
		case "compoundVent3": {
			writeHTML(`
				t Through the grate you can see a large kennel, each cage containing a sleeping dog.
				t !flag houndsDisturbed; The grate here is loose too, you can [sneak in|compoundKennels] if you're careful. However it's a solid drop, so you won't be able to climb back in.
				t ?flag houndsDisturbed; Several soldiers are trying to calm the dogs down, it's not safe to go in. You're certain one is looking at you, if they set those dogs loose to hunt you down...
				t You'd rather not think about it.
				t You can head [forwards|compoundVent4], [backwards|compoundVent2], or [wait|compoundVent3].
			`);
			calculateMovement();
			break;
		}
		case "compoundVent4": {
			writeHTML(`
				t Through the grate you can see wifeF, resting on a bed. She's asleep, and the bolts on this vent have been properly maintained. No breaking in from here.
				t You've crawled in a sort of circle, so her room must be directly next to yours. If you could unlock the doors in this place you'd have no trouble getting to her.
				t You can head [backwards|compoundVent3], or [wait|compoundVent4].
			`);
			calculateMovement();
			break;
		}
		case "compoundGasRoom": {
			writeHTML(`
				t You're in a room filled with transparent canisters filled with a strange pink gas. They're probably the same as the ones you saw in the suitcase back at your hotel.
				t From here you can't actually climb back up to the vents, you're in serious danger if you're caught.
				t !item Wire Cutters; A large machine seems to be pumping the strange pink gas. The hoses look a lot stronger than the one you saw before.
				t ?item Wire Cutters; A large machine seems to be pumping the strange pink gas. The hoses look a lot stronger than the one you saw before. Hidden on your person you could use your [wire cutters|gasDisturbed] to cause some serious mayhem with just a small cut, then sneak out.
				t You're hiding behind the doorway as a patrol goes by, you should be able to [sneak out of the room|compoundMain] if you're careful.
			`);
			calculateMovement();
			break;
		}
		case "gasDisturbed": {
			writeHTML(`
				t You pull out the wire cutters and get to work creating a gas leak.
				t Only a very small cut will do, you can't risk exposing yourself to the stuff. Good thing too, because this is some seriously thick rubber tubing. It's under a lot of pressure, so once you break through you can hear a *HSSSSS* as pink smoke is released. You hold your breath and hide in a corner until a group of people runs in.
				t Now, while the soldiers are focused on the gas leak, you need to [sneak out of the room|compoundMain]!
			`);
			addFlag("gasDisturbed");
			break;
		}
		case "compoundKennels": {
			writeHTML(`
				t You're in some kind of kennel, but the animals in here are anything but normal. Large, tough-looking hounds that could tear a man apart at a moment's notice. Good thing they're asleep.
				t From here you can't actually climb back up to the vents, you're in serious danger if you're caught.
				t There's an exit to some kind of main lobby, this whole building isn't too secure against people who still have functioning brains. Still, there are still patrols going on, you can't get out without causing a distraction. Speaking of a distraction... If you [rattled these cages a little|houndsDisturbed] these boys would probably make enough noise for the whole facility to hear, allowing you to sneak around the compound.
			`);
			//t There's not a lot of room here, if you need to hide the only spot is [right behind one of the cages|compoundHounds], and squeezing behind there would absolutely wake the beast up. 
			calculateMovement();
			break;
		}
		case "compoundHounds": {
			writeEvent(scene);
			break;
		}
		case "houndsDisturbed": {
			writeHTML(`
				t The hounds are asleep, completely out of it at the moment. You silently tiptoe up to the cage...
				t And smack the fuck out of it.
				t The dog inside goes absolutely insane, thrashing about in surprise and anger, before it turns into just anger. He barks and snarls, and soon every dog in the room has joined him. You rush over to the side of the room until a few soldiers rush in to check on the hounds.
				t Now, while the soldiers are focused on the dogs, you need to [sneak out of the room|compoundMain]!
			`);
			addFlag("houndsDisturbed");
			break;
		}
		case "compoundWifeRoom": {
			writeHTML(`
				t You're in your wife's cell. She's currently asleep, although she's panting and rubbing her erect shaft through her clothes. You don't know how much longer she can take being cooped up in here. If you think the way out is clear, you can [wake her up and make a break for it|compoundEscape].
				t You've unlocked the door with the main console, the way into the [main hallway|compoundHall] is clear.
			`);
			calculateMovement();
			if (checkFlag('wifeCage') == true) {
				writeEvent("compoundCage");
			}
			break;
		}
		case "compoundHall": {
			writeHTML(`
				t You're in a small, nondescript hallway. From here the escape route is open and in the chaos you should be okay.
				t The door to your cell is here. You can [head inside|compoundRoom] if you have unfinished business.
				t The door to your wife's cell is here as well. You can [head inside to rescue her|compoundWifeRoom].
			`);
			break;
		}
		case "compoundMain": {
			writeHTML(`
				t You're in some kind of main lobby, various decorations welcoming you to 'the best vacation island on the ocean', multiple fliers advertising Sanctuary Hotel.
				t !flag gasDisturbed; There are still a few soldiers around, but you should be able to sneak into a [room full of gas canisters|compoundGasRoom]. However, there might be someone inside, so it's risky to enter from here.
				t ?flag gasDisturbed; There's a very mild pink fog seeping from the gas room, causing a distraction as the soldiers rush to repair the line.
				t !flag houndsDisturbed; There are still a few soldiers around, but you should be able to sneak into what appears to be a [kennel room|compoundKennels]. However, there might be someone inside, so it's risky to enter from here.
				t ?flag houndsDisturbed; The hounds are barking, attracting attention to the kennels. You should be safe to roam around the compound.
				t You can sneak over to a [small door, probably some kind of closet|compoundCloset].
				t Finally, there's a larger door, cracked halfway open. Inside you can see some monitors, it must be a sort of [control room|compoundControlRoom].
			`);
			addFlag("closetDoor");
			break;
		}
		case "compoundControlRoom": {
			writeHTML(`
				t You're in some kind of cobbled together control room, varying monitors showing what appear to be feeds all across the island. Several monitors are just command consoles, displaying stats measuring how many people have been infected, and what vector transformed them.
				t Typing 'help' into the main console lists a variety of options, only a few of them are particularly useful though. You enter in the unlockDoor(*) command immediately, and a little feedback message shows that <b>all locked doors inside the building are now unlocked</b>.
				t !item Kennels Opened; item[Set the kennels to open up on a timer|Kennels Opened].
				t ?item Kennels Opened; You've set the kennels to open on a short timer, that should stop anyone from following you.
				t !item Gas Opened; item[Start overloading the gas room, so that it explodes after a short buildup|Gas Opened].
				t ?item Gas Opened; You've set the gas to build up, ignoring the warnings that critical mass will cause an explosion of infectious material soon. That'll serve them right for infecting wifeF.
				t In the chaos you should be able to [sneak back out of the control room, into the lobby|compoundMain].
			`);
			addFlag("playerDoorUnlocked");
			break;
		}
		case "compoundEscape": {
			writeHTML(`
				t You shake wifeF awake, and her eyes shoot open. For a second you worry she's completely turned, until...
				sp wife; Honey? What's-
				sp player; No time, we need to get out of here. Come on!
				t You take her by the hand and the two of you sprint off. This place is about to go to hell, so not much point in being sneaky anymore. In the chaos the two of you manage to get out of the facility, and it seems like the road is mostly clear of infected.
				t You cast one last look at the building behind you.
				...
			`);
			if (checkItem("Kennels Opened") == true) {
				if (data.player.beastDisabled != true) {
				writeHTML(`
					t The place has gone to hell indeed. Instilling loyalty in the hounds should probably have been a priority. That isn't a mistake the hounds will make though.
					t Teeth tear away at body armor, jaws crunch masks into pieces, and most soldiers go down before they can even fire a shot.
					im endingHounds.gif
					t The women are broken to serve the hounds, being pounded and knotted until their new masters cocks pop out, and they're turned into infected bitches soon after.
					t The "men" are turned more slowly, forced to serve the hounds with their mouths and tongues until they're addicted to the taste of virile beast, but in the end every one of them is turned as well. Their new breasts make service easier.
					...
				`);
				}
				else {
					writeText("This ending blurb has been disabled by your content preferences!");
				}
			}
			if (checkItem("Gas Opened") == true) {
				writeHTML(`
					t The building is rocked by a sudden explosion, before a thick gout of gas begins to seep into a fortified safe room with a pair of soldiers in it.
					t One of them simply sighs, and starts to undo the clasps on her mask.
					sp ???; im none; Tasha! What the fuck are you thinking?!
					sp ???; im none; It's over, the mission's a failure. The higher-ups are gonna write this whole thing off, why the hell would they stick their necks out for us?<br>... You've seen the faces of the people who inhale this stuff, right? I haven't been satisfied with one-night flings since the first time I used this stuff on somebody.
					t Tasha takes a deep breath of the pink gas and her eyes cross before they roll back in their sockets. The smoke works fast. It feels like lightning is arcing through her body, and she barely notices that her companion is undoing her belt.
					sp ???; im none; Fuck's sake, at least get undressed first. I'm on top, alright? Honestly, I have always kinda wondered what dicking somebody felt like...<br>Hey, you fried already? Jesus this stuff pops your brain fast.
					t It pops something else quickly too. By the time the soldier has her gasmask off Tasha is already able to start showing off her brand new cock.
					im endingGas.gif
					sp ???; im none; I'd better get one as big as that...<br>I guess something cute would be pretty hot too.
					t The soldier takes a deep breath, and within seconds can feel her gray matter turning to mush.
					...
				`);
			}
			writeHTML(`
				t Exhausted, you nearly collapse on the ground. You must be nearly half a mile away from the compound by now, yet you can still hear the sounds of chaos coming from the base.
				t You lean against a large rock for support, and wifeF leans against you nuzzling you.
				sp wife; We're... We're not making it off the island, are we? 
				t Her voice isn't nearly as ragged as yours. Maybe the infection has it's perks after all. Yet there's an airyness to her breath, like she's barely holding herself back.
				sp player; I don't think so.<br>... Make it quick, alright?
				sp wife; What?
				sp player; Living like this... I don't know how long I'll manage, but I'd rather go on my own terms, with you. I'm ready.
				t You close your eyes, waiting to hear the sounds of wifeF shedding her clothes, or maybe the sensation of her tackling you, yet all you feel is a swift slap to the face.
				sp wife; After all that, you're giving up?! Fuck that! If I've managed to hold back this long...<br>It wasn't for nothing. We're making it off this shithole, and I'm leaving a one-star review.
				sp player; But...
				sp wife; But nothing! I may... May not be able to hold myself back forever, but you and I are figuring out what made me like this. I'm not spending a single hour on this godforsaken island unless I at least have some company who can smash two brain cells together!
				t You sigh, happy that wifeF seems to be keeping a hold on her ego at least. You aren't sure how she's managed to resist turning into a zombie, but she has a point. Wandering the island as a pair of horny dickgirls sounds a lot better than being hunted, or turning mindless yourself.
				t The two of you have another adventure ahead of you, but for now, this is...
				t <b>THE END</b>
			`);
			writeText("...");
			writeText("You've done it! It's unsure what fate will befall this pair of lovebirds, but at least this honeymoon will be a lot more passionate than it would be otherwise. If this was the goal you've been shooting for, then great job!");
			writeText("Check out the 'Author Information' section in the scenario selection menu for a more detailed set of credits, or play the game again to try and get some of those other scenes.");
			writeTransition("spreadIsland", "Thanks for playing!");
			if (checkFlag("wifeCage") == true) {
				writeEvent("wifeCage");
			}
			else {
				if (checkItem("Opened") == false) {
					writeEvent("compoundFailure");
				}
			}
			break;
		}
		//Scarlet Mansion
		case "mansionIntro": {
			document.getElementById('wrapperBG').style.backgroundImage = "url(scripts/gamefiles/locations/scarletMansion.jpg)";
			writeMed("scripts/gamefiles/characters/Scarlet Mansion.jpg");
			writeText("Scenario 4 - Scarlet Mansion");
			writeText("In a forest bordering a quarantined city there's a scarlet mansion not listed on any map.");
			writeText("You, searching for your missing sister, must explore the mansion and live to find out what bizarre experiments are underway. You may play a male or female character for this scenario.");
			data.player.scenario = "Scarlet Mansion";
			updateMenu();
			countScenes();
			writeTransition("mansionStart", "Play as a man");
			writeTransition("mansionGirlStart", "Play as a woman");
			writeTransition("scenarioSelect", "Go back");
			break;
		}
		case "mansionStart": {
			data.items = [];
			data.quicksave = null;
			data.player.flags = "";
			sceneTransition('scarletIntro');
			break;
		}
		case "mansionGirlStart": {
			data.items = [];
			data.quicksave = null;
			data.player.flags = "female";
			sceneTransition('scarletIntro');
			break;
		}
		case "scarletIntro": {
			writeSpeech("Lansing", "images/Scarlet Mansion/lansing.jpg", "This is Sergeant Lansing of Charlie Team, two days into the search and no sign of the missing civilians. We had an ATV searching the forest near the border of Mongoose City, but have since lost contact with them.");
			writeText("We found signs of Franklin, member of Bravo Team, in a barricaded section of the forest three hours ago. Scraps of cloth, ID tags, and several samples of an unknown white substance with narcotic effects. We hope to see the results soon, but communication with the police station we sent the samples to has suddenly stopped.");
			writeText("The team and I have stopped at a mansion in the forest, not present on any maps. Local wildlife is highly agitated, so we'll be taking shelter here until backup arrives.");
			if (checkFlag("female") == true) {
				writeText("That was the last anyone heard of Charlie Team, it was more than a week ago. Backup never came, orders were sent to all local divisions to abandon the city and set up security checkpoints as roads leading in. Defying their blockade, you set out through the barricade lines with an unregistered vehicle. Your sister, Kelsey Lansing would hate to hear you've come to rescue her, but what kind of big sister would you be if you just left her out here?");
			}
			else {
				writeText("That was the last anyone heard of Charlie Team, it was more than a week ago. Backup never came, orders were sent to all local divisions to abandon the city and set up security checkpoints as roads leading in. Defying their blockade, you set out through the barricade lines with an unregistered vehicle. Your sister, Kelsey Lansing would hate to hear you've come to rescue her, but what kind of big brother would you be if you just left her out here?");
			}
			writeText("You come to a stop just off the road. According to the mission logs, the mansion should be close. All you need to do now is [make your way through the forest|scarletForest].");
			break;
		}
		case "scarletForest": {
			writeText("The air is cold and dry, it's late autumn so the forest's floor is covered in leaves. The trees are too dense for your car, so you decide to hoof it. Unable to sneak in any powerful weaponry, all you've got is a stun-gun, your wilderness exploration gear, and a radio.");
			writeText("Along the path is a [broken-down shed|scarletShed]. It looks like at one point it was heavily barricaded, meant to keep something out.");
			writeText("In the distance, you can make out the sounds of dogs howling and barking. It doesn't seem to be getting louder, so they aren't getting any closer. Aside from that, the forest is pretty calm. You should be fine to [continue on|scarletMansionEntrance].");
			break;
		}
		case "scarletShed": {
			writeText("You decide to check out the shed. The door and windows are boarded up, but the west-facing wall has been smashed through.");
			writeText("You can see scraps of clothes and some kind of fluid stains the walls and floor. You stick your head in through the broken wall for a better look.");
			writeText("A smell hits you right away, kind of like fish and sweat. It's pretty awful, something about it is making you feel woozy. It'd be best to [keep moving|scarletForest].");
			break;
		}
		case "scarletMansionEntrance": {
			writeText("Not having anything in particular to go on, you follow the sound of the dogs. Soon enough the trees give way to a massive clearing with a looming mansion in the center.");
			writeText("It's got red walls and all of the windows are covered by curtains. One of the lights is on, but the window is blocked off by iron bars.");
			writeText("A pair of [sturdy wooden doors|scarletEntry] on the front, the mansion is pretty intimidating. This is something straight out a horror flick. Still, if Kelsey is alive, she's probably inside. You should try the door.");
			writeText("Near the entrance is a [massive kennel|scarletKennel], several dogs wildly barking and throwing themselves against the cage walls to get at you.");
			break;
		}
		case "scarletKennel": {
			if (data.player.beastDisabled == false) {
				writeText("The walls are sturdy enough, so you should be fine to check it out. You get closer to the cage.");
				writeText("The dogs are absolutely rabid. They're all Dobermans, but something is off about them. They gnaw and claw at their containment, probably eager to tear you apart. There's some fluid splattered on the inside of the cage. They're all wearing studded leather collars with big... Batteries? They're some kind of shock collars probably, way stronger than anything you could get over the counter.");
				writeText("After a minute, it hits you as to what's different about them. They're bigger than their breed should be, more muscular, and they're all erect. They've each got a second collar on, fastened around their dicks each also with a smaller battery on them. They don't seem happy about it, you should [get moving|scarletMansionEntrance].");
			}
			else {
				writeText("The walls are study enough, so you should be fine to check it out. You get closer to the cage.");
				writeText("The dogs are absolutely rabid. They're all Dobermen, but they're all partially decomposed, like they're straight out of a zombie movie. They gnaw and claw at their containment, probably eager to tear you apart. There's the some fluid splattered on the inside of the cage. They're all wearing studded leather collars with big... Batteries? They're some kind of shock collars probably, way stronger than anything you could get over the counter.");
				writeText("They don't seem happy about their containment, you should [get moving|scarletMansionEntrance].");
			}
			break;
		}
		case "scarletEntry": {
			writeText("You approach the door, it has multiple scratch marks on the front but otherwise it's pristine. You give a knock, but there's no response. Testing your luck you turn the knob, and the door opens. It wasn't locked.");
			writeText("As you walk in you hear something metal snap behind you, and the barking from the kennel gets louder and closer. Thinking quickly you shut the door and feel something slam against it. Looks like you'll need to find someone, preferably their owner, to deal with the hounds if you want to leave.");
			writeText("The windows are all guarded from the inside by thick metal, so there's no need to worry about the dogs getting in from that end. You should be able to safely [explore the mansion foyer|scarletFoyer].");
			break;
		}
		case "scarletFoyer": {
			writeText("You're in the mansion foyer. It's very dark in here, and flipping the lightswitch doesn't seem to do anything. You can hear the sounds of scratching and barking from outside. The big wooden door stands sturdy though.");
			if (checkFlag("dogMet") != true) {
				writeText("There's a large staircase leading up to the second floor, with three doors. One [center door|scarletParlor], one to the [west wing|scarletWestWing], and one to the [east wing|scarletDogIntro].");
			}
			else {
				if (checkFlag("eastHallwayGassed") != true) {
					if (checkItem("remote") == true) {
						if (checkFlag("dogShock1") == true) {
							writeText("There's a large staircase leading up to the second floor, with three doors. One [center door|scarletParlor], one to the [west wing|scarletWestWing], and one that leads to the [east wing|scarletEastWing]. ");
						}
						else {
							writeText("There's a large staircase leading up to the second floor, with three doors. One [center door|scarletParlor], one to the [west wing|scarletWestWing], and one is guarded by the infected woman. It's risky, but you could try [using the remote you found on her|dogShock1]. ");
						}
					}
					else {
						writeText("There's a large staircase leading up to the second floor, with three doors. One [center door|scarletParlor], one to the [west wing|scarletWestWing], and the other is guarded by the infected woman. ");
					}
				}
				else {
					writeText("There's a large staircase leading up to the second floor, with three doors. One [center door|scarletParlor], one to the [west wing|scarletWestWing], and one that leads to the [east wing|scarletEastWing]. ");
				}
			}
			writeHTML(`
				t !item heart key; Below the staircase is a large oaken door, the two knobs chained together with a massive, rusty iron lock keeping the door closed. There's a large heart on the lock.
				t ?item heart key; Below the staircase is a large oaken door, it seems like the lock is made to match with your heart-shaped key. From here the door should lead to the [building's main hall|scarletMainHall].
			`);
			break;
		}
		case "scarletParlor": {
			writeHTML(`
				t You're in the parlor. A pair of comfortable chairs have small end-tables next to them. There's a variety of books here, ranging from fiction to an extensive collection of encyclopedias ranging from history to microbiology. A lot of them have been thrown haphazardly across the ground in a giant mess.
				t The fireplace is lit, suggesting someone has been here recently. That said, the fireplace is locked up tight, it's not clear how it was opened to ignite the wood in the first place.
				t !item spade key; Atop the fireplace is a [single crooked candelabra|spadeKey], it stands out as it's the only thing atop the mantle that hasn't been thrown to the ground.
				t ?item spade key; You've already aligned the candelabra and taken one of the spade keys. Who could've taken the other one?
				t There are multiple ventilation shafts, too small for a person. They emit a vaguely citrus-esque scent. A clear sign someone's keeping this place powered somehow.
				t You can head forwards through a [heavily damaged door|scarletWardrobe], or go back to the [mansion foyer|scarletFoyer] from here.
			`);
			break;
		}
		case "scarletWardrobe": {
			writeHTML(`
				t You're in the wardrobe room. The walls are lined with wardrobes and closets. Some fancier wardrobes hold elegant looking dresses or suits, there are also a few designated for casual clothing, and a few that are locked up tight.
				t One of the wardrobes designed to be locked has been smashed open, and you can see it's filled with labcoats and hazard equipment.
				t !item login credentials; You can see one labcoat has a [piece of paper inside|loginCredentials].
				t ?item login credentials; Nothing inside seems too useful, but there's [that other scrap of paper you found|scientistLetter1].
				t !item diamond key; In addition as you search through the room you find a [small crawlspace|scientistIntro] you could climb into. It's a tight fit though.
				t ?item diamond key;You can't progress through the crawlspace. Best not to risk getting stuck in there for nothing.
				t There's a side-door here that leads to an [outdoor balcony|scarletBalcony], and you can return to the [parlor|scarletParlor] through a big wooden door.
			`);
			break;
		}
		case "scarletBalcony": {
			writeHTML(`
				t !item tape 1; You're standing on the mansion's balcony. A storm has picked up, the wind is raging and the rain is not pleasant to stand in. It's even damaging the house, leaving the secret[light next to the door crooked|hiddenTape1].
				t ?item tape 1; You're standing on the mansion's balcony. A storm has picked up, the wind is raging and the rain is not pleasant to stand in. You found the secret tape here already.
				t Beneath you are a half-dozen guard dogs patrolling the area, not one seems fazed by the bad weather. It appears you're still trapped here in this strange mansion. Speaking of which, you should probably get [back inside|scarletWardrobe].
			`);
			break;
		}
		case "scarletWestWing": {
			writeHTML(`
				t You're in the west wing, the door creaks behind you as you enter. The halls are lined with soft blue and white stripes.
				t !item tape 2; The lights in here are dim, but at least they work. secret[One is twisted to the side|hiddenTape2], making the room a little darker than you'd like.
				t ?item tape 2; The lamps are all correctly aligned, you already found the hidden compartment.
				t !item spade key; Numerous doors line the hall. The first leads to the west wing bedroom, and another is marked as "Guest Bedroom" but they're both locked locked. There's a spade symbol above the locks.
				t ?item spade key; Numerous doors line the hall. The first leads to the [west wing bedroom|scarletWestGuest], you can hear the faint sounds of moaning from inside, and another is marked as ["Guest Bedroom"|scarletWestBedroom].
				t Next is a smaller door, leading to the [west wing bathroom|scarletWestBathroom].
				t Finally you can head [back out into the foyer|scarletFoyer].
			`);
			break;
		}
		case "scarletWestBedroom": {
			writeHTML(`
				t You're in the guest bedroom. Despite the 'guest' part of the name the room has clearly been in use for a while.
				t !flag chastityInfected; Across from you is a large bed with white sheets, and chained onto the bed is a woman. She's completely naked except for a black blindfold and a chastity cage containing her flaccid dick. When you enter she shudders for a moment and thrusts her hips into the air as if begging for release.
				t !flag chastityInfected; She groans, but there's no trace of intelligence in her voice. Talking to her is pointless, like she doesn't even register you are speaking. Not only that, but you have the feeling like setting her free would be a very bad idea.
				t !flag chastityInfected; There's a device on a dresser near the bed, it's the same color as the woman's chastity cage. It looks mishmashed together, like it was scrapped together by an amateur. There's a dial on the device, you could [toy around with it|chastityInfected].
				t ?flag chastityInfected; Across from you is a large bed with stained sheets, and chained onto the bed is a woman. She's completely naked excepted for a black blindfold and a chastity cage containing her flaccid dick.
				t ?flag chastityInfected; When you enter she shudders for a moment and tenses up, as if trying to seek shelter from you, but is otherwise unresponsive.
				t Otherwise, the room is empty. If you're done here, you can head back out to the [west wing hallway|scarletWestWing].
			`);
			break;
		}
		case "scarletWestBathroom": {
			writeHTML(`
				t You're in the west wing bathroom. The room is dark, but at least a window lets in some moonlight. There's the usual fixtures, a sink, a tub, and so on. The unusual part is the rather petite woman chained to the toilet. 
				t She's got on iron clasps but they're nothing compared to the complex looking gag on her mouth. It seems to be designed to completely shut down her ability to make sounds aside from a muffled sighs. It modulates any of the sounds she makes.
				t !flag gaggedSiren; The whole deal is way too complex for you to free her from, but it doesn't seem like she wants to be freed. Next to the toilet is a [large button|gaggedSiren], you probably have a decent ability to guess what it does.
				t ?flag gaggedSiren; The woman chained to the toilet is laying back as far as her bondage will allow, the machine she's hooked up to is probably out of juice, and so is she.
				t ?flag gaggedSiren; Something bizarre is going on here. You're in a strange, dangerous place and you spent time doing that to her... And you don't seem to care how risky it was.
				t It doesn't seem like there are any answers to be found in here, probably best to head [back out into the west wing hallway|scarletWestWing].
			`);
			break;
		}
		case "scarletWestGuest": {
			writeHTML(`
				t You're in the west wing bedroom. The wallpaper is decorated sparsely by black spade icons. The room is pretty musty, and has a familiar scent. The noise you heard is coming from a [computer|tapeVaccine] playing a video file on loop.
				t The bed and the sheets are damp, but stepping towards it you're hit by a strong smell that you can't identify. Near the bed you find a [scrap of paper|scientistLetter3].
				t !item heart key; Finally there's a small box, and inside are two different slots for keys. One is taken, but the item[other key, with a heart shape on it|heart key], is still here.
				t ?item heart key; You already took the heart key from the lockbox, but there's no trace of the other key.
				t If you're finished in here you can head [back out into the west wing hallway|scarletWestWing].
			`);
			break;
		}
		case "scarletMainHall": {
			writeHTML(`
				t !item tape 4; You're in the main hall of the mansion, it's a maze of hallways down here. And if that weren't enough, secret[one of the lights is out too|hiddenTape4].
				t ?item tape 4; You're in the main hall of the mansion, it's a maze of hallways down here. At least you found that hidden tape.
				t With some wandering you can find a number of other doors, luckily most of them have bronze nameplates to ease navigation. The first one you find is labeled ["Theater"|scarletTheater].
				t There's also ["Dining Room"|scarletDining], ["Maintenance"|scarletMaintenance], and ["Garage"|scarletGarage].
				t And there's a [bathroom|scarletMainBathroom] here too.
				t !item diamond key; Finally you find an unlabeled room, it appears to be some kind of storage closet, but it's locked. There's a diamond over the lock. 
				t ?item diamond key; Finally you find an unlabeled room, it appears to be some kind of [storage closet|scarletStorage]. There's a diamond over the lock, and you have the key.
				t Otherwise, you can head back to the [foyer|scarletFoyer].
			`);
			break;
		}
		case "scarletMaintenance": {
			writeHTML(`
				t You're inside the mansion's maintenance room.
				t The room is extremely bare. Water heaters, power cables, but most notably a fuse box. Once locked, someone's ripped the cover right off its hinges. A broken, solid steel lock lays on the floor. Who could have done this?
				t There's a lot of switches, each with a string of numbers and letters meaningless to you. All except for one.
				t "Mansion Master Power/Security"
				t A second lock was ripped off this switch, it seems whoever owns this place <i>really</i> didn't want anyone touching this thing. If you're feeling brave, you could [cut the power to the entire mansion, including the security systems|scarletPowerPrompt].
				t If you're finished in here, you can return to the [main hall|scarletMainHall].
			`);
			break;
		}
		case "scarletPowerPrompt": {
			writeHTML(`
				t Are you absolutely sure? It's a heavy, quite mangled switch and something tells you that you won't be able to just flip the security back on no problem. You can't see why you'd <i>need</i> to cut the mansion's security to find Lansley.
				t [Flip the switch, cut the power.|scarletPowerOut]
				t [Don't, head back out to the main hall.|scarletMainHall]
			`);
			break;
		}
		case "scarletPowerOut": {
			writeHTML(`
				t You take a deep breath and cut the mansion's power. The lights go out, and you're in total darkness.
				t And after just a few seconds, you can hear someone. Screaming? Yelling? Multiple voices all calling out, the sounds of things breaking, followed by total silence.
				t <b>You cut the mansion's power. Good luck.</b>
				t [Head back out to the main hall.|darkMainHall]
			`);
			addFlag("scarletDark");
			break;
		}
		case "scarletMainBathroom": {
			writeHTML(`
				t You're in the heart bathroom, the light in here glows a soft yellow.
				t Inside the bathroom is an infected woman sitting where a toilet should be. Her cock and balls have been chained to the floor leaving her immobile, but instead of struggling and trying to attack you as you enter, she just closes her eyes and lifts her cuffed hands up to her mouth like a cup.
				t !flag urinalInfected; She opens her mouth to let her tongue loll out, as if demonstrating that she knows her place. There doesn't seem to be anything else in here, you could carefully [relieve yourself|urinalInfected] or leave.
				t If you're finished in here, you can return to the [main hall|scarletMainHall].
			`);
			break;
		}
		case "scarletGarage": {
			if (data.player.flags.includes('garageHounds') != true) {
					writeText("You open the door to the garage and flip the lightswitch, but no results. Luckily some moonlight is shining into the room, so you at least aren't blind.");
					writeText("The room is strange. You're in some kind of glass box in the corner of the garage with no way to actually explore the rest of the room. It isn't fragile stuff either, it's tempered glass, bulletproof.");
					writeText("There's a figure in the darkness of the room, not moving. It looks vaguely human, like someone chained to the ground. It could also be some kind of a statue or something.");
					writeText("There's a button in the box with you, it looks like the kind used to open garage doors. You could [press it|scarletHounds].");
					writeText("If you're done in here, you can go back to the [hall|scarletMainHall].");
				}
				else {
					writeText("You're in the garage, or at least some blocked off section of it.");
					writeText("The room is strange. You're in some kind of glass box in the corner of the garage with no way to actually explore the rest of the room. It isn't fragile stuff either, it's tempered glass, bulletproof.");
					writeText("In the center of the room is an unconscious woman, coated in sexual fluids.");
					writeText("If you're done in here, you can go back to the [hall|scarletMainHall].");
				}
			break;
		}
		case "scarletHounds": {
			writeText("You press the button. It must run off a different battery, because the garage door begins to open. Fat lot of good it does you since you're stuck in a box, the only door in here leading you back to the foyer.");
			writeText("As the garage door opens, moonlight shines into the room, revealing the figure in the center. There are no cars, no equipment, just a disorientated looking woman chained to the ground.");
			writeText("As she notices you, she lunges towards your box, only to stop when the chain on her leg is pulled taut.");
			writeText("There's a feral hunger in her eyes, but more noticeably there's a pretty large dick between her legs.");
			writeText("You don't have the time to figure that out though, because it looks like the garage door is partially blocked off by a cage. There's a tunnel connecting the garage door to something outside, and soon enough you see why.");
			writeText("A pair of hounds make their way inside, the whole contraption designed to allow access to the kennel when the button is pushed.");
			writeText("The chained woman doesn't seem to care, and keeps trying to lunge at you as the dogs approach her.");
			writeText("The tempered glass is way too strong for you to break, there's no way to save the woman. It's pretty clear what's about to happen, so you can either [get out of here|scarletMainHall] as soon as possible or stay and [indulge your morbid curiosity|garageHounds].");
			addFlag("garageHounds");
			break;
		}
		case "scarletTheater": {
			writeHTML(`
				t You're in what appears to be some kind of home theater room, though it's only meant for one person at a time for some reason.
				t There's a large television, a chair, and a number of smaller devices meant for video playback. One of them is a tape player, and there's a tape already here ready for viewing, but there's no telling how recently someone's used the room. There is a [scrap of paper on the floor though|scientistLetter2].
				t event[Play the tape labeled "TOY REVENGE".|tapeHound]
				t ?item tape 1; event[Play the tape labelled "LICKER STUDY".|tapeLicker1]
				t ?item tape 2; event[Play the tape labelled "CONGEALANT PROPERTIES".|tapeCongealant]
				t ?item tape 3; event[Play the tape labelled "CHASTITY".|tapeChastity]
				t ?item tape 4; event[Play the tape labelled "SUBMISSION".|tapeSubmission]
				t ?item tape 5; event[Play the tape labelled "LICKER REVENGE".|tapeLicker2]
				t If you're finished you can head out into the [main hallways|scarletMainHall]
			`);
			break;
		}
		case "scarletDining": {
			writeHTML(`
				t You're in the mansion's dining hall, the place is a mess.
				t Multiple people must have been in here at least semi recently. The table is set with a number of half-eaten foods, certainly not fresh but nowhere near moldy. You aren't hungry at the moment, and even if you were, the thick white glaze covering most of the food is... Suspect.
				t !item remote; Near one of the glazed plates is a remote with a item[single large button|remote].
				t ?item remote; You already picked up the remote. If you had to guess you'd say it looks kinda like a dog's shock collar remote, but smaller. Pressing it causes a small light to flicker, the text under says "out of range".
				t !flag firstTurn; At first it seems like nothing else's here, until you see one of the walls has a flaccid phallus sticking out. On closer inspection it's a gloryhole, someone's on the other side!
				t !flag firstTurn; From what you've seen of the mansion so far it'd be risky to try and save them. There's a strange iron valve on the immediate left of the cock, [maybe you could try turning it|firstTurn]?
				t ?flag firstTurn; The living cumtap is here.
				t ?flag firstTurn; !flag secondTurn; You turn the knob, it's suprisingly smooth, like it's recently installed. As you do you hear a sudden moan and the cock jerks, rapidly hardening and leaking precum onto the floor. Do you want to [turn it again|cumtapInfected]?
				t ?flag firstTurn; ?flag secondTurn; !flag thirdTurn; You can her her labored breathing through the wall as she helplessly tries to thrust, shake, anything to get off further. Will you [turn it again|cumtapInfectedRepeat]?
				t ?flag firstTurn; ?flag secondTurn; ?flag thirdTurn; She's completely exhausted, the mess on the carpet is the only proof she ever had any virility.
				t Nothing else here, it might be best to [head back into the hall|scarletMainHall].
			`);
			break;
		}
		case "scarletStorage": {
			writeHTML(`
				t You're in some kind of storage closet. It's very bare, and feels even larger than it is because the west wall is just one giant mirror. The mirror seems to be on wheels, suggesting the wall can be moved to make even more space.
				t !flag storageOpen; The back wall is entirely made from brickwork, secret[one of the bricks seems a little bit more upraised than the others|storageOpen], but other than that the room is entirely unmentionable. With nothing in here, it's probably best to head [back into the main hall|scarletMainHall]. This is what the diamond key opened?
				t ?flag storageOpen; The mirror wall has been pushed back by the switch, revealing a [secret passage to the mansion's basement|scarletBasement]. There could be something dangerous down there, you could head [back to the main hallways|scarletMainHall].
			`);
			break;
		}
		case "scarletBasement": {
			writeHTML(`
				t You're in the hidden basement of the mansion. It's pretty dark, but a great deal of obvious effort has been put into keeping the passageway clean and tidy. The walls are made of smooth stone, not fitting in with the spooky decor of the mansion at all.
				t There are a few doors on the way down. One is labeled ['Security Room'|scarletSecurity], and the other is labeled ['Containment Chambers'|scarletChambers].
				t At the base of the stairs is a thick iron door, there's a keycard reader next to it but it isn't necessary. The reader has been smashed and the lock beneath the handle broken, so you can easily [enter and see what, or who, is inside|scarletControl]. But just looking at the door... You can tell whatever is in there isn't safe.
				t You can head back up the [stairs to the storage room|scarletStorage] from here.
			`);
			break;
		}
		case "scarletChambers": {
			writeHTML(`
				t You're in the containment room. There's a single computer here with a half-dozen monitors, most of them displaying a list of "ongoing projects", a series of notes and video feeds from an unauthorized user.
				t event[Project 1 - Buttslut|labAnal]
				t event[Project 2 - Cumhole Whore|labUrethra]
				t event[Project 3 - Good Sissy|labSissy]
				t event[Project 4 - Oral Trainrrrrrr|labOral]
				t !item login credentials; There's a locked file too, not like the others. You don't have the authority to read it though.
				t ?item login credentials; One is different, it's some kind of [record log from an authorized researcher|scarletDiary].
				t !flag eastHallwayGassed; Finally, there's a monitor just showing what appears to be the east wing hallway, with the woman serving as a guard dog visible, and a command saying ["Release Gas"|scarletGas].
				t ?flag eastHallwayGassed; Finally, there's a monitor just showing what appears to be the east wing hallway, with the woman serving as a guard dog laying on the ground. She seems to be waking up, but is languidly resting on the floor.
				t If you're finished in here you can [head back into the basement hub|scarletBasement].
			`);
			break;
		}
		case "scarletGas": {
			writeHTML(`
				t You activate the function, a pink gas begins to fill the hallway, surprising the guard woman. She seems nervous for a second, but after a moment she lunges for the closest vent, although she's held back by her leash.
				t A bar begins to fill onscreen, showing a saturation meter at 5%.
				t The woman pants and licks at the air, a pleased smile forming on her face.
				t The meter is at 15%.
				t The woman visibly relaxes and stops tugging at her leashing, laying on her back and rubbing her breasts.
				t The meter is at 40%.
				t The woman begins to spasm on the floor, no longer rubbing her chest she's gripping the carpet as her tongue lols out and her hips arch. Her cock wags in the air.
				t The meter is at 60%.
				t She begins to cum, seemingly from no sensation at all. More squirting, really, the monitor's high definition lets you see her cum is extremely thin and watery.
				t The meter is at 80%.
				t Her eyes have rolled back and she's collapsed, her cock still squirting, but also growing rapidly smaller until so small you'd mistake it for being flaccid if it wasn't standing straight up.
				t The meter hits 100%, and begins to rapidly fall. The pink tint of the room begins to fade, the guard woman is completely unconcious.
				t The command has completed, [you should keep exploring|scarletChambers].
			`);
			addFlag("eastHallwayGassed");
			break;
		}
		case "scarletSecurity": {
			writeHTML(`
				t You're in the security room. 
				t The room is directly adjacent to an extremely large testing chamber, although there's nobody inside and the door is wide open. According to the data chart on the front, the room should be containing a research subject.
				t ?flag scientistGone; There's a secondary chamber with [someone inside|labCumbreather], and a timer next to the door. It's only been running for a few minutes...
				t There's a [scrap of paper|scientistLetter5] in the trashcan. Other than that the place is empty, so you can head back out into the [basement|scarletBasement].
			`);
			break;
		}
		case "scarletControl": {
			writeScene("scarletEndingStart");
			break;
		}
		case "scarletEastWing": {
			writeHTML(`
				t You're in the east wing, thankfully safe from the woman serving as a guard dog.
				t !flag eastHallwayGassed; !flag dogShock2; She assumes her position the moment she sees you, it seems she's intelligent enough to register you have the remote to her little accessory, but not enough to be able to speak. Part of you worries she might try to get you from behind, [another application of the remote|dogShock2] could help with that...
				t !flag eastHallwayGassed; ?flag dogShock2; !flag dogShock3; She's laying on the ground, twitching in pleasure. It <i>could</i> be an act, maybe [another shock|dogShock3]?
				t !flag eastHallwayGassed; ?flag dogShock3; She's laying on the floor, still leaking. She's in no condition to do much of anything at the moment.
				t ?flag eastHallwayGassed; !flag dogGassed; She's currently on her knees panting like the dog she's supposed to be. It seems like the gas made her a lot friendlier, and it made her dick a lot smaller. The device she was wearing has fallen off, although she doesn't seem hostile anymore. When she notices you she lays on her back as a sign of submission, as if to ask you to [pet her|dogGassed].
				t ?flag eastHallwayGassed; ?flag dogGassed; She seems more content to rub herself on the carpet than pay attention to you.
				t !item tape 3; secret[One of the lamps in this room is broken too|hiddenTape3], the same one actually, it's twisted in the same way.
				t !flag scientist; There's another door here, with a lock crudely affixed to it that has a heart shape above it. There's the sound of moaning from here as well, but... You have a bad feeling about this room.
				t ?flag scientist; !flag scientistEscape; !item heart key; There's another door here, with a lock crudely affixed to it that has a heart shape above it. This must be the room the scientist is trapped in! Without the key you can't enter though, the doors in this mansion are extremely sturdy.
				t ?flag scientist; !flag scientistEscape; ?item heart key; There's another door here, with a lock crudely affixed to it that has a heart shape above it. This must be the room the scientist is trapped in! With your heart key in hand, you can [free her|scientistEscape]!
				t ?flag scientistEscape; !flag scientistVanish; You need to get moving, quick! The scientist is still here, but you shouldn't have too long before she turns!
				t !flag analInfected; Numerous doors line the hall. The first leads to the [east wing bedroom|scarletEastBedroom]. You can hear someone inside.
				t ?flag analInfected; Numerous doors line the hall. The first leads to the east wing bedroom, currently occupied by an infected woman undergoing some kind of training.
				t Next is a door marked as an [administrator bedroom|scarletEastGuest], and finally is a smaller door, leading to the [east wing bathroom|scarletEastBathroom].
				t Finally you can head [back out into the foyer|scarletFoyer].
			`);
			if (checkFlag("scientistVanish") == true && checkFlag("scientistGone") != true ) {
				addFlag("scientistGone");
				sceneTransition("scientistVanish");
			}
			if (checkFlag("scientistEscape") == true && checkFlag("scientistVanish") != true) {
				addFlag("scientistVanish");
			}
			break;
		}
		case "scarletEastBedroom": {
			//Anal girl is here
			writeEvent("analInfected");
			addFlag("analInfected");
			writeHTML(`
				t [You should probably go.|scarletEastWing]
			`);
			break;
		}
		case "scarletEastBathroom": {
			writeHTML(`
				t You're in the east wing bathroom. It's clean, but highly cluttered.
				t Near the bathtub are a variety of cleaning supplies. Some conventional rubber gloves, some with strange textures on the fingertips like they're designed to stimulate flesh. These are atop a massive collection of shampoos and bodywashes for extremely hard to remove scents.
				t !flag tubInfected; The bathtub is currently full, with a thick layer of soap covering the water. There's a chain coming out of it connected to a holder on the wall, designed to allow someone outside the tub to [drain it|tubInfected].
				t ?flag tubInfected; The infected woman is still laying in the tub, her sleeping form occasionally twitching. Her dick is at full mast, either a result of morning wood or just being out of water is stimulating her body.
				t It doesn't seem like there's anything useful in here, you can [head back into the hallway|scarletEastWing] at any time.
			`);
			break;
		}
		case "scarletEastGuest": {
			writeHTML(`
				t You're in the administrator's bedroom. It's noticably better furnished than the other rooms, but just like some of the others it looks like a tornado's gone through the place.
				t There are a lot of torn up documents in here, some of them are... Unreadable, to say the least. What few scraps are unruined are mostly useless, [except one piece of a memo|scarletMemo] and a [piece of a letter|scientistLetter4]
				t !item tape 5; Laying near the bed is item[a tape with a scratched-off label|tape 5].
				t The smell makes you feel dizzy. You [should leave as soon as you can|scarletEastWing]
			`);
			//t Finally, there's a massive safe with no knob and no clear way to open it.
			break;
		}
		//Old Demo Stuff
		case "scarletFoyer": {
			writeText("You're in the mansion foyer.");
			writeText("It's very dark in here, and flipping the lightswitch doesn't seem to do anything.");
			writeText("You can hear the sounds of scratching and barking from outside. The big wooden door stands sturdy though.");
			writeText("There's a large staircase leading up to the second floor, with two doors. One leads to the [West Wing|scarletWestWing], and is unlocked. One leads to the East Wing, and has a small passcode lock on it.");
			writeText("Below the staircase is a large oaken door, the two knobs chained together with a massive, rusty iron lock keeping the door closed.");
			writeText("There's a rather flimsy wooden door off to the side with a window on it. It's really dark, but it seems to lead to some kind of [garage|scarletGarage].");
			if (data.player.flags.includes('scarletChastityTorture') == true && data.player.flags.includes('scarletHoundsTorture') == true) {
				writeSpecial("You've already found both of the events of this scenario, it is a demo after all. Thanks for playing!");
			}
			break;
		}
		case "scarletWestWing": {
			writeText("You're in the west wing.");
			writeText("The door creaks as you walk into the long hallway. It's pretty dark, but you can see the wallpaper is decorated with soft blue and white stripes.");
			writeText("There's a pair of brown doors, probably leading to bedrooms. The door marked with a clover is closest to you is locked, and the [other door marked with a spade|scarletSpadeBedroom] has a dented knob and is unlocked.");
			//writeText("There's a white door with a sign on the front labeled ['Bathroom'|scarletWestBathroom].");
			writeText("The [stairs back to the foyer|scarletFoyer] are behind you.");
			break;
		}
		case "scarletSpadeBedroom": {
			if (data.player.flags.includes('scarletChastityTorture') != true) {
				writeText("You're in the spade bedroom");
				writeText("The wallpaper is white, decorated sparsely by black spade icons. The room is pretty musty, and has a familiar scent.");
				writeText("Across from you is a large bed with white sheets, and chained onto the bed is a woman. She's completely naked excepted for a black blindfold and a chastity cage containing her flaccid dick.");
				writeText("When you enter she shudders for a moment and thrusts her hips into the air as if begging for release.");
				writeText("She groans, but there's no trace of intelligence in her voice. Talking to her is pointless, like she doesn't even register you are speaking.");
				writeText("There's a device on a dresser near the bed, it's the same color as the woman's chastity cage. It looks mismashed together, like it was scrapped together by an amateur. There's a dial on the device, you could [toy around with it|scarletChastityTorture].");
				writeText("Otherwise, the room is empty. If you're done here, you can head back out to the [west wing|scarletWestWing].");
			}
			else {
				writeText("You're in the spade bedroom");
				writeText("The wallpaper is white, decorated sparsely by black spade icons. The room is pretty musty, and has a familiar scent.");
				writeText("Across from you is a large bed with white sheets, and chained onto the bed is a woman. She's completely naked excepted for a black blindfold and a chastity cage containing her flaccid dick.");
				writeText("When you enter she shudders for a moment and tenses up, as if trying to seek shelter from you.");
				writeText("Otherwise, the room is empty. If you're done here, you can head back out to the [west wing|scarletWestWing].");
			}
			break;
		}
		case "scarletChastityTorture": {
			writeEvent(scene);
			data.player.flags += 'scarletChastityTorture';
			break;
		}
		case "scarletWestBathroom": {
			if (data.player.flags.includes('scarletSirenTorture') != true) {
				writeText("You're in the west wing bathroom.");
				writeText("The room is dark, but at least a window lets in some moonlight. There's the usual fixtures, a sink, a tub, and so on. The unusual part is the rather petite woman chained to the wall. She's got on iron clasps but they're nothing compared to the complex looking gag on her mouth. It seems to be designed to allow her tongue to stick out, but completely shut down her ability to make sounds aside from a muffled and... Modulated? Grunt.");
				writeText("The whole deal is way too complex for you to free her from, but it doesn't seem like she wants to be freed.");
				writeText("The door was hooked up to some kind of wire. When you opened it, it led to some kind of mechanical dildo machine being activated, and after a few *whirrs* the device starts to power up.");
			}
			else {
				writeText("You're in the west wing bathroom.");
				writeText("The room is dark, but at least a window lets in some moonlight. There's the usual fixtures, a sink, a tub, and so on.");
				writeText("The woman chained to the wall is laying against the tiles behind her, the machine she's hooked up to is probably out of juice.");
				writeText("Dangling from her neck is a metal tag, listing an identification number and a name, 'Kennedy'. It belongs to a member of Charlie Team, one of Kelsey's squadmates!");
				writeText("She often told you about him being reprimanded for getting too handsy with women. How on earth did this girl get his tag?");
				writeText("It doesn't seem like there are any answers left in here, probably best to head back out into the west wing hallway.");
			}
			break;
		}
		case "scarletSirenTorture": {
			writeEvent(scene);
			break;
		}
		//Dark Mansion
		case "darkFoyer": {
			writeText("You're in the mansion foyer. It's very dark in here, and flipping the lightswitch doesn't do anything. You can hear the sounds of scratching and barking from outside. The big wooden door stands sturdy though.");
			writeText("It's got red walls and all of the windows are covered by curtains. One of the lights is on, but the window is blocked off by iron bars.");
			if (checkFlag("dogMet") != true) {
				if (checkFlag("eastHallwayGassed") != true) {
				writeText("There's a large staircase leading up to the second floor, with three doors. One [center door|darkParlor], one to the [west wing|darkWestWing], and one to the [east wing|dogDark].");
				}
				else {
					writeText("There's a large staircase leading up to the second floor, with three doors. One [center door|darkParlor], one to the [west wing|darkWestWing], and one that leads to the [east wing|darkEastWing].");
				}
			}
			else {
				if (checkFlag("eastHallwayGassed") != true) {
					if (checkItem("remote") == true) {
						if (checkFlag("dogShock1") == true) {
							writeText("There's a large staircase leading up to the second floor, with three doors. One [center door|darkParlor], one to the [west wing|darkWestWing], and one that leads to the [east wing|darkEastWing]. ");
						}
						else {
							writeText("There's a large staircase leading up to the second floor, with three doors. One [center door|darkParlor], one to the [west wing|darkWestWing], and one is guarded by the infected woman. It's risky, but you could try [using the remote you found on her|dogDark]. ");
						}
					}
					else {
						writeText("There's a large staircase leading up to the second floor, with three doors. One [center door|darkParlor], one to the [west wing|darkWestWing], and the other is guarded by the infected woman. ");
					}
				}
				else {
					writeText("There's a large staircase leading up to the second floor, with three doors. One [center door|darkParlor], one to the [west wing|darkWestWing], and one that leads to the [east wing|darkEastWing]. ");
				}
			}
			writeHTML(`
				t !item heart key; Below the staircase is a large oaken door, the two knobs chained together with a massive, rusty iron lock keeping the door closed. There's a large heart on the lock.
				t ?item heart key; Below the staircase is a large oaken door, it seems like the lock is made to match with your heart-shaped key. From here the door should lead to the [building's main hall|darkMainHall].
			`);
			break;
		}
		case "darkParlor": {
			writeHTML(`
				t You're in the parlor. A pair of comfortable chairs have small end-tables next to them. There's a variety of books here, ranging from fiction to an extensive collection of encyclopedias ranging from history to microbiology. A lot of them have been thrown haphazardly across the ground in a giant mess.
				t The fireplace is dead, it's not clear how it was even opened to ignite the wood in the first place.
				t !item spade key; Atop the fireplace is a [single crooked candelabra|spadeKey], it stands out as it's the only thing atop the mantle that hasn't been thrown to the ground.
				t ?item spade key; You've already aligned the candelabra and taken one of the spade keys. Who could've taken the other one?
				t There are multiple ventilation shafts, too small for a person. There's the faint smell of citrus, but the vents are no longer blowing.
				t You can head forwards through a [heavily damaged door|darkWardrobe], or go back to the [mansion foyer|darkFoyer] from here.
			`);
			break;
		}
		case "darkWardrobe": {
			writeHTML(`
				t You're in the wardrobe room. The walls are lined with wardrobes and closets. Some fancier wardrobes hold elegant looking dresses or suits, there are also a few designated for casual clothing, and a few that are locked up tight.
				t One of the wardrobes designed to be locked has been smashed open, and you can see it's filled with labcoats and hazard equipment.
				t !item login credentials; You can see one labcoat has a [piece of paper inside|loginCredentials].
				t ?item login credentials; Nothing inside seems too useful, but there's [that other scrap of paper you found|scientistLetter1].
				t !flag scientist; In addition as you search through the room you find a [small crawlspace|scientistIntro] you could climb into. It's a tight fit though.
				t ?flag scientist; You can't progress through the crawlspace. Best not to risk getting stuck in there for nothing.
				t There's a side-door here that leads to an [outdoor balcony|darkBalcony], and you can return to the [parlor|darkParlor] through a big wooden door.
			`);
			break;
		}
		case "darkBalcony": {
			writeHTML(`
				t You're standing on the mansion's balcony. A storm has picked up, the wind is raging and the rain is not pleasant to stand in.
				t Beneath you are a half-dozen guard dogs patrolling the area, not one seems fazed by the bad weather. It appears you're still trapped here in this strange mansion. Speaking of which, you should probably get [back inside|darkWardrobe].
			`);
			break;
		}
		case "darkWestWing": {
			writeHTML(`
				t You're in the west wing, the door creaks behind you as you enter. The halls are lined with soft blue and white stripes.
				t The lamps are all out, the only illumination comes from the infrequent light of a lightning strike in the distance.
				t !item spade key; Numerous doors line the hall. The first leads to the west wing bedroom, and another is marked as "Guest Bedroom" but they're both locked locked. There's a spade symbol above the locks.
				t !flag chastityDark; ?item spade key; Numerous doors line the hall. The first leads to the [west wing bedroom|darkWestGuest]. You can hear the faint sounds of moaning from the other, inside a door marked as ["Guest Bedroom"|darkWestBedroom].
				t ?flag chastityDark; ?item spade key; Numerous doors line the hall. The first leads to the [west wing bedroom|darkWestGuest]. You can hear the faint sounds of moaning from inside the other, a door marked as "Guest Bedroom". The woman inside must still be trying to cum, getting off in equal measure to the time she spent torturously unable to cum.
				t Next is a smaller door, leading to the [west wing bathroom|darkWestBathroom].
				t Finally you can head [back out into the foyer|darkFoyer].
			`);
			if (checkFlag("gaggedSiren") != true && checkFlag("female") != true) {
				writeScene("gaggedDark");
			}
			break;
		}
		case "darkWestBedroom": {
			writeHTML(`
				t You're peering into the guest bedroom. Despite the 'guest' part of the name the room has clearly been in use for a while.
				t The woman is still gleefully squeezing her caged member and her puffy, well-loved balls. There's no telling how long she'll be distracted like this, or if the cage can still contain her if she sets her eyes on someone she wants to infect.
				t Otherwise, the room is empty. If you're done here, you can head back out to the [west wing hallway|darkWestWing].
			`);
			if (checkFlag("chastityDark") != true) {
				writeScene("chastityDark");
			}
			break;
		}
		case "darkWestBathroom": {
			writeHTML(`
				t You're in the west wing bathroom. The room is dark, but at least a window lets in some moonlight. There's the usual fixtures, a sink, a tub, and so on. The unusual part is the rather petite woman chained to the toilet. 
				t She's got on iron clasps but they're nothing compared to the complex looking gag on her mouth. It seems to be designed to completely shut down her ability to make sounds aside from a muffled and it modulates any of the sounds she makes.
				t !flag gaggedSiren; She's humming a light tune, muffled by her mask but somehow it sounds... Off. Like it's scratching at the inside of your brain.
				t ?flag gaggedSiren; The woman chained to the toilet is laying back as far as her bondage will allow, the machine she's hooked up to is probably out of juice, and so is she.
				t ?flag gaggedSiren; Something bizarre is going on here. You're in a strange, dangerous place and you spent time doing that to her... And you don't seem to care how risky it was.
				t It doesn't seem like there are any answers to be found in here, probably best to head [back out into the west wing hallway|darkWestWing].
			`);
			break;
		}
		case "darkWestGuest": {
			writeHTML(`
				t You're in the west wing bedroom. The wallpaper is decorated sparsely by black spade icons. The room is pretty musty, and has a familiar scent. 
				t The bed and the sheets are damp, but stepping towards it you're hit by a strong smell that you can't identify. Near the bed you find a [scrap of paper|scientistLetter3].
				t !item heart key; Finally there's a small box, and inside are two different slots for keys. One is taken, but the item[other key, with a heart shape on it|heart key], is still here.
				t ?item heart key; You already took the heart key from the lockbox, but there's no trace of the other key.
				t If you're finished in here you can head [back out into the west wing hallway|darkWestWing].
			`);
			break;
		}
		case "darkMainHall": {
			writeHTML(`
				t You're in the main hall of the mansion, it's a maze of hallways down here, not helped in the least by the pitch black darkness.
				t With some wandering you can find a number of other doors, luckily most of them have engraved bronze nameplates to ease navigation. The first one you find is labeled ["Theater"|darkTheater].
				t There's also ["Dining Room"|darkDining], and ["Garage"|darkGarage].
				t And there's a [bathroom|darkMainBathroom] here too.
				t !item diamond key; Finally you find an unlabeled room, it appears to be some kind of storage closet, but it's locked. There's a diamond over the lock. 
				t ?item diamond key; Finally you find an unlabeled room, it appears to be some kind of [storage closet|darkStorage]. There's a diamond over the lock, and you have the key.
				t Otherwise, you can head back to the [foyer|darkFoyer].
			`);
			break;
		}
		case "darkMainBathroom": {
			writeHTML(`
				t You're in the heart bathroom, the lights are out.
				t Inside the bathroom you can just barely make out the form of an infected woman sitting where a toilet should be. Her cock and balls have been chained to the floor leaving her immobile, but instead of struggling and trying to attack you as you enter, she just closes her eyes and lifts her cuffed hands up to her mouth like a cup. 
				t She's so well trained that even with the power out, she doesn't seem to be making an attempt to escape.
				t !flag urinalInfected; She opens her mouth to let her tongue loll out, as if demonstrating that she knows her place. There doesn't seem to be anything else in here, you could carefully [relieve yourself|urinalInfected] or leave.
				t If you're finished in here, you can return to the [main hall|darkMainHall].
			`);
			break;
		}
		case "darkGarage": {
			if (data.player.flags.includes('garageHounds') != true) {
					writeText("You open the door to the garage and flip the lightswitch, but no results. Luckily some moonlight is shining into the room, so you at least aren't blind.");
					writeText("The room is strange. You're in some kind of glass box in the corner of the garage with no way to actually explore the rest of the room. It isn't fragile stuff either, it's tempered glass, bulletproof.");
					writeText("There's a figure in the darkness of the room, not moving. It looks vaguely human, like someone chained to the ground. It could also be some kind of a statue or something.");
					writeText("If you're done in here, you can go back to the [hall|darkMainHall].");
				}
				else {
					writeText("You're in the garage, or at least some blocked off section of it.");
					writeText("The room is strange. You're in some kind of glass box in the corner of the garage with no way to actually explore the rest of the room. It isn't fragile stuff either, it's tempered glass, bulletproof.");
						writeHTML(`
						t The infected woman, still partially bound, is joyfully servicing multiple hounds as best she can.
						t Until the lights come back on the security systems should be down, meaning you probably aren't safe to keep watching as you please. You should head back inside.
					`);
					writeText("If you're done in here, you can go back to the [hall|darkMainHall].");
				}
			break;
		}
		case "darkTheater": {
			writeHTML(`
				t You're in what appears to be some kind of home theater room, though it's only meant for one person at a time for some reason.
				t There's a large television, a chair, and a number of smaller devices meant for video playback. One of them is a tape player, and there's a tape already here ready for viewing, but there's no telling how recently someone's used the room. There is a [scrap of paper on the floor though|scientistLetter2].
				t With the power shut down there's no way to play videos right now.
				t If you're finished you can head out into the [main hallways|darkMainHall]
			`);
			break;
		}
		case "darkDining": {
			writeHTML(`
				t You're in the mansion's dining hall, the place is a mess.
				t Multiple people must have been in here at least semi recently. The table is set with a number of half-eaten foods, certainly not fresh but nowhere near moldy. You aren't hungry at the moment, and even if you were, the thick white glaze covering most of the food is... Suspect.
				t !item remote; Near one of the glazed plates is a remote with a item[single large button|remote].
				t ?item remote; You already picked up the remote. If you had to guess you'd say it looks kinda like a dog's shock collar remote, but smaller. Pressing it causes a small light to flicker, the text under says "NO SIGNAL, NON FUNCTIONAL".
				t !flag thirdTurn; Sticking out of the wall is an erect penis, it's owner is rapidly thrusting against the wall trying to break free, or to masturbate using the wall as a gloryhole fleshlight. Either way, it's dangerous to stay in here.
				t ?flag thirdTurn; Sticking out of the wall is a flaccid penis, the owner is completely exhausted, the mess on the carpet is the only proof she ever had any virility.
				t Nothing else here, it might be best to [head back into the hall|darkMainHall].
			`);
			break;
		}
		case "darkStorage": {
			writeHTML(`
				t You're in some kind of storage closet. It's very bare, and feels even larger than it is because the west wall is just one giant mirror. The mirror seems to be on wheels, suggesting the wall can be moved to make even more space.
				t The mirror wall has already been pushed back, revealing a [secret passage to the mansion's basement|darkBasement]. There could be something dangerous down there, you could head [back to the main hallways|darkMainHall].
			`);
			break;
		}
		case "darkBasement": {
			writeHTML(`
				t You're in the hidden basement of the mansion. It's pretty dark, but a great deal of obvious effort has been put into keeping the passageway clean and tidy. The walls are made of smooth stone, not fitting in with the spooky decor of the mansion at all.
				t There are a few doors on the way down. One is labeled ['Security Room'|darkSecurity], and the other is labeled ['Containment Chambers'|darkChambers].
				t At the base of the stairs is a thick iron door, there's a keycard reader next to it but it isn't necessary. The reader has been smashed and the lock beneath the handle broken, so you can easily [enter and see what, or who, is inside|darkControl]. But just looking at the door... You can tell whatever is in there isn't safe.
				t You can head back up the [stairs to the storage room|darkStorage] from here.
			`);
			break;
		}
		case "darkChambers": {
			writeHTML(`
				t You're in the containment room. There's a single computer here with a half-dozen monitors, but all of them are dark and blank. With the mansion's power shut down you'll never know what they might have displayed.
				t If you're finished in here you can [head back into the basement hub|darkBasement].
			`);
			break;
		}
		case "darkSecurity": {
			writeHTML(`
				t You're in the security room. 
				t The room is directly adjacent to an extremely large testing chamger, although there's nobody inside and the door is wide open. According to the data chart on the front, the room should be containing a research subject.
				t The floor is wet with an unknown fluid, and has a salty smell.
				t There's a [scrap of paper|scientistLetter5] in the trashcan. Other than that the place is empty, so you can head back out into the [basement|darkBasement].
			`);
			break;
		}
		case "darkControl": {
			writeScene("darkEndingStart");
			break;
		}
		case "darkEastWing": {
			writeHTML(`
				t You're in the east wing, thankfully safe from the woman serving as a guard dog.
				t !flag eastHallwayGassed; !flag dogShock3; She assumes her position the moment she sees you, it seems she's intelligent enough to register you have the remote to her little accessory, but not enough to be able to speak. Part of you worries she might try to get you from behind, but when lightning strikes outside she jumps a little. She's obviously nervous, but that helps to humanize her just a bit. You should be safe.
				t !flag eastHallwayGassed; ?flag dogShock3; She's laying on the floor, still leaking. She's in no condition to do much of anything at the moment.
				t ?flag eastHallwayGassed; !flag dogGassed; She's currently on her knees panting like the dog she's supposed to be. It seems like the gas made her a lot friendlier, and it made her dick a lot smaller. The device she was wearing has fallen off, although she doesn't seem hostile anymore. When she notices you she lays on her back as a sign of submission, as if to ask you to [pet her|dogGassed].
				t ?flag eastHallwayGassed; ?flag dogGassed; She seems more content to rub herself on the carpet than pay attention to you.
				t !flag scientist; There's another door here, with a lock crudely affixed to it that has a heart shape above it. There's the sound of moaning from here as well, but... You have a bad feeling about this room.
				t ?flag scientist; !flag scientistEscape; !item heart key; There's another door here, with a lock crudely affixed to it that has a heart shape above it. This must be the room the scientist is trapped in! Without the key you can't enter though, the doors in this mansion are extremely sturdy.
				t ?flag scientist; !flag scientistEscape; ?item heart key; There's another door here, with a lock crudely affixed to it that has a heart shape above it. This must be the room the scientist is trapped in! With your heart key in hand, you can [free her|darkEscape]!
				t !flag analInfected; Numerous doors line the hall. The first leads to the [east wing bedroom|analDark]. You can hear someone inside.
				t ?flag analInfected; Numerous doors line the hall. The first leads to the east wing bedroom, currently occupied by an infected woman undergoing some kind of training.
				t Next is a door marked as an [administrator bedroom|darkEastGuest], and finally is a smaller door, leading to the [east wing bathroom|darkEastBathroom].
				t Finally you can head [back out into the foyer|darkFoyer].
			`);
			break;
		}
		case "darkEastBathroom": {
			writeHTML(`
				t You're in the east wing bathroom. It's clean, but highly cluttered.
				t Near the bathtub are a variety of cleaning supplies. Some conventional rubber gloves, some with strange textures on the fingertips like they're designed to stimulate flesh. These are atop a massive collection of shampoos and bodywashes for extremely hard to remove scents.
				t !flag tubInfected; The bathtub is currently full, with a thick layer of soap covering the water. There's a chain coming out of it connected to a holder on the wall, designed to allow someone outside the tub to [drain it|tubInfected].
				t ?flag tubInfected; The infected woman is still laying in the tub, she isn't twitching at the moment. Her dick is at full mast, either a result of morning wood or just being out of water is stimulating her body.
				t It doesn't seem like there's anything useful in here, you can [head back into the hallway|darkEastWing] at any time.
			`);
			break;
		}
		case "darkEastGuest": {
			writeHTML(`
				t You're in the administrator's bedroom. It's noticably better furnished than the other rooms, but just like some of the others it looks like a tornado's gone through the place.
				t There are a lot of torn up documents in here, some of them are... Unreadable, to say the least. What few scraps are unruined are mostly useless, [except one piece of a memo|darkMemo] and a [piece of a letter|scientistLetter4]
				t !item tape 5; Laying near the bed is item[a tape with a scratched-off label|tape 5].
				t The smell makes you feel dizzy. You [should leave as soon as you can|darkEastWing]
			`);
			//t Finally, there's a massive safe with no knob and no clear way to open it.
			break;
		}
		case "darkMemo": {
			writeHTML(`
				t The memo is cut off at the top and bottom, this is all you could find among mostly-intact pieces.
				t <i>"I think it has an incredible number of uses for security, but we'd need to train the employees on it. I hid some of the spade keys this way, but Smith was too dense to notice an askew candlestick. I've hidden some of the favorites among our research recordings, hopefully they're enough of an incentive. They're pretty obvious spots too.</i>
				t <i>The hard part is that it blocks casual observation, you'll need to force yourself to consciously interact with things, even if it seems pointless, otherwise you miss them. Obviously the best use is to hide the entrance in the storage closet, but there are still four left, one's in the West W-</i>
				t [That's where it ends.|scarletEastGuest]
			`);
			break;
		}
		case "chastityDark": {
			writeEvent(scene);
			addFlag("chastityDark");
			writeHTML(`
				t Before she notices you, you should [leave|darkWestWing]
			`);
			break;
		}
		case "gaggedDark": {
			writeEvent(scene);
			writeHTML(`t [Your fate is sealed in the dark halls...|gaggedEnding]`)
			break;
		}
		case "gaggedEnding": {
			writeEvent(scene);
			writeTransition('scenarioSelect', 'GAME OVER', '#FF0000');
			break;
		}
		case "cumtapEnding": {
			writeEvent(scene);
			writeTransition('scenarioSelect', 'GAME OVER', '#FF0000');
			break;
		}
		case "dogDark": {
			writeEvent(scene);
			writeHTML(`t [Your fate is sealed in the dark halls, and when the power comes back on...|dogEnding]`);
			break;
		}
		case "dogEnding": {
			writeEvent(scene);
			writeTransition('scenarioSelect', 'GAME OVER', '#FF0000');
			break;
		}
		case "dogGassed": {
			writeEvent(scene);
			addFlag("dogGassed");
			writeHTML(`t You should probably [get going|scarletEastWing]. She seems harmless, but if she decides to mark her territory she might expose you to the virus.`);
			break;
		}
		case "darkEscape": {
			addFlag("scientistEscape");
			writeEvent("darkEscape1");
			writeHTML(`
				t ?item remote; The hounds start growling, and hesitantly you grab your zipper. As you unzip, you feel [something you've forgotten in your coat pocket.|darkEscape2a]
				t !item remote; The hounds start growling, and hesitantly you grab your zipper. [Is this the end for you?|darkEscape2b]
			`);
			break;
		}
		case "darkEscape2a": {
			writeEvent("darkEscape2a");
			writeHTML(`
				t It's time to [keep exploring|darkFoyer].
			`);
			break;
		}
		case "darkEscape2b": {
			writeEvent("darkEscape2b");
			writeHTML(`
				t It's time to [keep exploring|darkFoyer].
			`);
			break;
		}
		case "analDark": {
			addFlag("analInfected");
			writeEvent(scene);
			writeHTML(`t [You should get moving.|darkEastWing]`)
			break;
		}
		case "darkCumbreather": {
			writeEvent(scene);
			break;
		}
		case "darkEndingStart": {
			writeEvent(scene);
			writeHTML(`
				t And before you can take another breath, you feel a [hand gripping your hair|darkEndingFinishFemale]. ?flag female;
				t And before you can take another breath, you feel a [hand gripping your hair|darkEndingFinishMale]. !flag female;
			`);
			break;
		}
		case "darkEndingFinishMale": {
			removeFlag("scarletDark");
			writeEvent(scene);
			writeText("...");
			writeText("You've successfully found the secret of the Scarlet Mansion, or, rather, it found you.");
			writeText("Check out the 'Author Information' section in the scenario selection menu for a more detailed set of credits, or play the game again to try and find every nook and cranny still to be spelunked.");
			writeTransition("mansionIntro", "Thanks for playing!");
			break;
		}
		case "darkEndingFinishFemale": {
			removeFlag("scarletDark");
			writeEvent(scene);
			writeText("...");
			writeText("You've successfully found the secret of the Scarlet Mansion, or, rather, it found you.");
			writeText("Check out the 'Author Information' section in the scenario selection menu for a more detailed set of credits, or play the game again to try and find every nook and cranny still to be spelunked.");
			writeTransition("mansionIntro", "Thanks for playing!");
			break;
		}
		//Scarlet mansion events or system info
		case "escapeStorage": {
			writeHTML(`
				
			`);
			break;
		}
		case "escapeMainHall": {
			writeHTML(`
				
			`);
			break;
		}
		case "escapeFoyer": {
			writeHTML(`
				
			`);
			break;
		}
		case "escapeGiveIn": {
			writeHTML(`
				t You close your eyes and take a deep breath, trying to imaging who your sister used to be before all of this.
				t You can't run away from this. It's best to accept these last moments with grace, with at least some dignity.
				t You can see her face in your mind's eye, Looking like how she was before this mission. Just as you remember the sound of her voice, you can feel the queen's arms wrap around you.
				t ... Yet not strong enough to bind you. More like...
				t ... A hug?
				t You open your eyes and Lansley is indeed gently hugging you, not assaulting you like you expected. Maybe it's because she just got some relief and she's not horny, maybe it's because of her type of infection...
				t Maybe it's because she recognizes you?
				t Whatever the case, she isn't attacking you. You gently push Lansley back and she looks at you with a blank expression. 
			`);
			break;
		}
		case "spadeKey": {
			addItem("spade key");
			writeHTML(`
				t You reset the candelabra, and with a click a small panel on the mantle pops open, to reveal a half-dozen keys, all of which have a spade head. You take one, but one more is missing, meaning someone else here has one too.
				t No need for extras, it'd be best to [resume the search|scarletParlor].
			`);
			break;
		}
		case "loginCredentials": {
			addItem("login credentials");
			writeHTML(`
				t You fish through the exposed labcoat for a moment searching for anything useful. No key or keycard, or anything like that, but you do find a small piece of paper with what appears to be a username and password written on it.
				t You got the login credentials!
				t With that found, another labcoat has a [scrap of paper|scientistLetter1] that seems to be some kind of diary entry. Aside from that there [doesn't appear to be anything else to find|scarletWardrobe].
			`);
			break;
		}
		case "scarletDogIntro": {
			addFlag("dogMet");
			writeHTML(`
				t The lock of the east wing's door is pretty damaged, so you push through without any trouble expecting more creepy, empty hallway.
				t That is, until you hear rapid footsteps and the sound of a metal chain being pulled taut.
				t You stumble backwards and fall, staring you down practically frothing at the mouth is a woman held back by her collar, attached to the wall by a chain.
				t You quickly shut the door, panting heavily and trying to think through what you saw. Her eyes were crazed, she was like a feral dog ready to tear you limb from limb. She also had... A penis. An undeniably large one, with some kind of accessory on it.
				t You need to clear your head and [get moving|scarletFoyer], there's no telling how long that chain, or this door, will hold her now that she knows you're here.
			`);
			break;
		}
		case "scarletMemo": {
			writeHTML(`
				t The memo is cut off at the top and bottom, this is all you could find among mostly-intact pieces.
				t <i>"I think it has an incredible number of uses for security, but we'd need to train the employees on it. I hid some of the spade keys this way, but Smith was too dense to notice an askew candlestick. I've hidden some of the favorites among our research recordings, hopefully they're enough of an incentive. They're pretty obvious spots too.</i>
				t <i>The hard part is that it blocks casual observation, you'll need to force yourself to consciously interact with things, even if it seems pointless, otherwise you miss them. Obviously the best use is to hide the entrance in the storage closet, but there are still four left, one's in the West W-</i>
				t [That's where it ends.|scarletEastGuest]
			`);
			break;
		}
		case "gasResearch": {
			writeHTML(`
				t Currently only two types of gaseous chemical are used in the field.
				t DOMINANCE PHEROMONE
				t Gas appears to have nearly no effect on completely infected individuals, only subjects with at least some retained human intelligence are affected. Exposure causes an increase in cruelty or domineering tendencies. Current use is to condition researchers to be less compasionate towards subjects. Careful measurement of employee enthusiasm is required, the effects can take root only after minutes of exposure and affect descision making in subtle ways.
				t Extreme exposure testing is underway, preliminary results suggested that resistant infected would become even more intelligent temporarily, but all current subjects have achieved full mental degradation, thus testing ends inconclusively. High levels of security for subject ------- have been deemed no longer required.
				t Due to researcher preference a variation that does not have a citrus scent is still in development.
				t SUBMISSION PHEROMONE
				t Effects vary as follows between version 3 (current testing strain at this site) and version 4 (next generation ready for testing at other locations).
				t Version 3 Results:
				im
				t Same as uninfected test subjects, infected exposed to gas developed physical nature similar to Siren strain but without the ability to control males. That is to say shrinkage of genitalia and development of submissive and even masochistic tendencies.
				t Gas use is deemed unacceptable in situations where version 3 strain is primary infection type.
				t Status: Further research deemed unnecessary. Strain version 3 research has been discontinued at major test sites.
				t Version 4 Results:
				im
				t Subjects showed a wide range of responses. Cases of aggression and sexual potency up contrast with semi-predictable cases of strain mutation. Multiple subjects began to masturbate uncontrollably until the gas flow was stopped.
				t Status: Inconclusive. A test site has been found for further research at an island resort off the coast of [UNAVAILABLE WITH CURRENT CLEARANCE]. Report to be updated after testing concludes.
			`);
			break;
			}
		case "gasControl": {
			writeHTML(`
				
			`);
			break;
		}
		case "scientistIntro": {
			writeHTML(`
				define scientist = sp Scientist; im none;
				t You stretch a bit and climb into the crawlspace. It's dusty in here, and there's that same scent from before too. It's strange, strong smells usually make you feel dizzy, but for some reason you feel even more alert when you were getting chased by those dogs earlier.
				t You only make it a few feet before you're blocked off. Steel bars block you from entering the bedroom the crawlspace leads to. But they do let you look inside.
				im scientistIntro.gif
				t On a bed is a woman, very nearly naked, masturbating. All she's got are some kind of tank strapped to her back and a mask covering her face, both connected by a tube. The way she's loudly huffing she must be getting off on whatever she's breathing in.
				t Her eyes catch yours, and you move backwards instinctively.
				scientist Wait! Can you understand me!?
				t She seems human, and a lot more interested in talking than playing with herself. She rolls off the bed and moves towards you, seemingly uncaring that she's nude.
				scientist  You're human? Completely?<br>... You're not a member of the science or security teams, that much is obvious. She must be asleep then, otherwise she would have seen you on the security cameras. Listen, help me get out of here before she wakes back up.
				t She seems suspicious. What will you do? You could ask her about [who she is|scientistIdentify], [who 'she' is that might be watching you|scientistQueen], [about the mask she's wearing|scientistMask], or [how to help her|scientistHelp].
			`);
			break;
		}
		case "scientistIdentify": {
			addFlag(scene);
			writeHTML(`
				scientist I'm a scientist here. Our research is... Complicated. What have you seen?<br>... Actually, it doesn't matter. I've seen enough myself. <br>Listen, if you have a shred of humanity left, you know you need to get me out of here. I feel like this scent is fucking up my brain already, but if I get infected...
				t !flag scientistQueen; [Ask about who's keeping her here.|scientistQueen]
				t !flag scientistMask; [Ask about her mask.|scientistMask]
				t !flag scientistHelp; [Ask how to help her.|scientistHelp]
				t [Leave|scientistLeave]
			`);
			break;
		}
		case "scientistQueen": {
			addFlag(scene);
			writeHTML(`
				scientist 'She' is the queen. It's hard to explain. We test things here, on people. One of the things we tested was a strain meant to break fortified lines from the inside, someone who could control other... Infected.<br>It's not important, what is important is that we figure a way out of this mess before the queen wakes back up.
				t !flag scientistIdentify; [Ask about who she is.|scientistIdentify]
				t !flag scientistMask; [Ask about her mask.|scientistMask]
				t !flag scientistHelp; [Ask how to help her.|scientistHelp]
				t [Leave|scientistLeave]
			`);
			break;
		}
		case "scientistMask": {
			addFlag(scene);
			writeHTML(`
				scientist My mask? It's... Complicated. You've seen the women here, right? Well...<br>This mask is set up to turn me into one of them if I get out of line. The hose is hooked up to...<br>Well, it's fragrant, that's all I wanna say about it. I could take it off if I had the tools, so hurry and get me out of here.
				t !flag scientistQueen; [Ask about who's keeping her here.|scientistQueen]
				t !flag scientistIdentify; [Ask about who she is.|scientistIdentify]
				t !flag scientistHelp; [Ask how to help her.|scientistHelp]
				t [Leave|scientistLeave]
			`);
			break;
		}
		case "scientistHelp": {
			addFlag(scene);
			writeHTML(`
				scientist I've got no idea where the key to the room is, it's got a heart symbol on it. I know we keep some backups though, just in case we need to hide or lock doors behind us if there's a containment breach. The queen is smart though, smart enough to use the keys herself. Keep an eye out around the house.<br>There's a... Guard, at the door. She's got a remote somewhere that'll take her out of comission.
				t !flag scientistQueen; [Ask about who's keeping her here.|scientistQueen]
				t !flag scientistIdentify; [Ask about who she is.|scientistIdentify]
				t !flag scientistMask; [Ask about her mask.|scientistMask]
				t [Leave|scientistLeave]
			`);
			break;
		}
		case "scientistLeave": {
			addItem("diamond key");
			writeHTML(`
				scientist Wait! Before you go, I know a way to distract the hounds. If you can find me the key out of here we can escape together.<br> Here, this key will get you into the basement. There are multiple entrances, all hidden. She broke the only one I know during her escape, so you'll need to find your own way in.
				t <b>You got the diamond key!</b>
				t Key in hand, you should [crawl back out and get moving|scarletWardrobe].
			`);
			break;
		}
		case "scientistLetter1": {
			addFlag(scene);
			writeHTML(`
				t <i>"I think it's important to get these thoughts down. I've seen what can happen to people when they start to lose their minds. Even for people like us...</i>
				t <i>We spend all day on these projects, and I know what we all do at night after we finish. I know why we keep so many recordings and why they're available for us all to watch at any time. I can hear them through some of the walls.</i>
				t <i>The order came in, vaccination testing. There're some inoculations, but they're too expensive to have at the ready anywhere except the main labs. The top rungs won't even give us the ingredients. These are the alternative, but who are we testing them on?"</i>
				t ... That's all it says, but the paper is torn suggesting there's more. You should probably [get back to exploring|scarletWardrobe].
			`);
			if (checkFlag("scientistLetter1") == true && checkFlag("scientistLetter2") == true && checkFlag("scientistLetter3") == true && checkFlag("scientistLetter4") == true && checkFlag("scientistLetter5") == true) {
				writeEvent("scientistLetterFull");
				writeText("[That's all that's written.|scarletWardrobe]");
			}
			break;
		}
		case "scientistLetter2": {
			addFlag(scene);
			writeHTML(`
				t <i>"Vaccination testing means they shoot you up and drop you into a room with one of... Them. For hours. Then they pull you out, spray you down, and see if you're still human.</i>
				t <i>We could have used some of the captives, but it's not like they'd patiently comply with our testing or answer a questionaire afterwards. There was some debate on if we should just draw straws... Until I volunteered.</i>
				t <i>I don't know why I did it, it's like my mouth said the words on its own. Carlson looked at me with respect, but the rest of them...</i>
				t <i>Surprise, disgust, jealousy. I think they knew why I was volunteering."</i>
				t ... That's all it says, but the paper is torn suggesting there's more. You should probably [get back to exploring|scarletTheater].
			`);
			if (checkFlag("scientistLetter1") == true && checkFlag("scientistLetter2") == true && checkFlag("scientistLetter3") == true && checkFlag("scientistLetter4") == true && checkFlag("scientistLetter5") == true) {
				writeEvent("scientistLetterFull");
				writeText("[That's all that's written.|scarletTheater]");
			}
			break;
		}
		case "scientistLetter3": {
			addFlag(scene);
			writeHTML(`
				t <i>"The trial was a success. The vaccination hurt like hell, but what came afterwards...</i>
				t <i>It was worth every second.</i>
				t <i>It's so much better than just watching. The captives scream that we're monsters, but if they could sill talk or think after being infected they'd thank us.</i>
				t <i>I was nervous at first. Apparently even though the vaccine would turn me back to normal after a few hours, cumming after being infected would seriously reduce my fertility as a woman. I started having doubts, but...</i>
				t <i>When you have a massive cock hanging between your legs, you realize stuff like that is less important than the feeling of a thick load of cum pumping out of your dick. My fertility? Eggs? They could have said cumming would kill my brain cells and I wouldn't have jerked off less.</i>
				t <i>I passed the tests afterwards, I'm completely human, no more dick. But for three hours I was something better than human. I can't sleep unless I have the recordings playing on loop anymore. I've been supervising a purposefully slow transformation, enjoying watching her break down, but it isn't enough. I'm hungry all the time now.</i>
				t <i>But not for food."</i>
				t ... That's all it says, but the paper is torn suggesting there's more. You should probably [get back to exploring|scarletWestGuest].
			`);
			if (checkFlag("scientistLetter1") == true && checkFlag("scientistLetter2") == true && checkFlag("scientistLetter3") == true && checkFlag("scientistLetter4") == true && checkFlag("scientistLetter5") == true) {
				writeEvent("scientistLetterFull");
				writeText("[That's all that's written.|scarletWestGuest]");
			}
			break;
		}
		case "scientistLetter4": {
			addFlag(scene);
			writeHTML(`
				t <i>"I'm not crazy. I know I'm not crazy.</i>
				t <i>The new one, she's smart. I could see her muttering the code we set for her door after Charlie said it out loud. I know some infected still retain their minds, some even get smarter. But it's temporary. A month, a week, maybe even after a day she'll be like all the rest. I need to hold on.</i>
				t <i>I've been cleared for work again. Thank god I didn't slip up in the medical interview. Every second that goes by I can feel my crotch throb. I just wanna tear off my clothes and jerk off, but I don't have a dick. I'm just... Me.</i>
				t <i>But she knows it's taking up every waking thought. She'll act brainless and stupid when the others are around, but when it's just me and her...</i>
				t <i>I want to cum. I don't give a shit about my womb, about my mind. I don't want to squirt, I want to piss thick semen onto the floor as my balls clench up. I want to give in.
				t <i>I want her."</i>
				t ... That's all it says, but the paper is torn suggesting there's more. You should probably [get back to exploring|scarletEastGuest].
			`);
			if (checkFlag("scientistLetter1") == true && checkFlag("scientistLetter2") == true && checkFlag("scientistLetter3") == true && checkFlag("scientistLetter4") == true && checkFlag("scientistLetter5") == true) {
				writeEvent("scientistLetterFull");
				writeText("[That's all that's written.|scarletEastGuest]");
			}
			break;
		}
		case "scientistLetter5": {
			addFlag(scene);
			writeHTML(`
				t <i>"She's unique. We pump chemicals into the air in the mansion to make sure we're all willing to do what it takes. It makes us more sadistic, more creative. It shouldn't affect her...</i>
				t <i>For her it just seems to make her hungry. More cruel, it makes her want to torment. That's what I see when I look in her eyes.</i>
				t <i>Tests confirmed it, it's some genetic anomaly. Her children or other family could have it too. Even if she wasn't infected the chemicals would probably have the same effect. The others are convinced she's finished, and that her mind has finally faded. I'm the only one watching her now.</i>
				t <i>I keep telling myself this is just for study, but I think I know why I keep such a close watch on her.</i>
				t <i>I think she does too.</i>
				t <i>I broke. She came onto the glass right in front of me and kept stroking herself. Rubbing her fat cock against the glass, I...</i>
				t <i>I started licking the glass. I didn't care that she was watching me, that I was showing weakness. I knew there was a camera on me, but I didn't care. I started stroking my cunt, I'm so fucked up.</i>
				t <i>It's only a matter of time before someone sees the recording. My life is over, isn't it?</i>
				t <i>I know the code to her room. I know I shouldn't, but...</i>
				t <i>If my life is over, why not?"</i>
				t ... That's all it says, but the paper is torn suggesting there are previous parts. You should probably [get back to exploring|scarletSecurity].
			`);
			if (checkFlag("scientistLetter1") == true && checkFlag("scientistLetter2") == true && checkFlag("scientistLetter3") == true && checkFlag("scientistLetter4") == true && checkFlag("scientistLetter5") == true) {
				writeEvent("scientistLetterFull");
				writeText("[That's all that's written.|scarletSecurity]");
			}
			break;
		}
		case "vacineTest": {
			writeHTML(`
				im
				t Just touching the mouse causes the screen to turn on, it seems like a video file is on loop, and has been for more than seventy hours.
				t Titled 'Vaccination Test A', it seems like a woman is allowing herself to be infected. 
				im
				t The footage keeps glitching and skipping, showing different segments. It seems to be multiple hours long, and tracks the entire process in-depth until the vaccine takes effect and the changes are undone.
			`);
		}
		case "scientistEscape": {
			writeEvent(scene);
			addFlag(scene);
			writeHTML(`
				t There's no time to waste, you need to find some way to help her, if you still can. You should [get moving|scarletEastWing].
			`);
			break;
		}
		case "scientistVanish": {
			writeHTML(`
				t As you step into the hallway you think you can hear the sound of a door closing, but you don't know from where.
				t As you look around you realize that the scientist you saved is gone. You were only gone for a moment, and she was in no condition to move, so...
				t You feel a cold chill on the back of your neck. You feel like you're being watched. It was no coincidence her mask activated right after you saved her, or that she vanished so quickly afterwards. You already have someone else to search for, it'd be dangerous to get too attached to finding the woman. You need to [get moving|scarletEastWing].
			`);
			break;
		}
		case "hiddenTape1": {
			addItem("tape 1");
			writeHTML(`
				t Ignoring the rain you correct the lamp, suspecting something is fishy. Your instincts are dead on, and a hidden compartment with a tape inside is revealed!
				t [Take it|scarletBalcony]
			`);
			break;
		}
		case "hiddenTape2": {
			addItem("tape 2");
			writeHTML(`
				t As you turn the lamp it clicks into place, revealing a hidden compartment with a tape inside!
				t [Take it|scarletWestWing]
			`);
			break;
		}
		case "hiddenTape3": {
			addItem("tape 3");
			writeHTML(`
				t As you turn the lamp it clicks into place, revealing a hidden compartment with a tape inside!
				t [Take it|scarletEastWing]
			`);
			break;
		}
		case "hiddenTape4": {
			addItem("tape 4");
			writeHTML(`
				t You jump up to reach the light, causing it to come clean off the wall. At first you think you broke it off, but there just so happens to be a hidden tape attached to the back!
				t [Take it|scarletMainHall]
			`);
			break;
		}
		case "chastityInfected": {
			writeEvent(scene);
			addFlag(scene);
			writeText("No matter how many times you push her, she never seems to be satisfied. The smell is getting too powerful for you now. You have no choice but to set down the device and [leave the room|scarletWestWing] as soon as you can.");
			break;
		}
		case "tubInfected": {
			writeEvent(scene);
			addFlag(scene);
			writeText("[Nothing more you can do.|scarletEastBathroom]");
			break;
		}
		case "gaggedSiren": {
			writeEvent(scene);
			addFlag(scene);
			writeText("Finally winding down, it seems she's finished with her show. It seems the machine is finished as well, and the vibrations come to a stop. You should [finish up|scarletWestBathroom].");
			break;
		}
		case "tapeVaccine": {
			writeEvent(scene);
			addFlag(scene);
			writeText("There's no telling how long this has been playing on repeat. It makes for a decent noise to hide your footsteps though, so it might be best to [leave it on and keep searching the room|scarletWestGuest].");
			break;
		}
		case "urinalInfected": {
			writeEvent(scene);
			addFlag(scene);
			writeText("She seems satisfied, and if you are too you should [leave her be|scarletMainBathroom].");
			break;
		}
		case "garageHounds": {
			writeEvent(scene);
			writeText("With the show over, you should head back to the [mansion hallways|scarletMainHall].");
			break;
		}
		case "scarletDiary": {
			writeEvent("diary");
			writeText("[Close the file|scarletChambers].");
			break;
		}
		case "firstTurn": {
			addFlag(scene);
			writeScene("scarletDining");
			break;
		}
		case "cumtapInfected": {
			writeEvent(scene);
			addFlag("secondTurn");
			writeText("Despite how much she's already cum, her cock is still aching for more and leaking onto the floor. You could [stop here|scarletDining] or [go further|cumtapInfectedRepeat].");
			break;
		}
		case "cumtapInfectedRepeat": {
			writeEvent(scene);
			addFlag("thirdTurn");
			writeText("You're no expert, but you probably just broke the faucet. You should probably [get moving|scarletDining].");
			break;
		}
		case "storageOpen": {
			writeHTML(`
				t You press on the upraised stone, but nothing happens. Pulling, hitting, nothing continues to happen. It isn't until you try twisting the stone that there's some give.
				t After a good twist you can hear something go *click* and jump back as the mirror wall moves backwards, [revealing more of the room|scarletStorage].
			`);
			addFlag(scene);
			break;
		}
		case "dogShock1": {
			addFlag("dogShock1");
			writeEvent(scene);
			writeHTML(`
				t She has some kind of accessory on her now very hard dick, likely connected to your remote. It isn't clear if she's saying [you can go and she won't bother you anymore|scarletEastWing], or if she's [trying to entice you into more|dogShock2].
			`);
			break;
		}
		case "dogShock2": {
			addFlag("dogShock2");
			writeEvent(scene);
			writeHTML(`
				t As you release her from her sexual torment she still twitches a few times, her cock still leaking onto the floor. She looks dazed, but satisfied. [Perhaps you are too|scarletEastWing]?
			`);
			break;
		}
		case "dogShock3": {
			addFlag("dogShock3");
			writeEvent(scene);
			writeHTML(`
				t [If you're finished, you should go.|scarletEastWing]
			`);
			break;
		}
		case "scarletEndingStart": {
			writeEvent(scene);
			writeHTML(`
				t What will it be? [Stay|scarletEndingGood], or [run|scarletEndingBad]?
			`);
			break;
		}
		case "scarletEndingGood": {
			writeEvent(scene);
			writeText("...");
			writeText("You've successfully found the secret of the Scarlet Mansion, congratulations!");
			writeText("Check out the 'Author Information' section in the scenario selection menu for a more detailed set of credits, or play the game again to try and find every nook and cranny still to be spelunked.");
			writeTransition("mansionIntro", "Thanks for playing!");
			break;
		}
		case "scarletEndingBad": {
			writeEvent(scene);
			writeText("...");
			writeText("For better or worse, you've escaped the Scarlet Mansion! What the main character does from here on is an unknown fate.");
			writeText("Check out the 'Author Information' section in the scenario selection menu for a more detailed set of credits, or play the game again to try and find every nook and cranny still to be spelunked.");
			writeTransition("mansionIntro", "Thanks for playing!");
			break;
		}
		case "labCumbreather": {
			writeEvent(scene);
			writeHTML(`
				t [There's nothing you can do for her now|scarletSecurity].
			`);
			break;
		}
		//Typhoid Mary
		case "typhoidMary": {
			document.getElementById('wrapperBG').style.backgroundImage = "url(scripts/gamefiles/locations/typhoidMary.jpg)";
			writeMed("scripts/gamefiles/characters/Typhoid Mary.jpg");
			writeText("Scenario 5 - Typhoid Mary");
			writeText("The final chapter. A full year after the first outbreak began, the world is now an empty place where scavengers struggle to survive and the infected become more desperate than ever for release.");
			writeText("You are a survivor of an infected world who's fallen prey to infection herself. Infect the compound all while hiding your secret from the others until the time is right to strike.");
			data.player.scenario = "Typhoid Mary";
			updateMenu();
			countScenes();
			writeText("You are <input type='text' id='nameSubmission' value='Mary'>");
			writeFunction("renamePlayer()", "Start the Scenario");
			writeTransition("scenarioSelect", "Go back");
			break;
		}
		case "typhoidIntro1": {
			data.items = [];
			data.quicksave = null;
			data.player.flags = "";
			addFlag("pentUp");
			data.player.time = 1;
			writeText("You are a seasoned survivor. Almost a whole year alone, you eventually joined a group of survivors for greater security. Competent, skilled, and most importantly immune to the alluring voice of the Siren-type infected, you're an important friend to your fellow survivors. You try not to get attached, attachment leads to making emotional choices, and those lead to mistakes. You don't make mistakes.");
			writeText("Until today, that is.");
			writeText("It was a routine assignment on a hot day, just scavenge for supplies and head back safely. One of <i>them</i> jumped you from behind, pinning you to the ground and shoving her dick in your face. She must've been particularly starved for a fuck, because she was in a frenzy and leaking more precum than any human could fire in a full load. You tried to scream, that was the second mistake.");
			writeText("Thankfully you were lucky, you pushed her away and she was so on-edge that a few kicks were enough to set her off. While she has a good-old-fashioned ruined orgasm you're spitting and trying to wipe your mouth.");
			writeSpeech("player", "", "It wasn't enough... I'm safe... I gotta be, everything I've worked for, I can't be infected...");
			writeText("That's what you kept telling yourself, going into a full state of denial even as you felt a strong pressure in your abdomen.");
			writeText("Despite how you were sweating like a dog through the makeshift security checkpoint, you made it through without trouble. One of your fellow survivors walks up to you.");
			writeBig("monicaFull.jpg");
			writeSpeech("Monica", "monica", "Heeey! Fucking steaming today, yeah? But we made it, and...<br>Come here, I got something to show you.");
			writeText("Monica used to be the gruff and cynical type, but after you saved her ass, literally, she's since warmed up to you.");
			writeSpeech("player", "", "I can't right now, I'm, uh, gonna head back to my room real quick.");
			writeSpeech("Monica", "monica", "Don't be like that, check this out!");
			writeText("She pulls you aside to show off something she snuck through the checkpoint.");
			writeSpeech("Monica", "monica", "Genuine white chocolate. I mean, It's seriously melted, but this is the real stuff!");
			writeText("Your head is clouding over, it's getting harder to form words as the pressure in your gut builds.");
			writeSpeech("Monica", "monica", "Here, lemme open... Damn, this stuff'll get everywhere if you're not careful.");
			writeText("A small glob of melted chocolate lands on her cheek as she forces the bag open.");
			writeSpeech("player", "", "<i>Ghh... I'm fine, I'm fine, just need to get to my room... I can't look sick in front of everyone...</i><br>That's really cool, but I need to-");
			writeText("Your pussy throbs as you see her scoop the bit of chocolate off her cheek and suck it off her finger. Soon enough she takes a scoop from the melted candy and pushes it into your mouth too.");
			writeSpeech("Monica", "monica", "I know you aren't the type for 'weak stuff' like some of us are, okay? But you gotta relax sometime. It's not like anybody out there is still making candy, you gotta enjoy what little bits of happiness we find along the way.<br>Damn girl, you really like chocolate, don't you?");
			writeText("She misunderstands your actions as you suck and slurp on her index finger. The sugary taste actually helps a bit somehow. The heavy fog on your mind clears just a little and you can think again. You still feel like somebody stuffed ball bearings into your womb, but you'll take what you can get.");
			writeSpeech("player", "", "S-sorry, yeah I really like it. I gotta-");
			writeText("She places the wrapper filled with melted candy in your hands.");
			writeSpeech("Monica", "monica", "I don't owe you anymore, okay? Go enjoy it, and don't tell anybody I gave it to you. You know how doc gets when we sneak in food, even sealed stuff. And go get some rest, you look like one of those infected bitches.");
			writeSpeech("player", "", "Thank you...");
			writeText("You stumble off all the way to the part of the compound your room is located. Luckily nobody is around, they're all celebrating the latest scavenging haul, otherwise they might've commented on how you've got a bowlegged gait.");
			writeText("Just walking is getting harder, the pressure on your abdomen is getting more intense with every step.");
			writeSpeech("player", "", "Not infected... Not infected... Not-");
			writeText("Suddenly it hits you, the pressure reaches its peak and your clit feels engorged. The room spins and the pressure releases by a very muffled *pop*.");
			writeText("Gasping for air like you're a fish out of water, you still manage to barely stumble your way to your door and make it into the [privacy of your room|typhoidIntro2].");
			break;
		}
		case "typhoidIntro2": {
			writeText("You collapse onto your bed and shimmy your now too-tight pants off to expose your tights.");
			writeText("You have a reputation for being a bit of a no-nonsense hard-ass, but you still have at least one reminder of what life used to be like. A pair of partially transparent tights. They're not very practical for your lifestyle, but everyone in the base has at least one weakness. Not to mention it's one more (albeit flimsy) line of defense.");
			writeText("You can see your new cockhead straining against the fabric. Your infection is clear at this point, there's no more denying it, yet all you can think is...");
			writeSpeech("player", "", "Don't rip, don't rip, don't rip...");
			writeText("As your cock engorges you still have your mental faculties for some reason. At least one last hope you can continue to live a normal life as a member of the survivor's compound. Even if it means lying to the people you trust. And the first step is hoping your dick stays a manageable length.");
			writeText("You've heard rumors that some infected cocks can get up to fifteen inches, there's no way you'll pass as a girl with a cock that massive, so your only hope is that you stay small enough to avoid tearing the fabric.");
			writeBig("intro1.gif");
			writeText("But it isn't looking good. The newly grown dick and your freshly birthed set of balls rub against the soft fabric sending signals to your brain it was never meant to deal with and can't make sense of.");
			writeText("The logic centers of your brain are in full panic mode as you clutch the sheets of your bed to try and resist jacking off. Your brain is trying to make excuses, 'stroking it a little won't hurt, you're gonna cum anyway', but you grit your teeth and try to hold back.");
			writeText("Finally, it feels like your cock has stopped growing. The fabric is intact, you can hide the size. The only strange thing is that your body won't relax, every muscle is still ten-");
			writeBig("intro2.gif");
			writeSpeech("player", "", "Ghhg~!");
			writeText("Your cock twitches and splurts, pumping messy, gooey spunk through the tights and onto your bedsheets. Your fat balls twitch and tighten as they pump out what used to be the eggs of your womb, now ravaged by the infection into infectious slime.");
			writeText("Barely holding back tears as the sensation overloads your brain, your body finally relaxes and you feel the sperm soak your tights and pool at your crotch.");
			writeSpeech("player", "", "Ghh... I'm fucking dead...<br>No, it can't get any w-");
			writeText("*KNOCK* *KNOCK*");
			writeText("The darker, hungrier part of your brain governed by your cock and the logical part barely keeping control work in unison for once as you, on autopilot, grab and hide your sperm-covered sheets and anything else you can find that you sprayed on. You grab an old pair of pants and hope it'll be enough to hide the jizz-soaked mess at your crotch.");
			writeSpeech("player", "", "Who-*ahem*<br>What's up?");
			writeText("The door opens and Monica walks through.");
			writeSpeech("Monica", "monica", "Hey, just checki-<br>Damn girl, it stinks like sweat in here. Uh, just checking on you, you were acting a bit weird earlier.");
			writeSpeech("player", "", "Sorry, I'm fine. Totally good-");
			writeText("You try to angle yourself away from her as the jizz begins to darken your pants, hoping she'll leave soon, but it backfires when, after a moment of silence, you turn back to see her holding your soiled sheets.");
			writeSpeech("Monica", "monica", "Fuck, I'm really sorry, I should've told you it was me right away. I can't believe you hid it like this.");
			writeSpeech("player", "", "I... I didn't...");
			writeText("Panic runs through your thought process, a dark instinct is pushing you to <b>take her before it's too late</b>.");
			writeSpeech("Monica", "monica", "Hiding the candy in your bedsheets, bad call. I wonder if it's still good?");
			writeText("She takes a tiny bit onto her finger. You could [stop her|typhoidIntro3a], or you could [remain silent|typhoidIntro3b]");
			break;
		}
		case "typhoidIntro3a": {
			writeEvent(scene);
			writeTransition("typhoidPlayerRoom", "Start");
			break;
		}
		case "typhoidIntro3b": {
			writeEvent(scene);
			addFlag("infectedMonica");
			writeTransition("typhoidPlayerRoom", "Start");
			break;
		}
		case "typhoidDebug": {
			var charactersList = ["Tyler", "Amy", "Lisa", "Angela"];
			writeHTML(`
				t The time block clock is at `+data.player.time+`
				t ?flag pentUp; You are horny
				t !flag pentUp; You are not horny
			`);
			for (debugList = 0; debugList < charactersList.length; debugList++) {
				writeHTML(`
					t !flag infected`+charactersList[debugList]+`; `+charactersList[debugList]+` is not infected.
					t ?flag infected`+charactersList[debugList]+`; `+charactersList[debugList]+` -is- infected.
					t !flag sabotage`+charactersList[debugList]+`; `+charactersList[debugList]+` is not sabotaged.
					t ?flag sabotage`+charactersList[debugList]+`; `+charactersList[debugList]+` -is- sabotaged.
				`);
			}
			writeHTML(`
				t [north|typhoidNorth]
			`);
			break;
		}
		case "typhoidNorth": {
			writeHTML(`
				title You're on the north side of the compound.
				t It's quiet today, the survivors here try not to draw too much attention. The [front gate outside|typhoidGates] is outside.
				t From here, you can head to the [compound's cafeteria|typhoidCafeteria], [medical office|typhoidMedical], or the [strategy room|typhoidStrategy]
				t Aside from that, there's a [general storage room|typhoidStorage] used for various items.
				t You can also head to the [south side of the compound|typhoidSouth], where all the residential spaces are.
			`)
			data.player.lastLocation = scene;
			break;
		}
		case "typhoidCafeteria": {
			writeHTML(`
				title You're in the compound's cafeteria.
				t Across a counter are various bottled and contained drinks to keep survivors hydrated and alert throughout the day. Fluids are the one thing that shouldn't be rationed, after all.
				t !flag sabotageAmy; No one else is here right now. Looking over the various drinks, if you wanted to infect the compound these would make for an excellent, if slow vector. You're likely to be discovered before the end of the day though, so a slow method like this won't be enough to take the entire compound. Will you spend time [sabotaging the drinks?|sabotageAmy]
				t ?flag sabotageAmy; No one else is here right now. That's probably a good thing, there's a faint odor to the place that hopefully most people won't be able to recognize.
				t !flag typhoidWaterPrep; With no one around, you can safely make some [subtly spiked water|typhoidWaterPrep] as a way to gently influence someone not in the mood for a hard drink.
			`);
			if (data.player.time == 4 && checkFlag('infectedAngela') != true) {
				document.getElementById('output').innerHTML = '';
				writeHTML(`
					title You're in the compound's cafeteria.
					t It seems like [Angela|typhoidAngelaChat] is here drinking some water, so you can't do anything in this area to sabotage the compound right now.
					t ?flag sabotageAmy; She doesn't seem to have noticed the sabotage, thankfully.
				`)
			}
			if (data.player.time == 1 && checkFlag('infectedAmy') != true) {
				document.getElementById('output').innerHTML = '';
				writeHTML(`
					title You're in the compound's cafeteria.
					t It seems like [Amy|typhoidAmyChat] is here eating a snack, so you can't do anything in this area to sabotage the compound right now.
				`)
			}
			writeHTML(`
				t If you're finished, you can [head back out to the north end|typhoidNorth].
			`);
			data.player.lastLocation = scene;
			break;
		}
		case "typhoidMedical": {
			writeHTML(`
				title You're in the compound's medical office.
				t A large cabinet houses over a dozen varied medical goods ranging from bandages to oral medications. It's not like you can jerk off into a pill bottle and expect someone not to notice, so there's not much to do here.
				t !item pills; With Lisa away you spy a item[small bottle of sleeping pills|pills] you could nab. They won't help you directly, but who knows when they might be useful.
				t ?item pills; You nabbed the small bottle of sleeping pills
			`);
			if (data.player.time != 5 && checkFlag('infectedLisa') != true) {
				document.getElementById('output').innerHTML = '';
				writeHTML(`
					title You're in the compound's medical office.
					t A large cabinet houses over a dozen varied medical goods ranging from bandages medications. It's not like you can jerk off into a pill bottle and expect someone not to notice, so there's not much to do here unless you have a plan.
					t And even if you do, [Lisa|typhoidLisaChat] is here. You can't rummage around for pills while she's around.
				`)
			}
			writeHTML(`
				t If you're finished, you can [head back out to the north end|typhoidNorth].
			`);
			data.player.lastLocation = scene;
			break;
		}
		case "typhoidStrategy": {
			writeHTML(`
				title You're in the compound's strategy room.
				t This is where you and your fellow survivors get briefings before you head out on missions.
				t A large table surrounded by old, ratty chairs has a large map of the city, as well as numerous notes on supplies and records of past scavenging missions. While they're important documents, they won't help you infect the compound.
			`);
			if (data.player.time == 1 || data.player.time == 4) {
				if (checkFlag('infectedTyler') != true) {
					writeHTML(`
						t [Tyler|typhoidTylerChat] is here.
					`)
				}
				
			}
			if (data.player.time != 1) {
				writeHTML(`
					t [Keith|typhoidKeithChat] is here.
				`)
			}
			writeHTML(`
				t If you're finished, you can [head back out to the north end|typhoidNorth].
			`);
			data.player.lastLocation = scene;
			break;
		}
		case "typhoidStorage": {
			writeHTML(`
				title You're in the compound's storage room.
				t It's stuffed almost to the brim with useful supplies, many of which you brought in yourself. Food and medical supplies are taken elsewhere, so this room is for everything that doesn't have a designated home.
				t !flag sabotageLisa; Lining the walls are numerous weapons, all for self-defense. It'd be cruel to see these used on your sisters. You could [take some time to sabotage them|sabotageLisa], although that wouldn't directly help you infect everyone.
				t ?flag sabotageLisa; Lining the walls are numerous weapons, each subtly sabotaged in a different way from parts missing, damaged, or the weapons are otherwise just hidden away out of sight.
				t There's a massive crate tucked farther in the back, contained inside is a necessity in this sort of apocalypse to prevent going stir-crazy, but not the kind of thing survivors like to talk about. Inside is a [sizeable cache of sex toys|typhoidCrate].
			`);
			writeHTML(`
				t If you're finished, you can [head back out to the north end|typhoidNorth].
			`);
			data.player.lastLocation = scene;
			break;
		}
		case "typhoidCrate": {
			writeHTML(`
				title You peer into the large storage crate.
				t Peering into the box you can see toys of varying shapes and sizes that make your heart throb, but you can't let yourself get too distracted from your mission. You need to calm yourself down before the strategy meeting, so only the most useful ones catch your eye.
				t !flag caged; There's a pair of chastity cages you could wear to hide your cock. One's [made of a flimsy plastic|cagedWeak], the other is [a stronger metal one, but it's very small|cagedStrong] and the paired key is rusted.
				t ?flag caged; You're already wearing a chastity cage. Hopefully it'll be enough to keep your cock hidden.
				t !flag caged; ?flag pentUp; There's an extremely attractive-looking [sealed fleshlight|typhoidFleshlight], a primal part of your mind seems very interested in it.
				t !flag caged; ?flag pentUp; On the opposite end is a [suction-cup dildo|typhoidDildo]. A more familiar tool, but one you haven't tried since your body last changed.
				t ?flag caged; ?flag pentUp; There are a few attractive-looking sex toys in here, but you can't use them while caged. 
				t !flag pentUp; There are a few attractive-looking sex toys in here, but thankfully you aren't horny at this exact second. 
			`);
			writeHTML(`
				t Nothing else stands out, it might be best to [go back|typhoidStorage].
			`);
			data.player.lastLocation = scene;
			break;
		}
		case "typhoidGates": {
			writeHTML(`
				title You're outside the compound, at the front entrance.
				t Thick iron bars and a large stone wall separate the compound from the rest of the city, and all the infected.
				t ... <i>Most</i> of the infected.
				t However secrecy is the most important line of defense. If the infected knew of this place they could probably scale the walls, or even just stand outside the gates jerking off until the survivors went crazy from the smell.
				t !flag sabotageTyler; But the <i>first</i> line of defense, the [thick lock on the gate, to which all scouts have the key|sabotageTyler], that's what you have your sights on. right now. If you open the gate and spend a good amount of time calling the horde towards you, the other survivors will have far less time to prepare and the defenses will be weakened.
				t ?flag sabotageTyler; But with the <i>first</i> line of defense sabotaged, the horde will have a much easier time breaching the compound when the time finally comes. Hopefully it was worth the time.
			`);
			if (data.player.time > 3) {
				if (checkFlag('infectedAmy') != true && checkFlag('sleepingAmy') != true){
					document.getElementById('output').innerHTML = '';
					writeHTML(`
						title You're outside the compound, at the front entrance.
						t Thick iron bars and a large stone wall separate the compound from the rest of the city, and all the infected.
						t ... <i>Most</i> of the infected.
						t It seems like [Amy|typhoidAmyChat] is here standing guard, so you can't do anything in this area to sabotage the compound right now.
					`)
				}
			}
			var sabotageTotal = 0;
			if (checkFlag("sabotageTyler") == true) {
				sabotageTotal += 1;
			}
			if (checkFlag("sabotageAmy") == true) {
				sabotageTotal += 1;
			}
			if (checkFlag("sabotageLisa") == true) {
				sabotageTotal += 1;
			}
			if (checkFlag("sabotageAngela") == true) {
				sabotageTotal += 1;
			}
			switch (sabotageTotal) {
				case 0:
					writeHTML(`t <b>You haven't sabotaged any of the compound's defenses yet, you can either focus on weakening the defenses to make the horde's job easier, or infect the other survivors yourself.</b>`);
				break;
				case 1:
					writeHTML(`t <b>You've sabotaged one of the compound's defenses in preparation for the coming assault, but you're nowhere close to making sure the place is overrun with no hope of escape.</b>`);
				break;
				case 2:
					writeHTML(`t <b>You'd say you're about halfway to sabotaging the compound's defenses!</b>`);
				break;
				case 3:
					writeHTML(`t <b>Nearly all of the compound's defenses have been sabotaged! Soon, the horde will be able to infect everyone here in one fell swoop!</b>`);
				break;
				default: {
					writeHTML(`t <b>You've sabotaged everything you should need to call the horde!</b>`);
				}
			}
			writeHTML(`
				t If you're finished, you can [head back into the compound|typhoidNorth].
			`);
			data.player.lastLocation = scene;
			break;
		}
		case "typhoidSouth": {
			writeHTML(`
				title You're at the southern end of the compound.
				t Most of the personal rooms are here, all delightfully framed by drab, gray walls. Hey, at least it's home.
				t From here you can head into [Tyler's room|typhoidTylerRoom], [Amy's room|typhoidAmyRoom], [Lisa's room|typhoidLisaRoom], [Angela's room|typhoidAngelaRoom], or [Keith's room|typhoidKeithRoom].
				t ?flag infectedMonica; Monica's room is here too, but there's no reason to enter, she's in [your room|typhoidPlayerRoom].
				t !flag infectedMonica; Monica's room is here too, but there's no reason to enter, she's out on a mission. Aside from that is [your empty room|typhoidPlayerRoom].
				t Aside from that, there's not much to do on this side of the compound, aside from a [small back door leading to the rear fence|typhoidBack], most of your time outside your room is spent on the [north end of the compound|typhoidNorth].
			`)
			data.player.lastLocation = scene;
			break;
		}
		case "typhoidTylerRoom": {
			writeHTML(`
				title You're in Tyler's room.
				t It belongs to the subby little pushover always making bedroom eyes at Lisa. It's pretty sparse in here, he keeps a pretty clean room.
			`);
			if (data.player.time == 2 || data.player.time == 5) {
				if (checkFlag('infectedTyler') != true) {
					writeHTML(`
						t [Tyler|typhoidTylerChat] is here, laying on his bed waving his feet through the air as he reads some sort of magazine.
					`)
				}
			}
			if (checkFlag("waitingTyler") == true) {
				if (checkFlag('infectedTyler') != true) {
					document.getElementById('output').innerHTML = '';
					writeHTML(`
						title You're in front Tyler's room.
						t He's currently waiting for Lisa for the surprise you mentioned, he probably won't be letting you in.
						t ?flag infectedLisa; It'd be rude to keep him waiting, why not [send Lisa in now?|infectedTyler]
					`)
				}
			}
			if (checkFlag('infectedTyler') == true) {
				document.getElementById('output').innerHTML = '';
				writeHTML(`
					title You're in Tyler's room.
					t It belongs to the subby little pushover [Tyler|inspectTyler]. It's pretty sparse in here, he keeps a pretty clean room. Or at least it was before [his girlfriend|inspectLisa] raped him into becoming an infected sissy slut, now it's a mess as she uses whatever she wants as a sweatrag, and uses him as a cumrag.
					t Tyler and Lisa are infected, now for the rest.
				`)
			}
			writeHTML(`
				t If you're finished, you can [head back out of the room|typhoidSouth].
			`);
			data.player.lastLocation = scene;
			break;
		}
		case "typhoidKeithRoom": {
			writeHTML(`
				title You're in Keith's room.
				t It belongs to your fearless leader. The room is kind of a mess, weapons kept in arms reach even when sleeping, maps of the city, notes jotted down on supplies.
				t !item wireCutters; Notably, there's a item[sharp pair of wire cutters|wireCutters] here, the only things that really stand out as useful to you.
				t ?item wireCutters; You grabbed the wire cutters. You don't really need a weapon, but they might come in handy somewhere.
			`);
			if (data.player.time == 1) {
				document.getElementById('output').innerHTML = '';
				writeHTML(`
					title You're in Keith's room.
					t It belongs to your fearless leader. The room is kind of a mess, weapons kept in arms reach even when sleeping, maps of the city, notes jotted down on supplies.
					t [Keith|typhoidKeithChat] is here resting right now, this is not the best time to snoop.
				`)
			}
			writeHTML(`
				t You don't have forever to linger here. You can [head back out of the room|typhoidSouth].
			`);
			data.player.lastLocation = scene;
			break;
		}
		case "typhoidAmyRoom": {
			//t Oh, and how scandalous, it looks like [Amy left her journal on her bed|typhoidJournal].
			writeHTML(`
				title You're in Amy's room.
				t It belongs to the asocial girl who's just a little <i>too</i> interested in zombie behavior. Everything in the room has a home, and it seems like that home is the floor, the room's a mess!
				
			`);
			if (data.player.time == 2 && checkFlag('infectedAmy') != true) {
				document.getElementById('output').innerHTML = '';
				writeHTML(`
					title You're in Amy's room.
					t It belongs to the asocial girl who's just a little <i>too</i> interested in zombie behavior. Everything in the room has a home, and it seems like that home is the floor, the room's a mess!
					t [Amy|typhoidAmyChat] is here right now, it looks like she's reading something.
				`)
			}
			if (checkFlag('sleepingAmy') == true) {
				document.getElementById('output').innerHTML = '';
				writeHTML(`
					title You're in Amy's room.
					t It belongs to the asocial girl who's just a little <i>too</i> interested in zombie behavior. Everything in the room has a home, and it seems like that home is the floor, the room's a mess!
					t [Amy|infectedAmy] is here, completely unconcious as a result of the pills you gave her.
				`)
			}
			if (checkFlag('infectedAmy') == true) {
				document.getElementById('output').innerHTML = '';
				writeHTML(`
					title You're in Amy's room.
					t It belongs to the asocial girl who's just a little <i>too</i> interested in zombie behavior. Everything in the room has a home, and it seems like that home is the floor, the room's a mess!
					t And it's even more of a mess now that [its owner|inspectAmy] has become the infected she secretly dreamed of becoming.
				`)
			}
			writeHTML(`
				t You don't have forever to linger here. You can [head back out of the room|typhoidSouth].
			`);
			data.player.lastLocation = scene;
			break;
		}
		case "typhoidLisaRoom": {
			writeHTML(`
				title You're in Lisa's room.
				t It belongs to your very domineering friend. The room's tightly organized, you can tell she'd be ready to abandon the compound in less than a few minutes if she needed to.
				t !flag typhoidSexToys; While she's made it clear your life ends the minute she finds you [snooping around|typhoidSexToys], she isn't around right now, is she?
				t ?flag typhoidSexToys; You already found her set of strap-on toys, and you have a really good idea exactly which twink they belong to.
			`);
			if (data.player.time == 5 && checkFlag('infectedLisa') != true) {
				document.getElementById('output').innerHTML = '';
				writeHTML(`
					title You're in Lisa's room.
					t It belongs to your very domineering friend. The room's tightly organized, you can tell she'd be ready to abandon the compound in less than a few minutes if she needed to.
					t [Lisa|typhoidLisaChat] is here right now, it looks like she's nursing her migraine.
				`)
			}
			if (checkFlag('infectedLisa') == true) {
				document.getElementById('output').innerHTML = '';
				writeHTML(`
					title You're in Lisa's room.
					t It belongs to your [once very domineering friend|inspectLisa]. The room's tightly organized, you can tell she'd be ready to abandon the compound in less than a few minutes if she needed to.
					t If she weren't furiously jerking off right now, that is. She can barely control herself~!
					t !flag typhoidSexToys; Since she's indisposed at the moment [snooping around|typhoidSexToys] won't put you at any risk. Aside from being in her splash zone of course.
					t ?flag typhoidSexToys; You already found her set of strap-on toys, and you have a really good idea exactly which twink they belong to.
				`)
			}
			if (checkFlag('infectedTyler') == true) {
				document.getElementById('output').innerHTML = '';
				writeHTML(`
					title You're in Lisa's room.
					t It used to belong to your very domineering friend. You get the impression that she's content to life with her new girlfriend forever.
				`)
			}
			writeHTML(`
				t You don't have forever to linger here. You can [head back out of the room|typhoidSouth].
			`);
			data.player.lastLocation = scene;
			break;
		}
		case "typhoidAngelaRoom": {
			writeHTML(`
				title You're in Angela's room.
				t It belongs to the unofficial team mom. The room's cluttered, but the clutter makes it feel more homely. Stuffed animals. A well-loved potted plant. Framed pictures of life before all... <i>This</i>.
				t !flag typhoidFoundCanine; You could [search it a bit more thoroughly if you wanted|typhoidFoundCanine].
				t ?flag typhoidFoundCanine; Having found Angela's personal toy and her special additions, you filled it with a special booby trap and now [just need to wait|infectedAngela].
			`);
			if (data.player.time == 5 || data.player.time == 2) {
				if (checkFlag('infectedAngela') != true) {
					document.getElementById('output').innerHTML = '';
					writeHTML(`
						title You're in Angela's room.
						t It belongs to the unofficial team mom. The room's cluttered, but the clutter makes it feel more homely. Stuffed animals. A well-loved potted plant. Framed pictures of life before all... <i>This</i>.
						t [Angela|typhoidAngelaChat] is probably in here somewhere amidst all the stuff.
					`)
				}
			}
			if (checkFlag('infectedAngela') == true) {
				document.getElementById('output').innerHTML = '';
				writeHTML(`
					title You're in Angela's room.
					t It belongs to the unofficial team mom. The room's cluttered, but the clutter makes it feel more homely. Stuffed animals. A well-loved potted plant. Framed pictures of life before all... <i>This</i>.
					t And as if to remind of what life is like <i>now</i>, the [aforementioned team mom|inspectAngela] is gently licking at her knotted dildo, uncaring that she looks like a shameless whore fantasizing about doggy-cock.
				`)
			}
			writeHTML(`
				t You don't have forever to linger here. You can [head back out of the room|typhoidSouth].
			`);
			data.player.lastLocation = scene;
			break;
		}
		case "typhoidPlayerRoom": {
			writeHTML(`
				title You're in your room. 
				t ?flag infectedMonica; Aside from your [mirror on the wall though|inspectPlayer] and your [new roommate|inspectMonica], there's not much to see in here. You keep things quite tidy! 
				t !flag infectedMonica; There's not much to see in here. You keep things clean. There is a [mirror on the wall though|inspectPlayer]. You can only wonder how monika would react if she saw you now...
				t ?flag infectedTyler; You've infected Tyler.
				t ?flag infectedAmy; You've infected Amy.
				t ?flag infectedLisa; You've infected Lisa.
				t ?flag infectedAngela; You've infected Angela.
			`);
			var sabotageTotal = 0;
			if (checkFlag("sabotageTyler") == true) {
				sabotageTotal += 1;
			}
			if (checkFlag("sabotageAmy") == true) {
				sabotageTotal += 1;
			}
			if (checkFlag("sabotageLisa") == true) {
				sabotageTotal += 1;
			}
			if (checkFlag("sabotageAngela") == true) {
				sabotageTotal += 1;
			}
			switch (sabotageTotal) {
				case 0:
					writeHTML(`t <b>You haven't sabotaged any of the compound's defenses yet, you can either focus on weakening the defenses to make the horde's job easier, or infect the other survivors yourself.</b>`);
				break;
				case 1:
					writeHTML(`t <b>You've sabotaged one of the compound's defenses in preparation for the coming assault, but you're nowhere close to making sure the place is overrun with no hope of escape.</b>`);
				break;
				case 2:
					writeHTML(`t <b>You'd say you're about halfway to sabotaging the compound's defenses!</b>`);
				break;
				case 3:
					writeHTML(`t <b>Nearly all of the compound's defenses have been sabotaged! Soon, the horde will be able to infect everyone here in one fell swoop!</b>`);
				break;
				default: {
					writeHTML(`t <b>You've sabotaged everything you should need to call the horde!</b>`);
				}
			}
			switch (data.player.time) {
				case 1:
					writeHTML(`t <b>You still have a good amount of time before the strategy meeting you need to attend later today.</b>`);
				break;
				case 2:
					writeHTML(`t <b>The strategy meeting will be soon, but you have enough time to do something in the meanwhile.</b>`);
				break;
				case 3:
					writeHTML(`t <b>With the strategy meeting over, you aren't too pressed for time right now. Still, you can feel the infection inside yourself, your heart is racing. You're finishing this before nightfall.</b>`);
				break;
				case 3:
					writeHTML(`t <b>Nightfall approaches, and so does the horde, you can tell you're subconciously drawing them in. You don't have much time left, finish your final business and let this place burn.</b>`);
				break;
			}
			writeHTML(`
				t As entertaining as your little pet is, you should probably [head back out of the room|typhoidSouth].
			`);
			data.player.lastLocation = scene;
			break;
		}
		case "typhoidBack": {
			writeHTML(`
				title You're on the south end of the compound.
				t Nothing much is here, the fence surrounding the compound is covered up with tarps where it isn't blocked by bricks or other whatsits.
				t !item wireCutters; All aside from a small patch of uncovered fencing. Even with your new body though, you aren't tearing through that without hurting your precious hands.
				t ?item wireCutters; !flag sabotageAngela; All aside from a small patch of uncovered fencing. With Keith's wirecutters you could [cut through a section of the fence and hide it with tarp|sabotageAngela]. The process would take a while but it would lower the compound's defenses.
				t ?flag sabotageAngela; And beneath a certain section of tarp is the fence you cut a hole into.
			`);
			if (data.player.time == 1) {
				if (checkFlag('infectedAngela') != true) {
					document.getElementById('output').innerHTML = '';
					writeHTML(`
						title You're on the south end of the compound.
						t Nothing much is here, the fence surrounding the compound is covered up with tarps where it isn't blocked by bricks or other whatsits.
						t [Angela|typhoidAngelaChat] is here, although she doesn't have any reason to be. While she's around you can't sabotage the compound's defenses.
					`)
				}
			}
			writeHTML(`
				t If you're finished, you can [head back into the compound|typhoidSouth].
			`);
			data.player.lastLocation = scene;
			break;
		}
		case "typhoidTylerChat": {
			switch(data.player.time) {
				case 1:
					writeHTML(`
						im tyler1.jpg;
						t Tyler, the general helper around the compound, he's usually the one helping out Keith with minor chores.
						tyler Oh, hey! Glad you're back from the mission. Every time you girls go out there, I... W-well, I don't wanna jinx anything. I'm just prepping the stuff Keith needs for the meeting.
						player It's good to be back.
					`);
				break;
				case 2:
					writeHTML(`
						tyler Eh? playerF? Could I maybe get some privacy?
						player Right, should have knocked.
					`);
				break;
				case 4:
					writeHTML(`
						tyler Hey playerF. I think the meeting went smoothly! You were acting a bit reserved, you okay? What am I saying, you're practically the glue that holds us together! Honestly, if you didn't seem to hate responsibility so much, you'd have a good shot at taking the spot of leader from Keith.
						player What about Lisa?
						tyler Heh... She seems tough at first, but I know she's a real softy.
					`);
				break;
				case 5:
					writeHTML(`
						tyler Eh? playerF? Could I maybe get some privacy?
						player Right, should have knocked.
					`);
				break;
				default: {
					writeHTML(`
						t ERROR, DEFAULT TIME CASE CALLED
					`);
				}
			}
			if (data.player.time > 3) {
				writeHTML(`
					t ?flag typhoidSexToys; Knowing what Lisa has in her room, you could [put a brilliant plan into action|typhoidTylerSpecial].
				`);
			}
			writeHTML(`
				t [Finish chatting|`+data.player.lastLocation+`].
			`);
			break;
		}
		case "typhoidTylerSpecial": {
			writeHTML(`
				player Hey, so, I was talking to Lisa earlier.
				tyler Oh? Did she mention me?
				player Yeah actually. She said something about a surprise later today.
				tyler ...!
				player She said to wait with a blindfold on. Something going on between you two?
				tyler I'm sure it's just her joking around. Thanks for the message.
				t You step out of the room and close it behind you, and you can hear Tyler start to hum something and the sound of drawers opening.
				t [Finish chatting|typhoidTylerRoom].
			`);
			addFlag("waitingTyler");
			break;
		}
		case "typhoidAmyChat": {
			switch(data.player.time) {
				case 1:
					writeHTML(`
						im amy.jpg;
						t Amy, the youngest around the compound. Always upbeat, she's usually the first to volunteer for scavenging and scouting missions. In the past it bothered you how comfortable she was with the idea of the infected, she almost seems to be alright with how the world's turned out at times.
						amy Mmm. I swear, the moment the world runs out of packaged cookies, I'm throwing myself out there in the city with a target painted on my cunt. Life's only downhill once I can't eat any more of these.
						player Always with the dark jokes...
						amy Eh. You guys are so scared of it, but honestly, being infected would only <i>kinda</i> suck. Sure I'd be mindless, but at least I'd have fun.
						player You sound ready to head out there anytime.
						amy Hah! And miss out on snacks like these? No way, sis.
					`);
				break;
				case 2:
					writeHTML(`
						amy Whoa, privacy?!
						t Amy quickly hides a book beneath her blanket.
						player Sorry.
					`);
				break;
				case 4:
					writeHTML(`
						t Amy lets out a long, drawn-out yawn as you approach. Clearly exaggerated, she'd never actually fall asleep on the job.
						amy What can I do for you?
					`);
				break;
				case 5:
					writeHTML(`
						t Amy lets out a long, drawn-out yawn as you approach. Clearly exaggerated, she'd never actually fall asleep on the job.
						amy What can I do for you?
					`);
				break;
				default: {
					writeHTML(`
						t ERROR, DEFAULT TIME CASE CALLED
					`);
				}
			}
			if (data.player.time > 3) {
				writeHTML(`
					t ?item pills; She seems tired. You could [hand Amy the sleeping pills, and lie about what they are.|typhoidAmySpecial]
				`);
			}
			writeHTML(`
				t [Finish chatting|`+data.player.lastLocation+`].
			`);
			break;
		}
		case "typhoidAmySpecial": {
			writeHTML(`
				player Tired?
				amy How could you tell?
				player Well, luckily I snagged some caffeine pills on the last outing. I could maybe spare-
				amy Hell yeah! Gimme gimme, I swear if I hear Lisa chew me out one more time about dozing off on the watch, I'll cunt punt her!
				player I think she'd probably break your foot before you finish swinging it.
				amy Heh. Yeah, probably. 
				t You hand Amy a few pills and she takes them without hesitation.
				amy Mmm, thanks! I can already feel them working.
				player No problem. Mind if I keep you company for a bit?
				amy Wow, you are just the nicest today. Where's this goody-two-shoes act been all my life?
				t ...
				t You spend some time chatting with Amy, but it doesn't take long for the pills to start kicking in.
				amy Mmm... I think those pills... Expired or something...
				player Maybe you should stop spending the night masturbating and start getting to sleep on time.
				amy Whaaa... Damnit, I knew the walls... Were too thin...
				player Here, lemme take you back to your room, I'll handle the rest of your shift.
				amy Hehe, you're a real pal playerF... 
				t You escort Amy back to her room, it should only be a little while until she's totally out.
				t [Close the door behind her|typhoidSouth].
			`);
			addFlag("sleepingAmy");
			break;
		}
		case "typhoidLisaChat": {
			switch(data.player.time) {
				case 1:
					writeHTML(`
						im lisa.jpg;
						t Lisa, the most rough-and-tumble around here, attitude-wise at least..
						lisa You need something? I'm just organizing the meds.
						player Got any-
						lisa Stop. If you ask me for hallucinogens again, I will <i>break</i> you. My head hurts, but I can't find the painkillers.
						player Point taken... Can I get you a drink?
						lisa Sure. Just water though.
					`);
				break;
				case 2:
					writeHTML(`
						lisa Hello playerF. You need something? I'm just organizing the meds.
						player Got any-
						lisa Stop. If you ask me for hallucinogens again, I will <i>break</i> you. My head hurts, but I can't find the painkillers.
						player Point taken... Can I get you a drink?
						lisa Sure. Just water though.
					`);
				break;
				case 4:
					writeHTML(`
						lisa Hello playerF. You need something? I'm just organizing the meds.
						player Got any-
						lisa Stop. If you ask me for hallucinogens again, I will <i>break</i> you. My head hurts, but I can't find the painkillers.
						player Point taken... Can I get you a drink?
						lisa Sure. Just water though.
					`);
				break;
				case 5:
					writeHTML(`
						lisa Ugh... My head... playerF, get me some water to drink...
						player You sure?
						lisa If I drink something caffinated or sugary right now, I'll hurl...
					`);
				break;
				default: {
					writeHTML(`
						t ERROR, DEFAULT TIME CASE CALLED
					`);
				}
			}
			if (checkFlag("typhoidWaterPrep") == true) {
				writeHTML(`
					t You could [give Lisa the spiked water|infectedLisa].
				`);
			}
			writeHTML(`
				t [Finish chatting|`+data.player.lastLocation+`].
			`);
			break;
		}
		case "typhoidAngelaChat": {
			switch(data.player.time) {
				case 1:
					writeHTML(`
						im angela.jpg;
						t Angela, the team mom. She's been kind and compassionate for as long as you've known her, it seems like she misses her old life the most of any of you.
						angela  Oh, hello playerF! I was just...
						t She stands awkwardly, you notice a bowl of what seems to be kibble behind her.
						player Feeding the strays? You know-
						angela I know, they aren't quite 'dogs' anymore, but... Imagining them going hungry... Please don't tell-
						player Don't worry, your secret's safe with me.
						angela Oh, thank you playerF!
					`);
				break;
				case 2:
					writeHTML(`
						angela Hmm~ It's the little things in life, the relaxing moments, those are what make all that rummaging through the city worth it.
					`);
				break;
				case 4:
					writeHTML(`
						angela Hmm? playerF, did you need something?
						player Not really. You keeping busy?
						angela Yep! Just resting. It clears my head, it's hard to feel free when fenced up like this.
						player I get what you mean.
					`);
				break;
				case 5:
					writeHTML(`
						angela Hmm? playerF, did you need something?
					`);
				break;
				default: {
					writeHTML(`
						t ERROR, DEFAULT TIME CASE CALLED
					`);
				}
			}
			writeHTML(`
				t [Finish chatting|`+data.player.lastLocation+`].
			`);
			if (data.player.time == 2 || data.player.time == 5) {
				if (checkFlag("typhoidFoundCanine") == true) {
					sceneTransition("infectedAngela");
				}
			}
			break;
		}
		case "typhoidKeithChat": {
			switch(data.player.time) {
				case 1:
					writeHTML(`
						im keith.jpg;
						t Keith, the team leader. He plans the missions, he preps the schedule. Sometimes you wonder if he feels cooped up or useless at times due to not being allowed to go out on missions like you, and that's why he focuses so much on keeping things tidy and organized around here.
						keith playerF, you should stop entering people's rooms without knocking.
						player You got it, boss.
						keith I'd appreciate it if just once you could call me that without sounding sarcastic.
					`);
				break;
				case 2:
					writeHTML(`
						keith playerF, the meeting isn't for a while. You're never early, so I assume you lost track of time.
						player Heh, you hit the nail on the head, boss.
						keith Always with the sarcasm.
					`);
				break;
				case 4:
					writeHTML(`
						keith playerF, hurry up and get some rest. You were barely on Earth during the meeting, let alone in the strategy room with us.
						player Sure thing, dad.
						keith You're healthy enough to be sarcastic, at least.
					`);
				break;
				case 5:
					writeHTML(`
						keith playerF, you feeling any better?
						player I am, boss. How are you?
						keith No sarcasm for me this time? The world must be about to end.
						player Sure does feel like it.
					`);
				break;
				default: {
					writeHTML(`
						t ERROR, DEFAULT TIME CASE CALLED
					`);
				}
			}
			writeHTML(`
				t [Finish chatting|`+data.player.lastLocation+`].
			`);
			break;
		}
		case "inspectPlayer": {
			writeHTML(`
				t You take a long look at yourself in the mirror. You look... Good. Most of your physical imperfections, blemishes, are gone. But somehow it's subtle. You look more refined if anything.
				t And as you stare longer, information begins to fill your mind.
				t <b>Queen Strain, No Deviation.</b>
				t A non-deviated strain designed by the authors of the Great Strain, meant to overpower intelligent and careful pockets of resistance through organized horde assaults.
				t + Dominance: Allows the Queen to control and command lesser servants, even overpowering natural urges of sexual hunger.
				t + Instinctive Knowledge: Allows the Queen to instinctually understand the type, deviation, strengths, and weaknesses of other infected.
				t + Intelligence (Abnormal). Fault in the strain detected. This trait should not be present, chemical agent has likely caused improper infection of the mind. Over time, this trait should fade.
				t ...
				t You come back to your senses on your bed.
				player So, it's real, I'm special, somehow.<br>And temporarily intelligent? Looks like I'm on the clock then. Maybe I'll find more of whatever chemical agent's keeping my mind intact.
				t [Inspection Complete|`+data.player.lastLocation+`]
			`);
			break;
		}
		case "inspectTyler": {
			writeHTML(`
				t You watch as Tyler is mercilessly used by his lover, and information begins to fill your mind.
				t <b>Siren Strain, Deviation Beta; Lesser Nymph</b>
				t A deviation achieved rarely when a male considered "worthless" for the purpose of spreading the infection is infected willingly through rough anal sex. Considered a failure by the authors, who are attempting to remove the deviation from the Great Strain. This servant does not possess many of the typical siren traits, including the ability to sing.
				t + This servant has no significant traits, only an increased tolerance for pain. This strain would best be used as a sexual relief tool for more dominant virile strains such as a bull or stud.
				t [Inspection Complete|`+data.player.lastLocation+`]
			`);
			break;
		}
		case "inspectAmy": {
			if (data.player.wsDisabled == true) {
				writeHTML(`
					t You stare at Amy's sleeping, cum-soaked, and very content form, and information begins to flood your mind.
					t <b>Soldier Strain, Deviation Gamma; Trap-Setter</b>
					t A deviation of the most common outcome of the Great Strain which can occur from any infection at a low frequency. This servant has a gleeful fascination with covering surfaces with infected semen preferring to disguise it as food above all else. This servant would best be used for long-term cleanup of already infected areas, infecting survivors through the tainting of scavenged goods.
					t + Active Imagination: This results in constant mental stimulation in the form of thoughts of humans unknowingly swallowing infected jizz and becoming addicted. As a result she will near constantly be leaking searching anything not already coated in sperm they can to orgasm onto.
					t + Masturbation Addiction: The near constant urge to stroke their dick to the point where they would compulsively jerk off in front of a survivor rather than attack them. This is likely due to their extremely high penis sensitivity which allows them to constantly spread jizz, suggesting they would experience total mental breakage if stimulated effectively via actual sex. As a result if she were to actually manage to insert her member inside of a human woman's pussy, she would likely begin to orgasm from the moment of insertion and would uncontrollably thrust leading to a complete bloat of the human woman's womb before the prey even has time to be transformed.
					t [Inspection Complete|`+data.player.lastLocation+`]
				`);
			}
			else {
				writeHTML(`
					t You stare at Amy's sleeping, cum-soaked, and very content form, and information begins to flood your mind.
					t <b>Whizzer Strain, No Deviation.</b>
					t A non deviated strain which results from a low chance from any infection. This servant has a gleeful fascination with wantonly pissing wherever she pleases preferring to disguise it as drinkable fluid above all else. This servant would best be used for long-term cleanup of already infected areas, infecting survivors through the tainting of water and fluid supplies.
					t + Semen to Piss Conversion: A complete cease of sperm cell production as the body's resources become devoted towards producing infective urine. As a byproduct she achieves an orgasmic state upon releasing or losing control of her bladder, and should not be used for direct assaults due to human chases potentially being interrupted by a sudden need to piss right on the spot even as the human escapes.
					t + Pheromones (Abnormally Enhanced): Causes male humans to irrationally desire to drink her piss to the point of complete self delusion. A man could watch this servant piss a frothing puddle directly onto the ground, and the human would feel the overwhelming <i>need</i> to immediately lap up her piss and beg her to grab them by the hair and piss down their throat. Doing so would mean a guaranteed infection, as this strain is capable of pissing what can seem like a gallon at a time without remorse, leaving any human a bloated, piss-soaked mess before the infection settles in.
					t [Inspection Complete|`+data.player.lastLocation+`]
				`);
			}
			break;
		}
		case "inspectLisa": {
			writeHTML(`
				t As you stare at Lisa she stares back at you, with <i>almost</i> a trace of defiance in her eyes. As you stare, information floods into your mind.
				t <b>Bull Strain, No Deviation.</b>
				t A non-deviated strain that results from infecting an individual with a high degree of dominance and sadism. This servant is best used against larger, tougher individuals to overpower them. This servant has a low obedience score, it is recommended to regularly reward this servant for good behavior and use them to enforce discipline and loyalty among other strains.
				t + Rape Addiction: This servant strongly prefers to handle its partners as roughly and forcefully as their body will allow. Improperly stimulated her libido and sperm levels will spike during orgasm, preventing any form of relief from anything except brutal sex which treats their partner as nothing more than a hole to be raped. As an example an orgasm caused by a slow, milking handjob would immediately cause this servant's balls to surge in productivity and cause them to quickly search for a hole to abuse without any refractory period. A ruined orgasm would likely cause a complete lack of obedience from the sheer spike in libido that would follow.
				t + Enhanced Virility: A much higher infection rate than ordinary strains, even allowing precum to have a 100% infection rate. As a byproduct this causes sperm released by this strain to be extremely potent and attracts humans and submissive infected from long distances if not consumed immediately. Their extremely thick sperm alters the minds of even infected, changing them to become more submissive and easily manageable. Due to the potency, an ordinary soldier would likely develop strong masochistic urges after only a single gut-bloating creampie from a pent-up bull.
				t [Inspection Complete|`+data.player.lastLocation+`]
			`);
			break;
		}
		case "inspectAngela": {
			if (data.player.beastDisabled != true) {
				writeHTML(`
					t You watch as Angela, still unconscious from her sudden transformation, takes slow, exhausted breaths, and information begins to fill your mind.
					t <b>Siren Strain, Deviation Epsilon; Echidna</b>
					t A support deviation only recently introduced into the Greater Strain, achieved by infecting one who strongly desires an animal partner. This servant desires to tend to other infected, sexually relieving them of burdens such as heavy balls or pent-up lust. This servant shall take extra delight in servicing livestock and beasts, eagerly sexually pleasing a harem of Hound Strains, or a single husband of Stallion Strains.
					t + Delighted Masochism: This servant will remain especially loyal when regularly treated roughly by animalistic husbands. This results in a complete inability to orgasm via penis stimulation as is normal for a Siren Strain, but as a result this servant's go-to masturbation method when not around her husbands will be to achieve orgasm via methods such as large anal insertions, nipple squeezing, and self-spanking. However these are a far cry from their true desire of being completely dominated and abused.
					t + Siren Seduction (Deviated): A trait which allows this servant to seduce males, however the deviation has altered this trait to appeal primarily to animals, attracting new "children" into the pack to roughly fuck this servant into orgasmic bliss. This trait does not cause any familial love to develop in these new arrivals, in fact to her delight the opposite occurs and children become extremely rough and sexually brutal towards their 'mother'.
					t [Inspection Complete|`+data.player.lastLocation+`]
				`);
			}
			else {
				writeHTML(`
					t You watch as Angela, still unconscious from her sudden transformation, takes slow, exhausted breaths, and information begins to fill your mind.
					t <b>Siren Strain, Deviation Epsilon; Echidna</b>
					t A support deviation -------------------------
					t ---------------------------
					t ---------------------------
					t <b>Queen distaste for beast-variant strains detected. The details of this deviation have been disabled to prevent mental damage.</b>
					t [Inspection Complete|`+data.player.lastLocation+`]
				`);
			}
			break;
		}
		case "inspectMonica": {
			writeHTML(`
				t You stare at Monica for a moment. She seems nervous, tense, like she absolutely desperately wants to cum as much as she can as soon as she can, but her loyalty is just barely overpowering her urges.
				t Then, information begins to fill your mind, like you're tapping into some genetic knowledge the virus ingrained into your DNA.
				t <b>Soldier Strain, No Deviation.</b>
				t The most common result of a human being infected by the Great Strain. Her spread is complete, the authors would be proud. All traces of former identity have faded, leaving only This servant to the queen.
				t Ordinary soldiers are stronger, faster, and vastly more virile and superior compared to humans. Their purpose is to spread the Great Strain, wandering through areas assaulting humans, protecting submissive strains, and submitting to dominant strains.
				t + This servant has no significant traits outside of the normal results of being infected by the Great Strain, including increased vitality, the generation of pheromones through sweat, and the rapid production of infectious sperm. Milkings should be allowed to occur regularly when this servant is not saving their sperm for a greater purpose, such as the joining of a horde to infect many humans at once in a messy, sperm-soaked gangbang.
				t [Inspection Complete|`+data.player.lastLocation+`]
			`);
			break;
		}
		case "typhoidMidpointTrigger": {
			writeHTML(`
				t You hear a soft ding from somewhere in the compound, breaking your concentration. It's the signal to gather.
				t The strategy meeting will be starting soon, you'll need to attend to avoid drawing attention to yourself.
				t You should [head to the strategy room|typhoidMeeting].
			`);
			break;
		}
		case "typhoidEndpointTrigger": {
			writeHTML(`
				t You feel a sudden rush of adrenaline course through you, breaking your concentration. You can feel...
				t <b>Them</b>.
				t Your sisters have gathered, a horde has reached critical mass and they can wait no longer. They've started their final approach and the compound will soon be under siege.
				t [The compound has run out of time|typhoidHorde].
			`);
			break;
		}
		case "sabotageTyler": {
			writeHTML(`
				t Obviously you can't just leave the gates hanging open, and you can't just totally destroy the gate's hefty lock outright, so you focus on weakening the defenses in subtle ways.
				t Moving boxes and objects outside to allow your sisters better areas to scale the wall. Damaging, but not destroying the main lock. Looking for blind spots from the lookout tower.
				t As you do all of this, it sticks in your mind that as a queen capable of controlling the infected, perhaps you can communicate to some very basic degree with a large horde of them, and these weaknesses will allow your sisters an easier time breaking in.
				t A queen. The title sounds... Fitting.
				t In any case you've spent a sizable amount of time preparing, and more than that there's more to the compound's defenses than just the front gate.
				t [Finish|`+data.player.lastLocation+`]
			`);
			data.player.time += 1;
			addFlag(scene);
			break;
		}
		case "sabotageLisa": {
			writeHTML(`
				t You crack your knuckles and get to work disabling these awful, awful weapons. Just the thought of these guns and bats being used on your sisters is revolting.
				t Firing pins removed, the better melee weapons hidden under the more worn and crappier ones. Lisa is a beast with a bat in her hand, but she won't be so threatening if all she can grab in the heat of the moment is a frying pan, a plastic lawn gnome, and a two-foot dildo.
				t ... Actually, you decide to hide that last one too. 
				t [Finish|`+data.player.lastLocation+`]
			`);
			data.player.time += 1;
			addFlag(scene);
			break;
		}
		case "sabotageAngela": {
			writeHTML(`
				t Snap, clip, cut.
				t It's oddly relaxing, dooming your companions to a sure infection by choping a hole in the back line. That said, you can't make the hole too large or it'll be suspicious, but in the end the hole you've cut out will be just large enough for any hounds in the area to squeeze through.
				t It was a lot of work, hopefully the other survivors will appreciate a little forced back-door action.
				t [Finish|`+data.player.lastLocation+`]
			`);
			data.player.time += 1;
			addFlag(scene);
			break;
		}
		case "typhoidSexToys": {
			writeHTML(`
				t You gingerly explore the room, ready at any moment to hide at the first sign of another survivor's approach. You could probably take anyone who showed up right now, but they might scream and alert everyone else before you can grab them.
				t Naughty underwear, emergency weapons, a collection of thongs, hygiene items, you continue to rifle through the drawers looking for anything useful.
				player God damn Lisa, for having such a minimalist style, you sure do have a shitload of lingerie. Most of these bras definitely don't fit... You?
				t One drawer you really have to fiddle with to get open, you hear something snap, probably a very old busted lock, and inside you find the jackpot.
				t Sex toys, specifically ones designed for male anal play, and a collection of strap-ons of varying sizes and a fuckton of half-empty lube bottles.
				player Well well well, I wonder who these could be for? I think I'm starting to hatch a plan~
				t [Finish|`+data.player.lastLocation+`]
			`);
			addFlag(scene);
			break;
		}
		case "typhoidMeeting": {
			var infectionTotal = 0;
			var sceneTarget = "typhoidWrapup";
			if (checkFlag("infectedAmy") != true) {
				infectionTotal += 1;
			}
			if (checkFlag("infectedTyler") != true) {
				infectionTotal += 1;
			}
			if (checkFlag("infectedLisa") != true) {
				infectionTotal += 1;
			}
			if (checkFlag("infectedAngela") != true) {
				infectionTotal += 1;
			}
			writeHTML(`
				t You arrive at the strategy meeting. Keith has a dozen maps and other documents all organized across the table.
				t !flag infectedLisa; You take a seat directly next to Lisa. The chair on your right is empty, it's normally reserved for Monica.
				t ?flag infectedLisa; You take a seat next to the ones reserved for Lisa and Monica. With them absent though, you have a decent bit of privacy.
				keith Alright, it's time. Sound off.
				player Here.
				tyler !flag infectedTyler; Here, boss.
				amy !flag infectedAmy; Here.
				lisa !flag infectedLisa; Present.
				angela !flag infectedAngela; Present and accounted for.
			`);
			if (infectionTotal > 0) {
				writeHTML(`
					keith Seems like we're missing someone.
					player I'll catch them up after the meeting. It's another scavenging prep briefing, right, I-
					keith You volunteer, yes, I know. I swear you practically live out there.
					t With Keith distracted, it seems he's not bothered by the incomplete gathering. Success!
					keith !flag infectedMonica; Now, Monica's out doing a final scout of a tourist shop about two miles south...
					keith ?flag infectedMonica; Now, there's a tourist shop about two miles south. I asked Monica to investigate, she must have left already...
				`);
			}
			else { 
				writeHTML(`
					keith We're all here then? Good. Alright, this us another scavenging prep briefing. Now, Monica should be out doing a final scout of a tourist shop about two miles south...
				`);
			}
			if (checkFlag("pentUp") == true) {
				sceneTarget = "typhoidUndiscoveredHorny";
				if (checkFlag("infectedLisa") != true && checkFlag("caged") != true) {
					sceneTarget = "typhoidDiscoveredMeeting";
				}
			}
			if (checkFlag("cagedStrong") == true) {
				sceneTarget = "typhoidUndiscoveredStrong";
			}
			if (checkFlag("cagedWeak") == true) {
				sceneTarget = "typhoidDiscoveredWeak";
			}
			writeHTML(`
				t The meeting [drones on, and on, and on...|`+sceneTarget+`]
			`);
			break;
		}
		case "typhoidUndiscoveredStrong": {
			writeEvent(scene);
			writeHTML(`
				t What you <i>need</i> is to [get moving|typhoidPlayerRoom] and finish infecting the other survivors.
			`);
			break;
		}
		case "typhoidUndiscoveredHorny": {
			writeEvent(scene);
			writeHTML(`
				t After the surge of cum stops you're left quaking for a while longer before reason returns to you. You work quickly, finding some cloths to clean up and hide the evidence before anyone arrives, and thankfully you're not spotted. [You still have work to do.|typhoidSouth].
			`);
			break;
		}
		case "typhoidWrapup": {
			writeHTML(`
				t You've never had trouble focusing during these meetings before. Typically the life and death nature of the missions you volunteer for keeps you focused on the briefing material.
				t Instead you find your mind wandering. Planning, searching for holes in the compound's defenses you'll be able to take advantage of. Luckily you got off recently, otherwise you'd be focusing on another kind of hole.
				t How have you fallen so far, so fast? Is it some byproduct of the virus you've been infecting with, altering your mind and turning you against your fellow desires? Or maybe... Maybe these macabre desires were inside of you all along?
				t You shake your head. No, it's the virus. You're just along for the ride, resisting as best you can. Believing anything else would be-
				keith playerF? You alright?
				player Y-yeah, sorry. Just visualizing it all in my head.
				keith Right. Well, the mission's tomorrow. 
				player <i>Not if I have anything to say about it...</i>
				keith Don't get too worked up over it. And have Lisa check you out after this, you don't look too good.
				t He continues on, and you continue just barely paying attention. 
				t ... 
				t The meeting finishes, finally. Keith waves everyone off.
				t !flag infectedTyler; Tyler starts cleaning up.
				t !flag infectedAmy; Amy stretches, and heads out to prep for her lookout shift.
				t !flag infectedAngela; Angela seems to head out somewhere in a hurry.
				lisa !flag infectedLisa; Hey, you alright? You look seriously flushed and exhausted.
				player !flag infectedLisa; Yeah, I'm fine. Exercised pretty heavily earlier.
				lisa !flag infectedLisa; Alright then. Don't go too hard on yourself... Fuck, my head still aches.
				t With everything concluded you stand, plenty of ideas still floating in your mind on how to infect your remaining companions. But for now, [you should get moving|typhoidStrategy].
			`);
			break;
		}
		case "typhoidHorde": {
			writeHTML(`
				t In less than an hour, the horde is already approaching the gates. 
				t ...
			`);
			var sabotageTotal = 0;
			var infectionTotal = 0;
			var endingTarget = "typhoidFailureEnding";
			if (checkFlag("infectedAmy") != true) {
				if (checkFlag("sabotageAmy") == true) {
					sabotageTotal += 1;
					if (data.player.wsDisabled == true) {
						writeHTML(`
							This scene has been disabled due to your fetish settings. If you'd like to view it, re-enable watersports content and check the gallery.
						`);
					}
					else {
						writeHTML(`
							amy Seriously, you sure this is alright?
							player Yeah, Tyler said he'd manage it just fine.
							amy Aww, thanks! You know hiding stuff you find while scavenging is against the rules... But what the hell.<br>Jeez, the bottle is really warm, huh? I miss my fridge.
							t She takes the bottle and unscrews the cap.
							amy Jesus that smell, one sniff and my nose is fried to hell. What is this?
							player Some kind of health drink probably.
							t She takes a swig and there's a look of shock on her face as she almost spits it back out, but holds back and swallows.
							im sabotageEndingAmy1.gif
							t <b>Gulp</b>
							amy Ga-haad fucking damn, that tasted like it smelt! What's in this stuff?
							t She shakes the bottle curiously, a few droplets landing in her shirt.
							player Tastes bad? The bottle was sealed a while ago, so you're basically drinking straight from the tap.
							amy I mean I drink whiskey, I'm no pussy. Maybe I'll use it to keep me awake during the rest of my shift tonight.
							t She places the bottle down next to her and the two of you have a bit of smalltalk. It doesn't really go anywhere as Amy's mind is clearly still focused on the drink. Despite her claim she picks it back up and takes another swig.
							t <b>Gulp</b>
							amy Gah, it's like treating my tongue to an acid bath.
							player So why do you keep drinking it?
							amy Hah! Good point. Guess I'm thirstier than... Than I thought.
							t She keeps licking the inside of her mouth, her focus on the conversation keeps lapsing and she brings the bottle back to her lips.
							im sabotageEndingAmy2.gif
							t <b>Glug glug glug glug</b>
							t She breaks the seal she made with her lips to take a breath, panting like a dog, but quickly brings it back to drink more.
							t <b>Glug glug glug glug</b>
							t The bottle emptied she sucks in hard enough to cave in the plastic a little and runs her tongue along the rim to collect the last drops.
							player Damn, you were seriously thirsty.
							amy Y-yeah, guess I was...
							t She lifts her shirt and actually begins trying to suck out the few drops she spilt earlier.
							player You know, I could get you some more.
							t This has her attention, she dryly swallows clearly wanting as much as she can get.
							player Hey watch out for me, I need to take a piss.
							amy B-but you said you'd get me more... Gh...!
							player I am.
							t Instead of a more private position you strip down and whip out your dick right in front of Amy and aim yourself right at the dining table.
							t If there was ever any proof that the drink fried Amy's brain, it's the fact that instead of realizing you infected her, or calling for help...
							im drink1.gif
							t She takes one whiff and crawls towards you for another taste. As her lips wrap around the head of your cock and she starts to suckle from the source, she thrusts her hips forwards like she's cumming and you notice a bulge in her pants.
							player Stop. Show me.
							im drink2.gif
							t The infected formerly known as Amy lets out a small whine as she relieves herself.
							player Huh, you're an infected who gets off on pissing? Hmm~
							t ...
						`);
					}
					unlockScene("sabotageEndingAmy");
				}
				else {
					writeHTML(`
						t With Amy awake and aware on active lookout duty, the survivors are able to be warned well in advance.
					`);
				}
			}
			else {
				infectionTotal += 1;
			}
			
			if (checkFlag("infectedTyler") != true) {
				if (checkFlag("sabotageTyler") == true) {
					sabotageTotal += 1;
					writeHTML(`
						t There's no fear, no hesitation, not for a second. The group needs him.
						tyler Hey! Over here! C'mon you bunch of shrimp-dicked bimbos, buffet at the front gate!
						t They're approaching on all sides, and even just a few extra seconds will buy the rest of the team some time, so without even waiting for so much as a 'any volunteers' Tyler raced to the front.
						t Despite the front gate being incredibly sturdy, despite the fact that he just needed to make some noise on the north end to draw some away from the back of the compound, despite the fact that by all accounts he shouldn't be in any danger, this is still undeniably an incredible act of bravery on Tyler's part.
						t One that is rewarded immediately as the gate swings open. The women were closer to the entrance than he realized, and the gate's lock was damaged earlier.
						t No fear, there's no time to react. He can't even manage to turn around before five are on him and the rest are running into the compound.
						tyler NO! LIS-
						t Muffled in a flash by a dogpile of women with bigger dicks than his. His brain says 'I always knew this is how I'd go', he hopes, dimly, that his brain was trying to make a joke, not reminding him of his darkest fantasies.
						t No fear. It's over, there's no point. His body accepts it quickly, there's not even a surge of adrenaline. He's been here before, admittedly only in his daydreams.
						tyler <i>I... If I can hold back five of them... Five of them... All at once...<br>I hope they don't tear Lisa's birthday gift...</i>
						t His clothes are torn, they don't seem to care about the lingerie he's wearing beneath them.
						im sabotageEndingTyler1.gif
						t A finely trained asshole takes to the women well. Nobody's around to judge him for being a whore, so...
						im sabotageEndingTyler2.gif
						tyler Fuuuh~! Yesshhh~
						t ...
					`);
					unlockScene("sabotageEndingTyler");
				}
				else {
					writeHTML(`
						t Tyler, uninfected, comes up with a strategy to use himself as bait at the front gate. Because the gate is sturdy and strong, he's able to buy the survivors just the smallest bit of extra time to react and prepare.
					`);
				}
			}
			else {
				infectionTotal += 1;
			}
			if (checkFlag("infectedLisa") != true) {
				if (checkFlag("sabotageLisa") == true) {
					sabotageTotal += 1;
					writeHTML(`
						lisa Already inside, huh?! Eat shit, bitches!
						t *CLICK*
						lisa Wh-
						t She's tackled to the ground, not even so much as a dildo in reach to use as a weapon.
						lisa I won't! Go down! So eas-
						t In sync, the women who tackled her start rubbing themselves, stroking their steel-hard cocks right above her head.
						lisa N-no! Get off me! GET THE FUCK OFF-
						im sabotageEndingLisa1.gif
						t The first rope splatters across her face, followed by several more. It's probably the slowest infection method possible.
						t She spits, she sputters, she tries to turn away, but there's so much jizz packed onto her face after just a moment that if she wants to breath, she's taking the cum inside of her somehow.
						t She pushes forwards, and for some reason the crowd parts. She runs, trying to spit and rub away as much cum as she can, but some of it's in her mouth, some of it's probably down her throat already.
						lisa No, please, please, please!
						t She feels a pressure on her abdomen, she's running on instinct, unable to see under the blanket of infected jizz.
						t She feels cool air on her body, she must be outside.
						lisa Tyler! Help!
						t She's able to wipe off just enough cum to see out of one eye.
						t She's at the front gate, and Tyler is nowhere to be seen, just a wild orgy of more infected, and she collapses to her knees.
						lisa Why... Ghh~!
						t She clenches her stomach. She feels bloated, uncomfortable, scared.
						t *POP*
						t Relieved...
						t She can feel them dangling between her legs, although she doesn't want to believe it. Pulsing. Throbbing. Hungry. Full.
						t Several members of the orgy are moving. It was actually a gangbang, all centered around one small-dicked slut in the middle.
						lisa No more... I just... Want to give in.
						t She approaches the group's cumslut. She's...
						t ?flag infectedTyler; <b>Perfect</b>.
						t ?flag infectedTyler; Something about this girl... She's beautiful... Wonderful... But right now all she really cares about is finding some kind of release.
						t !flag infectedTyler; <b>Pathetic</b>.
						t !flag infectedTyler; The girl beneath her is small, weak, and more than anything looks like a sub-par sloppy fuck. But she doesn't need beauty right now, she needs to empty her balls of jizz.
						im sabotageEndingLisa2.gif
						t ...
					`);
					unlockScene("sabotageEndingLisa");
				}
				else {
					writeHTML(`
						t Lisa's able to grab weapons and disperse them around to other survivors.
					`);
				}
			}
			else {
				infectionTotal += 1;
			}
			if (checkFlag("infectedAngela") != true) {
				if (checkFlag("sabotageAngela") == true) {
					sabotageTotal += 1;
					if (data.player.beastDisabled == true) {
						writeHTML(`
							This scene has been disabled due to your fetish settings. If you'd like to view it, re-enable beast content and check the gallery.
						`);
					}
					else {
						writeHTML(`
							im sabotageEndingAngela1.gif
							t There really was no hope for Angela. No real form of struggle. The moment the hound slipped through the hole you made and pounced on her, the moment she knew she was alone with this beast...
							angela Yes~! Yes~! Cum for mommy~! Mommy wants your huge, doggy knot inside of her~!
							t It's just a fetish, just masturbation. She tells herself this, deludes herself, but it's fine. Her mind will be gone to the pleasure soon enough, why struggle? She just wants to indulge herself.
							t And it's exactly like how she always imagined it. Better, even. The thick knot slapping against her folds, the rapid humping far rougher, far more animalistic, far more wonderful than she could have hoped for.
							angela Yesyesyes~<br>Cum, please, cum please, I want to cum... Just as you...
							im sabotageEndingAngela2.gif
							angela FFFFFuuuuck~!
							t "Mommy" is satisfied, having held back her first orgasm to genuine dog-dick until she felt that knot forced inside of her. Until she felt that mind-meltingly hot animal cum pump against and into her womb.
							angela Ghhhfffuck yes~!<br>God I hope you have frieeeeends~!
							t And as she hears light steps approaching, she knows she's been granted her second wish of the day.
							t ...
						`);
					}
					unlockScene("sabotageEndingAngela");
				}
				else {
					writeHTML(`
						t Preparing to escape through the back of the compound, Angela somehow knows that hounds gather near the back. With this knowledge, everyone else isn't caught off-guard when the hounds try to circle the escape route.
					`);
				}
			}
			else {
				infectionTotal += 1;
			}
			if (sabotageTotal > 2) {
				var endingTarget = "typhoidSabotageEnding";
			}
			if (infectionTotal > 3) {
				var endingTarget = "typhoidInfectionEnding";
			}
			writeHTML(`t [Continue|`+endingTarget+`]`);
			break;
		}
		case "typhoidFoundCanine": {
			writeHTML(`
				player Hmm... Not a lot worth noting. Vacation pictures, souvenirs, can't believe she's kept all this during an apocalypse. Guess she liked dogs too...
				t You rifle through drawers, check under the bed, everything seems on the up-and-up. That is, until you check under the sizeable collection of pillows...
				t And find a genuinely intimidating knotted dildo!
				t It even has a slot for... You search for just a little while longer, and find a squeeze-pump connected to a small container of an off-white fluid. Fake cum for the kind of pervert who wants to get off imagining being creamed as she's knotted.
				t You rub your hands together, you really have no choice anymore. The moment you found the toy, Angela's fate is sealed.
				t ...
				t You wipe the sweat off your forehead and hide the container full of much, much thicker off-white fluid away. Now you just need to come back later.
				t For now though, you should [head back to your room|typhoidPlayerRoom] to avoid drawing any suspicion to yourself, and take a short breather to collect your thoughts.
			`);
			data.player.time += 1;
			addFlag(scene);
			break;
		}
		case "typhoidJournal": { //dropped
			writeHTML(`
				t [Finish reading|typhoidAmyRoom].
			`);
			break;
		}

		case "infectedTyler": {
			writeEvent(scene);
			data.player.time += 1;
			addFlag(scene);
			writeHTML(`t [Finish|`+data.player.lastLocation+`]`)
			break;
		}
		case "sabotageAmy": {
			writeEvent(scene);
			data.player.time += 1;
			addFlag(scene);
			writeHTML(`t [Finish|`+data.player.lastLocation+`]`)
			break;
		}
		case "infectedAmy": {
			writeEvent(scene);
			data.player.time += 1;
			addFlag(scene);
			removeFlag("pentUp");
			writeHTML(`t [Finish|`+data.player.lastLocation+`]`)
			break;
		}
		case "infectedLisa": {
			writeEvent(scene);
			data.player.time += 1;
			addFlag(scene);
			//removeFlag("pentUp");
			writeHTML(`t [Finish|`+data.player.lastLocation+`]`)
			break;
		}
		case "infectedAngela": {
			writeEvent(scene);
			//data.player.time += 1;
			addFlag(scene);
				writeHTML(`
					t [She's finished, but your work is not yet done|typhoidAngelaRoom].
				`);
			break;
		}
		case "typhoidWaterPrep": {
			writeEvent(scene);
			addFlag(scene);
			writeHTML(`t [Finish|`+data.player.lastLocation+`]`)
			break;
		}
		case "cagedWeak": {
			writeEvent(scene);
			addFlag(scene);
			writeHTML(`t [Finish|`+data.player.lastLocation+`]`)
			break;
		}
		case "cagedStrong": {
			writeEvent(scene);
			addFlag(scene);
			writeHTML(`t [Finish|`+data.player.lastLocation+`]`)
			break;
		}
		case "typhoidFleshlight": {
			writeEvent(scene);
			data.player.time += 1;
			removeFlag("pentUp");
			writeHTML(`t [Finish|`+data.player.lastLocation+`]`)
			break;
		}
		case "typhoidDildo": {
			writeEvent(scene);
			data.player.time += 1;
			removeFlag("pentUp");
			writeHTML(`t [Finish|`+data.player.lastLocation+`]`)
			break;
		}
		case "typhoidDiscoveredAmy": {
			writeEvent(scene);
			writeTransition('scenarioSelect', 'GAME OVER', '#FF0000');
			break;
		}
		case "typhoidDiscoveredWeak": {
			writeEvent(scene);
			writeTransition('scenarioSelect', 'GAME OVER', '#FF0000');
			break;
		}
		case "typhoidDiscoveredMeeting": {
			writeEvent(scene);
			writeTransition('scenarioSelect', 'GAME OVER', '#FF0000');
			break;
		}
		case "typhoidDoggy": {
			writeEvent(scene);
			writeHTML(`t [Finish reading|`+data.player.lastLocation+`]`)
			break;
		}
		case "typhoidSabotageEnding": {
			writeEvent(scene);
			if (checkFlag("infectedMonica") != true) {
				writeHTML(`t [Later...|typhoidMonicaEpilogue]`)
			}
			else {
				writeText("...");
				writeText("You've successfully infected the compound, with no survivors!");
				writeText("Check out the 'Author Information' section in the scenario selection menu for a more detailed set of credits, or play the game again to try and find every nook and cranny still to be spelunked.");
				writeTransition("typhoidMary", "Thanks for playing!");
			}
			break;
		}
		case "typhoidInfectionEnding": {
			writeEvent(scene);
			if (checkFlag("infectedMonica") != true) {
				writeHTML(`t [Later...|typhoidMonicaEpilogue]`)
			}
			else {
				writeText("...");
				writeText("You've successfully infected the compound, with no survivors!");
				writeText("Check out the 'Author Information' section in the scenario selection menu for a more detailed set of credits, or play the game again to try and find every nook and cranny still to be spelunked.");
				writeTransition("typhoidMary", "Thanks for playing!");
			}
			break;
		}
		case "typhoidFailureEnding": {
			writeEvent(scene);
			writeTransition('scenarioSelect', 'GAME OVER', '#FF0000');
			break;
		}
		case "typhoidMonicaEpilogue": {
			writeEvent(scene);
			writeText("...");
			writeText("You've successfully infected the compound, with no survivors!");
			writeText("Check out the 'Author Information' section in the scenario selection menu for a more detailed set of credits, or play the game again to try and find every nook and cranny still to be spelunked.");
			writeTransition("typhoidMary", "Thanks for playing!");
			break;
		}
		
		//On the Record
		case "onTheRecord": {
			document.getElementById('wrapperBG').style.backgroundImage = "url(scripts/gamefiles/locations/forTheRecord.jpg)";
			writeMed("scripts/gamefiles/characters/On the Record.jpg");
			writeText("Scenario Preview - On the Record");
			writeText("Rumors of a new bioweapon are all the buzz, and now a small town mall has been selected as the latest target.");
			writeText("You, the hottest of hot-shot journalists, must infiltrate the mall and get the scoop on what's going on.");
			data.player.scenario = "On the Record";
			updateMenu();
			//writeSpecial("This scenario is only a demo, so all that's available are a few short scenes that are made to convey the idea of what this route will contain. If you like it, please voice your opinion! Rainy DayZ has a smaller fanbase than my other two current works, so please (respectfully) voice your desires and ideas for what you'd like this game to contain. Leave comments or a review on TFGames, voice what you like on Discord, or support me on Patreon.");
			//writeTransition("recordStart", "Play the Preview");
			writeTransition("scenarioSelect", "Go back");
			break;
		}
		//System info
		default: {
			writeText("Something went wrong, and you've encountered a bug. Keep in mind where you just where and what you did, and let me know so I can fix it.<b>ERROR CODE:</b> Scene Write Failure, Scene "+scene+" does not exist.");
			writeText("Here's a list of important details. If you message me directly with these jams, I should have a better idea of what caused the problem:");
			document.getElementById('output').innerHTML += JSON.stringify(data);
			writeText("Inventory window:" + invHidden + "");
			writeText("Browser:" + navigator.appCodeName  + "");
			writeText("OS:" + navigator.platform  + "");
			writeBig("images/butts.jpg");
			writeTransition("start", "Go back to the title.");
		}
	}
}

function renamePlayer() {
	data.player.name = document.getElementById('nameSubmission').value;
	sceneTransition("typhoidIntro1");
}

function calculateMovement(location) {
	data.player.guardMovement += 1;
	if (data.player.guardMovement == 5) {
		data.player.guardMovement = 1;
	}
	if (checkFlag("houndsDisturbed") == true || checkFlag("gasDisturbed") == true) {
		//Do nothing
	}
	else {
	switch (data.player.currentScene) {
		case "compoundRoom": {
			if (data.player.guardMovement == 1) {
				writeText("You can hear someone outside the door. Peering through the window of the door you can see several figures in what looks like special ops gear, fully covered and holding strange guns.");
			}
			break;
		}
		case "compoundVent2": {
			if (data.player.guardMovement == 1) {
				writeText("You can hear someone moving outside the vent, but they're getting farther away. If you exit the vent now you'll be caught.");
			}
			if (data.player.guardMovement == 2) {
				writeText("The sound of footsteps have stopped, you should be safe to exit now.");
			}
			break;
		}
		case "compoundVent3": {
			if (data.player.guardMovement == 2) {
				writeText("You can hear someone moving outside the vent, but they're getting farther away. If you exit the vent now you'll be caught.");
			}
			if (data.player.guardMovement == 3) {
				writeText("The sound of footsteps have stopped, you should be safe to exit now.");
			}
			break;
		}
		case "compoundGasRoom": {
			if (data.player.guardMovement == 1) {
				writeText("You can hear someone approaching! You'll need to hide quickly!");
			}
			if (data.player.guardMovement == 2) {
				writeEvent("compoundCaught");
			}
			break;
		}
		case "compoundKennels": {
			if (data.player.guardMovement == 2) {
				writeText("You can hear someone approaching! You'll need to hide quickly!");
			}
			if (data.player.guardMovement == 3) {
				writeEvent("compoundCaught");
			}
			break;
		}
		case "compoundWifeRoom": {
			if (data.player.guardMovement == 3) {
				writeText("You can hear someone approaching! You'll need to hide quickly!");
			}
			if (data.player.guardMovement == 4) {
				writeEvent("compoundCaught");
			}
			break;
		}
	}
	}
}

function renameWife() {
	var goof = document.getElementById('nameSubmission').value;
	data.player.wife = goof;
	sceneTransition('spreadStart');
}

function writeEvent(n) {
	wrapper.scrollTop = 0;
	document.getElementById('output').innerHTML = '';
	switch (n) {
		//Rainy DayZ
		case "basic1": {
			writeText("You're so exhausted you barely even register the feeling as the zombie pushes you down into a puddle of running rainwater. Try as you might, you don't have the strength to resist as she rips your pants off.");
			writeSpeech("player", "", "No! Please, stop!");
			writeText("She either can't understand you or doesn't care, either way she doesn't stop as she rips off your panties. She tries to thrust right in but is stopped as her dick is just way too big to fit inside of you. You feel relieved for almost a second until she starts jacking herself off just inches away from the entrance to your pussy.");
			writeText("After a few seconds she spurts out a dollop of thick precum onto your labia and lets go of her dick. You try to squirm away, but she grabs you by the throat with one hand as she starts rubbing her jizz into your skin with the other.");
			writeText("Her fingers thrust inside of you, covering the entrance to your vagina with her infectious dickslime. Everywhere it touches you feel your skin grow puffy and sensitive. For as disgusting as you feel right now, her fingers feel really, really <b>good</b>.");
			writeText("But before you get the chance to start enjoying the sensations they've already stopped. She pulls the hand that was just fingerfucking you and puts it around your neck as she positions the head of her dick against your womanhood.");
			writeSpeech("player", "", "No...");
			writeText("Your objections are a lot quieter now, but that's just because you're being choked. It's not like you wanted to become a mindless fuck machine, right?");
			writeText("But she doesn't care about any of that as she presses dick dick against you and pushes. Inch after inch sinks into you as you resist with all the might you can muster.");
			writeText("And it's enough to stop her. A few inches in and you're just too tight to take any more. The situation is hopeless, no one is coming to save you, but this last act of defiance is enough to keep you focused and in the moment.");
			writeText("Until she pumps another load of precum into you. The pain of your resistance starts to fade and firecrackers start going off in your brain as your body starts to relax against your will. She pushes deeper and deeper inside of you before she starts pulling out.");
			writeBig("basic1-1.gif");
			writeText("She's relentless in her thrusting, and with every few thrusts comes another load of precum that makes the situation feel even better. Every time you absorb her juice into your body, your infection grows more complete.");
			writeText("With one big thrust you can feel her dick impact the entrance to your womb, but instead of body-clenching pain you feel spine-melting pleasure. She starts cumming, real jizz instead of precum, and you <b>feel</b> it being pumped into your womb.");
			writeText("You can feel your body changing by the second. Your breasts start growing and you feel the entrance to your womb start to dilate in preparation for becoming a brand new dick.");
			writeText("But she isn't done with you yet. She starts thrusting again to prolong her orgasm and the tip of her dick kisses your womb's entrance over and over again until her dick's tip pushes through. She pushes her head in, pumping her jizz directly into you stuffing you full of her infected milk.");
			writeBig("basic1-2.gif");
			writeText("She tries to pull out but stops and just starts tugging. Your womb, desperate for more, is wrapped around the head of her dick and won't let go. She keeps getting more and more forceful and pushes back in before trying to pull out.");
			writeText("After an audible *POP* she pulls herself free, and you start squirting her infected load out of your body. With every squeeze you push out more, and the bulge of your overstuffed womb starts to flatten. After one last push you squirt the cum a good few inches out, but you feel something else getting pushed out as well.");
			writeText("You look down, and standing at full mast is a brand new dick. You can feel a heavy pair of balls below it large enough to rest in the puddle of water beneath you.");
			writeText("Your mind can't hold a complete thought anymore. This is your new life.");
			if (data.player.currentScene != "gallery") {
				writeTransition('rainyDayZ', 'GAME OVER', '#FF0000');
			}
			break;
		}
		case "basic2": {
			writeText("The zombie pushes you down and starts rubbing herself against your face, covering your features in the sweat from her dick and balls to mark you with her scent. Once she's done that she grabs you by the hair and presses the head of her dick against your lips, but your keep your mouth firmly shut.");
			writeText("Frustrated, she throws you to the ground and, with one smooth motion, rips off your pants.");
			writeSpeech("player", "", "No, wait! You can use my mouth instead!");
			writeText("You open your mouth and wiggle your tongue around to tempt her, and it actually works. She lets you sit back up and presses her dick against your lips again.");
			writeText("You lay some gentle kisses onto her head and stroke her shaft with your hand. Quickly she takes a sharp breath and her balls tighten. Seeing the signs, you point her dick away from you and start licking the shaft. Her sweat makes your tongue tingle, like a sour candy as she pumps a glob of precum onto the floor.");
			writeText("The room is quickly filled with a heady scent. The precum is thick and cloudy, but your attention is pulled away from it by the zombie growling.");
			writeText("She obviously doesn't approve of the waste, and grabs you by the hair and forces her dick into your mouth. Out of fear of what she'll do to you if you don't comply, you make sure she can't feel your teeth.");
			writeText("The sensation is overwhelming, her sweat is all at once salty, bitter, and sweet like honey. She delivers some shallow thrusts punching the back of your throat but never pushes past it. With a grunt, it becomes clear that she wants you to taste this next load.");
			writeText("A dollop of precum is pumped out, this time directly onto your tongue. Your mouth feels like it's going numb, tingling like you just chomped on a giant mint. You know you should hold it in your mouth and spit it out as soon as you can, but...");
			writeSpeech("player", "", "*Gulp*");
			writeText("And just like that, you're <b>infected</b>. You can feel the load as it goes down your throat and into your stomach. Her cock pulses in your mouth and she decides its time to go even deeper. As she pushes herself into your throat, you can feel your body changing.");
			writeText("Your clit is rubbing against your suddenly much-tighter panties, and you feel your pussy start to gape open. You try to clench up to tighten it closed, but each time yo do you feel a lovely squeezing sensation. One last squeeze is all it takes for you to start cumming as you feel an explosive pleasure wash over you. The zombie unsheaths herself from her throat to reach down and tear off your panties to set your newborn balls and shaft free.");
			writeText("You can feel your pelvis clench as you spurt out a small load of clear fluid, but you still feel really backed up. You feel dazed, but you also want more as you wrap your lips around her shaft again.");
			writeBig("basic2-2.gif");
			writeText("She pushes herself back inside you and you offer no resistance. At this point your gag reflex is a thing of the past and you're loving every pump of precum she spurts down your throat.");
			writeText("Soon enough your own cock is at full mast and she hilts herself balls deep inside of you before she starts cumming for real. After only a few spurts though, she starts to pull out.");
			writeText("You try to stop her, sucking on her dick with all your might, but she pushes your head and slams you into the wall freeing herself.");
			writeBig("basic2-1.gif");
			writeText("She jacks herself off before she fires another rope of sticky jizz onto your face, and then she steps back to spray another onto your cock. You feel yourself changing again as pleasure starts to build up inside of you, and you <b>spuuurt</b> out a thick load of sludge-like cum. It's more like jelly than jizz as you pump out every last bit of the contents of your old womb to make room for new infectious sperm.");
			writeText("Your ability to speak is long gone and your brain is foggy as you sit and enjoy the new sensations.");
			if (data.player.currentScene != "gallery") {
				writeTransition('rainyDayZ', 'GAME OVER', '#FF0000');
			}
			break;
		}
		case "basic3": {
			writeText("You slowly sneak up behind the zombie as thoughts run through your head. Your brain is saying things like 'Don't leave your weapon behind!', 'Don't fight her without a weapon!' 'Don't tear your clothes off so your fat dick can swing freely!'");
			writeText("You ignore them as you tackle the zombie to the ground and roll her over. She's startled, but doesn't fight back once you lay your dick on top of hers.");
			writeText("Yours is larger than hers is, and you literally rub in that fact by grinding your dick into hers. You're excited to be the one doing the fucking for a change, but it doesn't go according to plan as the feeling of her shaft rubbing against yours feels <b>really</b> good.");
			writeText("You frot against her until you feel your balls clench up and you prematurely start spurting your dickmilk onto her shaft. After three shots your dick has stopped firing, but you're still rubbing against her until she pushes you away.");
			writeText("She rubs her shaft to lubricate it with your cum and starts sinking her dick into your virgin hole. The feeling of her dick spreading you apart feels fantastic until she bumps up against something that makes you quiver on the spot.");
			writeText("She thrusts a few times trying to push past the block, your fat prostate is enough to stop her from getting any further until she <b>SLAMS</b> past it.");
			writeBig("basic3-2.gif");
			writeText("The feeling of your bitch-button getting flattened is enough to make you start pumping out a fresh load of dickmilk much larger than the first. She hilts herself deep inside you and you rub your hand along the bulge in your midsection, appreciating the outline of her head as she uses you like a human fleshlight before she pushes you down and starts thrusting.");
			writeBig("basic3-1.gif");
			writeText("You don't have any words as you feel your prostate being ground into paste, but you do have some for every time her massive balls slam into yours.");
			writeSpeech("player", "", "Oh FUCK! YES. YES. YES.");
			writeText("You love every second of it, you feel your balls tightening for the third time as she rearranges your intestines.");
			writeText("...");
			writeText("By the end of it you're still in control of yourself. Your inoculation means that you can't go full zombie like the rest, but that doesn't mean anything as you suckle on your new master's dick to prepare her for round two.");
			if (data.player.currentScene != "gallery") {
				writeTransition('rainyDayZ', 'GAME OVER', '#FF0000');
			}
			break;
		}
		case "horde1": {
			writeText("You fling a marble from your bag and it *PLING*s off the hood of a distant car.");
			writeText("One marble after the other is thrown, and you make your way a good distance through the mess of cars. The zombies are in a frenzy now, confident that there's a human in the area, but they don't know where.");
			writeText("You're about to throw another as you turn the corner of a broken-down police cruiser as you make eye contact with one of the infected.");
			writeText("She reacts much more quickly than you do. You barely have time to dodge out of the way of her tackle, and you drop your bag of marbles onto the road as you do. Thinking quickly, you plant your boot in her face as she goes for another lunge, pushing her down a small incline.");
			writeText("With her out of the way, you break into a sprint to escape the highway, only to stop a few steps later as you realize you've been surrounded.");
			writeSpeech("player", "", "Fuck.");
			writeText("There's an unmistakable hunger in their eyes. In every direction, they block all avenues of escape.");
			writeSpeech("player", "", "Please, no!");
			writeText("...");
			writeBig("horde1.gif");
			writeSpeech("player", "", "Fuck! Yes!");
			writeText("You tune changed quickly once you got some infected precum in you. At first it was awful. They had no idea of the concept of how to take turns, so the horde wasn't satisfied until you had a dick in your mouth and ass, and two at once penetrating your once-tight pussy.");
			writeText("It wasn't all that bad though. The hopelessness of the situation made it a lot easier to give in.");
			writeText("You could feel the rumbling of your body changing earlier, now whenever they hit the back of your pussy they bump against something that sends shivers down your spine. Their thrusts get more and more shallow until eventually they're force to pull out, and as they do two fat balls pop out of what used to be your vagina.");
			writeText("You're one of them now. And until you can satisfy the whole crowd with your mouth and ass, you're the bottom bitch around here.");
			if (data.player.currentScene != "gallery") {
				writeTransition('rainyDayZ', 'GAME OVER', '#FF0000');
			}
			break;
		}
		case "horde2": {
			writeText("You want to punch your brain. Of course, you weren't thinking with your head when you took the route down the highway. But you're pretty sure your fucked-up half-zombie brain would enjoy a punch to the dick.");
			writeText("You never had a chance. The inclines on the sides of the road were flooded, so you had to walk down the road directly using the cars for cover. You bumped face-first into a zombie's dick, now here you are.");
			writeText("To your credit, you actually tried to fight back. Sure, it was more playful slapping than fighting, but at least you tried.");
			writeText("Some of them simply walk by and pay you no mind, probably on the lookout for some fresh meat to infect. Others haven't had a chance to stick their dick into something for days and really need some relief.");
			writeBig("horde2.gif");
			writeText("Maybe someday all of them will have left you alone for a better source of fuckmeat. You won't be the same though, not that you care anymore.");
			if (data.player.currentScene != "gallery") {
				writeTransition('rainyDayZ', 'GAME OVER', '#FF0000');
			}
			break;
		}
		case "siren1": {
			writeText("You turn on your flashlight. The best defense is a good offense, so the best course of action is to find what's chasing you and take them on directly.");
			writeText("There, in the corner! You see a flash of blond hair as something runs just out of sight.");
			writeText("You make chase, and you can hear a soft giggling as you run. For some reason it almost seems to replay in your head as you think about the sound, over and over.");
			writeText("You almost get lost for a second until another giggle gives them away. They're pretty fast, but you won't give up the chase!");
			writeText("...");
			writeText("Many corners turned, many catwalks crossed, and lots of laughing later, you finally catch up to her and corner her in a dead end.");
			writeText("She turns around to face you, biting her lip and looking sad that the game is over. She's beautiful, not a trace of the dirt or sweat you'd find on any other zombie.");
			writeText("Her dick is flaccid and cute, she wiggles it from side to side when she notices you're staring.");
			writeText("You can feel yourself hardening, and she can see it through your clothes. She turns around and wiggles her ass at you invitingly.");
			writeText("There's a strange scent in the air, it's somehow relaxing and energizing at the same time. You can't remember why you started chasing her in the first place as she giggles again.");
			writeText("... Oh, that's right! You were going to punish this naughty girl for running away from you, of course!");
			writeText("...");
			writeBig("siren1.gif");
			writeText("Everything just feels nice. You can't even remember why you ever wanted to leave the factory in the first place. Thrusting into her ass feels so good...");
			writeText("And she likes it too. She lets out happy little giggles whenever she cums on the floor, and the nice scent in the air just gets stronger.");
			writeText("She leans back and kisses you, and you feel yourself cumming again into her ass. Every time you do, it almost feels like your dick gets a little shorter, and your body gets a little softer, but you don't mind.");
			writeText("You don't think you could stop even if you wanted to.");
			if (data.player.currentScene != "gallery") {
				writeTransition('rainyDayZ', 'GAME OVER', '#FF0000');
			}
			break;
		}
		case "siren2": {
			writeText("With some rope and a decent amount of hunting knowledge, your snare trap is set. If anyone tries to approach you while you rest, they'll be strung up and you can escape.");
			writeText("All you need to do now is wait, so you create a bit of cover using a sheet of cloth between some boxes.");
			writeText("...");
			writeText("You wake up with a jolt as you hear the rope of your trap make a loud *CRACK* sound as it tightens around something.");
			writeText("Your trap has been sprung, you knew someone was following you!");
			writeText("You exit your makeshift shelter, ready to start running, but you stumble when whatever you trapped begins... Whining?");
			writeText("It sounds like the fake sort of crying a child would do when they want attention but for some reason you can't seem to keep it out of the front of your mind. The sound is ringing in your ears over and over, and your vision starts to blur as your body moves on autopilot.");
			writeText("<b>HELP</b>");
			writeText("Soon enough you're where you set your trap, and suspended in midair by her leg is a naked woman.");
			writeText("<b<HELP ME</b>");
			writeText("You need the noise to stop. The ringing in your ears is making you see stars.");
			writeText("<b>I'M STUCK</b>");
			writeText("But slowly the painful ringing stops, and whatever was making the sound goes quiet. As your mind unclouds, you realize you've just finished undoing the knot of your trap.");
			writeText("You step back, the zombie is freed. Worse than that, you can hear the sounds of other zombies closing in, obviously alerted by the sound.");
			writeText("The zombie you freed tackles you. She's light, and a lot weaker than the others, so you should have no trouble thr-");
			writeText("... All your worries start to fade away as her lips meet yours. What were you thinking about?");
			writeText("She's so soft. Her dick is so small compared to yours. Why are you wearing clothes again? You take them off with her help.");
			writeText("...");
			writeBig("siren2.gif");
			writeText("You can't close your ass anymore, and you lost count of how many of them have used you already.");
			writeText("You blissfully giggle as her palm cups your cheek, and she gives you another kiss.");
			writeText("Every zombie in the factory came running when she was in trouble, so she had to repay them somehow. And you helped! You're a great helper. You're going to be helping her for as long as she wants, and she'll keep rewarding you with those beautiful lips of hers.");
			if (data.player.currentScene != "gallery") {
				writeTransition('rainyDayZ', 'GAME OVER', '#FF0000');
			}
			break;
		}
		case "hunter1": {
			if (data.player.beastDisabled == false) {
				writeText("You drop your pack and heft your bat. There's only one thing left to do now.");
				writeText("One of the zombified dogs comes out from behind a tree. You're wounded and have almost no energy left, but you still have some fight in you.");
				writeText("He bars his teeth and growls, and you get ready to start swinging.");
				writeSpeech("player", "", "Come get some, motherfucker.");
				writeText("...");
				writeBig("hunter1.gif");
				writeSpeech("player", "", "Yes! Yes! Deeper!");
				writeText("You couldn't even take down the first one, smacking him with the bat just made him angry.");
				writeText("But that made him fuck you harder, so who's the real winner here?");
				writeText("It's pretty clearly you as you cum for the third time on his massive dogcock, before he stops his quick thrusting and goes balls deep into you.");
				writeSpeech("player", "", "Oooooh...");
				writeText("You can feel his inflated knot inside you as he starts to fill you with hot cum.");
				writeText("Maybe if you act bratty enough to one of the other members of the pack, they'll treat you like the bitch you are and start using your face.");
			}
			else {
				writeText("This scene has been disabled by your fetish settings. If for some reason want to watch it, you can still view it in the gallery after changing your settings.");
			}
			if (data.player.currentScene != "gallery") {
				writeTransition('rainyDayZ', 'GAME OVER', '#FF0000');
			}
			break;
		}
		case "hunter2": {
			if (data.player.rimDisabled == true || data.player.beastDisabled == true) {
				writeText("This scene has been disabled by your fetish settings. If for some reason want to watch it, you can still view it in the gallery after changing your settings.");
			}
			else {
				writeText("You feel a jolt of pain go through your arm, your wound must be worse than you thought. You fall over, you can't keep running. You drop whatever you can, your pack, the supplies, anything weighing you down.");
				writeText("But it isn't enough. They surround you quickly. Even if you had your weapon, you'd need more energy than you have to fight them off.");
				writeText("You can feel your eyes start to water as one of the infected dogs comes out from behind a tree.");
				writeSpeech("player", "", "Please, no...");
				writeText("You try to crawl away, but when you start moving he lets out a growl that stops you in your tracks.");
				writeText("But he isn't attacking yet. You still hold on to one last glimmer of hope. He comes up and starts sniffing your, face, before backing off to start walking circles around you.");
				writeText("You realize you'll need to be completely submissive if you don't want to be torn to shreds. He stops in front of you and turns around. He looks over his shoulder at you expectantly. He can't seriously mean...");
				writeText("...");
				writeBig("hunter2.gif");
				writeSpeech("player", "", "*Slurp*<br><i>Oh god, I can't believe I'm doing this... </i>");
				writeText("With every lick you can feel your fear fading away into a mess of feelings, lust being the dominant one.");
				writeText("He's panting, his huge cock is hanging down and dripping with precum as you make out with his asshole.");
				writeText("But he might not be enjoying it as much as you are. You switch from slurping licks to worshipful kisses and back again.");
				writeText("More of them are starting to fill the clearing, surrounding you. Each of them look just as hungry as the first one did.");
				writeText("You push your tongue deeper into his ass as he starts to cum onto the dirt floor. This is your life now.");
			}
			if (data.player.currentScene != "gallery") {
				writeTransition('rainyDayZ', 'GAME OVER', '#FF0000');
			}
			break;
		}
		case "worms1": {
			if (data.player.wormDisabled == false) {
			writeText("The feeling of a soft pillow and softer blanket offer a comfort you haven't felt in weeks. The dingy mattress you have at the safehouse is simply no comparison. Soon enough, what was intended to be just a nap is drawn out into a deep sleep");
			writeText("...");
			writeText("Vision flutters across your eyes. Tingling sensations pass through your limbs and you feel a presence spread across your body.");
			writeText("The soreness of your eyelids tells you that you're tired, but still awake. Strangely, you feel a breeze over various parts of your body. Stranger still, you can't move a muscle.");
			writeText("Panic begins to wake you up as your skin crawls, but you're completely paralyzed.");
			writeText("You look down, head propped up on the pillow, and see the reason for the feeling of cold air on your body.");
			writeText("Small, wriggling worms are creeping across your body, a mucous that covers them has eaten through the fabric of the blanket and chunks of your clothes.");
			writeSpeech("player", "", "Nnnn!");
			writeText("They're exploring across your body. Wherever they touch feels like a mild coursing electricity.");
			writeText("You try to struggle, to roll off the bed, to do anything, but to no avail.");
			writeText("The panic within you burns even brighter as you can feel the sensation of one of them getting through your underwear, tiny feelers rubbing against the outer lips of your folds.");
			writeText("The creatures continue crawling upon your skin, coating your body with an aphrodisiac meant to ensure compliance once the paralysis has worn off. Against your will the sensations, the teasing of the creatures has caused you to grow wet.");
			writeText("Entirely working off instinct, this is the signal to the worm at the entrance to your pussy to push forwards in search of a host.");
			writeSpeech("player", "", "NNNN-!");
			writeText("No one hears your screams as the worm inches inside of you. The aphrodisiac coating it's body makes the sensations feel incredible in combination to the worm's 2-inch thickness.");
			writeText("You suck in a breath trying to focus through the sensation of lightning coursing through your body as you orgasm. The paralytic is beginning to wear off, but it's too late. The other worms smell the scent coming from your quivering pussy and have begun to swarm around your hole.");
			writeText("One after another begins to push inside of you as the first presses against the entrance to your womb. Feelers press against the closed cervix and your body spasms.");
			writeText("Your movement scares the remaining worms away, but there are still four inside of you. Your walls clench around them, and they squirm inside of you in response.");
			writeSpeech("player", "", "AAAH~!");
			writeText("The worm in front presses itself hard against the entrance to your womb, trying to squirm its way inside. The chemical coating causing your body to welcome, rather than reject the intrusion.");
			writeText("You can feel your insides spasm to allow the worms inside, and as they begin to push through your back arches and you <b>spray</b> all over the bed in a mixture of squirt and the worm's fluid.");
			writeText("There's a small pricking sensation as they latch onto the walls of your womb and begin to pump body-altering chemicals into your bloodstream. Your eyes dilate as a cool calmness washes over you.");
			writeText("Barely conscious, you can feel yourself roll over and prop your ass into the air. Instinctively you know you're signaling for the escaped worms to return.");
			writeBig("worms1.gif");
			writeText("To return to a willing host.");
			}
			else {
				writeText("This scene has been disabled by your fetish settings. If for some reason want to watch it, you can still view it in the gallery after changing your settings.");
			}
			if (data.player.currentScene != "gallery") {
				writeTransition('rainyDayZ', 'GAME OVER', '#FF0000');
			}
			break;
		}
		case "worms2": {
			if (data.player.wormDisabled == false) {
				writeText("The feeling of a soft pillow and softer blanket offer a comfort you haven't felt in weeks. The dingy mattress you have at the safehouse is simply no comparison. Soon enough, what was intended to be just a nap is drawn out into a deep sleep");
				writeText("...");
				writeText("Vision flutters across your eyes. Tingling sensations pass through your limbs and you feel a presence spread across your body.");
				writeText("More than that, you can feel yourself at a full erection, your shaft standing unrestricted by the underwear you were wearing.");
				writeText("You're completely paralyzed, all you can do is look down to stare at the head of your cock, and the creature slowly crawling up the side of your shaft.");
				writeText("There's a slime coating it's body, and as it crawls along your skin it leaves a powerful tingling sensation.");
				writeText("You lay there, motionless, watching with a disturbed curiosity as it reaches the head. It's tiny feelers rub along the sensitive flesh and you can already feel yourself starting to leak precum.");
				writeText("But the creature isn't satisfied to just sit and taste, and it positions itself directly at your urethra and begins to push itself inside.");
				writeSpeech("player", "", "NNNN-!");
				writeText("This is where the real panic starts, as you struggle helplessly against the chemical paralysis.");
				writeText("It might be the creature's mucous, or it could be part of your infection, but rather than burning pain and tearing skin, all you feel are firecrackers in your brain as your cumhole stretches to accommodate the worm.");
				writeText("The feeling of a creature crawling down inside your dick is an alien one, even for someone who grew a dick just earlier today. Once you realize that the sensation is closer to pleasure than pain, your panic begins to fade.");
				writeText("Your eyes roll back in their sockets and you let out whining moans as the creature makes it past the halfway point. Your balls tighten and you hope that you'll find relief from the creature soon, but no dice.");
				writeText("The jet of cum you'd hope to fire across the bed is instead stuck, and the creature continues pressing on. The sensation that you're on the edge of an orgasm doesn't fade.");
				writeText("It travels farther down, set on finding the source of your cum. By the end of this, you'll be infected even farther.");
				writeText("You twitch and shake, mouth agape as the creature continues to coat the inside of your dick with an aphrodisiac. The tingling almost makes you wish that there were a zombie there who could fuck the hole to end the buzzing sensation.");
				writeText("The size of the worm left your urethra gaping slightly, and the hole winks every time your dick tries to push out an orgasm.");
				writeText("There's a pricking sensation as the worm rubs against your prostate, and you can feel a coolness enter your bloodstream.");
				writeText("Your body must be changing, because you pump out a massive load of cum that arcs through the air far enough to splatter against the wall. Your whole body clenches as your fire another rope, the room now smelling like fresh lemons coated in sperm.");
				writeText("You lift yourself up slightly and end up rolling off the bed, trying to feel for where the new changes to your body are, as a continuous stream starts to leak out of the head of your infested cock.");
				writeBig("worms2.gif");
				writeText("You can hear the sound of slithering in the walls as the scent of your tainted sperm begins to attract more of the creatures. The thought of more worms trying to fight their way into your perverted dickmeat causes another bit of splooge to ooze out of you.");
			}
			else {
				writeText("This scene has been disabled by your fetish settings. If for some reason want to watch it, you can still view it in the gallery after changing your settings.");
			}
			if (data.player.currentScene != "gallery") {
				writeTransition('rainyDayZ', 'GAME OVER', '#FF0000');
			}
			break;
		}
		case "flower1": {
			writeText("You walk up to the infected woman and get ready to either fight or run, but suddenly you can smell that powerfully sweet scent from the flower earlier.");
			writeText("You look around, but there aren't any flowers around.");
			writeText("Just then a chill runs up your spine, and you feel your hands moving on their own grabbing the bottom of your shirt and pulling the cloth up.");
			writeSpeech("player", "", "<i>W-what the hell is going on? </i>");
			writeText("You can't even speak aloud, it's like your body is on autopilot as you throw aside your shirt and leave your chest exposed. Right away your body is moving again to take off your pants.");
			writeText("Something's got control of your body, and it isn't letting go. The infected woman still hasn't noticed you, luckily, but that could change at any second.");
			writeText("Panic fills your eyes as you squat down and spread your legs, exposing yourself to the woman yet hoping she won't turn around to see you.");
			writeText("Your hand moves to your snatch, either terror or a bizarre sudden exhibitionism fetish has caused it to become dripping wet.");
			writeText("It takes all the resistance you can muster not to whorisly moan as your fingers begin teasing your slit.");
			writeSpeech("player", "", "<i>What the fuck is wrong with me?! God, I need to snap out of this... Please, let this be a dream! </i>");
			writeBig("flower1.gif");
			writeSpeech("player", "", "Khhh...");
			writeText("The loud weather is the only thing saving you now, as you can't resist heavily breathing while you play with yourself.");
			writeSpeech("player", "", "<i>Fuck... FINE! </i>");
			writeText("You give in, hoping that an orgasm will shock you back to your senses.");
			writeText("Completely leaning in to your urges, you relax and start fingerfucking yourself. At this point you don't know if you're enjoying this so much because your body is being controlled, or if you really, really need to cum.");
			writeSpeech("player", "", "Ggghhh~");
			writeText("No longer caring if she sees you, you lean back on the ground, pussy high in the air as you fuck your quim with your hand.");
			writeText("Finally it hits you like a wave.");
			writeSpeech("player", "", "Cumming!");
			writeText("You let out a barely held-back squeal as you squirt all over your hand and the ground, before your body falls slump.");
			writeText("The rain is falling around you as your body quakes.");
			writeSpeech("player", "", "<i>Fuck, that was amazing... I could get addict-</i>");
			writeText("Your thought process goes dead in the water as a cock appears in your field of vision from below, your show had an audience after all.");
			writeText("You can still run, evade the infected bitch and use the rain to your advantage. You can still survive!");
			writeText("But instead, on autopilot, all you can do is lick your lips and spread your pussy for the infected woman.");
			if (data.player.currentScene != "gallery") {
				writeTransition('rainyDayZ', 'GAME OVER', '#FF0000');
			}
			break;
		}
		case "flower2": {
			writeText("You walk up to the infected woman and get ready to either fight or run, but suddenly you can smell that powerfully sweet scent from the flower earlier.");
			writeText("You look around, but there aren't any flowers around.");
			writeText("Just then a chill runs up your spine. Your needy cock has been trying to get your attention on the run here, but for the sake of surviving your trip home you opted to ignore it. Yet, for some reason you feel your hands moving on their own grabbing your pants and pulling them down.");
			writeText("Something's got control of your body, and it isn't letting go. The infected woman still hasn't noticed you, luckily, but that could change at any second.");
			writeText("Panic fills your eyes as you squat down and spread your legs, exposing your bobbing shaft to the woman yet hoping she won't turn around to see you.");
			writeBig("flower2.gif");
			writeSpeech("player", "", "Khhh...");
			writeText("The infected woman is just down the way, either searching for shelter or a victim to infect, but you're stuck in the middle of the street stroking yourself.");
			writeSpeech("player", "", "<i>Fuck... FINE! </i>");
			writeText("You give in, hoping that an orgasm will shock you back to your senses.");
			writeText("Completely leaning in to your urges, you relax and start fingerfucking yourself. At this point you don't know if you're enjoying this so much because your body is being controlled, or if you really, really need to cum.");
			writeSpeech("player", "", "Ggghhh~");
			writeText("No longer caring if she sees you, you lean back on the ground, desperately jerking off to a big release.");
			writeText("Finally it hits you like a wave.");
			writeSpeech("player", "", "Cumming!");
			writeText("You let out a barely held-back squeal as you spray all over your hand and the ground, before your body falls slump.");
			writeText("The rain is falling around you as your body quakes.");
			writeSpeech("player", "", "<i>Fuck, that was amazing... I could get addict-</i>");
			writeText("Your thought process goes dead in the water as a cock appears in your field of vision from below, your show had an audience after all.");
			writeText("You can still run, evade the infected bitch and use the rain to your advantage. You can still survive!");
			writeText("But instead, on autopilot, all you can do is lick your lips. Your cock is softening, so it's obvious who will be on top.");
			if (data.player.currentScene != "gallery") {
				writeTransition('rainyDayZ', 'GAME OVER', '#FF0000');
			}
			break;
		}
		case "survivor1": {
			writeText("Rational thought went out the window the moment you saw her. A human woman? Here, in town? Your body moved on autopilot when you tackled her down to the ground.");
			writeText("She struggled, sure, but she's probably been running on maybe an hour of sleep in the last three days. She gives up and just goes limp soon enough.");
			writeSpeech("player", "", "Hey, relax, I'm human.");
			writeText("That gets her attention. She stops struggling long enough for you maneuver yourself to sit down on her hips, pinning her to the ground.");
			writeSpeech("player", "", "Now listen, you're got two choices. We do this the easy way, or the hard way.");
			writeText("She tells you to let her go, that she doesn't have any supplies left, and that you should just leave her out here to die.");
			writeText("Her voice dies in her throat once you've started taking your pants off. The moment she sees your cock, she starts to struggle again.");
			writeSpeech("player", "", "Calm down, calm down. You know better than to scream out here, right? Good. <br>Look, I'm special alright? I'm infected, but I can still think and talk like normal. I can make you special too.");
			writeText("She stops struggling, the glob of precum you pumped onto her coat while talking probably helped.");
			writeSpeech("player", "", "Think about it. I'm a walking vaccine. You don't need to worry about being infected, you can eat or take whatever you want safely.");
			writeText("You're bullshitting, but she doesn't need to know that. You push your dick against her cheek, and let out a small moan as you pump out another dollop onto her cheek.");
			writeSpeech("player", "", "So, again, the easy way, or the hard way?");
			writeText("...");
			writeText("The two of you started fucking like rabbits the moment your clothes came off. Apparently your sperm has the same aphrodisiac effect as the other zombies, good to know.");
			writeSpeech("player", "", "Gonna cum! Gonna cum inside you, you ready?");
			writeText("She shudders in response, unable to form words. Your thrusts are getting more shallow, her body has already begun to change.");
			writeSpeech("player", "", "Cumming!");
			writeText("Her body jerks with each load of cum you pump into her womb. You feel a pressure at the head of your dick, so you pull out. She writhes on the table, but soon enough her change is complete.");
			writeBig("survivor.gif");
			writeText("A brand new set of balls hangs below a fresh dick.");
			writeSpeech("player", "", "So? How does it feel?");
			writeText("Any sort of thought or feeling aside from hunger is gone from her eyes. Her hands wrap around her new dick and she starts jerking off.");
			writeText("This isn't right. There's no recognition to your words, all she's focused on is her dick. At this point, it seems like...");
			writeText("Like she's just a regular infected.");
			writeText("You start to leave as she has her first orgasm, pumping out sperm hard enough to splatter the walls of the room. You need to get home and figure this out");
			break;
		}
		case "survivor2": {
			writeText("You approach the girl with shaking hands, she's definitely younger than you. It's a damn tragedy what happened.");
			writeText("You tell yourself you just need to check her pulse, that you need to see if she's still alive, but that doesn't explain why your hand runs along her sticky skin after you check.");
			writeSpeech("player", "", "I... Need to stop this... I could get infected. <br>But maybe that's not such a bad thing-");
			writeText("You shake your head to rid yourself of the intrusive thought. How could that not be a bad thing? You need to leave but...");
			writeText("You can't bring yourself to stop rubbing her skin, the smell in the air is intoxicating.");
			writeSpeech("player", "", "Maybe... Maybe there isn't a chance I'll make it out of this... Wouldn't it be better to just live as one of them? And this is a chance for it to happen on my own terms, so that I don't end up like...");
			writeText("You shudder as you look at the girl's unconscious face, and rub your fingers along her slightly gaped cunt.");
			writeText("She shudders, there's a squishing sound as the smell in the room suddenly intensifies, and you lift your hand back up to see a glob of smelly infected sperm.");
			writeSpeech("player", "", "This is so fucked up...");
			writeText("You feel yourself shaking as you lift your hand over your head, and you stick your tongue out. Is this how addicts feel when they're holding the drug they need in their hand?");
			writeText("A long glob slowly extends down your tongue, and when it finally lands you can feel your cunt throb as your brain tries to make sense of what you just tasted.");
			writeSpeech("player", "", "So good... So good! Fuck, I need to... Stop...");
			writeText("You try telling yourself it's okay, that just a taste is fine, and that you'll spit it out.");
			writeText("Swishing around the delicious glob of infection in your mouth, you ignore the creeping panic in the back of your mind as you pull the hem of your pants out and begin to rub your sperm-coated hand against the lips of your pussy.");
			writeSpeech("player", "", "F-fuuuuck~!");
			writeText("You relish the taste on your tongue, your body shaking in pre-orgasm jitters already. It feels so much better to masturbate like this, right on the edge of danger.");
			writeText("You slip one of your fingers into your cunt and swallow hand, and your body shakes as you have a powerful squirting orgasm from sealing your own fate. The inside of your pants are completely ruined, just like your body.");
			writeText("The girl groans, pulling you out of your stupor. You aren't lying to yourself anymore, you need more of that cum. That's why you're so disheartened when a bit of it splurts out of her pussy and onto the floor.");
			writeText("You move quickly, trying to lap up as much of the sperm as she can splurt out.");
			writeBig("survivor2.gif");
			writeText("Her groans are getting louder and she's writhing in pleasure. More cum is getting pushed out of her body, so you skip the middle man, plant a fat kiss on her cunt lips, and start trying to suck out her infected creampie.");
			writeText("You let out a shameful whimper as more cum floods your mouth, your pussy squirting again as you can feel your body changing.");
			writeText("Suddenly she screams like she's trying to deliver a baby, her body shaking in an orgasm as you suck harder. This time as you suck, instead of more sperm, you're rewarded with a pair of fat shemale balls plunging out of her now-destroyed cunt and into your mouth.");
			writeText("You feel another orgasm coming on as you feel the balls throb in your mouth. Inside are the content of this girl's old womb, her identity as a woman, being turned into an infectious sludge.");
			writeText("Her clit has become an absolutely monstrous cock, and soon you'll be changing the same way.");
			if (data.player.currentScene != "gallery") {
				writeTransition('rainyDayZ', 'GAME OVER', '#FF0000');
			}
			break;
		}
		case "tainted": {
			writeText("The donuts are stale at best, and the glaze is certainly not fresh, but you've been eating nothing but rations for weeks now, so they taste better than anything you can even think of.");
			writeText("One, two, and three are already in your stomach before you start to feel a weird sensation wash over you. Had they gone bad? You try to set the fourth back down, but find you just can't and keep shoveling food into your mouth like a desperate pig.");
			writeText("You can feel the sensation growing, your body changing and your pants becoming tighter. Once you've eaten every one you're bringing the plate up to your mouth and licking the glaze off of it. It's salty taste overpowers your senses.");
			writeText("You feel dazed once the plate is clean, and you look around the room for anything else like it. When you can't find any you feel a pressure coming from your pelvis and you double over.");
			writeText("The sensation throbs and you feel your pussy lips start to spread like you're being fucked. Clenching only increases the pressure, so you relax and hear a muffled *POP* from your pussy and your pants suddenly feel way to tight. You slide them off and undo the clasp on your underwear, and...");
			writeBig("tainted.gif");
			writeSpecial("You now have a dick.");
			writeSpeech("player", "", "Oh no, oh no no no...");
			writeText("You grasp it to make sure it's real, and it is. The sensation of pleasure that hits out as you grasp it ensure that it is 100% a real, fat, dick.");
			writeText("You feel your insides churning and hear a small gurgling sound as suddenly your dick pumps out a torrent of white slime onto the floor, pushing out what remains of your old womb. You should be panicking, but...");
			writeSpeech("player", "", "Sho... Sho geeEEEEWD!");
			writeText("This is a real orgasm, not like the clenching and squirting from before. Once your balls have been emptied of white slime, you can actually see them inflating before your eyes as they start preparing sperm to infect others with.");
			writeText("But you pry your hands away from your dick. The slime on the floor smells FUCKING AMAZING! But you need to stay in control. You've just been infected, but you can still think rationally. Maybe you've infected yourself with a weaker version of the virus? If that's true, you're a walking vaccine now, but whatever the case you still need to get back home. Who knows what further infection will do to you?");
			if (data.player.currentScene != "gallery") {
				data.player.infected = true;
				writeText("[convenienceStore|Go back to scavenging]");
			}
			break;
		}
		case "infected": {
			writeText("You're safe! Every time you skirted death has been worth it. A full month's storage of food and water, all in the safety of your well-barricaded home.");
			writeText("The storm rages outside, but it's muffled by the thick walls. This place used to be someone's bunker, but they never got the chance to use it. They left plenty of things lying around, some more useful than others.");
			writeText("As you take off your gear and lay down to relax, you feel your cock start to harden. You still have one last thing ti deal with.");
			writeText("Luckily you have plenty of time now. You can study your strain of infection, maybe even find some kind of vaccine for the infection.");
			writeText("But more importantly, starting now, you need to practice some discipline. You are the one who controls the dick, not the other way around.");
			writeText("...");
			writeBig("infected.gif");
			writeSpeech("player", "", "Wanna cum, wanna cum!");
			writeText("A whole month in complete solitary with nothing but an insatiable dick to entertain yourself with? You didn't even last a week.");
			writeText("Some of the stuff you thought was useless turned out to be pretty amazing. Now you wouldn't trade those boxes of adult toys for all the food in the world.");
			writeText("The storm ended a long time ago, and you haven't exactly been conserving food or water lately. It isn't your fault though! Why bother saving resources, when it feels so much better just to pump them out as cum instead?");
			writeText("You'll need to leave soon to get more supplies. Something tells you that next time you leave, you'll find something a lot more fun than buttplugs and fleshlights, and you won't want to come back.");
			if (data.player.currentScene != "gallery") {
				writeTransition('rainyDayZ', 'GAME OVER', '#FF0000');
			}
			break;
		}
		case "journalBasic": {
			writeBig("basic.jpg");
			writeText("These are the most common types of zed wandering out there. The virus has transformed them, man and woman, into sex-crazed machines. They don't need to eat or drink, and they're stronger than two men together. I've seen one almost leap a six-foot fence in a single jump.");
			writeText("Their weakness is that they're fucking stupid. They'll slam themselves against a door for hours and never even try the handle. They also need to sleep, and boy do they need to breed.");
			writeText("I've been watching the ones outside the building for a week now. They're disinterested towards each other most of the time, but when they're pent up they'll attack each other or just start stroking themselves on the spot. It only takes about a few hours for them to reach this point too.");
			writeText("Their sperm is how they spread the virus, some prefer to do it anally over orally. But their saliva and body fluids have an almost hypnotic effect and smell. I've seen some of the smarter ones just start pissing and jacking away outside of barricaded buildings until the survivors inside can't take it anymore. They also seem to understand enough to infect any food they can find laying around.");
			writeText("They cum differently too, it's more like they start leaking semen right away, pumping out entire liters before they finally cum for real in really viscous, sticky ropes.");
			writeText("I've found that a couple of air fresheners, swapped out every week can help prevent the smell, so long as nobody here opens a window.");
			writeText("[journal|Finish reading the section].");
			break;
		}
		case "journalHunter": {
			writeBig("hunter.jpg");
			writeText("Dogs infected by the virus. I think any breed can be infected, but I've only ever seen Mrs. Fincher's dobermans in the area.");
			writeText("Like the regular breed of zombies they don't need to eat or drink, but these ones can hunt via the scent of blood. They can track down people from a pretty huge distance away, and their speed has greatly improved too. I once saw a survivor driving a car through town. A hunter was able to catch up to the car as it was driving and jumped on top. It thrust itself through the open window, but I didn't see what happened after that. If they're like the regular zombies, it probably started spraying semen all over the inside of the car.");
			writeText("I've noticed that they're actually a lot less aggressive than normal zombies if you act submissive. Mrs. Fincher didn't even try to fight back, so the hunters were really gentle with her. That is, before she finished turning and started begging for rougher treatment.");
			writeText("I think they've moved on to the forest now, so there shouldn't be any hunters in the city at the moment.");
			writeText("[journal|Finish reading the section].");
			break;
		}
		case "journalSiren": {
			writeBig("siren.jpg");
			writeText("These fuckers are the most dangerous types I've ever seen. Normal zombies are tough to deal with, but they have weaknesses. These ones are smart, really smart. They can open doors, use tools, climb up ladders, the works.");
			writeText("They aren't anywhere near as strong though, I think I could take one in a fight if I had to, but the most dangerous part of them is their pheromones.");
			writeText("See, their bodies are way softer, and their dicks are so small I don't think they even can get erections. What they do is emit pheromones that turn men into sex fiends, and try to seduce the men into fucking their asses. They infect the men like an STD while still pumping spunk all over everything.");
			writeText("Not only that, but they can make this weird screech sound. It hurt like a glass shard in the head, but for some reason whenever a man heard it they started running out of cover towards her.");
			writeText("They got Tommy, so the building is compromised. I was able to stay safe and eventually everyone else left the building. Sirens still get horny like normal zombies but they can't cum from their dicks, so they need to keep an entourage with them when they aren't hunting. When they find prey they'll ditch their fuckbudies, who are still covered in pheromones.");
			writeText("Now, Tommy wasn't exactly what I'd call a 'tough guy'. Honestly he was a do-no-harm sort of pacifist, pretty rare these days. I guess that's why he turned into a girly-looking one unlike the rest.");
			writeText("The one that got Tommy was headed to the south last time I saw her, maybe she's heading to that unfinished place at Ridgewood?");
			writeText("[journal|Finish reading the section].");
			break;
		}
		case "journalWorms": {
			writeBig("worms.jpg");
			writeText("I've never actually seen one of these, but they sound terrifying. Supposedly they're the cause of the virus, they're tiny worms that crawl inside of your vagina or dick and start to transform you from the inside-out.");
			writeText("The guy who saw them said his house was infested with them. They got into his wife while they were sleeping, and it only took a few seconds once one got into her worm for her to stop crying and start squirting all over the bed. They got his son too, they slithered into his balls and while he said it hurt at first, his son's cock started pumping out load after load of thick cum. He got them out of the house but they started to turn into zombies. He said he just left one night while he heard his wife fucking his son in the ass...");
			writeText("He brought it up that they love the scent of lemons, which is the same scent as my air fresheners. Luckily he's from a town over, and they aren't the most mobile of types.");
			if(data.player.cityBattle == true && data.player.infected != true) {
				writeText("You hear a sound from behind you and turn around. While you were reading, the [cityBattle|zombified previous resident] escapes the closet and is attacking you!");
			}
			else {
				writeText("[journal|Finish reading the section].");
			}
			break;
		}
		case "journalFlower": {
			writeText("<i>This note is written on a torn-out crumpled piece of paper.</i>");
			writeText("Alright, this is going to sound like bullshit, but I swear it's true.");
			writeText("I saw McKenzie on her way back from a scouting mission, she said she felt a bit off after smelling some weird flower. It was strange that she'd be out and about after a mission, since everybody usually can't wait to get their rations and lock themselves up in their rooms after going out.");
			writeText("I tailed her for a bit and she actually went back out. I saw her in the streets start to strip. She snuck around and found an infected bitch, and she started jilling off right there!");
			writeText("I know this sounds like she just got a strong whiff of some infected jizz, or maybe she had some kind of a danger fetish, but I swear she wasn't like that.");
			writeText("She had a real look of terror on her face as she stripped, and she saw me just before the infected noticed her. She mouthed 'Help Me'.");
			writeText("It keeps playing in my head, so I just needed to get it out onto paper. I'm trying to keep these logs realistic and practical, but I just can't explain this one. Between this and the worms, I got no idea what's going on. If you're reading this, God help you, I don't have any answers.");
			writeText("[journal|Finish reading].");
			break;
		}
		//The Facility
		case "start": {
			writeBig("player.jpg");
			writeText("It all happened so fast.");
			writeText("There was no blaring alarm, no warning broadcast, nothing official to warn everyone. All you had were an alert on your phone from a close friend telling you to be ready to fight for your life, a squadron of armed soldiers bursting into your office for a 'surprise inspection', then the lights went out.");
			writeText("Blind panic hit your assistants, they didn't last long. They ran out of the room and were taken immediately. The soldiers followed soon after, and now you are alone.");
			writeText("But you're not like them. You're brilliant, and willing to do whatever it takes. From all the evidence you have, it's clear exactly what's going on, what pathogen is breaching containment.");
			writeText("So you get a vial from cryo-storage, a green fluid inside and a biohazard warning on the front.");
			writeSpeech("player", "", "I'm not ending up like the rest of them... I didn't sacrifice everything to lose my mind.");
			writeText("Taking the final plunge you down the vial and double over just moments later.");
			writeSpeech("player", "", "Hagck... Fuck, it's awful.");
			writeText("The viral agent you swallowed works its way through your body, infecting you with a weaker version of the virus running rampant outside.");
			writeSpeech("player", "", "Kggh~!");
			writeText("From the awful taste you were expecting this to be a lot more painful.");
			writeText("Instead it's a strong, but not unpleasant pressure on your womb.");
			writeSpeech("player", "", "Focus... focus...");
			writeText("There's a stretching sound, then a tearing sound as your clit strains against your now too-tight stockings. You feel your pussy dilate as something is trying to push it's way out.");
			writeSpeech("player", "", "Gotta stay... In... Cont-");
			writeText("And finally it happens with a muffled *POP*, what remains of your womb descends and a pair of brand new testicles burst free as your stockings are torn to shreds.");
			writeText("Your skin burns and itches, every but of your body trying to fight back against the infection, and your mind is fighting too.");
			writeSpeech("player", "", "Guuuddd~! Fuhhhck~!");
			writeText("You tear off your clothes, throwing every piece as far as you can to escape the feeling of confinement.");
			writeText("Naked and writhing on the floor, you feel on the edge of defeat, of succumbing, as you feel your new balls pulse with life.");
			writeBig("start.gif");
			writeText("A heavenly feeling overtakes you as you feel your humanity drain from your cockhead. First a trickle, then globs from your pisshole landing in the ground with a wet *splat*.");
			writeText("With your body ravaged, even if you manage to obtain the cure and revert the changes, any hope you had of bearing children is pumped out of your new dangling nuts.");
			writeText("Your vision wavers and fades. All your energy devoted to fighting the virus, you let the darkness overtake you.");
			writeText("...");
			writeText("Slowly, you come too. The first thing to take your attention is not the sight of the dark room or the musky scent filling the lab, but the tingling of the cold air on your morning wood. Your plan was a success. Infected have already been through here, but they chose to ignore you. Not only that but you have full control of your mind as well!");
			writeText("Well, maybe not full control. It feels like a massive chunk of your brain, one previously devoted to pushing the boundaries of bioscience, has been smushed and relabeled as 'pump cock and splurt cum'.");
			writeText("The facility's lockdown has failed. Top priority is to find a cure for the virus before your body loses the battle and it completely consumes your mind. After that, escape. The infected won't ignore you for long now, if you've been out for very long they're likely starved enough to not care that you're already one of them.");
			if (data.player.currentScene != "gallery") {
				writeText("[It's time to get moving.|weaponLab]");
			}
			break;
		}
		case "mindWorms": {
			if (data.player.wormDisabled == false) {
				writeText("You know this is the wrong place and absolutely the wrong time, but you can't hold yourself back anymore. You NEED to cum, and you need to cum now.");
				writeText("You take a seat on the stained chair and start jerking off. The danger of it all is intoxicating, arousing, until you feel something touch your hand. You lift it to get a closer look.");
				writeBig("worm-1.gif");
				writeText("a small worm extends it's tentacles over your skin, and your body goes slack as a paralytic chemical spreads through your bloodstream.");
				writeSpeech("player", "", "<i>Fuck! Fuck fuck fuck, no!</i>");
				writeText("The worm slithers up your arm, your shoulder, your neck, and then the side of your face. All you can do is look around the room in a blind panic.");
				writeText("The worm slithers up to the entrance of your ear and extends its tentacles out again.");
				writeSpeech("player", "", "<i>Shit, I need to-... I need to... </i>");
				writeText("Your panic begins to fade as a new chemical races through you. The worm retracts its tentacles and begins crawling into your ear. It doesn't hurt, it all feels cold, goopy, but mostly it feels pleasant.");
				writeText("The worm slithers into your head, bumping into your brain and extends its tentacles again inside you. It feels around trying to figure you out. Your vision blacks out for a moment, a brief flicker of pain shoots up your leg and then vanishes, your arm jerks up before coming back down onto the arm of the chair.");
				writeText("Finally the worm finds the part of your brain it's looking for and squeezes.");
				writeBig("worm-2.gif");
				writeText("Reflexively you cum as your vision and mind go a bright white of pleasure. It's recognized you as resistant to the virus, so now it's taking away your rationality by force.");
				writeText("Desperately in the back of your mind, the part not bathing in pleasure chemicals, you try to think of a last way out. Of a way you could be useful to the worm. Anything at all to save your mind.");
				writeText("But the worm squeezes again on what's left of your rationality and starts injecting chemicals directly into your grey matter. No longer capable of rational thought, or of any thought at all, you'll sit here spurting and leaking until the worm is finished with you. ");
			}
			else {
				writeText("This scene has been disabled by your fetish settings. If for some reason want to watch it, you can still view it in the gallery after changing your settings.");
			}
			if (data.player.currentScene != "gallery") {
				writeTransition('theFacility', 'GAME OVER', '#FF0000');
			}
			break;
		}
		case "whizzer": {
			if (data.player.wsDisabled == false) {
			writeText("Your lips and throat feel dryer than ever before. You weren't thirsty a moment ago, but now you feel like you could pass out from thirst at any second. With shaking hands you lift the beaker full of yellow fluid.");
			writeSpeech("player", "", "I-it's lemonade. Or tea! That's gotta be... Gotta be...");
			writeText("You lift the beaker to your lips, the smell feels like it's burning your nose and frying your brain. The moment the liquid touches your lips... ");
			writeText("You can't hold back, you start chugging it as fast as you can. It's piss. Warm, infected piss and you can't hold back from drinking a pint of it.");
			writeText("The taste defies words. You brain tells you this should be disgusting, but you don't want to ever drink anything else again. It dribbles down your chin, you feel a pang of sorrow knowing you're spilling but you can't slow down.");
			writeText("It's warm and bitter, you can only imaging how much better it would taste directly from a shemale's cock. Your own dick is fully erect and leaking, and soon you can feel the changes occurring inside you.");
			writeText("A moan is forced from your throat as you can feel an orgasm coming on, causing you to choke and sputter on the flow of piss going down your throat. It gets into your lungs speeding up the infection process, and you keep drinking.");
			writeText("Your cock jerks without you even touching it, soon enough you're leaking a solid stream of jizz from your cockhead. With every gulp the stream gets more powerful, the sperm becoming more watery and less thick.");
			writeText("Eventually you reach the end of the pitcher. You lick the inside as best you can as your cock sprays one last gout of piss and sperm onto the floor, and then you collapse.");
			writeText("You can barely move as the second infection of the night spreads through you, changing you even further. Your balls feel empty for once, but you feel a sudden strong pressure on your bladder.");
			writeBig("whizzer.gif");
			writeText("Wordlessly you shake as your cock starts spraying infectious piss onto yourself and the floor.");
			writeSpeech("player", "", "Gguuuuhhhd~!");
			writeText("It feels like an orgasm but just won't stop. Every time you try to stem the stream it builds up unbearably and feels even better when you relax and blast with full force.");
			writeText("Thoughts of survival and the cure are far behind you. As the stream weakens you feel your bladder working overtime as more piss is ready to be released already. You do your best to hold yourself back, only a small stream leaking out as you stand up.");
			writeText("Piss dribbling down your leg with every step, you begin making your way to the containment lab. The infected in there are going to have a very good drink tonight. ");
			}
			else {
				writeText("This scene has been disabled by your fetish settings. If for some reason want to watch it, you can still view it in the gallery after changing your settings.");
			}
			if (data.player.currentScene != "gallery") {
				writeTransition('theFacility', 'GAME OVER', '#FF0000');
			}
			break;
		}
		case "solo1": {
			writeText("You take a seat to finally relax, the throbbing of your cock has been a constant distraction so far. You lift up the nameplate on the desk. ");
			writeSpeech("player", "", "Sorry... Amanda. I'll be using your desk. I suppose you won't ever be using it again anyways. ");
			writeText("You're about to start when something pink catches your eye in a partially-open drawer.");
			writeSpeech("player", "", "Oh my...");
			writeText("It's a dildo, a pretty decently sized one too. Work here can involve subjects that are a little... Arousing, for some to work on. Since being distracted can be dangerous, workers are encouraged to satisfy themselves regularly.");
			writeSpeech("player", "", "Still, a dildo in your drawer? Hmm... <br>I've never done anal before, but with this new body it might be fun. I'll give it a quick shot and go back to jerking off if it hurts.");
			writeText("...");
			writeBig("solo1.gif");
			writeSpeech("player", "", "Ah~! Ah~! Fuck, this is so much better than my pussy was~!");
			writeText("You'd always wondered why some men were so into anal. Some would get downright religious about it. But every time the toy hits your bitch button of a prostate, you get a little closer to understanding.");
			writeSpeech("player", "", "Fuck yessss~!");
			writeText("You start cumming hard, splurting a glob of your infected jizz onto the floor every time you pump your toy into your asshole. Towards the end you can't even muster the force to keep jerking your cock, focusing all your attention on slamming your prostate even after you stop cumming.");
			writeText("...");
			writeSpeech("player", "", "Alright, I need to go. No matter how good it felt...");
			writeText("At some point you managed to stop and clean the cum off yourself. You're satisfied now, even if it did take a few more anal sessions after the first. ");
			if (data.player.currentScene != "gallery") {
			writeText("[Get moving|chemLab]");
			}
			data.player.horny = false;
			break;
		}
		case "solo2": {
			writeText("Study the layout of the room carefully to make sure there are no surprises, you take a seat on a chair. Only a single slow stroke of your new cock and you're rock hard, you can't hold back any longer. ");
			writeText("The infected are becoming more aggressive in their containment cell, pent up and hungry, even for someone who's already infected. ");
			writeText("Electric sensation runs up your spine as you jerk yourself off in front of all of them. Yet it isn't enough. ");
			writeText("You feel your heartbeat in your ears as you decide to play things a little risky and push a button on the console. The clear plastic shield lifts, meaning that the only thing between you and the infected is a set of iron bars.");
			writeBig("solo2-1.gif");
			writeText("One puts on a show for you, eagerly waving her cock side to side to entice you.");
			writeSpeech("player", "", "Ghh... This is so wrong... I'm jerking off in front of a bunch of... <br>Fuck, I don't have much longer...!");
			writeBig("solo2-2.gif");
			writeSpeech("player", "", "Nnnhg!");
			writeText("Your legs shake as you paint the floor with your infectious spunk, filling the room with the heady scent of jizz. The infected are even more riled up now. You move to press the button to close the outer shield, but the large red button next to it...");
			if (data.player.currentScene != "gallery") {
			writeText("A simple slip up, [pushing the wrong button and letting them all out|hordeRelease]. Nobody would judge you for an honest mistake like that, right?");
			writeText("But you've got to remain in control and close the outer shield, then you need to [get back to finding the cure|containmentLab]. ");
			}
			data.player.horny = false;
			break;
		}
		case "releaseTheHorde": {
			writeText("Your finger slipped. That's what you'll tell yourself. It doesn't matter any more anyways, the iron bars are raising and the horde is set loose. Pent up and teased, each of them want nothing more than to turn you into their cum-sock, and they aren't interested taking turns.");
			writeText("They grab you and throw you to the ground, your cock already hardening again at the rough treatment. ");
			writeSpeech("player", "", "Ah~! Yes~! I wanna be your bitch, I don't wanna think anymore~! Cock~! Co-Mmph~! ");
			writeBig("horde.gif");
			writeText("They pile on to you. As many as your holes can take all at once, then more. The horde is angry, the ones who can't fuck you yet begin smashing the console in frustration, you quiver at what it'll feel like when they do that to you.");
			writeText("Alarms go off as more cages and doors around the facility are unlocked. After a moment of hard, bareback sex with the infected shemales, more infected begin to pile into the room. They're all hungry, and you're on the menu.");
			writeText("Your chances at reaching the cure are long gone now. You threw away your humanity into a jizz puddle, there's no turning back. ");
			if (data.player.currentScene != "gallery") {
				writeTransition('theFacility', 'GAME OVER', '#FF0000');
			}
			break;
		}
		case "mindBreak": {
			writeHTML(`
				t The moment the first drip of the mixtures touch, you realize this was a bad idea. The liquids meeting fuzz and bubble, before exploding outwards in a cloud of gas. 
				t You cough and run back, but you definitely inhaled a lot of the stuff. As you clear your lungs and wait for the cloud to dissipate, you feel... Different. 
				t Your body is the same, your cock dangling between your legs and your fat nuts twitching as they prepare a load of infectious cum for an unassuming woman. 
				t ... Why did that thought pass your mind? You squint and rub your temples, trying to focus. Yet all you can think about is... 
				im mindBreak1.gif
				t Chasing down some woman, forcing your cock between her legs until her screams of terror are replaced with begging for more. 
				t You stumble backwards, trying to regain a hold on your sanity. Yet the only thing on your mind is... 
				im mindBreak2.gif
				t The image of someone you've hunted down. Once a proud man, now someone near the end of their infection process gleefully taking your cum as his body changes. 
				t Your mind is slipping away, becoming more primal. The scientist is gone, you are a regular infected now, someone on the hunt. 
				t But there's no one here left to hunt, and your ruined brain can't think of a way out. You are stuck here, maybe forever. 
				t Oh well, you can jerk off for as long as you want at least. 
			`);
			break;
		}
		case "cockBreak": {
			writeHTML(`
				t The moment the first drip of the mixtures touch, you realize this was a bad idea. The liquids meeting fuzz and bubble, before exploding outwards in a cloud of gas. 
				t You cough and run back, but you definitely inhaled a lot of the stuff. As you clear your lungs and wait for the cloud to dissipate, you feel... Different.
				t Your cock is rock hard between your legs, yet you look down at it and feel... Dissapointed. You reach down and grasp it, stroking it up and down. 
				t Yet your grip is rough, ineligant, like you're trying to punish yourself. You stroke faster as your balls clench up, before you pull your hand away and thrust at the air. 
				t Without any kind of stimulation the orgasm is ruined. It'll allow you to cum, but without any relief from the sexual hunger overtaking you. 
				im cockBreak.gif
				t As your cock spews out it's last rope of cum it sags, noticably smaller than before. It looks almost whimpy, pathetic like this, but for some reason you can't help but feel happy. 
				t You reach down to grasp the head between your index finger and thumb, stroking yourself again. Your hips jerk as you coo, and you feel yourself about to cum even faster this time. 
				t Suddenly you pull your hands away again, this time right on the edge. You can feel yourself about to cum, you're so close. 
				t On autopilot again, you stick out your ass and lift your hand, before... 
				t *SPANK*! 
				sp player; Oooh~! 
				t You spray, no, <b>squirt</b> cum onto the floor, the load much smaller and weaker than the last. You grab and knead your asscheeks, spreading your soft asshole between them. 
				t Your precious little dicky is so soft and small now. With what's left of your mental faculties you remember that there's a supply of containment devices around here. The thought of what you'd look like all caged-up and desperate...
				t Oh no! Just the thought of your tiny clitty all locked up in a cage got you all excited again. It's time for another punishment session! 
			`);
			break;
		}
		case "cockMind": {
			writeHTML(`
				t The moment the first drip of the mixtures touch, you realize this was a bad idea. The liquids meeting fuzz and bubble, before exploding outwards in a cloud of gas. 
				t You cough and run back, but you definitely inhaled a lot of the stuff. As you clear your lungs and wait for the cloud to dissipate, you feel... Different, yet you aren't sure where.
				t Your wide hips are normal, your ass is still fat and jiggly, your cock is the same, long size. As you brush your fingers over your lips though, you realize how you've changed. 
				im cockMind1.gif
				t You lips are much larger, and way more sensitive. Just rubbing your fingers over your lips... No, your fat, plush fuckpillows, is enough to send a shiver down your spine. 
				t You realize you're walking out of the chemical lab. You're worried your mind is being affected by the mixture, but you're probably just checking for some clues to escape. 
				t You open the door of the containment lab and take a good, long look at the infected in captivity before licking your lips. You're probably just hungry. 
				t You walk straight over to the control console. You're probably just checking for any hidden keycards. 
				t You press the button to open the containment cells. You probably have some kind of plan, right? 
				t You blow the biggest looking one of them a kiss as they approach you from all sides. You're... Probably... 
				... 
				im cockMind2.gif
				t From the very first moment one of their cocks touched your newly altered lips, you were hooked. Your only worried that one of them would use your ass instead, leaving you one less load to slurp down. Luckily every single one seems to be completely hypnotized by your lips. 
				t The one you're sucking off squirms and moans. She seemed like the dominant type, but the moment her cock was between your lips she was buckling at the knees. 
				t You must be generating some kind of pleasure drug from your lips, but that's not what you're focused on anymore. All that matters is that your mouth feels amazing! Every thrust into your mouth is like firecrackers in your brain. Your cock splurts onto the floor, completely untouched. Your mouth is the only sexual hole you need any more. 
				t But the other girls are starting to get antsy. At this rate you'll need to let two, maybe even three of them fuck your mouth at once. 
			`);
			break;
		}
		case "cockRock": {
			writeText("You take the cure vial and an injector. Carefully, you align the needle and pump the vial's contents into yourself.");
			writeSpeech("player", "", "Khh...");
			writeText("You nearly topple over. A second round of body shifting is taking its toll on you. The cure systematically eradicates traces of the infection in your body, then reconstructs you to your older self.");
			writeText("Clutching your stomach you fall down to your knees. Your balls, which dangled down at least two inches before, now tightly press against your taint.");
			writeHTML(`
				t You bite your lip as cum is forced out of you. Your balls being squeezed tight as they're forced back into your body. Your cock pulses and throbs, returning to the size of your normal clit but retaining the sensitivity.
				t Then suddenly your eyes shoot wide open. You grit your teeth and another load fires from your urethra covering the floor in even more of your nut sludge. 
				t You writhe on the floor as your cock is burning in white-hot pleasure. You try to grab it instinctualy, and you realize it's even bigger than before. 
				im rockCock.gif
				t The sensations won't stop, and neither will the growth. In the back of your mind you realize that the unstable mixture you exposed yourself to earlier is almost certainly the cause, but there's nothing you can do now. 
				t It takes every bit of your willpower to pull your hand away from your cock. You forcibly try to control your breathing. Even if the innoculation failed, even if you are stuck with this fact dick for the rest of your life, you still can remain in control. 
				t Until your cock says otherwise. Even without any kind of stimulation, you feel another rush of cum building up, your balls tightening, and what's left of your willpower spurt out of your penis in the form of sticky, white sperm. 
				t Your cock surges again, becoming even larger and more sensitive. If you couldn't resist before, there's no chance now. You splurt again, the load actually taking a few seconds just to make it all the way up your towering dick. The second to last thought that passes through your mind is "this is the end". 
				t After that, the last thought you have is "COCK! CUMMING! SPLURTING!" 
			`);
			if (data.player.currentScene != "gallery") {
				writeTransition('theFacility', 'GAME OVER', '#FF0000');
			}
			break;
		}
		case "cure": {
			writeText("You take the cure vial and an injector. Carefully, you align the needle and pump the vial's contents into yourself.");
			writeSpeech("player", "", "Khh...");
			writeText("You nearly topple over. A second round of body shifting is taking its toll on you. The cure systematically eradicates traces of the infection in your body, then reconstructs you to your older self.");
			writeText("Clutching your stomach you fall down to your knees. Your balls, which dangled down at least two inches before, now tightly press against your taint.");
			writeText("You bite your lip as cum is forced out of you. Your balls being squeezed tight as they're forced back into your body. Your cock pulses and throbs, returning to the size of your normal clit but retaining the sensitivity.");
			writeText("The fog over your mind finally begins to fade and a sweet happiness floats onto you as your womb begins to take shape.");
			writeText("After the pressure and the cold sweat have ended, you stand on wobbly legs before rubbing between them.");
			writeBig("infectionCure.gif");
			writeSpecial("You are no longer infected!");
			writeText("It's not a perfect change. You'll need some seriously potent sperm to get pregnant given how damaged your eggs are, but at least you have a womb again. With no time to waste you get to work. Now you'll need to escape the compound. Getting infected again would likely be permanent, you can't take any risks.");
			writeText("...");
			writeText("You grab a wire and snap it clean out of place. The lights shudder before snapping off and the room is bathed in white light. The lab's doors shudder and unseal, and there's a loud BANG containment cell from the containment cell behind you");
			break;
		}
		case "failure": {
			writeHTML(`
				t You... Can't seem to move. 
				sp player; Shit... Shit shit shit... What is wrong... With... 
				t You nearly topple over as your mind races with equal parts panic and lust as you realize what's going on. 
				t You mave have cum when you drank the innoculation potion, but you never truly relieved yourself. All that pent up lust that came with your transformation into an infected shemale, combined with the lust of your newly highly sensitive pussy... It's all too much. 
				im failure1.gif
				t With reckless abandon you realize your only answer is to cum. <b>Hard</b>. You stroke your cunt, fingering it hoping you'll achieve release before the Alpha... 
				t Your brain stops thinking as a powerful musk fills your brain. You turn your head and see her feet, and you intended to let your gaze travel up to her face, but the moment your eyes rest on her mammoth, 15 inch cock... 
				im failure2.gif
				t You squirt, spraying the floor. You're cumming harder than you ever have in your entire life, and you know it isn't because of your fingering. 
				t She steps forwards and you pant tongue-out in desperate submission. 
				t ?fetish ws; She chuckles, a deep, throaty noise that makes your pussy clench. She lifts her half-hard titan of a dick and sighs. 
				t ?fetish ws; Your fingers never stopped stroking, and only grow faster as you realize what's happening. A thick, yellow fluid sprays from her cock as you squirt again putting your previous orgasm to shame as she marks you. 
				t ?fetish ws; Her piss shouldn't be infectious, shouldn't be addictive, shouldn't be turning you into a mewling, squirting bitch, and yet only one thought reins supreme in your mind. 
				t <b>Your Alpha is here.</b>
			`);
			if (data.player.currentScene != "gallery") {
				writeTransition('theFacility', 'GAME OVER', '#FF0000');
			}
			break;
		}
		case "alpha1": {
			writeText("The door slides open and you race into the chemical lab searching for a place to hide, the only realistic hiding place is underneath a desk. You get beneath one away from the door, now all you can do is wait.");
			writeText("The door starts to slide closed, but you don't actually hear it seal. Rather, the sliding is interrupted by a metallic whir and the crunch of a broken motor, before it's ripped off its track and tossed aside.");
			writeText("<b style = 'font-size: 200%;'>\"HUNGRY.\"</b>");
			writeText("The voice booms through the lab. You pull in your knees draw yourself tightly into a ball as the sound of footsteps grows closer.");
			writeText("Your heart is pounding, but there's something in the air that's making your thoughts feel hazy. Her voice keeps echoing in your head, and despite your fear you reach between your legs.");
			writeText("Terror and panic hold you back from moving quickly, but you are absolutely soaked. You do your best to try and focus on the footsteps as they grow closer still, hoping she can't find you and that she'll get bored of hunting. You'll make a break for it once she leaves, and-");
			writeBig("chemChase1.gif");
			writeText("Every thought running through your mind comes to a screeching halt as she walks by your desk. Your heart is pounding, but not out of fear. It might be something in the air, or a part of her infection, but you lose every bit of self control you still had. Her massive cock, glistening with sweat hangs in front of you.");
			writeText("You take action, reaching out and moving out from beneath the desk. As she sees you, she gives a deep, bemused chuckle that sends a tingle down your spine.");
			writeSpeech("player", "", "It's so... So big, and so perfect... I w-... I need it...");
			writeText("You grasp the massive fuckmonster, feeling its weight in your hands. You lean in forwards and give the length a worshipful lick.");
			writeText("The beast is incredible, the head as thick as an apple. There's no way you could take it, so all you can do is reverently stroke it with your hands and press the head against your mouth. Sticking out your tongue, you french kiss her urethra like you're making love just in time for her to grunt and a single massive glob of sperm hits your tongue and is forced into your mouth.");
			writeText("You try to swallow it right away, but it's just too thick. Your eyes water and your sinuses burn, but you wouldn't dare try to spit it out. All of your work curing your infection fades away as you chew the alpha's ball jelly.");
			writeText("Finally you pull your mouth away from her cock with a lewd squelch, tip your head back, and suck in your cheeks as you force the thick, sticky load down your throat. You can actually feel the distention on your neck as the jizz slides down, infecting you. The heat travels down through your chest until you swear you can hear it enter your stomach.");
			writeText("You lower a hand to start fingering your sopping twat, you won't have it much longer anyway. Having ingested such a thick load of infected sperm, it'll be replaced with a cock within minutes. Already you can feel yourself riding your jizz-fueled high to an orgasm, and-");
			writeBig("chemChase2.gif");
			writeText("<b style = 'font-size: 200%;'>\"HUNGRY.\"</b>");
			writeText("You're snapped out of your stupor and want to slap yourself as you realize how selfish you were being. You belong to her now, and you pull your hand away from your cunt back to her massive dick,");
			writeText("But she isn't interested in worshipful kisses or a slow handjob. She grabs you by the hair and mashes her cockhead against your mouth. It can't possibly fit, you tell yourself, but you don't try to stop her. You are hers to toy with, and hers to break.");
			writeText("But to your amazement you feel your jaw stretching until finally her massive cockhead is forced inside. It stretches your jaw, and then your throat too. Soon enough you're impaled on her throbbing cock and she grabs you by the neck to pump her fuck-baton in and out of her new jizz-sock.");
			writeText("She lets out another deep grunt. That first load was just precum, just enough to prepare you to accept her monstrous length. Now you can feel the full effects of her cum as her balls tighten and throb.");
			writeText("As you fondle and squeeze her massive balls you feel your gushing pussy get slammed by the hardest, and last orgasm it'll ever have, all completely hands-free.");
			writeText("Tears run down your face as the massive distentions of jizz stop at your lips, your mouth is unable to open any further to accept her wonderful gift. But each time her balls throb her jizz spurts push harder until finally you hear a 'pop' and your jaw relents.");
			writeText("What must be pounds, gallons of jizz travel down the length of her cock. Much thicker than her precum. Ropes of the infectious stuff are pumped into your stomach to meet the load of pre you already swallowed.");
			writeText("You can feel your mind fading, but you fight harder than ever to remain conscious and in control. You're the only one in the facility who can work machinery.");
			writeText("You're the only one left who can help your alpha escape.");
			if (data.player.currentScene != "gallery") {
				writeTransition('theFacility', 'GAME OVER', '#FF0000');
			}
			break;
		}
		case "alpha2": {
			writeText("The door slides open and you race into the containment lab searching for a place to hide, and you immediately move for the crawlspace. Nothing big enough to break through a containment wall like that would fit through a space like this.");
			writeText("The only problem is you can't fit through it either.");
			writeSpeech("player", "", "Fuck, fuck, fuck! Nonono!");
			writeText("You've got your body up to the waist in the vent, the problem is that your lower body is still sticking out. You inch and wiggle your way in further, you'll figure out how to escape later, but for now-");
			writeText("There's a loud BANG on the containment lab door. The infected in their cells are going crazy over your naked body, and it's got the attention of whatever's out there.");
			writeSpeech("player", "", "Almost... Almost... Yes!");
			writeText("You manage to get unstuck just as you hear the containment lab door explode open. You crawl further into the vent, just a little further and-");
			writeText("You feel your heart jump as a strong hand grasps you by the ankle.");
			writeSpeech("player", "", "No! No!");
			writeText("You're forcefully dragged out of the vent until the alpha infected has what she wants. Your upper body still in the crawlspace she roughly jabs her cockhead against your pussy.");
			writeText("Your skin tingles, even her sweat is enough to start the infection process and haze your mind. But even still the fact of the matter is that she's simply way too big. All she can do is rub her cockhead against your twat, but it's enough.");
			writeSpeech("player", "", "Ghhhhg~!");
			writeText("In the darkness of the vent you can't see anything, all you can do is focus on your convulsing cunt as a load of infectious slime covers your pussy and ass. Thoughts of escape and survival as a human are beginning to fade away. Everything fades, until you feel her cockhead pressing against your pussy again.");
			writeSpeech("player", "", "S-stop~! You're too.. Too... NNNNHG~!");
			writeText("You feel like you're being tazed, you can hear your heartbeat in your head as her cock spreads your pussy. Her sperm must already be altering your body beyond hope of repair. You'll never be satisfied with anything less again.");
			writeBig("containmentChase.gif");
			writeText("Every thrust is a battle, even for the alpha. A human's cunt wasn't meant for this kind of treatment.");
			writeSpeech("player", "", "YESH~! HARDER~!");
			writeText("But you aren't human anymore. Less than halfway in and her cockhead bumps against your cervix. You can feel a distention on her cock going down your cunt, stretching you even more, until your womb is suddenly packed full of alpha infected cum.");
			writeText("It won't stay that way for long. As your womb is attacked by the virus, the alpha pulls out from your pussy leaving you a gaping mess, and aligns her cock with your asshole. Your consciousness begins to fade, but that's okay.");
			writeText("You'll probably wake up to the feeling of her load in your ass.");
			if (data.player.currentScene != "gallery") {
				writeTransition('theFacility', 'GAME OVER', '#FF0000');
			}
			break;
		}
		case "alpha3": {
			if (data.player.rimDisabled == false) {
			writeHTML(`
				t You dive into the weapon lab, hoping the alpha didn't see you. 
				t As you hide in a small cabinet, the infected woman tied up in a testing cell begins to tug at her bonds. Good, she should work as a distraction. All you need to do is hope the alpha isn't intelligent enough to realize she's been tricked and you're home f-
				t <b style = 'font-size: 200%;'>"HUNGRY"</b>
				t The voice shakes you to your core, and you almost cry out on impulse. You hear the heavy footfalls of the alpha move past you, and you open the cabinet door just a peek. 
				t The infected woman squirms and you hear a small snapping sound as she finally breaks free of her restraints. Pent up and in a frenzy, she charges at the alpha. 
				... 
				im weaponChase.gif
				t Thrust, clench, pull back. 
				t Thrust, clench, pull back. 
				t <b>Thrust, clench, pull back. </b>
				t You'd swear you can hear every splurt of alpha cum filling up the infected woman's ass. You can <b>SEE</b> the distention of the alpha's cockhead bulge in the infected woman's body, and you can see the way the bulge inflates for just a moment with every rope of sperm fired. 
				t But once the flow stops, the Alpha just resumes thrusting. No cool down, no waiting. The infected woman has either accepted her fate or fainted, you can't hear her struggling anymore. All that's left of her is a twitch of her legs as she paints the floor again. 
				t You can't hold yourself back anymore as you push open the cabinet door and crawl out. Part of you is screaming to escape, to get to the elevator, but... 
				t You aren't crawling towards the exit. 
				t The infected woman can barely be seen. The alpha's massive balls slamming into the much smaller pair, you'd be suprised if the infected could fuck anything with her dick after the alpha is finished. 
				t You whimper as you struggle to stop yourself. Your eyes locked on the meaty ass making the infected woman its bitch with every thrust. You whimper reflexively. 
				t The alpha notices you and reaches back with her strong hand and grabs you by the hair like a cheap whore, before yanking you forwards to press your face against her asshole.
				t Your cunt throbs and your womb feels like jelly as you realize the faster you help the alpha to cum... 
				t The sooner you'll be next. 
			`);
			}
			else {
				writeText("This scene has been disabled by your fetish settings. If for some reason want to watch it, you can still view it in the gallery after changing your settings.");
			}
			if (data.player.currentScene != "gallery") {
				writeTransition('theFacility', 'GAME OVER', '#FF0000');
			}
			break;
		}
		case "spider": {
			if (data.player.beastDisabled == false) {
			writeText("The door slides open and you race into the parasite lab searching for a place to hide, the desks are all covered with strange substances. You run around the room searching for any form of escape, even running into a containment cell.");
			writeText("With lots of suspended rock formations, this must have been made for some kind of animal. For now you put that aside, hoping the alpha chasing your can't climb.");
			writeText("You make your way up and into some kind of small cave, or crawlspace. The floor and walls are lined with extremely sticky white ropes.");
			writeSpeech("player", "", "Fuck... I wish I were wearing... Anything, really.");
			writeText("You barely hear the parasite lab door smash open. You should be safe but you keep crawling forwards just in case.");
			writeText("Suddenly, a rope wraps around your face covering your mouth. Your arms and legs follow and are pulled taut when the sticky ropes on the floor lift a few inches off the ground.");
			writeText("You hear the sounds of inhuman legs scuttling around you as another rope, they must be some kind of webbing, wraps around your face covering your eyes.");
			writeSpeech("player", "", "Mmph! Mmmpfh!");
			writeText("Desperately you try and struggle, bite, and gnaw through the webbing, only to find your muscles start to relax and your heartbeat slow. The webs taste...");
			writeSpeech("player", "", "<i>Good?</i>");
			writeText("There's some kind of substance in the webs, your eyes lid over as you swallow some kind of intoxicating substance. Continued munching relaxes you further, even as you're turned over and dragged down the tunnels.");
			writeText("At this point there's no chance of rescue anymore. You're pulled into a large room completely coated with webbing as small spider-like creatures scurry about between large web sacks hung from the ceiling.");
			writeText("Every so often you notice one of the sacks let out a moan and shudder.");
			writeSpeech("player", "", "Mmph... Mpph...<br><i>More... Moreee...</i>");
			writeText("You're hoisted up and covered in more webs, every part of your body cocooned except for your ass and pussy. You can still breath, but the air filtered through the webbing is intoxicating.");
			writeText("You feel one of the spiders crawl up to your rear and something long and thin presses against your pussy. You draw a sharp breath as it pushes inside you filling your lungs with more of the addictive narcotic.");
			writeText("The spider's length pushes farther inside of you, just enough to lightly tap the entrance to your womb, before the length thickens and you feel a liquid being pumped into you.");
			writeSpeech("player", "", "Mmmmph~!");
			writeText("In your drugged state you can't struggle, but even the webbing can't hold back your moans as lightning flashes in your brain. It must be a super concentrated version of what's in the webs, injected directly into your cunt.");
			writeText("Soon after the flow stops and the length thins back down. You let out a whine before a large bump hits your cuntlips. Something's being sent down the spider's ovipositor, at least three inches wide. You're stretched out as an egg is forced into your cunt, traveling down until you can feel it press against your cervix.");
			writeText("More eggs travel down the length, putting pressure on the first as it presses against the entrance to your womb. The drugs having relaxed your body, eventually you have no choice but to relent and allow the egg inside.");
			writeText("Then another. And another.");
			writeSpeech("player", "", "Mmmph... <br><i>MORE! MORE!</i>");
			writeText("The drugs race through your system preventing you from even the slightest movement. More eggs are stuffed into you until no more could possibly fit, and the spider begins to pull out. All the eggs still inside the ovipositor are squeezed out into your cunt, and once the spider has fully withdrawn some of them fall out with a *plop*");
			writeBig("egg.gif");
			writeText("Barely awake, your consciousness fades as you feel another ovipositor press against your asshole. ");
			}
			else {
				writeText("This scene has been disabled by your fetish settings. If for some reason want to watch it, you can still view it in the gallery after changing your settings.");
			}
			if (data.player.currentScene != "gallery") {
				writeTransition('theFacility', 'GAME OVER', '#FF0000');
			}
			break;
		}
		case "fireteam1": {
			writeHTML(`
				t "So, we got any more info on the situation down there?" A woman in full body armor asks. The elevator she's riding in is quite cramped, as two other heavily armed figures are riding with her.
				t "None. We get in, activate containment procedures, ensure Alpha is still locked up, and get out."
				t The elevator door opens. 
				t The footage glitches, static covers the screen. 
				... 
				t "Captain... I can't... Please, don't leave me..." 
				t The speaker, the same soldier as was speaking before, is sitting against the wall, her pants are missing and cum is leaking from her cunt. In front of her a large metal door seals shut.
				t "I can't risk you infecting me, Jones. If you make it through this with your mind intact, the passcode is 828." Then the voice over the radio goes silent. "If you don't... I'll be in there soon to take your tags, and the recordings."
				t "Captain... Captain please, it... Ghh..." 
				t The soldier takes off her helmet and the camera attached to it and throws it aside as she writhes. Her muscles clench, she grunts, and there's a soft wet *POP* sound in the air. 
				t The helmet settles on the ground, allowing the camera to record the soldier. 
				im soldier1.gif
				t The soldier's body has changed. Her womb and clit unrecognizable. Where once a pussy was leaking infected cum, now a bulging cock is leaking infected precum. 
				t "F-fuck you, captain... You fucking... Ice queen bitch... When I make it out..."
				t She strokes her cock as her rambling becomes less coherent. Another hand goes between her legs, circling the rim of her asshole before she begins to finger herself. 
				t "Your husband... I know you peg him, how would he like... A real dick...<br>And that cute son of yours was looking at my ass last time, maybe I'll let him shoot out his first load inside me... Before I turn him into a girlyboy slut!" 
				t As she bucks her hips and begins to cum, spraying the floor and the camera in a thick layer of cum, her bitter fantasies melt away into wild grunts and moans. The audio makes it clear she's still pleasuring herself, but it's also clear her mind is long gone at this point. 
			`);
			break;
		}
		case "fireteam2": {
			if (data.player.wormDisabled == false) {
			writeHTML(`
				t This recording begins in the middle of a frantic battle in the parasite lab, or a different section you have access to at least. 
				t A heavily pregnant woman in a labcoat has pushed down the soldier wearing the camera. The soldier is barely holding her back as she turns towards the lab's door, which has just clicked shut. 
				t "Help! Don't leave me like this, this bitch is-" 
				t The pregnant woman giggles, before she turns her head. Out from her ear slowly slithers a red worm, the woman cooing as the parasite stimulates her brain on its journey. 
				t The soldier closes her mouth and tries to break free, but to no avail. The camera shakes as the soldier struggles, but suddenly she stops moving. 
				t "No, no no no, why can't I move!?" 
				t From the sounds of it the parasite has inflicted her with a paralytic chemical, and has now begun to crawl into her ear. Strange, the parasite should specifically target already infected woman, maybe it's a new mutation?
				t The soldier's scared grunting and sobbing slowly gives way to disorientated moans. The pregnant woman holding her down relaxes her grip and backs off. 
				t The soldier twitches, the mind worm parasite trying to figure out which parts of the brain trigger which reflexes. All the while the pregnant woman giggles and strokes the soldier's cunt to ease the transition along.
				im soldier2.gif
				t The soldier screams out as she's forced to a sudden, squirting orgasm that sprays the floor. The pregnant woman giggles again as her gut wriggles, suggesting that she's already been turned into a broodmother for these creatures. Once the mind worm has it's way, it's likely the soldier will willingly accept the same fate. 
			`);
			}
			else {
				writeText("This scene has been disabled by your fetish settings. If for some reason want to watch it, you can still view it in the gallery after changing your settings.");
			}
			break;
		}
		case "fireteam3": {
			if (data.player.beastDisabled == false) {
			writeHTML(`
				t "You... You sons of bitches! If you think you'll get away with this, you're dead wrong!" 
				t The woman on screen looks seriously pissed off. She's taken her helmet off and is using it as a recording device. 
				t "This is captain Rita Vasquez, I'm the captain of a fireteam for the fucked up organization that started all this shit. I was sent here on a containment job. All my subordinates have already been infected.
				t I completed my mission, but... Those mother fuckers... They're abandoning me. They'd rather just study what's going on in here, using their own employees as 'data points'. They've sent in hounds after me, I don't have much longer. I already infected myself too, I'd rather turn from drinking some vial than getting fucked by some zombie with a dick. It was some research sample called 'Echidna', maybe the hounds will ignore me since I'm infected. 
				t Listen, I know if you're reading this you probably work for them too. Get out. They don't care about your life, the world, or even money. Those motherfuckers just want to see the world burn. But I'm not scared."
				t She holds up a small handgun.
				t "I'm taking them down with me. I'm a warrior, I'll go down with honor, with dignity-"
				t The captain is interrupted as her makeshift blockade is shattered, and barking can be heard from offscreen. The camera is knocked over and glitches out. 
				... 
				t When the video resumes, a much happier-looking captain is smiling into the screen. She looks happy that the camera has resumed recording, and a large hound enters the frame and begins licking her face. 
				t The captain giggles, and begins reciprocating the kiss. It seems like she's already infected. 
				t The dog stops licking and the captain whines, until the dog steps onto the table. Its bright red, fully erect cock dangling between its legs. The captain looks completely hypnotized. She adjusts her uniform and softly gasps as the hound places his dick between her breasts. 
				im soldier3.gif
				t At the height of his thrusts the captain will lean her head forwards to plant a small smooch on the growing knot at the base of her lover's fat dick. The hound grunts and begins to thrust even faster, before slamming down his hips so that the knot rests between the captain's large breasts. 
				t The captain giggles as a sticky sound can be heard, the hound's cock beginning to fill her jumpsuit with infected jizz. 
				t ?fetish rim; She sighs appreciatively as she's marked, before she leans in to worship her new lover's ass. Wet sounds and moaning which would be much more appropriate for a steamy make-out session between lovers can be heard as the captain stimulates his anus with her tongue. 
				t There's another bark from offscreen. It seems as though the pack has been ordered to return to the surface.
			`);
			}
			else {
				writeText("This scene has been disabled by your fetish settings. If for some reason want to watch it, you can still view it in the gallery after changing your settings.");
			}
			break;
		}
		case "domination": {
			writeHTML(`
				t The woman struggles against her bindings. They don't look too strong but they're keeping her in place for now.
				t Unable to even jerk herself off she's clearly pent up and quite angry about it. As her bindings still keep her in place her anger fades for a moment replaced with a look of desperation. She thrusts her hips up and waves her cock in the air, hoping you'll stimulate it or at least fuck her into a squirting mess.
				t But this is about your relief, not hers, so you grab her by the hair and press your dick against her lips. She resists, but only for a moment as you draw your cock back.
				t *WHAP*!
				t You smack her across the face with your dick, she whines and her cock jerks up. Either you just slapped the resistance right out of her body or you just slapped a masochistic fetish into her brain.
				t Regardless she's a beast of instinct now, so all she cares about is that she got a surge of pleasure from that. She pants open-mouthed to show her apology for her earlier rudeness.
				...
				im domination.gif
				t Her eyes and nose water as you relieve yourself with her face. You have a large grin on your face as you grip her by the hair. If someone were watching, they wouldn't be able to tell which of you is the mindless infected. 
				t Dominance like this is... <b>Addictive</b>.
				t Her cock bobs with each thrust, pulsing as if on the edge. Her eyes flutter as her consciousness fades, clearly hungrier for sexual relief than air. You want to see how much longer she can last, but...
				t That's your inquisitive side thinking, it's why you're the best at what you do but you shouldn't play around too long. You firmly tug her by the hair and sigh in relief as your balls throb, and you begin to cum down her throat.
				t You hear something splattering beneath you, she must be cumming on the floor as she blacks out. Of course the poor dear won't be feeling any less pent up if her orgasm is ruined like this. Oh well.
				t You pull out, her body is still limp until she suddenly coughs and gasps for air. She looks up at you, eyes still full of desperate need even as her half-hard cock is flagging, but the only thing you give her is a facial.
				t Well, that was fun, but you should...
			`);
			if (data.player.currentScene != "gallery") {
				writeText("[Get moving|weaponLab]");
			}
			break;
		}
		//Spread Island
		case "frozen": {
			writeText("This must be a dream, it has to be. Some woman, with a dick, stumbles into your hotel room as your wife is out of her mind. You must've gotten wasted, or high, or passed out at some point after hitting your head.");
			writeText("The woman's thick cock spreads your wife's pussy, but only makes it a few inches in before it seems like the girl can't thrust any farther.");
			writeBig("frozen-1.gif");
			writeText("After only a few thrusts the woman, an annoyed look on her face, pulls out. Your wife groans, her eyes crossing as she makes one big push, and...");
			writeText("*POP*");
			writeText("A single, egg-sized ball pops out of your wife's pussy. She moans in relief, before grunting and spasming and...");
			writeText("*POP*");
			writeText("Tears run down your wife's face, ones of relief, and maybe even pleasure. Her clit has fully transformed into a full-on cock and her pussy has become a pair of fat nuts.");
			writeText("You finally work up the nerve to pinch yourself, and the pain is too real for you to pretend isn't there.");
			writeSpeech("player", "", "Please... Please let me wake up... Honey, please tell me this is-");
			writeText("The woman lunges at you, tackling you to the ground. With inhuman strength she begins to tear your clothes off while trying to smear her cock against your face.");
			writeText("You try to fight back, but you've got no spirit left. Everything that's happened up to now has been insane, you can't muster up the will.");
			writeText("The woman lets out a small hiss as her cock leaks a glob of precum onto your check, and you instantly feel much better. The hopelessness and despair seem a lot smaller now, and your head suddenly feels too stuffy to focus on anything except the woman atop you.");
			writeText("She repositions herself as she snaps the button on your pants and starts pulling them off. No longer needing to pin you down, she gets your bottom half exposed quickly.");
			writeText("You relax and close your eyes for a moment ready to accept your fate, until you feel something pressing against your cheek. As you opem them, you see your wife's loving eyes, her gorgeous body, and...");
			writeBig("frozen-2.gif");
			writeText("Her throbbing cock.");
			writeText("She presses her head against your lips, and you relent. At least this way you'll be together forever, just not in the way you expected when you married her.");
			writeText("The woman presses her head against your asshole as you take your wife's cock in your mouth. Whatever happened to them is happening to you too, your body is already changing. You feel softer, more ready for the pair of infected women about to spitroast you. Your mind has begun to fade away, your identity as a human is gone.");
			break;
		}
		case "hotelWifePleasureGas": {
			writeHTML(`
				t You gently nudge wifeF from her half-asleep state. 
				t She's disorientated, exhausted by the toll whatever is going on is having on her body. But more importantly she's still human, at least mentally. 
				sp player; Here, I found this... Thing in another room, I think it might be related to whatever apocalypse thing is going on right now. <br>It's a long shot, but it could be a cure, or some kind of vaccine. 
				t You hold up the small device with the nozzle pointed at her. She looks worried, but she trusts you. She closes her eyes, and you press the button. 
				t A small pink spray is released. wifeF takes a breath hesistantly and her eyes shoot open. She starts taking deep breaths and twitching, leaning forwards as if searching for more. Her cock throbs once, twice, then a third time as it goes from flaccid to fully erect. 
				t She clenches her teeth hard and her eyes cross. Her cock is cumming without the slightest bit of stimulation. 
				im wifePleasureDrug1.gif
				t You hold on to at least some last, small hope that it's just purging the virus from her system until you see her already large dick throb again, it's undeniably larger than it was a second ago. 
				t She looks at you, eyes tinged pink and without a trace of humanity behind them, and lunges. 
				t The next thing you know you're on the ground beneath her, her cock's head still leaking cum and pointed right at your face. She grabs the device from your hand, barely recognizing how close her dick is to an uninfected mouth. And to make matters worse you hear ferocious sounds of banging and clawing at the door. 
				t She nuzzles against it like a cat and catnip, bites at it, scratches it, everything to try and get <b>more</b>, until she finds the button by accident and sprays a large hit of it directly into her face. 
				t She takes a deep breath and makes a sound halfway between a squeal and a giggle before her body starts to shake. Her balls audibly <b>gurgle</b> as you make a last ditch effort to try and free yourself before... 
				im wifePleasureDrug2.gif
				t Too late. A few small shots, the kind you could expect a normal human to fire, before a torrent of infected nut-sludge is seemingly pissed out of her urethra onto your face and upper body.
				t You can barely avoid it getting into your mouth, let alone your nose. And to make matters worse wifeF knows how to use the device now. Holding down the button the room is nearly filled with the pink gas.
				t The pure jizz coating you, the strange pink chemical, it's too much.
				t You start to cum, splurting everything into your pants. Your jizz, your pride, dignity, every intelligent thought you've ever had as a human is splurted out by force as your body is changed and infected.
				t The door breaks down, letting a torrent of infected inside. Attracted by the gas, all they see is a room of pink, and two infected in the throes of orgasm. 
			`);
			if (data.player.currentScene != "gallery") {
				writeTransition('spreadIsland', 'GAME OVER', '#FF0000');
			}
			break;
		}
		case "hotelMainExit": {
			writeHTML(`
				t You make your way back into the room, the sound of you grabbing whatever you need to survive waking up wifeF from her stupor. 
				t She's exhausted from the toll the infection is taking on her body, but she's human. That's all that matters. 
				t You take her by the arm. You'll carry her if you need to, and head out towards the main lobby. 
				... 
				t It's a shitshow down here, people fighting to get in for shelter and people like you fighting to get out. Hotel staff are abandoning ship and looters have already started grabbing everything they can. You hold wifeF close as you force your way through the crowd of terrified people. 
				t "SHE'S INFECTED!" 
				t You aren't sure who said it, but the crowd around you pulls away. The chaos isn't stopping yet though. There's the sound of a window shattering, screams of panic and terror, and wifeF stumbles slowing you down. 
				sp wife; I can't... I can't stop!<br>I'm sorry, all these people...! 
				t She's got an erection tenting her dress, a dead giveaway of her infection, and she's doing her absolute best to control herself but can't stop from stroking her dick through the thin fabric of her dress. 
				t You reach out to help her, to pull her hands away so you can get moving again, but you feel a weight against you as the room goes sideways. 
				t The next thing you know a woman with a crazed look in her eyes is dry-humping you, a fat bulge in her shorts. 
				sp player; wifeF! Help! 
				t She steps forwards on shaky legs looking you right in the eyes. So close to exploding, her willpower on the verge of shattering, the sight of her husband pinned down by some crazy woman with a dick is enough to push her over the edge. 
				im mainExit.gif
				t Biting her lip she can't help but fully commit to jerking off, a large glob of precum splashing against your cheek as the infected woman tears apart your and her clothes. 
				sp wife; I'm sorry! I'm sorry, but I can't stop! 
				t She presses the head of her cock against your mouth as another jet of precum is fired. The other infected has finished clearing the way as well. The precum must already be infecting you, because the feeling of her dick against the rim of your anus makes you want to scream out in pleasure instead of pain. 
				t The edges of your vision go faint and the last thing you see is wifeF's crying face as she bites her lip, the horror of the situation making her cum even harder into your mouth. 
			`);
			if (data.player.currentScene != "gallery") {
				writeTransition('spreadIsland', 'GAME OVER', '#FF0000');
			}
			break;
		}
		case "hotelFreshSiren": {
			writeHTML(`
				t You reach out to hug the girl. Her skin is so soft... She hugs you back, her sobs feel musical to your ears. Everything in the room seems to darken in your vision, except for her. 
				t Dimly you're aware that you're being pushed down, and she parts her skirt to show you the small cock between her legs. 
				t She's infected. You should be scared of this girl who basically has you pinned, scared of the small dick that's about to infect you, yet... It's so cute! 
				t You reach down, unzipping your fly and trying to shimmy free of your pants, and she giggles to reward your newborn loyalty. You hear the door open, and the two of you look up. 
				sp wife; H... Honey? 
				t The girl atop you laughs when she sees your wife, terrified and distraught, and the sound seems to soothe her. Your wife steps forward not of her own accord, her dick at full erection. She appreciates the girl's voice as much as you do. 
				im freshSiren.gif
				t Every one of her moans feels so perfect in your ears. Even her gagging on your wife's dick is like a series of perfect notes to a wonderful song. 
				t Your wife squeals, rudly interupting the girl's voice, but the girl just giggles as her mouth is flooded with cum to let you know that everything is alright. 
				t The girl pulls her mouth off of your wife's cock, her cheeks puffed out, and leans down to you with her lips puckered for a kiss. 
				t Some small part of you is still trying to pull away, and she can see it, but she just giggles to rid you of that last pesky independent thought as your lips meet. Your mouth is flooded with a wonderful fluid you can feel changing you from the inside out.
				t You drink from her. From your wife's balls to her mouth to yours. Your own cock throbs and grows. She hums a playful tune, happy that she now has two toys to keep her satisfied while all hell breaks loose on the island. 
			`);
			if (data.player.currentScene != "gallery") {
				writeTransition('spreadIsland', 'GAME OVER', '#FF0000');
			}
			break;
		}
		case "hotelPleasureGas": {
			writeHTML(`
				t You tug at the hose attached to the wall. It's got a pretty strong bond, but if you just... Pull... Harder...! 
				t There's a sound of rubber breaking before the *KSSSSSSH* of gas as you break the seal. A thick gout of pink gas is spraying from the hose. You cover your mouth and nose, but it's too late. 
				t The gas makes your skin tingle, eyes water, and the small amount you inhaled begins fucking up your body from the inside. You feel a sudden, <b>intense</b> pressure from your nuts as it feels like they're about to burst.
				t You tug at your pants. Probably not the best course of action, but you can't think straight as it feels like the gas is melting away your brain. You gasp, no longer in control of yourself, but you do notice that your voice is higher. It almost has a songlike quality to it. 
				im pleasureGas.gif
				t By the time your pants are off your body has already changed enough that you don't recognize what you see.
				t You feel yourself fall down onto the floor, your cock feels white hot yet there's a cool sensation coming from your urethra, like you're leaking. 
				t Leaking... Leaking... 
				t A giggle escapes your lips. You feel... Smaller... The gas fills your body, replacing what's human with something else. 
				t Soon, there isn't anything human left in your mind or body. 
			`);
			if (data.player.currentScene != "gallery") {
				writeTransition('spreadIsland', 'GAME OVER', '#FF0000');
			}
			break;
		}
		case "hotelAbandon": {
			writeHTML(`
				t You've landed safely in the pile of soft bedding, and stand up unharmed. Looking around you can see a way out of the hotel towards the less populated expensive villa area. If anyone is getting help right now it'd be the richest folk. 
				t You take a step forwards, but can't help but look back up from where you came. 
				t wifeF... 
				t But there's no turning back now. Even if you could get back up she's already infected. Even if you can't live with yourself after escaping alone, you'll be alive to try. 
				t Yet as you make a break for it, you can only imagine what horrible things are happening to wifeF right now... 
				...
				im wifeAbandoned.gif
				t Scores of them have swarmed her, she's given up hope that you'll be back. 
				t As her mind goes blank, only one thing is for sure anymore. Even if you survive your journey off the island the image of your wife will always be on your mind, but your image won't ever cross hers again.
				t But you didn't escape the island. What happened to you remains to be seen, but the dream of escaping together, or even safely, is gone.
			`);
			if (data.player.currentScene != "gallery") {
				writeTransition('spreadIsland', 'GAME OVER', '#FF0000');
			}
			break;
		}
		case "wifeHelp": {
			writeHTML(`
				sp wife; It's not...! Not enough! <br>Please, you mouth, I need it! 
				sp player; But-
				sp wife; PLEASE! You can spit it out, but I <b>NEED</b> to cum! 
				t Your wife, broken and begging with tears in her eyes, convinces you to swallow your pride (and hopefully nothing else). 
				t The moment your lips are around the head of her cock a torrent of spooge floods your mouth. You back off quickly, spitting it out and hoping you aren't infected. 
				t Your mouth feels... Fuzzy. 
				sp wife; Put it back! Please! That was... Ggghh... Just precum! 
				t You look at her dick in disbelief, her precum was thicker than a normal man could shoot out in a full load. 
				t You try to keep focused, trying not to think about how the numbness in your mouth might actually be a plesant buzz, or how much more eager you are to continue than you were a second ago. 
				t You slide your lips back onto her dick again, carefully, slowly you-
				t She isn't having any of it. She grabs you by the hair and starts ruthlessly fucking your face. Another glob of precum fills your mouth and the entrance to your throat. You tap on wifeF's thigh begging for mercy. 
				t And that's when she thrusts deep enough to tap her balls against your chin, and starts cumming. 
				im wifeHelp.gif
				t You can hear the audible splorting noise, you're well past being infected now. But when you look up, the eyes staring back at you don't have a trace of human intelligence, just animalistic, bestial lust.
				t You feel the coolness of the infection overtaking you, and the edges of your vision darken. 
				t ?fetish ws; But just before you black out, you feel her hand stroking your hair lovingly. She sighs, and you feel something traveling down her urethra. The very last sensation you feel before blacking out is your wife's piss flooding your stomach. She moans, as if this is just as good as getting off. 
			`);
			if (data.player.currentScene != "gallery") {
				writeTransition('spreadIsland', 'GAME OVER', '#FF0000');
			}
			break;
		}
		case "wifeDildo": {
			writeHTML(`
				sp player; Here, I found this, I hope it-
				t She grabs the toy from your hand without a second thought, pressing it against her nutsack on instinct, before lowering it down to her asshole. 
				t Your wife had never been a fan of anal before, but... 
				im wifeDildo.gif
				t The sensation of the plastic dick is enough to trick her body, and she's <b>squirting</b> a full stream of cum from her infected cock. You need to keep away as she's spraying over a foot away. 
				t Her eyes are glazed over, like she's ignoring the whole world and focusing only on the thrusting of her anus, and the feeling of the dildo smashing against her prostate as quickly as she can bounce. 
				t As she slowly comes to a stop, her cock flagging and the dildo creating a distention on her abdomen, her eyes flutter closed. 
				sp player; Honey? Honey, are you okay? 
				sp wife; Mmm... Hmm? 
				sp player; Come on, we need to get moving, let's go. 
				t She nods, looking only half awake. She softly coos as she slides off the dildo, one last spurt of cum splattering the floor as she stands up on unsteady legs. 
				t The smell is... Intoxicating, but you can't succumb.
			`);
			break;
		}
		case "wifeCage": {
			writeHTML(`
				sp player; wifeF, please! <br>Whatever you shoot out, it's infectious. If we're going to make it off this island... 
				t You hold the chastity cage in one hand, and the ice cubs in the other. She looks at both, her face torn between what she knows she needs to do and terror at the idea. She closes her eyes, releases her dick from her hands, and clutches the sheets tightly. 
				t You move fast, there's no telling how long her willpower will hold out. Using the icecube, you-
				sp wife; Oooough~! 
				t Just the touch if it makes her jerk her hips, but that was absolutely not a moan of pain. You do your best to hold her down and force her cock to soften and numb, but it seems like she's getting off on this. 
				t Yet, not actually enough to cum. You say a little prayer, hoping her brain makes it through this intact. 
				... 
				im wifeCage.gif
				t Finally, the cage is in place. She's huffing and puffing on the bed like she's just been through labor. Just one look in her eyes tells you she's teetering on the edge of sanity. 
				sp player; Can you keep going?
				t She nods. It's clear that only one thing is on her mind though, and her fiddling with the chastity cage makes it clear what that is.
			`);
			break;
		}
		case "wifeSacrifice": {
			writeHTML(`
				t Fear, hesitation, doubt, all those things seem to just fade away.
				t Quietly you you walk up behind her, and...
				...
				t You push open the door to the king's villa, wifeF doesn't even stop jacking off as you walk in, but you can see her eyes widen in horror when she sees what... Or who, you brought for her.
				t There are no words between you two as you set the scavenger down on the bed besides your wife. This is finally enough to stop her jerking off. She might be hesitant to infect someone, but wifeF is positively drooling over your offering.
				t You decide to step out, but you leave the door open as you leave. You can hear wifeF's wanton moaning as you give her some time to get off.
				t As you step out of the building your hards start to shake and your gut churns. You've essentially doomed someone, thrown away their lives, but nothing matters anymore except escaping with wifeF.
				t Yet as you hear wifeF groan and a splorting sound letting you know she's cumming, you can't help but wonder... What's next? What if you do make it home? The way wifeF looked almost hungry when you reminded her of all her firends back on the mainland-
				t You're shocked out of deep thought as you hear a second moan from an unfamiliar voice from the villa behind you and you rush back to see wifeF and the scavenger, but your wife isn't the one on top.
				im wifeSacrifice.gif
				t Your wife flaccid from freshly cumming is gleefully taking the newly infected woman's cock, her floppy dick dangling between her legs.
				t The infected woman notices you and stos her thrusting, she pulls out of your wife's ass and sprints towards you.
				t wifeF meanwhile just whines in dissapointment, until she notices you as well. Rather than trying to help, her hands snake between her legs as her cock starts to harden again.
				t The infected woman tackles you, taking you down. Your head bumps against something and your vision goes dark, your last concious thought that <i>you</i> won't be waking up, and that the king's villa will soon be the home of three infected.
			`);
			if (data.player.currentScene != "gallery") {
				writeTransition('spreadIsland', 'GAME OVER', '#FF0000');
			}
			break;
		}
		case "wifeOnahole": {//unfinished
			writeHTML(`
				sp player; Here, I found this, I hope it helps.
				t You unbox the onahole and give it to wifeF. She takes it, prodding the entrance with her finger. She catches her bottom lip with her teeth and you can see her dick throb as her finger sinks inside, a small jet of precum running down her shaft.
				t The idea that wifeF would ever use one of these things... Just yesterday you would have laughed it off, but now...
				im wifeOnahole1.gif
				t You can't seem to take your eyes away.
				t wifeF lets out a muted squeal of bliss as she thrusts her hips forwards. The sextoy must be close enough to a human pussy, or ass, because she groans in contentment.
				im wifeOnahole2.gif
				t The thick scent of wifeF's infection overpowers your senses, but wifeF doesn't seem to mind as she reaches between her legs to help relieve her overstuffed balls.
				t Her cock flexes, stretching the dicksleeve to its limit. Any human at the end of her pole would be absolutely ruined...
			`);
			break;
		}
		case "compoundCage": {
			writeHTML(`
				t From the moment you walk into the room, you can tell something's off. Even though you've been avoiding patrols this entire time you still feel a predatory gaze on you.
				t You see her, on the bed. Her eyes are already open, staring at you. But the thing behind those eyes isn't wifeF, not even human.
				sp player; wifeF? Honey...?
				t She pushes off the blankets. Her clothes have been torn to shreds, the bed is soaked with a clear fluid that looks like precum, and her chastity cage is misshapen like she's been trying to claw it open.
				t She steps towards you. You avert your eyes for just a moment searching for something to protect yourself with, but that's a mistake as your focus is pulled back by a bloodthirsty scream from wifeF as she tackles you to the ground.
				t You fall, her bodyweight atop you. You try to push her off but in a flash her lips are on yours and she's shoved her tongue in your mouth while she grinds her caged cock against your leg. You aren't sure if your head is spinning from being taken to the ground, or if you're already infected from her saliva.
				t You can feel the resistance leaving your body. Your hope is fading, an adrenaline rush won't save you now that you're certainly infected. wifeF looks you in the eyes, her eyes filled with impotent rage and desperation before she finally makes a decision. She grabs your pants by the waist and begins tearing them off.
				t You feel a creeping tingle beginning to spread across your skin, and suddenly a wave of pleasure causes your mind to flash white. As you let out a moan more effeminate than before wifeF shoves two of her fingers in your mouth. You can't help but swallow and the tingling increases. Her fingers are coated in something, probably her leaking precum. 
				t As she pulls her fingers out you look down and you see your own dick standing at full mast. Stronger, larger than before. wifeF groans in delight as she steps forwards, rubbing her caged dick against your erect one. Your humanity is fading, before you thought to console her with a kind hug or to appeal to her humanity, now you know exactly what to do. You grab her by the wrists and push <i>her</i> down. You align your raging cock with her quivering hole and thrust. 
				im compoundCage.gif
				t Pent-up frustration is relieved as you push past the rim of her asshole and past something bloated and spongey. Even before you're balls deep the fluid leaking from her cock is more white than clear.
				t The two of you scream, both of your voices reaching the same pitch. Her abused cock is finally finding relief, and yours is leaking away your humanity, your intelligence. 
				t You keep thrusting but you can't last much longer. As you do you can hear the sound of a metal clasp breaking. As your first orgasm as an infected dickgirl ends, so does her chastity. 
				t <b>THIS</b> is her liberation, not when you came in through the door to lead her out. She looks ready to cry tears of joy. Hesitantly she reaches out, but stops. She sits up, your cum leaking from her asshole, and licks her lips.
				t As the last remnants of your humanity fade, one final thought passes through your mind.
				t <b>You're next</b>. 
			`);
			if (data.player.currentScene != "gallery") {
				writeTransition('spreadIsland', 'GAME OVER', '#FF0000');
			}
			break;
		}
		case "compoundCaught": {
			if (data.player.wormDisabled == false) {
				writeHTML(`
					t As you sneak about you're noticing your shoes sound different than normal. And that you can hear your footsteps even when you aren't... Walking... 
					t Before you can turn around your vision goes red, then black. 
					... 
					sp ???; im none; Honestly... I'm starting to see why the labcoats whipped this one up. It's slow and not great for spreading the infection by itself, but... 
					sp ???; im none; Right? Cmon, we're expendable. They'll throw us away someday, and I hope it's either to that gas or one of these babies.
					t You can feel your mind coming back to life. You feel... Full. In both body and mind you feel like you're full of a soft, gentle cloud. 
					sp ???; im none; We can view the recordings ourselves right? I know the labcoats save them for research on their "anti-resistance strains", but I seriously wanna jill off to watching that baby change this one. 
					t You can feel something shift inside you. You're in some kind of lab. The door is across the room and nobody's in here, you could make a break for it, yet... You can't seem to build up the strength to move.
					t You look over yourself, you've changed. Either this is a dream... Or you're already infected. You feel your insides shift again, and you see something long, thick, and pink inserted into your asshole. Whatever it is it must be at least four feet deep... 
					sp ???; im none; Oh god, he's gonna try to... <br>Fuck it, I can't wait for the recording. 
					t You can hear the sound of a belt unclasping from behind a dark window, but your attention is focused on the... Thing... Inside of you. You grasp it as best you can, it's covered in a slippery mucus, and begin to pull. 
					im compoundCaught.gif
					t And as you do you can feel your mind begin to shatter beneath an all-encompassing white pleasure. Whatever it is, it flexes in your grasp and wriggles inside of you, before it shifts again and begins to thrust itself even deeper inside you than before and your vision goes pure white. 
					t You're spread legged trying to relieve the pressure it's putting on you, and you feel a sticky fluid land on your face. Something the creature is doing, whether it's messing with your nervous system or releasing drugs inside of you... You've been cumming. Every flash of white has been a powerful enough orgasm to spray cum through the air. 
					t The creature shifts inside you, it's found you to be a suitable host, and your vision goes white again. 
				`);
			}
			else {
				writeText("This scene has been disabled by your fetish settings. If for some reason want to watch it, you can still view it in the gallery after changing your settings.");
			}
			if (data.player.currentScene != "gallery") {
				writeTransition('spreadIsland', 'GAME OVER', '#FF0000');
			}
			break;
		}
		case "compoundHounds": {
			if (data.player.beastDisabled == false) {
				writeHTML(`
					im compoundHounds.gif
				`);
			}
			else {
				writeText("This scene has been disabled by your fetish settings. If for some reason want to watch it, you can still view it in the gallery after changing your settings.");
			}
			if (data.player.currentScene != "gallery") {
				writeTransition('spreadIsland', 'GAME OVER', '#FF0000');
			}
			break;
		}
		case "compoundFailure": {
			if (data.player.wormDisabled == false) {
				writeHTML(`
					sp ???; im none; ...-ney ple-... 
					t Bright lights pass by your eyelids and pain slowly fades in as you feel yourself waking up. The back of your head hurts. Bad.
					sp wife; Stop it! Honey please, wake up! 
					t Your eyes shoot open, you're in wifeF's cell, and she's sitting across from you bound to a chair. As you struggle to move you realize you're bound the same way. 
					sp wife; Thank god you're alive... 
					t wifeF's eyes are puffy, she's been crying and she's entirely naked. 
					sp wife; It's okay, it's okay. We're gonna be alright. They caught us, they started beating you, and when I begged them to stop... 
					t She takes a deep breath and forces a smile. A soldier, her entire body covered in a black paramilitary uniform, walks beside her with a small canister. 
					sp wife; They said they'd do things to us... And that if we made it through they'd let us go. I convinced them to just do it to me, since I'm already, well... 
					t The canister lets out a small hiss of pressurized air. You begin to struggle against your bindings as a small wormlike creature crawls out. wifeF either doesn't notice or is trying to ignore it as she keeps a calm facade. 
					sp wife; We'll be okay. We're gonna make it out of here together, okay? I lov-GGHHHH! 
					t She's cut short as the worm spreads some kind of mucus onto her, nearly every one of her muscles tightens. 
					t As you thrash and desperately try to free yourself wifeF goes slack. Her eyes are filled with panic but she can't move as the worm moves from the canister to the entrance of her ear, then crawls inside. 
					t She lets out a shrill yet muffled shriek through gritted teeth as the worm crawls in, and suddenly her slack left arm jolts against her bindings. Then her right leg, then both hands, and finally her cock jerks up and sprays a lance of precum onto the floor. 
					t There's one last moment where she hyperventilates and looks into your eyes with terror, before her eyes roll back. She lets out a deep drawn-out groan as her cock begins to squirt out rope after rope of thick cum. 
					im compoundFailure.gif
					t Her eyes unfocus, there's no sign of movement or intelligence left besides an occasional jerk of her body when she splurts another rope of jizz.
					sp ???; im none; Whelp, she didn't last long. Don't worry, honestly you got the good end of the bargain. The bigger one we have for you is gonna feel waaay better. 
				`);
			}
			else {
				writeText("This scene has been disabled by your fetish settings. If for some reason want to watch it, you can still view it in the gallery after changing your settings.");
			}
			if (data.player.currentScene != "gallery") {
				writeTransition('spreadIsland', 'GAME OVER', '#FF0000');
			}
			break;
		}
		//Typhoid Mary
		case "drinkPrologue": { //Temporary, remove me
			data.player.scenario = "Typhoid Mary";
			writeHTML(`
				sp Amy; im images/Typhoid Mary/lookout.jpg; Hah, hey! Look who's better! You were lookin' kinda awful when you got back, Jefferson was seriously worried.<br>I knew you'd be fine though.
				t Amy's got lookout duty tonight, turning her will make the horde's assault a lot easier.
				t You've got a event[nice drink for her|drink] from the canteen that you prepared earlier.
				t Otherwise you should get moving, your time is running out.
			`);
			break;
		}
		case "drink": {
			writeHTML(`
				sp Amy; im images/Typhoid Mary/lookout.jpg; Aww, thanks! You know hiding stuff you find while scavenging is against the rules... But what the hell.<br>Jeez, the bottle is really warm, huh? I miss my fridge.
				t She takes the bottle and unscrews the cap.
				sp Amy; im images/Typhoid Mary/lookout.jpg; Jesus that smell, one sniff and my nose is fried to hell. What is this?
				sp player; Some kind of health drink probably.
				t She takes a swig and there's a look of shock on her face as she almost spits it back out, but holds back and swallows.
				t <b>Gulp</b>
				sp Amy; im images/Typhoid Mary/lookout.jpg; Ga-haad fucking damn, that tasted like it smelt! What's in this stuff?
				t She shakes the bottle curiously, a few droplets landing in her shirt.
				sp player; Tastes bad? The bottle was sealed a while ago, so you're basically drinking straight from the tap.
				sp Amy; im images/Typhoid Mary/lookout.jpg; I mean I drink whiskey, I'm no pussy. Maybe I'll use it to keep me awake during my shift tonight.
				t She places the bottle down next to her and the two of you have a bit of smalltalk. It doesn't really go anywhere as AmyF's mind is clearly still focused on the drink. Despite her claim she picks it back up and takes another swig.
				t <b>Gulp</b>
				sp Amy; im images/Typhoid Mary/lookout.jpg; Gah, it's like treating my tongue to an acid bath.
				sp player; So why do you keep drinking it?
				sp Amy; im images/Typhoid Mary/lookout.jpg; Hah! Good point. Guess I'm thirstier than... Than I thought.
				t She keeps licking the inside of her mouth, her focus on the conversation keeps lapsing and she brings the bottle back to her lips.
				t <b>Glug glug glug glug</b>
				t She breaks the seal she made with her lips to take a breath, panting like a dog, but quickly brings it back to drink more.
				t <b>Glug glug glug glug</b>
				t The bottle emptied she sucks in hard enough to cave in the plastic a little and runs her tongue along the rim to collect the last drops.
				sp player; Damn, you were seriously thirsty.
				sp Amy; im images/Typhoid Mary/lookout.jpg; Y-yeah, guess I was...
				t She lifts her shirt and actually begins trying to suck out the few drops she spilt earlier.
				sp player; You know, I could get you some more.
				t His has her attention, she dryly swallows clearly wanting as much as she can get.
				sp player; Hey watch out for me, I need to take a piss.
				sp Amy; im images/Typhoid Mary/lookout.jpg; B-but you said you'd get me more...
				sp player; I am.
				t Instead of a more private position you strip down and whip out your dick right in front of AmyF and aim yourself right at the dining table.
				t If there was ever any proof that the drink fried AmyF's brain, it's the fact that instead of realizing you infected her, or calling for help...
				im drink1.gif
				t She takes one whiff and crawls towards you for another taste.
				...
				sp player; Now, show me.
				im drink2.gif
				t The infected formerly known as amy lets out a small whine as she relieves herself.
				sp player; Huh, you're an infected who pisses, fitting. Head back to your room and stay quiet, when I give the signal we attack, got it? I'm sure there will be others who are as thirsty as you.
			`);
			if (data.player.currentScene != "gallery") {
				writeTransition('typhoidMary', 'Finish');
			}
			break;
		}
		//Scarlet Mansion
		case "scientistLetterFull": {
			writeText("This is another piece of the letter, or diary entry... In fact, the last part you needed. You should be able to piece together the parts now:");
			writeHTML(`
				t <i>"I think it's important to get these thoughts down. I've seen what can happen to people when they start to lose their minds. Even for people like us...
				t <i>We spend all day on these projects, and I know what we all do at night after we finish. I know why we keep so many recordings and why they're available for us all to watch at any time. I can hear them through some of the walls.
				t <i>The order came in, vaccination testing. There's some inoculations, but they're too expensive to have at the ready anywhere except the main labs. The top rungs won't even give us the ingredients. These are the alternative, but who are we testing them on?"
				...
				t <i>"Vaccination testing means they shoot you up and drop you into a room with one of... Them. For hours. Then they pull you out, spray you down, and see if you're still human.
				t <i>We could have used some of the captives, but it's not like they'd patiently comply with our testing or answer a questionaire afterwards. There was some debate on if we should just draw straws... Until I volunteered.
				t <i>I don't know why I did it, it's like my mouth said the words on its own. Carlson looked at me with respect, but the rest of them...
				t <i>Surprise, disgust, jealousy. I think they knew why I was volunteering."
				...
				t <i>"The trial was a success. The vaccination hurt like hell, but what came afterwards...
				t <i>It was worth every second.
				t <i>It's so much better than just watching. The captives scream that we're monsters, but if they could sill talk or think after being infected they'd thank us.
				t <i>I was nervous at first. Apparently even though the vaccine would turn me back to normal after a few hours, cumming after being infected would seriously reduce my fertility as a woman. I started having doubts, but...
				t <i>When you have a massive cock hanging between your legs, you realize stuff like that is less important than the feeling of a thick load of cum pumping out of your dick. My fertility? Eggs? Thy could have said cumming would kill my brain cells and I wouldn't have jerked off less.
				t <i>I passed the tests afterwards, I'm completely human, no more dick. But for three hours I was something better than human. I can't sleep unless I have the recordings playing on loop anymore. I've been supervising a purposefully slow transformation, enjoying watching her break down, but it isn't enough. I'm hungry all the time now.</i>
				t <i>But not for food."
				...
				t <i>"I'm not crazy. I know I'm not crazy.
				t <i>The new one, she's smart. I could see her muttering the code we set for her door after Charlie said it out loud. I know some infected still retain their minds, some even get smarter. But it's temporary. A month, a week, maybe even after a day she'll be like all the rest. I need to hold on.
				t <i>I've been cleared for work again. Thank god I didn't slip up in the medical interview. Every second that goes by I can feel my crotch throb. I just wanna tear off my clothes and jerk off, but I don't have a dick. I'm just... Me.
				t <i>But she knows it's taking up every waking thought. She'll act brainless and stupid when the others are around, but when it's just me and her...
				t <i>I want to cum. I don't give a shit about my womb, about my mind. I don't want to squirt, I want to piss thick semen onto the floor as my balls clench up. I want to give in.
				t <i>I want her."
				...
				t <i>"She's unique. We pump chemicals into the air in the mansion to make sure we're all willing to do what it takes. It makes us more sadistic, more creative. It shouldn't affect her...
				t <i>For her it just seems to make her hungry. More cruel, it makes her want to torment. That's what I see when I look in her eyes.
				t <i>Tests confirmed it, it's some genetic anomaly. Her children or other family could have it too. Even if she wasn't infected the chemicals would probably have the same effect. The others are convinced she's finished, and that her mind has finally faded. I'm the only one watching her now.
				t <i>I keep telling myself this is just for study, but I think I know why I keep such a close watch on her.
				t <i>I think she does too.
				t <i>I broke. She came onto the glass right in front of me and kept stroking herself. Rubbing her fat cock against the glass, I...
				t <i>I started licking the glass. I didn't care that she was watching me, that I was showing weakness. I knew there was a camera on me, but I didn't care. I started stroking my cunt, I'm so fucked up.
				t <i>It's only a matter of time before someone sees the recording. My life is over, isn't it?
				t <i>I know the code to her room. I know I shouldn't, but...
				t <i>If my life is over, why not?"
			`);
			break;
		}
		case "scientistEscape": {
			writeHTML(`
				t You slide the key into the lock and turn. You hear a soft *click* before the knob quickly turns and the scientist opens the door to freedom.
				scientist Thank god, finally. Thanks, let's get out of here, we might not have very long.
				t The two of you make your way down the hall before the scientist taps you on the shoulder.
				scientist Okay, so the dogs shouldn't be too big of a deal. There's no outrunning them, but they've been conditioned not to attack certain members of staff. More importantly is the mask, there's some power tools-
				t You hear a small beep, which catches you off guard for a second as you look around to see where it came from.
				t You turn back to see the scientist with a look of terror in her eyes, desperately clawing at her mask as the whir of machinery becomes audible.
				scientist No no no, help! Please! Get it off!
				t A thick white fluid travels up the tubing of her mask, her hyperventilating means she's probably getting a massive overload of the scent, strong enough to make her eyes water. You try to help pull at the mask as she falls to her knees, and you can hear that the carpet beneath her is rapidly growing wetter.
				t She takes a deep breath to try and hold it, one last desperate act to delay the inevitable. She can't hold it for long, coughing and sputtering as the white fluid is forced into her.
				t You keep pulling without any luck as her clawing grows weaker and weaker. Her terrified eyes give way to a drug-induced stupor, not even tracking as you wave your hands in front of her face.
				im scientistEscape.gif
				t There's an audible gulping sound as she starts to swallow down the thick fluid, but there's no way she'll be able to swallow it all before passing out. Her hand moves to her pussy and she starts to rub herself.
				t Her swallowing starts to slow and her eyes close, her body goes slack. 
				t Even with adrenaline coursing through you and every bit of strength you can muster, there's not an inch of give to the mask. Who the hell were these things designed for? Superhumans?
				t No longer able to keep pulling, you release the hose and stumble backwards. Catching your breath and trying to think clearly, you're interrupted as her body starts to convulse.
				t Her eyes open slightly but there's no sign of intelligence behind them as her pussy sprays onto the carpet, her lungs and stomach being filled with sperm somehow causing an extremely powerful orgasm.
				t You curse your luck and your helplessness as the seizing stops just as soon as it started, leaving nothing but the scent of her juices to fill the hall. Yet as you look closely you can see her chest continue to rise and fall. Could she still be breathing somehow?
			`);
			break;
		}
		case "scientistDark": {
			writeHTML(`
				t You hesitate before opening the door, and its probably for the best. Through the door you can hear frantic moaning and the sound of flesh on flesh.
				t The woman's contraption must have activated just before it was disabled, as you can smell something that burns our nostrils even from here. But there's also another obvious giveaway as you kneel down to peer through the keyhole.
				im scientistDark.gif
				t Trapped in a room forced to inhale the scent of pure sex without any relief, only to be infected just as freedom was just in reach... At the very least she seems to be enjoying herself, eagerly thrusting away to hump her own mouth. At this point she must feel used to feeling like she's breathing pure jizz anyways.
				t With a particularly deep thrust her balls cover her nose then contract as she begins stuffing her own throat with her cum. Her eyes roll back and there's no trace of her former self.
			`);
			break;
		}
		case "diary": {
			writeHTML(`
				t The file appears to be a collection of synopsized diary entries, with an associated security camera footage file attached.
				...
				t <i>I have a penis.
				t <i>It was an ordinary day yesterday. I woke up here. It's not a dream, and so long as I recount what happens accurately, you'll give me the cure. Is that okay? How long will I be-
				...
				t <i>The drug they used on me. They said I was "naturally resistant", so then why do I have this... Thing?
				t <i>And I have these hanging... Things... I have them too. They told me my womb is... In them. And if I cum I won't be able to have children. Why do I even need to repeat all this? You did this to me. You know how this works.
				...
				t <i>They... You, you're watching me right now, aren't you? I see how you look at me during my "checkups". You want to see me like this.
				t <i>You keep saying "use proper language", but no doctor would say... What you're saying. So why? Just please, let me go...
				t <i>Okay, I'll say the words. I have a cock. A dick. A pair of balls. Please, I've done everything you've asked, just-
				...
				t <i>It itched. So badly. I rubbed, and I kept rubbing. I stood, knock-kneed with a massive pair of balls between my legs and I was jerking off. I cracked, and although I managed to stop I leaked what must be a cup of sperm right onto the carpet. When they put me to sleep for the night the stain'll disappear, but it's still there now. Taunting me.
				t <i>I remember what I was thinking before I finally tore my hand away. My own mind was lying, saying "maybe if I just squirt out a little it'll be okay", or "I'll just have to try extra hard to get pregnant".
				t <i>I can't trust my own head.
				...
				t <i>I came in my sleep. I know it wasn't my fault, in a situation like this a wet dream was inevitable at some point.
				t <i>But that doesn't excuse the fact that I was stroking myself when I woke up.
				t <i>All that sperm on my hand... I felt so disgusting, like I was throwing away my chance at a happy family, and for what? I'll admit it felt good. Amazing even, but I avoided heroin for a reason. 
				t <i>I ate it, slurped up the load, but only because I didn't want to look at it anymore.
				t <i>At this point is it already over? My husband and I... We already had enough trouble conceiving. At this point I'd...
				t <i>I'd need a cock like this one to breed me.
				...
				t <i>I need to stay in control. I need to focus on what really matters. I can't even have sheets on the bed anymore <s>because I can't stop humping</s> helpmehelpmehelpmegetmeoutofhere
				...
				t <i>I'll adopt. That's what I'll do. I'll find someone and show them the love my kids deserve. Someone like the teenager next door, she was always so polite. I think she's in college now. I'd be supportive, I'd be there when she needs me. <s>I'd split her tight teenage pussy</s>
				t <i>A son. My husband talked about what he was like as a teenager, I think he was trying to make me scared of raising a boy. Teenage years, puberty, everything that comes with those years. I never felt scared. I wish I'd been there for him <s>Jerking off on his face until he's addicted to my cum and</s>
				t <i>I can't let go. I can feel it, bigger than any load so far. Honestly it gives me hope, maybe all I've done so far is just let out precum. But I feel it inside me, boiling. No stimulation, my mind is empty but I still feel the load building. Just one flex of the wrong muscle and it'll all come out I can't breath my husband would be ashamed I am in control I will not
				t <i>It would be so easy
				...
				t <i>Honey... Honey I don't think they ever planned to let me out. I can't remember things now. I can't remember my name anymore. I can't remember yours either.
				t <i>Don't think of me as human anymore, okay? This isn't my fault, you wouldn't last here either. It's okay for me to cum now, okay? You should be proud of how long I lasted.
				t <i>I don't think you'll ever see this, but if you do, I love you. Stop watching now, please. Your wife was strong. She never gave in no matter how good stroking her cock felt. Goodbye.
				...
				im diary.gif
				t Most of the video is pure masturbation. At some point she purposefully cums directly onto the camera, and the rest of the feed is a blank white.
			`);
			break;
		}
		case "analInfected": {
			writeHTML(`
				t Hesitantly pushing open the door you enter the bedroom. Inside...
				im analInfected1.gif
				t The noises you heard were partly from this infected woman, happily bouncing up and down to slam her prostate hard enough to squirt onto her well-soaked sheets, but also the audio playing from the headphones she's wearing.
				t She has a very high-tech set of goggles on her head hooked up to several workstations around the bed, each feeding her another video. You get closer to see exactly what she's watching...
				im analInfected2.gif
				t The workstation is also displaying several bits of information, such as "Playback time 3:27:33/10:16:00", "Mental Status 330% of specified parameters", and a counter reading "109", although as the woman's moans reach a high point and she squirts onto the bed again, the counter ticks up one further.
				t There doesn't seem to be anything useful in here, and if you stay too long the sights, sounds, and smells might get... Distracting.
			`);
			break;
		}
		case "tubInfected": {
			writeHTML(`
				t You grasp the chain and pull, you can hear the plug pop free and the water begin to drain.
				t As the water level lowers, you can see what, or who, is beneath the thick soap.
				im tubInfected.gif
				t An infected woman, who knows how long she's been here. She's blindfolded and her mouth and nose are masked by some kind of breathing apparatus, and her hands are bound to the bottom of the tub.
				t The cleaning materials, the gloves, and the chain designed not to fall into the tub. It seems like this room was more designed for cleaning animals than humans. Or maybe just for washing uncooperative prisoners.
				t She twitches at the feeling of fresh air on her skin, like she's highly sensitive. Perhaps she was being kept underwater while she underwent her transformation?
				t There's nothing you can do for her, the clasps on her hands and her mask are extremely complex. Hopefully rescue will come for all the women trapped here, but until then...
			`);
			break;
		}
		case "garageHounds": {
			if (data.player.beastDisabled == false) {
					writeText("The woman is abnormally fixated on you even as one of the massive hounds comes up to her and plants his paws on her back, mounting her.");
					writeText("You can see his erect cock dangling, fully erect, with the shock collar wrapped around his base.");
					writeText("He bucks up against the woman, his cock poking her dangling balls a few times before he lines himself up with her ass and slowly pushes in.");
					writeText("Finally the woman's attention is pulled away from you just as the dog roughly shoves his hips forward. The other dog lazily lies down in front of the woman, lifting his leg to expose his cock to her.");
					writeText("She doesn't seem to care that the cock in front of her is red, or that it has a pointed tip. Completely enamored at first sigh, she moves forward to start licking at the red length while the dog behind her starts to get into the flow of thrusting.");
					writeBig("dogTorture.gif");
					writeText("Quickly enough the one behind her is already ready to cum. His thrusts stop in the middle like he's blocked by something, and a squint shows that it's a growing bulge near the base of his dick. He's having trouble thrusting his knot inside until the woman bucks backwards in time with his thrusts. Soon after she lurches forwards to deepthroat the cock in her mouth.");
					writeText("Both creatures cumming, they stop their thrusting to dump their loads into the woman's holes. But just as things begin to slow down, a red light blinks on the cock-shock collars. The woman lets out a high-pitched muffled yelp, but the hounds seem more aggravated than pained. Despite still cumming, they both start to pick up the pace and begin thrusting faster.");
					writeText("The collars flash red again, causing the infected woman to spasm and let out a spurt of cum from her untouched dick. The shocks must be for her, and it seems like she's getting off on being zapped while being used as a bitch.");
					writeText("After having had enough, the hound behind the woman begins to tug and pull to try and free his knot from the woman's greedy asshole. He tugs and tugs, before finally the cock is freed with a big *POP* and a line of dog cum spurts from the woman's abused hole through the air.");
					writeText("Still leaking, drops of dog cum slide down her balls and down her erect, dangling shaft to mix with her cum before dripping down into the growing pool on the floor.");
				if (data.player.wsDisabled == false) {
					writeText("The hound using her mouth stands up, but before it decides it's done it lifts its leg. The dangling dog cock stands inches away from the woman's cum-coated face, before it sprays her with a blast of piss. The woman, barely conscious at this point, grasps the cock in her hand and moves the still-spraying tip to her mouth. Greedily, she opens up and begins sucking off the still-pissing dog until the stream weakens.");
				}
					writeText("As though trained, once the dogs are finished they leave the garage and head back to their kennels.");
				}
				else {
					writeText("This scene has been disabled by your fetish settings. If for some reason want to watch it, you can still view it in the gallery after changing your settings.");
				}
			break;
		}
		case "garageDark": {
			writeHTML(`
				t As you walk into the garage you see...
				im
				t The infected woman, still partially bound, is joyfully servicing multiple hounds as best she can.
				t Until the lights come back on the security systems should be down, meaning you probably aren't safe to keep watching as you please. You should [head back inside].
			`);
			break;
		}
		case "chastityInfected": {
			writeText("You pick up the device, it's like some kind of remote. The woman is still struggling against her bonds. There's also a small condom on her caged dick.");
			writeText("You turn the dial and the woman lets out a sudden breathy moan. A dim buzzing can be heard from her chastity cage, and she's gritting her teeth now.");
			writeText("You turn the dial up further and she stops struggling, now tightly clenching the bedsheet with her fingers as she thrusts her caged cock up and down helplessly.");
			writeText("You turn it up again and the light glows brighter. Her cock is desperately trying to grow erect but is prevented by the metal cage.");
			writeText("You can clearly hear a buzzing, it's muffled and barely audible from her abdomen. There must be some kind of toy inside her ass, maybe a vibrator or a shocker stimulating her prostate.");
			writeText("You turn the dial again and she desperately gasps, her arms and legs twitching. Her caged cockhead starts leaking thick, heady cum. Even with the condom the smell of it is powerful and more than a little intoxicating.");
			writeBig("chastityTorture.gif");
			writeText("She's already cumming, but you can't help yourself but turn the dial father. Her moans turn into screams rising in pitch with the dial as you turn it all the way up. The chastity cage actually seems to grow smaller and tighter as the light glows brighter, and her small, piddly stream of cum turns into an arcing jet that sprays from the hole in the cage.");
			writeText("The buzzing is very audible now, whatever it's doing must be incredibly intense. The orgasm just keeps going on until you turn the dial back down.");
			writeText("Panting and exhausted, she goes slack on the bed. But you, being the morbidly curious sort you are, suddenly turn the dial right back up.");
			writeText("No buildup, she lets out a scream as another several spurts of cum are forced out of her body. The shots get smaller and weaker, but the stream isn't stopping. The device must have more control over her than it seems, if you turn it to a certain level it seems like she's just on the edge of cumming.");
			break;
		}
		case "chastityDark": {
			writeHTML(`
				t You hesitantly enter the room, hearing soft cooing from inside, only to quickly stop and step back once you see someone inside.
				im chastityDark.gif
				t Wordlessly, a woman on the bed is masturbating as shamelessly as a mindless infected can, held back by the fact that her cock is entirely contained in a too-small cage.
				t Yet that seems to stop her. As lightning flashes illuminating the room you can see a manic grin on her face as she continues mindlessly squeezing her thighs to put even the slightest bit of pressure onto her caged penis, and in that flash you can see fluid arc through the air. She's so well broken in that the cage won't stop her from cumming to her dick's content.
			`);
			break;
		}
		case "analDark": {
			writeHTML(`
				t The open window lets in just the faintest bit of light, you can hear a woman inside gently cooing. Giggling. There's a massive amount of tech strewn about the room, several workstations that might have before shown progress or training data, but now the woman is free.
				im analDark.gif
				t But at least she doesn't seem eager to leave. Even in the poor light and from this distance you can see how absolutely ravaged, puffy, and unbelievably sensitive her ass must be.
				t What on earth could be going on in this place? What lunatic would capture and transform a human being like this? Yet it's tough to draw your eyes away.
			`);
			break;
		}
		case "gaggedSiren": {
			writeHTML(`
				t Pressing the button causes something to power on. Exactly what that is soon becomes clear.
				t As you hear something begin to buzz, the woman spreads her legs. Her eyes still don't register you, it's like she's been trained to show off whenever the machine is on.
				t Her moans are distorted, and the room is filled with a robotic sing-song voice before the wet sounds of sucking. The gag must extend down her throat, and at least some part of the machine is designed to stimulate her vocal chords.
				t But most of the machines work is below. The buzzing grows faster and faster, and the woman's penis begins to leak a clear fluid.
				t Her balls are pulled tight against her body without any reprieve. It isn't clear how the machine is working but it's clearly having an effect.
				im gaggedSiren.gif
				t The sounds of her suckling her gag grow louder and more frantic, the gag's vibrations are actually visible in her throat while whatever's stimulating her lower half continues to speed up as well. Worried for a moment you press the button again, but to no immediate effect until...
				t Her body goes slack and her eyes roll back in her head. There's a popping sound and the sound of a motor dying and the stimulation stops, leaving the woman, and the machine, both unresponsive.
			`);
			break;
		}
		case "gaggedDark": {
			writeHTML(`
				t As you walk into the west wing, careful to avoid bumping into anything, you can hear a faint voice coming from some nearby room.
				t There's something about that voice you just can't place... A song is audible, louder in your head than outside it like it's playing in your brain.
				t You feel relaxed. Like you're getting ready for bed. As you navigate through the hall you start making small deviations moving you closer to the noise, your brain coming up with excuses to take a breather.
				t You push open a door, and as you do you can hear the song clearly.
				t The lights flicker back on, the discarded manacles and mechanical gag make a clicking noise, but they were torn off and thrown aside long ago.
				im gaggedDark.gif
				t She turns to you, still humming an ephemeral tune, <i>very</i> eager to have some company after so long.
			`);
			break;
		}
		case "gaggedEnding": {
			writeHTML(`
				t The song echoing through the halls is no longer audible, replaced with wet noises and high-pitched moaning as you enjoy your reward for freeing the siren.
				im gaggedEnding.gif
				t With how enthusiastic she is, it's clear her time gagged made her desperate, hungry. Not only that, but as you thrust she looks back at you and lets her long tongue hang free, now that the gag is gone she's clearly signalling that as her new pet you'll be well taken care of.
				t As the lights of reason in your head start flickering off for the last time, all that's left is the knowledge that you <b>must protect her</b>, along with stray thoughts of how pleasurable your reward will be.
				t You've met either a terrible or wonderful fate on this rainy night, but either way it's permanent. Until the very foundation of the house rots, you are the siren's new pet.
			`);
			break;
		}
		case "cumtapInfected": {
			writeHTML(`
				t Despite the resistance you get a firm grip on the handle and turn with all the force you can muster. The faint muffled moan behind he wall grows much louder and a load of cum splurts free from the cock-faucet. It splatters into the carpet, quickly followed by a second load.
				im cumtapInfected.gif
				t You hear something click in the knob and realize it can't turn any farther, and can't turn back either. The muffled voice grunts again as even though her overworked cock just came and is growing softer, another load is forced out.
				t With the knob broken, or perhaps locked in place, her cock seems to be forced to leak out a stream of cum non-stop. Ignoring the sound of her jizz dripping onto the carpet, you can faintly hear the sound of fluid traveling through pipes. It seems despite her being trapped she's being at least somewhat taken care of.
				t The cock-faucet shakes as the infected it's attached to struggles, using whatever parts of her body she can to thrust and wag her hanging, untouched dick, but to no avail.
				t Frustrated sobbing gives way to a desperate muffled scream, then silence. The cock waves in the air again as if tempting, or begging you to give her some release.
			`);
			break;
		}
		case "cumtapInfectedRepeat": {
			writeHTML(`
				t Your hands on the knob again you give it a turn, and the muffled voice behind the wall gasps. Her cock quickly is at full mast again as she struggles to thrust it forwards ever so slightly, it's all she can manage.
				t With all the strength you can muster you turn the knob, and while you only manage a small turn the effects are apparent immediately.
				t Her balls have grown, as though they've physically expanded. They must be the size of baseballs at this point, and they're throbbing angrily.
				t You hear the voice grunt, and then again and again, faster and faster as her cock pulses. Despite you not turning the knob something is about to happen. The wall shakes as the infected woman struggles, her cock pulses and is an angry shade of red, and suddenly...
				t A clear stream of fluid leaks from her cock. The sound of struggling stops, replaced with a lust-drunk sobbing.
				t Before your eyes her penis and balls begin to shrink as the fluid spills out non-stop. Her once impressive 11-inch dick is barely half that length now as her leak-gasm continues.
				t The carpet is completely soaked now, and her penis reaches its flaccid length and continues to shrink even further. Three, two, one inch, finally coming to stop shrinking at a length shorter than your pinky and barely as thick as your thumb. Her balls have shrunk too, now looking like tender grapes hanging down several inches.
			`);
			break;
		}
		case "cumtapDark": {
			writeHTML(`
				t The dining room is pitch black, but you're careful to avoid bumping into any tables.
				t What you weren't expecting were the small objects you suddenly find yourself stepping on. They crunch beneath your shoes, feeling like... Wood chips? Almost like someone smashed a hole in the wall, and-
				t Suddenly every hair on your body stands on end and panic kicks in, coursing through your body as a hand grabs you by the head and throws you down. Just as suddenly you feel something long, hard, and very moist with sweat on your face. A pair of hands on your neck quickly let you know what resisting will get you.
				t [Your fate is sealed in the dark halls, and when the power comes back on...]
			`);
			break;
		}
		case "cumtapEnding": {
			writeHTML(`
				im cumtapEnding.gif
				t From your very first moment with her, it was clear that she's been on a hair trigger. Just the first thrust was all it took for her to start leaking infectious fluids into your mouth, and the thick gout of semen only grew thicker the longer it had to wait in her balls.
				t But that didn't matter, after the first few swallows you came to enjoy how quickly she could cum, because her refractory period was almost non-existant. Her loads were so thick and plentiful it was actually hard to tell when one finished and another began.
				t As the lights flicker back on you can clearly see her massive balls are still bloated, almost like they've been growing even larger with every cumload. And that's perfect.
				t Your stomach may be stuffed, but you still feel thirsty for more.
				t You've met either a terrible or wonderful fate on this rainy night, but either way it's permanent. Until the very foundation of the house rots, you are:
				t Drinker from the Cum-Tap
			`);
			break;
		}
		case "urinalInfected": {
			if (data.player.wsDisabled == false) {
			writeHTML(`
				t You sigh and reflexively look around, but your only audience is on her knees in front of you. You're careful not to get too close though, getting infected here would be an embarrassing way to go.
				t You pull aside your clothes, the perverse situation has your heart pumping like crazy. Though even if you felt like apologizing, it's not like she could understand you.
			`);
			if (checkFlag("female")) {
				writeHTML(`im urinalFemale.gif`);
			}
			else {
				writeHTML(`im urinalInfected.gif`);
			}
			writeHTML(`
				t She moans softly as you soak her face in a wave of piss. You can see her bound cock throb in its bindings.
				t Between gulps she lets out wet pants, she's like a dog drinking from a garden hose.
				t Once you're finished she lifts her cupped hands up to drink down everything that pooled up, taking slow methodical gulps as if savoring the taste.
			`);
			}
			else {
				writeText("This scene has been disabled by your fetish settings. If for some reason want to watch it, you can still view it in the gallery after changing your settings.");
			}
			break;
		}
		case "dogShock1": {
			writeHTML(`
				t You point the remote at the guard dog and push the largest button.
				t Her growling stops and she lets out something between a yelp and a squeal, her body suddenly shivering. She leans back to stand on the balls of her feet, as if repeating a practiced pose, some kind of show of submission
			`);
			break;
		}
		case "dogShock2": {
			writeHTML(`
				t You press the button again, holding it for a good few seconds. The effects are immediate, she lets out a lilted moan as her hips shake uncontrollably.
				t Once you've let go she falls backwards, panting, only to twitch and squeal when you tap the button a few more times.
				t She's a good dog, and doesn't seem to have much endurance for this kind of pleasure, so you press and hold the button down for her reward.
				im dogShock2.gif
				t Her squeals become a scream through grits teeth and it looks like she's having a seizure as her cock starts spurting freely through the air, soiling the carpet through her bizarre cock-shocker accessory.
			`);
			break;
		}
		case "dogShock3": {
			if (data.player.wsDisabled == false) {
			writeHTML(`
				t The guard dog woman quickly assumes her submissive position as you point the remote at her again, but you push the button again anyways.
				t She yelps and her hips shake back and forth, but she makes no move to stop you.
				t Again you push the button, her flaccid dick wags with her movements but still needs time to recover before it can get hard again.
				t Again you push the button, she grits her teeth and she's less shaking her hips as much as she is spasming now.
				t With one more push...
				im dogShock3.gif
				t She lets out a long, drawn-out squeal as her cock leaks out a wimpy stream of piss, a complete sign of disgrace and defeat.
				t You give it one more push for good measure, the shock is enough for her to fall backwards, her body shaking and cock still leaking. The stream becomes stronger as her ability to hold back fades completely and she coats her own chest and face in piss.
				t It seems like she's fainted now, pushing the button makes her twitch and leak a little more, but otherwise she's unresponsive. 
			`);
			}
			else {
				writeText("This scene has been disabled by your fetish settings. If for some reason want to watch it, you can still view it in the gallery after changing your settings.");
			}
			break;
		}
		case "dogGassed": {
			writeHTML(`
				t You kneel down next to the "guard dog" and begin to rub her belly. You can't go any farther without risking infection, but it can't hurt to enjoy something cute for the moment.
				t She enjoys it, closing her eyes as you rub. Honestly this mansion is a bizarre place, but you haven't actually been in danger so far, have you? She rolls around and you rub her head softly.
				t Well, unless the virus is airborne. But if that were the case you would already have been infected by now. Probably.
				t You're missing a lot of answers to exactly what-
				im dogGassed.gif
				t You're shocked out of your pondering by a moan. Apparantly she really enjoyed that belly rubbing.
				t Her tiny dicklette isn't even getting hard but it isn't stopping her from enjoying herself rubbing against the floor. Her other hand snakes between her legs to...
			`);
			break;
		}
		case "dogDark": {
			writeHTML(`
				t You push in to the east wing, careful to watch out for the guard dog. Though it's very dark you can still barely make out her chain on the wall, only...
				t It's hanging slack, with a broken collar at the end of it.
				t Without so much as a growl before you can rush back into the main hall, you feel your sense of balance collapse as someone slams into you from the side.
			`);
			break;
		}
		case "dogEnding": {
			writeHTML(`
				t Disorientation.
				t You don't feel right in the head. Something's wrong, like a rock's fallen on your chest, you open your eyes but everything's still black.
				t And before you can get your bearings suddenly your mouth feels warm. Nice. Your lips feel like you've just bitten into a fresh apple pie, the sensation is good enough to make you feel like you're back home.
				t Relaxing in bed, nowhere to be, a delicious breakfast. Everything's a daze, like you're in a dream but that <i>taste</i-
				t *CLACK* *CLACK*
				t The lights suddenly kick on, blinding you. You struggle to open your eyes, what's-
				im dogDark.gif
				t Questions and thoughts race through your mind as soon as you take in the image of the guard-woman's cock. How long have you been out, why is your body happily opening up on its own, how much of her infection have you swallowed already?
				t She seems to notice you're awake, her primary purpose already served by spreading her seed to you, she smiles. Like in the process of taking some cruel revenge.
				t She steps back, and your body instinctively follows.
				im dogDark2.gif
				t Her cock is absolutely leaking, there's no other word for it. Whatever pleasures and pains this mansion has inflicted upon her, it's had the very permanent effect of transforming her cock into a cumfaucet, the kind that wipes the minds clean of good innocent people like you and turns you into cock-hungry sluts.
				t Before you turn into a mindless infected dickgirl like her, that is. The taste is electric, but frying your brain instead of your body. You're already done for, so when the little voice in your head screams to resist...
				t You ignore it.
				t ...
				t You've met either a terrible or wonderful fate on this rainy night, but either way it's permanent and you've lost the face your sister would recognize. Until the very foundation of the house rots, you are the guard bitch's companion.
			`);
			break;
		}
		case "tapeCongealant": {
			writeHTML(`
				t Now beginning research log X-341 L-13 "CONGEALANT SIDE-EFFECT NOTES", authorized by researcher JONES.
				t "<i>Now commencing notes on defective strain-branch 32. All seems normal at first until subject achieves climax.</i>
				im tapeCongealant1.gif
				t <i>As you can see the semen ejaculated is extremely thick, barely able to move through a porous material. <br>Because of the consistency ejaculations become extremely drawn-out affairs, often lasting multiple minutes as the subject's kegel muscles are pushed to the limit trying to force out the load.</i>
				im tapeCongealant2.gif
				t <i>The process triggers the usual endorphin rush of an orgasm the entire time while also heavily stimulating the urethral nerves. <br>While unquestionably a failure in terms of effectiveness, the continued testing of this strain should be useful for both education and entertainment purposes."</i>
			`);
			if (data.player.currentScene != "gallery") {
				writeText("[Finish|scarletTheater]");
			}
			break;
		}
		case "tapeLicker1": {
			writeHTML(`
				t Now beginning research log X-341 L-13 "LICKER STRAIN APRIL", authorized by researcher SMITH.
				t <i>"This marks the thirteenth test of the potential Licker strain, an oral-focused type of infected. To recap this infection type uses their elongated tongue as an infection vector.
				im tapeLicker11.gif
				t <i>New to this strain-mod is a predisposition towards oral sex and analingus, as mouth-to-mouth infection rates were suboptimal and took time. Previous victims would need to essentially lock tongues with the Licker for upwards of half an hour, and while euphoria means they rarely escaped on their own, the potential to rescue was too high.
				im tapeLicker12.gif
				t <i>Though some have criticized it as being redundant when released alongside Siren strains, it is worth noting that the Licker has the potential to infect women as well. Any ability to draw potential human survivors out of hiding is valuable, and appealing to major fetishes is a viable pathway.<br>Still, the issue of infected fucking each other rather than the uninfected is a difficult problem to manage. I've yet to find a way to hold back the Licker from assaulting other strains. That's a priority for the fourteenth."
			`);
			if (data.player.currentScene != "gallery") {
				writeText("[Finish|scarletTheater]");
			}
			break;
		}
		case "tapeLicker2": {
			if (data.player.rimDisabled == false) {
			writeHTML(`
				t Now beginning research log X-342 L-14 "FUCKER GETS WHAT SHE DESERVES", authorized by researcher SMITH.
				t <i>"Please, let me out! For the love of god, stop! I wasn't one of the staff on your project!"
				t A naked researcher slams on the sealed door of the chamber, occasionally, screaming pleas to the camera, until a door in the back of the chamber hisses and opens. The researcher cowers when she notices this.
				t <i>"No, nonono please, I have a family!"
				t <i><b>"You shouldn't lie about that, Smith. I'm looking at your file now, you have quite the history here."</b>
				t From the open door comes the licker from the previous test, her elongated tongue lolling out of her mouth at the sight of the researcher's body.
				t <i>"It was just work! Please, I know you can stop her, I-<br>Mmmppph-!"
				im tapeLicker21.gif
				t The researcher struggles as the infected pushes her long tongue down the researcher's throat, her hands flailing and slapping at the infected's body until they grow limp.
				t The camera's view shifts to show the small bulge in the researcher's neck travelling downwards as her eyes roll back, a small puddle forming between her legs.
				t <i><b>"Now, the other side.</b>
				t The infected hesitates, before she slowly draws back her tongue from the researcher's mouth with a lengthy wet schlorp. Long strings of saliva splatter across the researcher's chest as she looks dazed to the point of looking drugged. She's easily turned over so that the infected woman has access to her rear.
				im tapeLicker22.gif
				t The researcher coos, her mind addled by the chemicals coating the licker's tongue as it pushes past her asshole to infect her from the other end.
				t The recording continues like this, only changing when the researcher approaches orgasm and squirts along the floor before slumping back down, until several minutes pass.
				t The researcher's back arches and her legs go stiff once again, but instead of squirting on the floor she just shakes. There's a pair of small bulges at the base of her abdomen, where her womb should be, traveling downwards. The researcher grunts in exertion and the infected spends almost another minute slowly withdrawing her tongue before plunging it between the researcher's extremely plump pussy lips.
				t The researcher screams, though it's muffled. Her lips have grown much thicker and are a vibrant shade of red, and as she screams her tongue rolls out of her mouth at least six inches long. 
				t Within moments the infected retracts her tongue with a wet pop, her tongue wrapped around a thick pair of balls. Her tongue gently squeezes and the researcher spreads her legs, revealing her newly-grown cock just as it splurts its first load onto the floor.
				t Both women are heavily infected now, the recording goes on for another few minutes  wherein the two infected share a perverted kiss, their tongues able to reach so deep a bulge can be seen in their stomachs.
			`);
			}
			else {
				writeText("This scene has been disabled by your fetish settings. If for some reason want to watch it, you can still view it in the gallery after changing your settings.");
			}
			if (data.player.currentScene != "gallery") {
				writeText("[Finish|scarletTheater]");
			}
			break;
		}
		case "tapeHound": {
			writeHTML(`
				t <i><b>"Wake up, Jameson. It's time to play a game."</b>
				t A dark room is illuminated and a nude man jolts awake in surprise.
				t The only other thing in the room is a large dildo in the shape of a canine penis, it's length glistening with an unknown fluid lubricant.
				t <i><b>"It's very simple. You fuck yourself on that, or I open the doors to the kennels."</b>
				t There's a buzzing sound before the researcher yelps as the lights illuminate a half dozen canine forms in the adjacent room, held back by soundproofed glass.
				t After a short moment of hesitation the researcher complies. His body is smooth and hairless, it isn't clear what happened before the recording began.
				t <i><b>"That's infected precum. Mine, to be precise. It should make this game a little easier for you, but I hope you have some strong willpower."</b>
				t His hands shaking the researcher takes a breath to steel himself.
				im tapeHound1.gif
				t Despite it's size the researcher doesn't seem to struggle, suggesting this isn't the first game the faceless voice has played with Jameson.
				t With some effort the researcher grits his teeth and pushes the fat knot past his rim, his dick flaccid, but still visibly leaking at the insertion.
				t Suddenly a look of panic overtakes the researcher's face. It's clear now that his hair seems a little longer than it was at the start of the recording, and his chest looks puffier.
				t <i><b>"Oh? Looks like you've realized it. This time the knot is filled with cum, and you're squeezing it out inside your ass!<br>Better hurry and pull it out!"</b>
				t Nearly hyperventilating the researcher struggles to pull the sex toy from his ass, his grunts growing higher in pitch as her strugling only succeeds in emptying the sperm-filled knot inside of him until...
				im tapeHound2.gif
				t With an audible pop the knot comes free, but the researcher's body has already passed the point of no return. With a leaking cock the fresh infected pulls and pushes the toy anew, not a trace of human intelligence in her eyes.
				t The recording ends here.
			`);
			if (data.player.currentScene != "gallery") {
				writeText("[Finish|scarletTheater]");
			}
			break;
		}
		case "tapeSubmission": {
			writeHTML(`
				t Now beginning research log Y-25 "Submissive State", authorized by researcher JONES.
				t <i>"Although the situations where this can arise are rare on the field, if an infected is sexually dominated for an extended period of time, they experience bodily and behavioral changes to fit their position.
				im tapeSubmission1.gif
				t <i>These changes can affect factors such as penis length, muscle mass, pleasure sensitivity, and spermcount. Submissive infected can even be trained for mundane tasks by sexually dominant humans or accept things like chastity cages, even if they should be strong enough to break through the cage themselves.
				im tapeSubmission2.gif
				t <i>Originally, this was thought to be exclusive to hordes, but ever since the Alpha strain breakthrough further testing has found almost all infected can be dominated in some way.<br>Human domination is quite risky though. The effects of infected fluid by taste or smell, combined with heir heightened senses to notice even the smallest moment of weakness, means that human dominants do not tend to stay human for long."
			`);
			if (data.player.currentScene != "gallery") {
				writeText("[Finish|scarletTheater]");
			}
			break;
		}
		case "tapeVaccine": {
			writeHTML(`
				im tapeVaccine1.gif
				t The file is a recording, labelled VACCINE TEST, followed by a string of numbers. Moving through the video file it focuses on a woman being assaulted, though not unwillingly. She even speaks up a few times but is interrupted repeatedly when either a tongue or a penis is shoved into her mouth.
				im tapeVaccine2.gif
				t As the test progresses the pair devolve into a sexual frenzy, the woman doesn't seem capable of rational thought anymore. The video cuts to her alone, where she frantically rubs her pussy until she spasms out of the camera's view, and there's a faint wet popping noise.
				t By the time she's back on camera, she's...
				im tapeVaccine3.gif
				t And after that the video cuts another time, to her getting dressed and there's clearly no cock between her legs. She's intelligent enough to be listing off symptoms such as disorientation, extreme hunger and thirst, and phantom sensations. She's clearly distracted, and quietly asks when the next test will be before the video loops.
			`);
			break;
		}
		case "tapeChastity": {
			writeHTML(`
				t <i>"A major weakness of version 3 of the strain is that if an infected finds themselves unable to achieve release, they experience bodily changes that reduce their infective potency.
				im tapeChastity1.gif
				t <i>Freedom after a single day leaves the infected in a high state of arousal, essentially putting them on a hair trigger. This is undesirable for our purposes, but temporary.
				im tapeChastity2.gif
				t <i>After that point the changes are permanent. Genetalia shrinkage to fit the size of their cage and erectile dysfunction. While their sexual fluids are still infectious (multiple tests confirm this), they are unlikely to be able to overpower victims.
				t <i>It is worth noting that sensitivity only rises, but aside from propaganda or entertainment purposes this is not relevant to the goal of global infection.
				t <i>With the improvements made in the upcoming strain version 4 these weaknesses are not present. Extended chastity will lead to a pent-up state until the infected escapes containment.
				t <i>While the main branches will be moving on to the next generation with the strain version 4, our work here is still essential to the global effort. Even though parasites are no longer frequently used as common infection vectors, continued research led to the Mind Worm's creation.
				t <i>Likewise, even though we are behind the main branch here, exploration of the version 3 strain and it's weaknesses could lead to a major breakthrough. Those found wasting resources because we are 'behind the times' or 'working with an obsolete strain' will be suitably punished. Any strain could lead to an Alpha with enough research. With time and patience even our research site could birth a god.
				t <i>In short, stop putting chastity cages on our infected test subjects because they look 'cute'. There will be no further warning on the subject."
			`);
			if (data.player.currentScene != "gallery") {
				writeText("[Finish|scarletTheater]");
			}
			break;
		}
		case "labAnal": {
			writeHTML(`
				im labAnal.gif
				t <i><b>"I swear she's a lot happier like this. I wonder if she remembers selling out her comrades. She probably doesn't care.
				t <i><b>It's getting hard to focus on anything lately, except for when I'm thinking about how to train all my playthings here. Whenever I'm bored it feels like my conciousness slips a little further away. I should turn things up a notch."
			`);
			if (data.player.currentScene != "gallery") {
				writeTransition('scarletChambers', "Finish");
			}
			break;
		}
		case "labUrethra": {
			writeHTML(`
				im labUrethra.gif
				t <i><b>"I notice I've been giving out a lot of chastity cages lately, a sign that these girls are misbehaving more often. She will be kept caged up to keep her desperate and horny at all times.
				t <i><b>Luckily for her I gave her a special toy to ensure she can achieve relief even while locked up. It took quite a bit of training to give her a masochistic streak large enough though."
			`);
			if (data.player.currentScene != "gallery") {
				writeTransition('scarletChambers', "Finish");
			}
			break;
		}
		case "labSissy": {
			writeHTML(`
				im labSissy.gif
				t <i><b>"I really didn't get this place, or the 'people' here. Honestly, just forget the dick growth. Just make it some kind of sex-frenzy std and a lot of people would sneak it through containment themselves.
				t <i><b>Maybe this is my new dick talking, but I think I understand now. Taking the monsters who did this to me and my mind, fucking them so hard in the ass their brains and common sense melt, slapping cages on them so they're perfect for my cock.
				t <i><b>I get it."
			`);
			if (data.player.currentScene != "gallery") {
				writeTransition('scarletChambers', "Finish");
			}
			break;
		}
		case "labOral": {
			writeHTML(`
				im labOral.gif
				t <i><b>"Felt so in control setting it all up
				t <i><b>Watching her choose between dildo and air
				t <i><b>Her eyes when the toy spurted my cum into her mouth
				t <i><b>The way she kept suckling for more
				t <i><b>But now its hard to think
				t <i><b>Have to go farther
				t <i><b>Cant lose myself here yet
				t <i><b>Have to let them know im okay
				t <i><b>And happy
			`);
			if (data.player.currentScene != "gallery") {
				writeTransition('scarletChambers', "Finish");
			}
			break;
		}
		case "labCumbreather": {
			writeHTML(`
				t Inside is the scientist you met earlier, floating naked in a tank of an unkown liquid. Her eyes are barely open, she doesn't seem to recognize you. Or anything at all for that matter.
				t Her mouth and nose are connected to a long tube extending beyond where you can see, giving her a constant feed of white goo.
				t The flow is steady, but slow. Slow enough that you can see she's actively moving her tongue and breathing deeply to draw the sperm in faster. How on earth is her body still functioning? What is this doing to her brain? Are you witnessing a new evolution to whatever's going on here?
			`);
			break;
		}
		case "darkEscape1": {
			writeHTML(`
				t You slide the key into the lock and turn. You hear a soft *click* before the knob quickly turns and the scientist opens the door to freedom. Even in the mansion's darkness you can tell she's no longer wearing her mask.
				scientist That was you? Thank god, finally. Thanks, let's get out of here, we might not have very long. She might be watching now that her cameras and electronic locks are down.
				t The two of you make your way down the hall before the scientist taps you on the shoulder.
				scientist Okay, so the dogs shouldn't be too big of a deal. There's no outrunning them, but they've been conditioned not to attack certain members of staff. 
				t You look at her skeptically, but she doesn't pay you any mind.
				scientist There's shelter in the forest we can rest out the rain in, much better than waiting here, anyways. Come on!
				t She grabs you by the hand, you almost forgot she's still totally nude. The shelter must be close if she's willing to go out into the forest like this.
				t ...
				t As you arrive at the front door, it's clear she's nervous about the scratching sounds despite what she said earlier.
				scientist Down!
				t After a moment the scratching stops, and she breaths a sigh of relief.
				scientist Stay close to me. For your own sake.
				t She opens the door, multiple dogs push their way inside but true to their word they aren't attacking.
				t One of them locks eyes with you, staring. It's not the look an animal should give, there's no "nature" behind those eyes. It's like a robot waiting for a command. A weapon waiting to be swung.
				scientist Now. Take off your clothes.
				t The dogs suddenly surround you.
				scientist Don't look so surprised. You've seen the kind of work I've done here. These loyal boys are my ticket out of here, and  <i>you</i> are my ticket to a pair of shoes. I'm glad you came here so well dressed, by the way~
			`);
			break;
		}
		case "darkEscape2a": {
			if (data.player.beastDisabled == false) {
			writeHTML(`
				t You reach into your pocket to pull out the familiar item, it's the remote you found in the dining hall! You pull it out and in the faint light of a lightning strike outside you can see the scientist's face go pale.
				t The hounds heel, stepping back from you, and the scientist makes a desperate lunge for your hand.
				t You hold the remote up and shout "ATTACK!" And in a blur of motion the hounds lunge for the scientist, tackling her firmly into the ground.
				scientist Please! Wait, I-
				t She goes silent when one of the dogs opens its mouth, barring its jaws to her neck.
				t Knowing that the remote protects you from the hounds is useful, so you decide...
				t "Punish her."
				t You speak the words softly, barely feeling like it's really you who's speaking, but the hounds get the message. Having just barely survived her ordeal with the mask she looks at you, clearly begging for mercy with her eyes, only to find no mercy waiting for her in yours.
				t She can't keep eye contact with you for long though, as a hound adjusts itself to hang it's massive animal cock directly in her face. She turns her head and holds her breath, beads of tears forming in her shut eyes as she feels another hound's throbbing cock align itself with her pussy. But eventually she can no longer hold out, and is forced to take a breath.
				t Tortured for days, maybe weeks by being forced to breath in sperm fumes, her cum-ruined brain knows when it's time to submit, and here, beneath a throbbing red dog-cock that must be at least ten inches long, this is certainly the time.
				t Her eyes glaze over, she's done for. Knowing that even if you tried at this point, you couldn't separate her from those cocks even if your life depended on it. She's long gone, her mind's already shifted towards being an onahole to these beasts.
				im scientistEscape2-1.gif
				t Her body is yet to change though, soon enough she'll be fully infected like the rest of the mansion's inhabitants. The hounds don't seem to mind that her pussy is growing more shallow each time they leak their infectious precum into her.
				t Experimentally, you press the button on the remote, and-
				scientist GHHHHHHK~
				t A gutteral scream of pleasure around a far-too-thick shaft, the hounds rapidly speeding up the pace of their bestial thrusts. This place is changing you, your old self never would have gotten a kick out of that reaction.
				im scientistEscape2-2.gif
				t But you notice more hounds waiting outside the door, uncaring that they're soaked in the wind and rain. Thinking quickly, you lock eyes with one of the hounds raping the scientist and point outside. It seems to get the message. It pulls free from the scientist's pussy, grabs her hair by the teeth, and the hounds drag her outside of the mansion.
				t The last thing you see of the would-be-traitor is her jerking, her pussy spraying, and her clit growing before your eyes. You close the door, your life barely saved by the remote, but you're back to where you started on finding your sister.
			`);
			}
			else {
				writeText("This scene has been disabled by your fetish settings. If for some reason want to watch it, you can still view it in the gallery after changing your settings.");
			}
			break;
		}
		case "darkEscape2b": {
			writeHTML(`
				t You close your eyes, cursing your luck. Out of all the people you could have saved, you stuck your neck out for <i>this</i> bitch.
				t But after a second you realize the growling has stopped. You open your eyes to see the hounds, and the scientist, facing the mansion's main doorway, which is creaking open.
				t You can't make them out clearly, but the figure behind the door must be at least eight feet tall and is shrouded in shadow.
				t The dogs immediately heel, and the scientist steps backwards and trips over.
				t You run without thinking, straight out the front door and closing it shut behind you. Whoever... <i>Whatever</i> that was, you'd rather face the weather and the... Hounds!
				t There are still some out here, and you look around in a panic, but relax when you see they're huddled back into their kennel in a submissive stance. Their master is nearby, they seem to sense it, and they won't act without a command.
				t Their posture is extremely strange, huddled with their tails between their legs, but without an expression of fear on their faces.
				scientist NO! PLEA-GHHHHK-!
				t You swallow nervously. The wet sounds from inside are so pronounced you can tell exactly what's happening inside.
				im scientistEscape1.gif
				t The scientist is doomed to the fate she just narrowly avoided earlier. You can <i>hear</i> it, rapid, deep thrusts of an inhumanely massive cock stretching her throat beyond belief. Huge, dangling testicles battering her face and leaving her completely disorientated, all without so much as a single source of light to orientate herself as she lays on her back beneath an alpha infected.
				t And what you can hear next... You can barely believe it. You can hear the infected alpha cumming, a load so thick it gurgles as she unloads down into the scientist's throat, filling her stomach like a condom. 
				t You can't hear her struggling anymore. The gurgling grows fainter, but you can tell it's because the source is moving. The alpha's picked her up, no cum splattering or coughing tells you she's moving back to where she came from with the scientist still impaled face-first on her dick.
				t You hold your breath, waiting. You count to ten, wind and freezing rain battering your face and body, before you open the door again and head inside.
				t Empty. Nothing but a pool of slime on the floor, a mixture of saliva and something... Unmentionable. Left behind as proof the scientist was here.
				t Whatever that creature was... It must be from a secret part of the mansion. You can't bear to imagine it's already gotten ahold of Lansley, your only hope is that somehow, against all logic, she's still somewhere in that secret area, and that she's still human.
			`);
			break;
		}
		case "scarletEndingStart": {
			writeHTML(`
				t And as you turn the corner you see her, illuminated by a series of monitors that display rooms across the house, each focused on a different trapped infected woman.
				t She turns to you, and you recognize her. Lansley.
				t Her 'chair' is actually a pair of infected, one with her mouth and an inhumanly-long tongue wrapped around her cock, and another clearly and very happily rimming Lansley's ass. Lansley clicks her tongue and the two infected let out a whine, but you can hear them begin to retract their tongues.
				t Lansley steps forwards, dragging her pair of slaves with her before they can finish pleasuring their master.
				t As she faces you her eyes are filled with shock and hatred, but those quickly fade away, replaced with surprise and... Sadness? Melancholy? But quickly she looks frustrated, and roughly grabs the infected choking down her baseball bat of a dick by the hair to shove her down even deeper. Her massive balls contract and you can somehow hear, despite the distance between you two, the sound of at least a pound of jizz filling and inflating the infected woman's stomach.
				t The two infected slaves squeal (or as best they can given their situation) with delight, as if knowing they've served properly as fleshlights with heartbeats is enough to grant them relief as well.
				t ?fetish ws; As they finally retract their tongues and wait dutifully at their master's side, Lansley grunts and a thick stream of piss begins to splatter against the floor. The pair of infected women move quickly, one lapping it up from the floor and the other sealing her mouth around the piss-spigot, her stomach visibly bulging as her swalling can barely keep pace.
				t !fetish ws; As they finally retract their tongues and wait dutifully at their master's side, Lansley grunts and a thick glob of precum, more than a normal man's load leaks from her cock. The two infected fight over it, hungrily licking it up and battling with their tongues passing it between them.
				t This... This monster isn't your sister. She can't be. Even though there's a glimmer of sadness, regret in her eyes as the two of you stare at each other, every other part of her body is...
				t Every instinct inside you is telling you to run. She takes a step forwards, arms outstretched as if asking you to come closer.
			`);
			break;
		}
		case "scarletEndingGood": {
			writeHTML(`
				t <i>"-nd so perhaps it's possible that with enough sexual satisfaction, the infected could enter a docile, dormant state.</i>
				t A familiar video is playing again in the background. In your first few weeks as the mansion's... 'Caretaker'... You obsessively searched for any trace of a cure, but without any luck. No method to reverse the infection was found, and trying to contact the outside world was fruitless.
				t So you acclimated. You've become the mansion's keeper, handling the cleaning, organizing the extremely plentiful food stores, and most importantly...
				im scarletEndingGood1.gif
				t Keeping Lansley satisfied with the rest of the infected. She regularly enjoys herself with the numerous infected around the mansion and you push her even farther to keep her docile, under your control.
				t As the queen she in turn keeps the rest of the infected in check, and the many, many perverted traps she created before losing her mind are good ways to teach the rowdier infected their place.
				im scarletEndingGood2.gif
				t In your off time you've begun experiments to better understand the infection, altering strains and even going so far as to test them on stragglers the hounds capture and study the results. You're feeling more like a mad scientist every day, growing farther and farther from your older, more noble goals.
				t The days have begun flying by. Everything is set up to be self-sustaining at this point and it's become tempting to join in with the rest and infect yourself.
				t But you still have so much more work to do.
			`);
			break;
		}
		case "darkEndingStart": {
			writeHTML(`
				t You turn the corner into complete darkness. You can faintly hear some kind of noise, a familiar one you've been hearing throughout the mansion.
				t You keep carefully steping forwards, an odd scent fills the air. A few more careful steps forwards.
				t Why have you come this far? Even if you find Lansley, it's not like you could take an infected here in the darkness.
				t You suddenly stop at that realization, why <i>have</i> you come down here? It's almost like... You're being pulled. Pulled by the smell, by the sounds, like someone's been gently tugging you by an invisible chain.
				t You turn around, your heart beating in your chest, when you feel someone's breath on your neck. And in this lightless mansion, there's no way to tell who it is.
			`);
			break;
		}
		case "darkEndingFinishMale": {
			writeHTML(`
				t You're pushed to the ground, voices around you, there must be at least two more besides the one who threw you. They're on you in seconds, rubbing their soaked bodies against yours, you can only imagine what they're soaked with. The thick smell of infected sperm fills your lungs, but you have to resist. If there's any chance of escape, of finding your sister, you have to take it.
				t The pair continue to grind themselves against you, but they don't even seem to attempt to press their cocks against your mouth or ass. You can't see anything in this darkness, but what you can <i>feel after they've ripped the clothes from your body...</i>
				im darkEndingFinish1a.gif
				t From the front, a pair of inhumanly soft dick-sucking lips wrapped around you, practically trying to suck the soul and semen out of your body.
				im darkEndingFinish1b.gif
				t From behind, another one has her lips pressed tightly against your ass, her tongue deep enough that it makes you shiver to the bone. 
				t You still need to keep fighting. Saliva isn't as potent as cum, and you've even heard stories of people keeping their minds somewhat intact after being infected. There's still hope!
				t And as the pair of infected women bring you towards an earth-shattering orgasm, somehow even the pitch-black room begins to spin. You're changing. You can feel it.
				t <i>She</i> approaches, pushing away her pets.
				t You just need to last a little longer, that's all you tell yourself. You need something, anything to hold onto.
				im darkEndingFinish1c.gif
				t For a chance to escape.
				im darkEndingFinish1d.gif
				t For the lights to kick back on.
				im darkEndingFinish1e.gif
				t For... For your humanity to fade.
				t ...
				t *CLACK* *CLACK*
				t The lights come back on, monitors flicker to life, only just barely illuminating the basement room. A darkness so deep you could hold it in your hands fades away.
				im darkEndingFinish1f.gif
				t But it's too late for you. You'd be unrecognizable even if you hadn't been fucked into eternal bliss by the woman atop you.
				t She's slow, taking her time to milk out absolutely <i>massive</i> orgasms, infecting you as deeply and completely as possible, almost like she delights knowing there's no hope of escape for you any more.
				t Is it over? Can you finally give in? No one would fault you for giving up here, now, right?
				t As you push back against her to bottom out for that extra bit of dick, the answer is clear.
			`);
			break;
		}
		case "darkEndingFinishFemale": {
			writeHTML(`
				t You're pushed to the ground, voices around you, there must be at least two more besides the one who threw you. They're on you in seconds, rubbing their soaked bodies against yours, you can only imagine what they're soaked with. The thick smell of infected sperm fills your lungs, but you have to resist. If there's any chance of escape, of finding your sister, you have to take it.
				t The pair continue to grind themselves against you, but they don't even seem to attempt to press their cocks against your mouth or pussy. You can't see anything in this darkness, but what you can <i>feel after they've ripped the clothes from your body...</i>
				im darkEndingFinish2a.gif
				t From the front, one with an inhumanly soft pair of lips mashed against your pussy, her tongue as deep inside you as it can reach eager to infect you as deeply as possible with just her tongue.
				im darkEndingFinish2b.gif
				t From behind, another one exploring you in ways you've never tried before, her tongue dancing around your ass like she's genuinely delighted, turned on beyond belief, at the chance to rim your ass.
				t You still need to keep fighting. Saliva isn't as potent as cum, and you've even heard stories of people keeping their minds somewhat in tact after being infected. Even if you transform, even if you wind up growing a massive, throbbing, leaking cock, there's still hope...
				t That's what you tell yourself as you grab the woman in front and grind your cunt against her face as hard as you can. You're just... Tiring her out! Then you'll escape! 
				t And as the pair of infected woman bring you towards an earth-shattering orgasm, somehow even the pitch-black room begins to spin. You're changing. You can feel it. Maybe you'll be strong. Strong enough to find your sister.
				im darkEndingFinish2c.gif
				t The first wave of thoughts that follow when you think of your sister make you want to scream, to cry. Images of you, having given in, having accepted the cock, pressing her down while you <i>mount</i> her.
				t You're losing control. You let out a gutteral scream, both hands gripping the hair of the woman beneath you, face-fucking her like you've already been fully infected, while the second eagerly continues to make out with your asshole. This bitch wants it rough. She <i>needs</i> it rough. You can be as brutal as you want to be, and when you're done with this practice...
				im darkEndingFinish2d.gif
				t You have to hold on... You have to see her again, as a human... You want to find her...
				t You cum. Your legs clench hard enough you're sure you're cutting off airflow to the infected bitch beneath you, but you don't care.
				t But it isn't stopping. You lurch forwards, perhaps out of some instinctive desire to escape, perhaps because the pleasure is just too much, but you feel like you're seizing with pleasure on the floor as <i>she</i> pulls her pets away from you.
				t The lights are still off. You feel warmth from your womb. You won't be able to see yourself change, but that just makes the mental image more vivid.
				im darkEndingFinish2e.gif
				t You're almost there. Your womb has changed, you have two, <i>massive</i> balls waiting and ready to be birthed, each full of sperm so potent you can feel it bubbling up inside of you.
				t Your legs keep shaking, you still haven't finished the orgasm you started atop the two women. Even in the pitch-black darkness of the room, the walls somehow are still spinning. Your hips shake, you can feel yourself squirt harder than ever before. Your old life is over.
				t Release.
				t You aren't squirting anymore. You can feel it, arc after arc, string after string pump through the air, painting the floor, the faces of the two infected whores, maybe even the walls...
				t Your eyes roll back. It somehow gets even darker as you black out.
				t ...
				t *CLACK* *CLACK*
				t The lights come back on, monitors flicker to life, only just barely illuminating the basement room. A darkness so deep you could hold it in your hands fades away.
				im darkEndingFinish2f.gif
				t But it's too late for you. You'd be unrecognizable even if you hadn't just grown a third leg, had a pair of massive, heaving nuts beneath your member.
				t <i>She</i> is impressed. You can just tell. The one who's been orchestrating the traps and tortures of the mansion...
				t Maybe she's got a submissive streak too.
			`);
			break;
		}
		case "scarletEndingBad": {
			writeHTML(`
				t You slam your way through the front door and run out into the rain, the sound of barking hounds barely audible over the growing storm.
				t No time to plan, no time to think, you sprint as fast as you can.
				t As you make your way away from the mansion, into the woods, you can hear the hounds gaining on you. Does an even worse fate than being Lansley's bitch await you?
				t But just as you feel the hound's breath on your neck, the sound of flesh striking flesh can be heard. Barking, whimpering, and a woman yelling at the top of her lungs sound like they're right behind you.
				t No time to look back, you need to keep running to survive.
				...
				t Your legs not able to push you a single step onwards, you collapse in a heap into the mud. You made it out alive, with your humanity intact.
				t You look back at the forest, no sign of the hounds or Lansley. But why? Either of them could have caught you easily? Were you saved? By who?
				t No answers, only questions remain. But you're still human. You'll stand when you can again, and make your way to the city to find help. There's no telling how far the infection will spread, how much of humanity is under siege, but you do know one thing:
				t You escaped the scarlet mansion.
			`);
			break;
		}
		//Typhoid Mary
		case "typhoidIntro3a": {
	writeSpeech("player", "", "S-stop I'm sorry, please go!");
	writeSpeech("Monica", "monica", "Okay, okay, fine. Listen, you've been acting weird lately, I just want you to know I'm here for you, no matter what. I'm heading out on a mission soon, scouting downtown, so-");
	writeSpeech("player", "", "Thank you, I know you mean it...<br>I just... I just need some privacy right now.");
	writeSpeech("Monica", "monica", "Okay. I gotcha, I'll go.");
	writeText("She walks out of your room, closing the door behind you.");
	writeSpeech("Monica", "monica", "Hey, you ever need someone to talk to, you let me know, alright? Everyone has broken down at least once at this point, being a survivor isn't easy. We've lost people we love, and it seems like things will never get better...");
	writeText("You don't say anything in response, listening carefully for the sound of her footsteps as she walks away.");
	writeText("She was right, everyone does break at some point. Usually it's depression or a panic attack as the world looks darker everyday, but as you pull off your pants and grab your tights, you have to admit that this is a bit of a different case.");
	writeText("*RIIIIIIIIIP*");
	writeBig("wanton.gif");
	writeText("Like the broken excuse for a human you are, you wantonly start to explore yourself in the middle of the room. The logical part of your brain is pleading to stop, but it feels way to good for that. Instead, while you enjoy coating your room in spunk, you start to think of a strategy.");
	writeText("You'll need to pass as uninfected for now, so you'll need a plan. The building being used as a compound does have a 'self-pleasure' room, keeping people pent up with a horde of dickgirls outside is a bad mix after all. Maybe you can find a chastity cage to keep yourself contained during the strategy meeting later today? In any case, it's no longer a matter of just surviving, it's finding some way of being a shemale obsessed with cumming in a post-apocalyptic world without getting caught.");
	writeText("Thoughts of survival are drifting away. Now the logical part of your brain is working in tandem with the darker, more instinctual part birthed by your infection.");
	writeSpeech("player", "", "If I can't have a normal life, why should they? They'd throw me out without a second thought if they knew the truth...");
	writeText("You begin hatching a scheme, if this is the end of your life as a survivor, you're going to see everything crumble with you. First is the strategy meeting later today, if you're going to infect the compound, you've got one day to do it.");
	break;
}
		case "typhoidIntro3b": {
			writeText("You say nothing as she scoops your cum and sucks it off her finger. Is it morbid curiosity? Selfishness? Or pure perversion that stops you from saying anything? You have no idea.");
			writeText("Immediately her face scrunches up in disgust.");
			writeSpeech("Monica", "monica", "Ah, fuck. I'm sorry girl, but when was the last time you washed these sheets? This shit's ruined, doesn't even taste sweet anymore. Listen, I can sneak these into the wash tomorrow, you good with... With...");
			writeText("The infection hits everyone at different speeds. The bit of precum you got must've been weak, it took an hour to really kick in, but this is the first load of a new set of balls, and Monica looks dizzy right away as she swallows on instinct.");
			writeText("You see the lump travel down her throat, and your cock throbs against your pants.");
			writeSpeech("Monica", "monica", "It's... God it's so fucking hot in here...");
			writeSpeech("player", "", "Yeah, life's a bitch sometimes.");
			writeSpeech("Monica", "monica", "I, uh... Could I sit down a moment? I don't feel...");
			writeSpeech("player", "", "Go ahead. Take as long as you need.");
			writeText("She lays back on your bed, her breathing erratic. The effects of the infectious sperm are hitting her hard and fast, and in a moment she's writhing and scratching at her jeans.");
			writeSpeech("Monica", "monica", "I... Hot! It's... Ugh, it hurts!");
			writeSpeech("player", "", "Are you sick? Maybe we should get those jeans off. And maybe you need... Need more fluids...");
			writeText("You grab the clump of bedsheets and press it against her face. Despite her protests at first, all she can do after a few breaths is squirm and moan. You let go of the cloth and start pulling off her pants. Before they're even down all the way, she screams into the bedsheets and the squirming stops.");
			writeText("You roll her over to better get her pants off, and appreciate the fruits of your labor.");
			writeBig("monica1.gif");
			writeSpeech("player", "", "No panties? Dangerous in our lines of work, makes it all the more convenient for an infected to have their way with you. Hold still.");
			writeText("She gives a tired grunt, but no other response as she stops failing about.");
			writeBig("monica2.gif");
			writeText("She should be desperately pawing at herself to get off, her first load churning in her new balls, but she obeys with nothing more than a whimper.");
			writeText("You start stroking faster. You can see her balls throb.");
			writeSpeech("player", "", "Don't cum, hold it in.");
			writeBig("monica3.gif");
			writeText("Despite being clearly in the throes of an orgasm, she does her best not to splurt onto the floor, only leaking a few droplets.");
			writeSpeech("player", "", "Stand up.");
			writeText("She obeys again. Her eyes have fogged over, there's not a trace of humanity left already.");
			writeSpeech("player", "", "I... Why are you listening to me? Sit.");
			writeText("She obeys again. She's obviously pent up and fidgets occasionally, but she doesn't touch herself.");
			writeSpeech("player", "", "You'll... You'll obey me now, won't you? Do whatever I want... Will others, too?");
			writeText("Hiding a member of the infected, especially one as mindless as Monica is now, while also explaining her disappearance would be tough, nearly impossible after a while. But if you can command the infected, suddenly a lot of possibilities open up. If you told her to lay low while you infected others...");
			writeSpeech("player", "", "I could infect the entire compound...");
			writeText("Thoughts of survival are drifting away. Now the logical part of your brain is working in tandem with the darker, more instinctual part birthed by your infection.");
			writeSpeech("player", "", "If I can't have a normal life, why should they? They'd throw me out without a second thought if they knew the truth...");
			writeText("You begin hatching a scheme, if this is the end of your life as a survivor, you're going to see everything crumble with you. First is the strategy meeting later today, if you're going to infect the compound, you've got one day to do it.");
			break;
		}
		case "infectedTyler": {
			writeHTML(`
				t Without so much as a knock, you open the door and instruct Lisa to walk in.
				tyler Lisa? Is that you? I heard you were planning a surprise for me~<br>But it's <i>your</i> birthday coming up, so... Could you lift this blanket off me?
				im infectedTyler1.gif
				tyler Ta-da~!
				t You can see Lisa's eyes actually dilate a little, a common occurence when looking at someone you love. Although for her, it might me more hunger...
				tyler Hehe, why so quiet? Your boyfriend's all tied up and ready for a fun night. Oh, did you think I'd forgotten? I would-
				im infectedTyler2.gif
				tyler Hooh~! Th-this blindfold really does a lot of work! Or maybe you've finally decided to get rough with me? Don't worry honey, I won't break, unless you ask me to~
				t Waiting for your orders be damned, Lisa clambers atop Tyler's bound form and lines herself up with his ass. Stroking herself, it doesn't take long with that fat ass in view for a truly monstrous surge of precum to leak from her cocktip onto and between his cheeks.
				tyler Ooh~! Warmed up, just for me? What brand is this? Did you find it out-
				t And without further warning, Lisa presses her monster shaft to his ass. What happens next is <b>NOT</b> lovemaking, that's for sure. It damn well isn't gentle either.
				tyler Hrrrgh~! W-what's gotten into you? You normally w-want to start with kissing and cuddles~! N-nnnghh~!<br>This is so much thicker than the last one!
				t You see her balls clench up. Instinctively you know, it's over. Lisa's so potent that the first covering of precum might have infected him just from being used as lube, but a fat glob of precum directly poured into his rectum?
				tyler HOOOH~! FFFFFUCK YES~! MY PROSTATE-!
				t Slam. Slam. Slam. No kindness. No gentleness. This is <i>rape</i> through and through. If it weren't for Tyler screaming his way to the strongest orgasm of his life this scene would be right out of the first days of the infection.
				im infectedTyler3.gif
				tyler Haaaaaaugh~
				t He's cumming, more squirting, really. You can make out his chest plumping up just a little, his twinkish face changing just so slightly. He wore makeup for this, and it's getting ruined as Lisa mashes his face into the bed to get better leverage while she absolutely destroys his bitchhole.
				tyler Luu... Lurve... Honey~... Cumm~
				t She goes balls deep and her balls clench, the sperm jet audible as it's injected probably right into his stomach. She loves him alright, he'll be her favorite onahole for life.
				t Tyler's fate is sealed, and Lisa ought to stay loyal now that she's got a dedicated ball-drainer on hand. You, meanwhile, have more work to do. If you waited until Lisa finished, you'd be here all day.
				t God knows they will be.
			`);
			break;
		}
		case "sabotageAmy": {
			if (data.player.wsDisabled == true) {
				writeHTML(`
					This scene has been disabled due to your fetish settings. If you'd like to view it, re-enable watersports content and check the gallery.
				`);
				/*
					t Just imagining those small, infectious tadpole-shaped sperms wriggling down an unsuspecting woman's mouth as she guzzles the drink down, completely unaware that she's forming her last addiction, is enough to have you messily splurting a thick load as you hump you hand.
					t While you planned to taint the drink, you quickly realize you hadn't expected exactly how much jizz you'd be oozing so quickly, and the container is filled.
					t *SPLAT*
					t And overfilled. You really should stop, but the sensation is heavenly. Drugs, alcohol, you don't think you'll have any vices but wild, raucous sex in the future.
					t By the time you're finished you're left with a container filled well beyond capacity with some of the most pungent yet enticing white fluid you can imagine, and a huge mess on the floor you'll need to clean up. Now, you could dispose of the mess quickly and spike just one drink, or you could take your time and infect the entire store of fluids.
				*/
			}
			else {
				writeHTML(`
					t You check to make sure nobody's around before heading to the back.
					t Energy drinks, a survivor's lifeblood on long days. That's most of them since the end of the world. With that said, nobody here will mind an extra bit of flavor~
					t You pop open the top of the first container, drain it a decent amount, and relax.
					im sabotageAmy.gif
					t Just imagining an unsuspecting woman, maybe Amy, guzzling down a load of your piss, ompletely unaware that she's forming a permanent addiction, causes the edges of your mouth to curl with glee. 
					t While you planned to taint the drink, you quickly realize you hadn't expected exactly how much piss you'd be spraying so quickly, and the container is filled.
					t *FSSSSSHHH*
					t And overfilled. You really should stop, but the sensation is heavenly. Drugs, alcohol, you don't think you'll have any vices but wild, raucous sex in the future.
					t By the time you're finished you're left with a container filled well beyond capacity with some of the most mind-wracking yet enticing yellow fluid you can imagine, and a huge mess on the floor you'll need to clean up. Now, you could dispose of the mess quickly and spike just one drink, but where's the fun in that?
				`);
			}
			break;
		}
		case "infectedAmy": {
			writeHTML(`
				t You step into Amy's room and start undressing her. She won't be needing these anymore, so you don't pay too much mind to each piece you have to tear in half.
				t Now you're both fully naked, you're straddled over her chest and you decide to give her face a nice paintbrush.
				t *WHAP*
				t Cockmeat across the cheek, resting against her nose as she takes a sharp breath, and to your suprise you see her eyes dreamily open and take in the view.
				amy Oh... Oh waaaaaow~<br>Issat for me? Ehehe~
				t She's completely out of her mind, although that doesn't excuse what her instincts say to do next.
				im infectedAmy1.gif
				amy Mmm, mmmph, mmmm~
				t She starts sucking your cock like she's <i>begging</i> to be infected. To be taken away from her boring, scary life with an orgasm so strong it melts her mind.
				t So you indulge. Roughly. You slap your hips against her and thrust your cock as deeply as it can go. Whatever dark fantasy of hers you're tapping into, it's doing some serious work on her body and her completely soaked cunt.
				im infectedAmy3.gif
				t Not all of that can be from your infectious precum. This is her. Amy. This is who she is deep down after all.
				player Brace yourself, whore.
				t You change position as you feel your nuts clench. You know she'd probably love to taste the load but if she has objections she sure as hell doesn't voice them.
				im infectedAmy4.gif
				t Her throat completely stuffed, her eyes rolling back, she's drinking every drop of your load happily.
				t The change is happening quickly. Within moments she's squirming and her rubbing grows more frantic. You see her flex her pubic muscles and hear a soft pop. You rip away her panties and throw the sopping rag aside.
				im infectedAmy5.gif
				t Already cumming, her very first load surging through her body and her brain can barely handle it. You graciously pull out, allowing her the chance to breath.
				t Or you would, but she grabs you by the ass and pulls down, thrusting her hips in the air as she rides her orgasm just a little longer.
				player Down!
				t It takes an actual order to convince her to breathe, the pleasure's just worth that much more than air to her.
				t With a schlorp caused by her vacuum seal around your dick, you pull free and let one last rope of jizz cover her face as she coughs and struggles to find air.
				t She flops around for a moment, her orgasm finally stopping before she passes out.
			`);
			break;
		}
		case "infectedLisa": {
			writeHTML(`
				lisa Thanks. 
				t She takes a swig of the spiked water. Then another. And another. What was just a way to help a headache suddenly looks like she's guzzling it like a stranded sailor.
				lisa F-fuck, that was good. Wait...
				t She notices a small strand on the rim. Not the kind of thing she'd see from just water, and she stumbles.
				lisa Sh-shit... What the fuck was in that?!
				player I think you know, Lisa.
				t You've shedded your clothing. It'll just be in the way for now.
				lisa What...
				t She looks at your member with... Awe? There's some shock mixed in, but still.
				player D'aww, where's that feisty resistance I was expecting? I was at least expecting a 'please, no'?
				lisa Why... Why did you...
				player Honestly? What more could I do? Nobody can resist this, you'll see.
				lisa F-fuck you, I won't-
				t You grab her and push her roughly down onto a table, before you start peeling off her pants.
				player You're gushing, see? This is just how life is. We were dealt a shit hand, Lisa. both of us.
				lisa C-coward... You aren't even trying to fight it!
				player Well, let's see if you're right.
				lisa No!
				im infectedLisa0.gif
				lisa NOOO!
				player Ghh~! So tight! Feeling it yet? It'll crawl into your mind, thoughts that aren't yours fill your head. You have <i>needs</i>, and everything else seems small in comparison.
				im infectedLisa1.gif
				lisa Stuh... Stop...! I can feel-
				player Feel the precum?
				lisa That's... That's just precum?!
				player Ngh~!
				t Forceful thrust leads to forceful thrust as you make a complete mess of her body. Her objections fade into loose collections of sounds and moans as the infection takes hold, but...
				lisa Nuh... Won't...
				player Cumming~!
				t Glug, glug, glug, thick load after thick load paints the inside of her pussy and all she can do is bring her hands to her face to try and hide herself from you and the world.
				im infectedLisa2.gif
				player Hah... It got shallow near the end. You're ready, aren't you?
				lisa N... Nuh...
				t Desperately, she holds on. With every fiber of her humanity left. She closes her legs and holds on tight.
				t And when the pressure fades suddenly, there's a split-second of relief. Of delusional hope. Of-
				im infectedLisa3.gif
				lisa ...
				player You're turned, Lisa. Now I'll make you a deal. Show me you can resist. Prove it's just that I'm weak, that willpower and-
				t Her new cock throbs, hard. It's an inch longer than it was before, and she wraps her hand around it without a thought. It grows again.
				im infectedLisa4.gif
				player Ho... Ly shit... So? You still think I'm weak for not being able to resist?
				t She doesn't respond, staring at her cock as she strokes its titanic length up and down.
				player About what I thought. Come on, let's get you to bed. You can masturbate to your heart's content until I need you.
				t She groans, her cock bobbing in the air. There's a tangible heat radiating off of her, she's definitely special.
			`);
			break;
		}
		case "infectedAngela": {
			writeHTML(`
				t You step into the room to check on Angela, only to find her completely unconscious with her toy knot-deep inside of her.
				t ...
				im infectedAngela.gif
				t Immediately, she could tell something was off. No, <b>wrong</b>. Mental alarms immediately began to sound at full volume, but were quickly drowned out by an unfamiliar sound as the room began to spin. 
				t That sound of course being her guttural, drawn out moan as her legs and eyelids quiver, louder and more whorish than any sound she'd made before in her life. All the while her hand reflexively squeezes harder around the bulb which causes the cock to splurt, pressuring more of the thick, infectious sperm up the silicone shaft.
				t She's experiencing a prime, zero to one-hundred complete infection and her pussy is squirting like a fountain as her unprepared body feels like it's melting away.
				t The moan quiets as she finds herself without the strength to take another breath, especially since her body's change has already begun. Her anal fun has already filled her body to capacity causing her newly developing prostate and balls to desperately fight to find space. 
				t To an outside observer it might seem like she's leaking from a fresh creampie, but actually there's just not enough space for her balls to hold the sperm she's so rapidly creating. She's cumming like one of the infected before her transformation is complete, which is only further compounding the pleasure she's experiencing as her vision flashes white and her entire body spasms and seizes. Unable to handle any more, she collapses like a broken toy onto the floor, allowing her new testicles and several inches of the dogcock toy to slide free. The knot still inside her ass, and her hand still pulsatingly squeezing on the cum pump reflexively as her consciousness fades.
				t ... 
				player My my my, you didn't hesitate at all to have some fun, did you?
				t You grab onto the dogcock and tug.
				t And tug, and tug, and tug. Even unconscious her selfish asshole won't give up without a fight, but finally the knot is pulled free, the size is actually daunting.
				t Impressively, even though her ass can't manage to wink shut, no sperm is leaking out. She must have absorbed every bit of it into her system. Granted, the toy is so large she may have been able to squirt your sperm as far as her stomach, but she's still earned your respect as the base's soon-to-be top cumdumpster.
				player I don't see you managing to help me take the place over, I don't think Keith or Tyler could satisfy that cavern you've got between your cheeks either. You just wait here and maybe we'll find you an infected dog, or maybe a nice stallion once the base is overrun, alright?
			`);
			break;
		}
		case "typhoidWaterPrep": {
			writeHTML(`
				t With a nice glass of water in hand, you ponder for a moment.
				t Cum is out of the question, unless you can convince your target they'd like to try drinking saltwater.
				t Most other fluids are out too, it needs to be close enough visually.
				player ... I got it.
				t ...
				im typhoidWaterPrep.gif
				t You let line after line of drool fall into the water and swish it around. It probably won't be enough to turn somebody, but there's no way they're resisting your advances after a nice hearty glass of this stuff.. 
			`);
			break;
		}
		case "cagedWeak": {
			writeHTML(`
				t You take the plastic cage and slide it onto your cock.
				t ... Sliiiiide it on.
				t ... Sliiiiii-
				player Fucking get in there already! I'm not infecting this place with a third... Fucking... Leg sticking... Out!
				t *CLICK*
				player Hoh... Holy shit...
				im cagedWeak.gif
				player Ffffuck... Why does it feel so...
				t It feels good. Really good, somehow. You feel locked down, but at the same time there's so much pressure it's impossible to focus on anything else.
				t Almost impossible, really.
				player Alright... Just gotta... Ghh... Fuck, this is gonna be hard.
			`);
			break;
		}
		case "cagedStrong": {
			writeHTML(`
				t You take the metal cage and slide it onto your cock.
				t ... Sliiiiide it on.
				t ... Sliiiiii-
				player Fucking get in there already! I'm not infecting this place with a third... Fucking... Leg sticking... Out!
				t *CLICK*
				im cagedStrong.gif
				player Fuck~!
				t The moment it slides on, it instinctually feels wrong. You're trapped. The key's in your hand but your instincts are screaming for release.
				player G-guess I don't like metal... But the plastic one would just break... Hold it together playerF, it's in the cage or out on the streets...
			`);
			break;
		}
		case "typhoidFleshlight": {
			writeHTML(`
				player Hah... Well, I guess settling for a toy will do for now. I'd prefer the real thing though.
				im typhoidFleshlight1.gif
				player Fuckfuckfuck~!
				t You completely underestimated the fleshlight, your hyper-sensitive cock can feel every single artificial groove on the inside of the pocket-pussy, especially the spongey ring which kisses your crown with every deep thrust.
				t You feel and look like some kind of animal, like a dog desperately humping away without remorse as your common sense takes a backseat to your libido. Squelching sounds fill the room as your titanic nuts fly forwards and back as you slap your hips into the soft texture.
				im typhoidFleshlight2.gif
				t Unlike a normal human you just keep thrusting as you cum, your heaving nuts clenching up as you spray the inside of the toy. Once, and you feel warmth backwash over your cock. Twice, and cum begins to leak from the vent holes in the back as the toy overflows. Three times is the finish, you pull free and you hear a popping sound.
				t The fleshlight is ruined, gaping open completely stuffed like an overfilled donut actively leaking sperm from both ends. This thing is a chemical weapon now, although not subtle enough to be used as one for your purposes.
			`);
			break;
		}
		case "typhoidDildo": {
			writeHTML(`
				player Hah... Well, I guess settling for a toy will do for now. I'd prefer the real thing though.
				im typhoidDildo1.gif
				player Ouuugh fuuuuck~!
				t Being unfamiliar with anal you took it slowly and carefully, no good risking your health just before you've taken over the compound. It was even uncomfortable at first.
				t Then you hit the prostate. Bumped up against it, and suddenly you saw the appeal.
				t Now you're bouncing at a steady rhythm, your cock flopping in sync, your body learning to love the feeling of being stretched wide. It has a good incentive to after all.
				t One more thrust and you feel yourself cumming, your muscles clenching in a familiar way but what's coming out feels so light. Another bounce, the feeling intensifies but you're still more leaking than cumming.
				t You've passed the limits of pleasure a regular orgasm can take you and it just keeps going. Bounce. Bounce. Bounce. Finally, you start to cum.
				im typhoidDildo2.gif
				t Or so you thought. What leaks out of you is entirely clear, not a trace of proper sperm, is this precum? Either way it just doesn't stop flowing, the results of a perfectly developed anal orgasm that just never seems to end.
				t You have to actively force yourself off the toy and use a nearby crate to support yourself. Your hearing is shot and the room is wobbling, how long were you masturbating? It's hard to tell.
				im typhoidDildo3.gif
				player That... That was way too dangerous~
			`);
			break;
		}
		case "typhoidDoggy": { // CUT
			writeHTML(`
				t placeholder for `+n+`
			`);
			break;
		}
		case "typhoidDiscoveredAmy": { //CUT
			writeHTML(`
				t placeholder for `+n+`
			`);
			break;
		}
		case "typhoidDiscoveredWeak": {
			writeHTML(`
				t You've never had trouble focusing during these meetings before. Typically the life and death nature of the missions you volunteer for keeps you focused on the briefing material.
				t Instead you find your mind wandering. Thoughts of plump, soft lips. Spreading your viral load. The feeling of...
				im cagedWeak.gif
				t The flimsy, plastic cage you've trapped your cock inside.
				t It's damn near torture. The pressure from the cage forces your mind to stay aware of your cock at all times. Each flex of your pelvic muscles causes you to strain against the cage, and it's like your balls instinctively react to feeling trapped by increasing sperm production. You feel like you could jerk off, even through-
				keith playerF? You alright?
				player Y-yeah, sorry. Just visualizing it all in my head.
				keith Right. Well, the mission's tomorrow. 
				player <i>Not if I have anything to say about it...</i>
				keith Don't get too worked up over it. And have Lisa check you out after this, you don't look too good.
				t He continues to cover escape avenues, and brings up a few other locations to be voted on. You just barely pay attention, all the while rubbing yourself through your clothes.
				player <i>Need to fuck... Need to cum...</i>
				t You can feel your dick pulsing, you just <i>know</i> it's causing you to change somehow. You aren't meant to be caged like this...
				t *CRACK*
				lisa !flag infectedLisa; Did you hear that?
				t *SNAP*
				tyler !flag infectedTyler; What is that sound?
				t Your eyes are wide open as the sound of plastic falling down your pant leg onto the ground is followed by your cock tenting, hard.
				t You just can't hold out any longer. Fuck this! You need to-
				t There's a brief flash of pain, and then blackness.
				t ...
				lisa !flag infectedLisa; I can't believe it... Is the compound compromised?
				keith I have a backup in a nearby warehouse... We have to assume they're on their way.
				lisa !flag infectedLisa; What do we do with her?
				t ... 
				t With a groan, conciousness returns to you. A few realizations hit you very quickly. You're face down on the ground, you're missing pants, and you're outside the compound's front gate.
				player Fuck!
				t You shoulder check the gate, but it stands strong. There's no sign of anyone else, either. Keith must have taken the others and fled.
				t ?flag infectedMonica; The others you infected are probably still inside too, barricaded off to prevent them from escaping. The last order you gave them keeping them quiet the whole time.
				t You fall to your knees. Not out of regret for what you've done, but out of frustration that one small pocket of humanity has survived a bit longer.
				t But you are patient. You'll grow, improve. You'll find them again, eventually, and you look forwards to seeing what Keith turns into when his humanity finally fades, just like yours already has.
			`);
			break;
		}
		case "typhoidDiscoveredMeeting": {
			writeHTML(`
				t You've never had trouble focusing during these meetings before. Typically the life and death nature of the missions you volunteer for keeps you focused on the briefing material.
				t Instead you find your mind wandering. Thoughts of plump, soft lips. Spreading your viral load. The feeling of...
				im typhoidUndiscoveredHorny1.gif
				t Your absolutely rock-hard cock struggling to steal your attention.
				t It's damn near torture. Each pulse of your dick forces your mind to stay aware of your cock at all times. Each flex of your pelvic muscles causing your cock to throb, the way your heavy nuts shift, it's like your body is reacting to all the uninfected humans around you. Cum. You need to cum, to jerk off right now, even through-
				keith playerF? You alright?
				player Y-yeah, sorry. Just visualizing it all in my head.
				keith Right. Well, the mission's tomorrow. Don't get too worked up over it. And have Lisa check you out after this, you don't look too good.
				t He continues to cover escape avenues, and brings up a few other locations to be voted on. You just barely pay attention, all the while rubbing yourself through your clothes.
				t You just can't hold out any longer. No more wait-
				t There's a brief flash of pain, and then blackness.
				t ...
				lisa I can't believe it... Is the compound compromised?
				keith I think so. I have a backup in a nearby warehouse...
				lisa What do we do with her?
				t ... 
				t With a groan, conciousness returns to you. A few realizations hit you very quickly. You're face down on the ground, you're missing pants, and you're outside the compound's front gate.
				player Fuck!
				t You shoulder check the gate, but it stands strong. There's no sign of anyone else, either. Keith must have taken the others and fled.
				t ?flag infectedMonica; The others you infected are probably still inside too, barricaded off to prevent them from escaping. The last order you gave them keeping them quiet the whole time.
				t You fall to your knees. Not out of regret for what you've done, but out of frustration that one small pocket of humanity has survived a bit longer.
				t But you are patient. You'll grow, improve. You'll find them again, eventually, and you look forwards to seeing what Keith turns into when his humanity finally fades, just like yours already has.
			`);
			break;
		}
		case "typhoidUndiscoveredStrong": {
			writeHTML(`
				t You've never had trouble focusing during these meetings before. Typically the life and death nature of the missions you volunteer for keeps you focused on the briefing material.
				t Instead you find your mind wandering. Thoughts of plump, soft lips. Spreading your viral load. The feeling of...
				im typhoidUndiscoveredStrong.gif
				t The sturdy, metal cage you've trapped your cock inside.
				t It's damn near torture. The pressure from the cage forces your mind to stay aware of your cock at all times. Each flex of your pelvic muscles causes you to strain against the cage, and it's like your balls instinctively react to feeling trapped by increasing sperm production. You feel like you could jerk off, even through-
				keith playerF? You alright?
				player Y-yeah, sorry. Just visualizing it all in my head.
				keith Right. Well, the mission's tomorrow. 
				player <i>Not if I have anything to say about it...</i>
				keith Don't get too worked up over it. And have Lisa check you out after this, you don't look too good.
				t He continues to cover escape avenues, and brings up a few other locations to be voted on. You just barely pay attention, all the while rubbing yourself through your clothes.
				player <i>Need to fuck... Need to cum...</i>
				t You can feel your dick pulsing, you just <i>know</i> it's causing you to change somehow. You aren't meant to be caged like this, and when you're out you can instinctively tell you'll never fit in this cage again.
				t ... 
				t The meeting finishes, finally. Keith waves everyone off.
				t !flag infectedTyler; Tyler starts cleaning up.
				t !flag infectedAmy; Amy stretches, and heads out to prep for her lookout shift.
				t !flag infectedAngela; Angela seems to head out somewhere in a hurry.
				lisa !flag infectedLisa; Hey, you alright? You look seriously flushed and exhausted.
				player !flag infectedLisa; Yeah, I'm fine. Exercised pretty heavily earlier.
				lisa !flag infectedLisa; Alright then. Don't go too hard on yourself... Fuck, my head still aches.
				t Everything concluded you head back to your room as quickly as you can. You end up with tunnel vision as you push open the door, close it behind you, and grab the key in a flash.
				t And with a click, you're finally free. You toss the cage aside and look at yourself.
				im typhoidUndiscoveredStrong2.gif
				t You've clearly gained at least a few inches, and you leak a clear dollop of precum from your tip suggesting your balls are more active too. 
				t And yet... You feel focused. Honed. Now that you're free you don't need to jerk off anymore.
			`);
			break;
		}
		case "typhoidUndiscoveredHorny": {
			writeHTML(`
				t You've never had trouble focusing during these meetings before. Typically the life and death nature of the missions you volunteer for keeps you focused on the briefing material.
				t Instead you find your mind wandering. Thoughts of plump, soft lips. Spreading your viral load. The feeling of...
				im typhoidUndiscoveredHorny1.gif
				t Your absolutely rock-hard cock struggling to steal your attention.
				t It's damn near torture. Each pulse of your dick forces your mind to stay aware of your cock at all times. Each flex of your pelvic muscles causing your cock to throb, the way your heavy nuts shift, it's like your body is reacting to all the uninfected humans around you. Cum. You need to cum, to jerk off right now, even through-
				keith playerF? You alright?
				player Y-yeah, sorry. Just visualizing it all in my head.
				keith Right. Well, the mission's tomorrow. Don't get too worked up over it. And have Lisa check you out after this, you don't look too good.
				t He continues to cover escape avenues, and brings up a few other locations to be voted on. You just barely pay attention, all the while rubbing yourself through your clothes.
				t ... 
				im typhoidUndiscoveredHorny2.gif
				t Thank god you already took Lisa out of the equation. While it's still risky, at least you can jerk off without a guarentee of getting caught.
				t The pillar of boiling heat between your legs has made its demand, you can only obey. You're in another world despite the risk. In fact, just knowing you could be caught at any moment is only making you ready to cum harder.
				t The meeting finishes, finally. Keith waves everyone off and you don't even wait to see what everyone else is doing. You try to tuck away your cock as best you can before you leave clutching your head to act as though you feel sick. Only a puddle of precum remains as a sign of what's happened, hopefully nobody checks the room thoroughly before you finish infecting the others.
				t Making your way to the south side of the compound and your pants are around your ankles as soon as you have just enough privacy that you might be able to get away with this.
				t Your eyes are lidded and your breathing is just slightly ragged, but thankfully those are the last signs you're pumping out a truly massive load after fucking your fist for just a few short strokes. <i>This</i> is why the infection is so powerful, so effective. It takes the animalistic loss of reason humans have just before the greatest orgasm of their lives. That's the level an infected dickgirl hits where a human would be "mildly horny".
				t Your face is pressed against the stone wall as you hump your hand. You'll never go back. Never. If you could turn back now you'd refuse, you'd probably never be able to cum again with your previous body. This meaty pike, spitting out jizz so thick it clumps and drools slowly down the wall. Trying to get off with a clit would be like trying to cum from rubbing your tongue compared to this.
			`);
			break;
		}
		case "sabotageEndingAmy": {
			if (data.player.wsDisabled == true) {
				writeHTML(`
					This scene has been disabled due to your fetish settings. If you'd like to view it, re-enable watersports content and check the gallery.
				`);
			}
			else {
				writeHTML(`
					amy Seriously, you sure this is alright?
					player Yeah, Tyler said he'd manage it just fine.
					amy Aww, thanks! You know hiding stuff you find while scavenging is against the rules... But what the hell.<br>Jeez, the bottle is really warm, huh? I miss my fridge.
					t She takes the bottle and unscrews the cap.
					amy Jesus that smell, one sniff and my nose is fried to hell. What is this?
					player Some kind of health drink probably.
					t She takes a swig and there's a look of shock on her face as she almost spits it back out, but holds back and swallows.
					im sabotageEndingAmy1.gif
					t <b>Gulp</b>
					amy Ga-haad fucking damn, that tasted like it smelt! What's in this stuff?
					t She shakes the bottle curiously, a few droplets landing in her shirt.
					player Tastes bad? The bottle was sealed a while ago, so you're basically drinking straight from the tap.
					amy I mean I drink whiskey, I'm no pussy. Maybe I'll use it to keep me awake during the rest of my shift tonight.
					t She places the bottle down next to her and the two of you have a bit of smalltalk. It doesn't really go anywhere as Amy's mind is clearly still focused on the drink. Despite her claim she picks it back up and takes another swig.
					t <b>Gulp</b>
					amy Gah, it's like treating my tongue to an acid bath.
					player So why do you keep drinking it?
					amy Hah! Good point. Guess I'm thirstier than... Than I thought.
					t She keeps licking the inside of her mouth, her focus on the conversation keeps lapsing and she brings the bottle back to her lips.
					im sabotageEndingAmy2.gif
					t <b>Glug glug glug glug</b>
					t She breaks the seal she made with her lips to take a breath, panting like a dog, but quickly brings it back to drink more.
					t <b>Glug glug glug glug</b>
					t The bottle emptied she sucks in hard enough to cave in the plastic a little and runs her tongue along the rim to collect the last drops.
					player Damn, you were seriously thirsty.
					amy Y-yeah, guess I was...
					t She lifts her shirt and actually begins trying to suck out the few drops she spilt earlier.
					player You know, I could get you some more.
					t This has her attention, she dryly swallows clearly wanting as much as she can get.
					player Hey watch out for me, I need to take a piss.
					amy B-but you said you'd get me more... Gh...!
					player I am.
					t Instead of a more private position you strip down and whip out your dick right in front of Amy and aim yourself right at the dining table.
					t If there was ever any proof that the drink fried Amy's brain, it's the fact that instead of realizing you infected her, or calling for help...
					im drink1.gif
					t She takes one whiff and crawls towards you for another taste. As her lips wrap around the head of your cock and she starts to suckle from the source, she thrusts her hips forwards like she's cumming and you notice a bulge in her pants.
					player Stop. Show me.
					im drink2.gif
					t The infected formerly known as Amy lets out a small whine as she relieves herself.
					player Huh, you're an infected who gets off on pissing? Hmm~
				`);
			}
			break;
		}
		case "sabotageEndingTyler": {
			writeHTML(`
				t There's no fear, no hesitation, not for a second. The group needs him.
				tyler Hey! Over here! C'mon you bunch of shrimp-dicked bimbos, buffet at the front gate!
				t They're approaching on all sides, and even just a few extra seconds will buy the rest of the team some time, so without even waiting for so much as a 'any volunteers' Tyler raced to the front.
				t Despite the front gate being incredibly sturdy, despite the fact that he just needed to make some noise on the north end to draw some away from the back of the compound, despite the fact that by all accounts he shouldn't be in any danger, this is still undeniably an incredible act of bravery on Tyler's part.
				t One that is rewarded immediately as the gate swings open. The women were closer to the entrance than he realized, and the gate's lock was damaged earlier.
				t No fear, there's no time to react. He can't even manage to turn around before five are on him and the rest are running into the compound.
				tyler NO! LIS-
				t Muffled in a flash by a dogpile of women with bigger dicks than his. His brain says 'I always knew this is how I'd go', he hopes, dimly, that his brain was trying to make a joke, not reminding him of his darkest fantasies.
				t No fear. It's over, there's no point. His body accepts it quickly, there's not even a surge of adrenaline. He's been here before, admitedly only in his daydreams.
				tyler <i>I... If I can hold back five of them... Five of them... All at once...<br>I hope they don't tear Lisa's birthday gift...</i>
				t His clothes are torn, they don't seem to care about the lingerie he's wearing beneath them.
				im sabotageEndingTyler1.gif
				t A finely trained asshole takes to the women well. Nobody's around to judge him for being a whore, so...
				im sabotageEndingTyler2.gif
				tyler Fuuuh~! Yesshhh~
			`);
			break;
		}
		case "sabotageEndingAngela": {
			if (data.player.beastDisabled == true) {
				writeHTML(`
					This scene has been disabled due to your fetish settings. If you'd like to view it, re-enable beast content and check the gallery.
				`);
			}
			else {
				writeHTML(`
					im sabotageEndingAngela1.gif
					t There really was no hope for Angela. No real form of struggle. The moment the hound slipped through the hole you made and pounced on her, the moment she knew she was alone with this beast...
					angela Yes~! Yes~! Cum for mommy~! Mommy wants your huge, doggy knot inside of her~!
					t It's just a fetish, just masturbation. She tells herself this, deludes herself, but it's fine. Her mind will be gone to the pleasure soon enough, why stuggle? She just wants to indulge herself.
					t And it's exactly like how she always imagined it. Better, even. The thick knot slapping against her folds, the rapid humping far rougher, far more animalistic, far more wonderful than she could have hoped for.
					angela Yesyesyes~<br>Cum, please, cum please, I want to cum... Just as you...
					im sabotageEndingAngela2.gif
					angela FFFFFuuuuck~!
					t "Mommy" is satisfied, having held back her first orgasm to genuine dog-dick until she felt that knot forced inside of her. Until she felt that mindmeltingly hot animal cum pump against and into her womb.
					angela Ghhhfffuck yes~!<br>God I hope you have frieeeeends~!
					t And as she hears light steps approaching, she knows she's been granted her second wish of the day.
				`);
			}
			break;
		}
		case "sabotageEndingLisa": {
			writeHTML(`
				lisa Already inside, huh?! Eat shit, bitches!
				t *CLICK*
				lisa Wh-
				t She's tackled to the ground, not even so much as a dildo in reach to use as a weapon.
				lisa I won't! Go down! So eas-
				t In sync, the women who tackled her start rubbing themselves, stroking their steel-hard cocks right above her head.
				lisa N-no! Get off me! GET THE FUCK OFF-
				im sabotageEndingLisa1.gif
				t The first rope splatters across her face, followed by several more. It's probably the slowest infection method possible.
				t She spits, she sputters, she tries to turn away, but there's so much jizz packed onto her face after just a moment that if she wants to breath, she's taking the cum inside of her somehow.
				t She pushes forwards, and for some reason the crowd parts. She runs, trying to spit and rub away as much cum as she can, but some of it's in her mouth, some of it's probably down her throat already.
				lisa No, please, please, please!
				t She feels a pressure on her abdomen, she's running on instinct, unable to see under the blanket of infected jizz.
				t She feels cool air on her body, she must be outside.
				lisa Tyler! Help!
				t She's able to wipe off just enough cum to see out of one eye.
				t She's at the front gate, and Tyler is nowhere to be seen, just a wild orgy of more infected, and she collapses to her knees.
				lisa Why... Ghh~!
				t She clenches her stomach. She feels bloated, uncomfortable, scared.
				t *POP*
				t Relieved...
				t She can feel them dangling between her legs, although she doesn't want to believe it. Pulsing. Throbbing. Hungry. Full.
				t Several members of the orgy are moving. It was actually a gangbang, all centered around one small-dicked slut in the middle.
				lisa No more... I just... Want to give in.
				t She approaches the group's cumslut. She's...
				t ?flag infectedTyler; <b>Perfect</b>.
				t ?flag infectedTyler; Something about this girl... She's beautiful... Wonderful... But right now all she really cares about is finding some kind of release.
				t !flag infectedTyler; <b>Pathetic</b>.
				t !flag infectedTyler; The girl beneath her is small, weak, and more than anything looks like a sub-par sloppy fuck. But she doesn't need beauty right now, she needs to empty her balls of jizz.
				im sabotageEndingLisa2.gif
			`);
			break;
		}
		case "typhoidSabotageEnding": {
			writeHTML(`
				player And everything comes tumbling down~
				keith playerF! We need to-
				t He stops and stares. You're naked, so of course he realizes what happened immediately.
				keith You... You fucker! I'll kill you! How could you turn on us! We were like family!
				player Please, you would have thrown me out the moment you found out I was packing.
				keith That's... For the good of the compound, I'd-
				player <i>This</i> is what's good for the compound, buddy. Now, I'll be nice enough to let you choose. Amy? She'll go down easy. Lisa's a <i>beast</i> so I don't recommend her, and Tyler's her fucktoy, no way I'm separating them. And speaking of beasts, Angela-
				keith Please. We'd never abandon you. You can still talk, still think, there's still hope!
				t A dozen infected turn the hallway behind you, and a dozen more come in from the opposite side behind Keith.
				player Looks like you're stuck! But yeah, there's still hope. You were a good little boy and didn't notice as I sabotaged every defense in the compound. You didn't get in my way, so I'll be gentle~
				t ...
				player I wonder how much longer I have. Staying intelligent, I mean.
				t A soft, effeminate moan.
				player I've gotta find what kept my mind from turning. Truth is, I really, really want to find other survivors. Seeing all of you succumb? God, I might just splurt thinking about it.
				t A coo, a squeeze.
				player Hmm... So many in my army now. My horde. Keith, what do you think?
				im typhoidSabotageEnding.gif
				t The slave doesn't answer, instead moaning softly as you continue to tease her.
				player Come on, answer me~<br>If you do, I'll let Lisa rape you again~
				t She gasps, and wiggles her ass against your teasing hands.
				player Hmm, good enough~<br>Ah, it's good to be queen~
			`);
			break;
		}
		case "typhoidInfectionEnding": {
			writeHTML(`
				keith Amy! Amy where are you!
				t Silence.
				keith Tyler! We've got incoming! There could be sirens, where are the earplugs?!
				t Silence.
				keith Lisa!?
				t Silence.
				keith Angela! playerF! ANYONE!
				t *WHAM*
				t *THUD*
				player Quiet.
				t ...
				player Hmm... What to do next... Let's see...
				im typhoidInfectedEnding1.gif
				player Lisa's brutalizing Tyler's throat as usual. That rape addiction really never lets up, huh? I could use her as a frontliner though.
			`);
			if (data.player.wsDisabled != true) {
				writeHTML(`
					im typhoidInfectedEnding2.gif
					player Amy's still making a mess on herself. Still, I guess those maps have all the good supply locations. I should send her to taint some of them.
				`);
			}
			if (data.player.beastDisabled != true) {
				writeHTML(`
					im typhoidInfectedEnding3.gif
					player Angela's making love to her latest husband. Honestly, a girl could get jealous~<br>Still, having scouting hounds ready and loyal is worth a bit of envy.
				`);
			}
			writeHTML(`
				t The door to the strategy room creaks open.
				player Keeeeith? Have you stopped struggling yet?
				im typhoidInfectedEnding4.gif
				t A muffled, high-pitch scream, a grunt.
				player So it <i>was</i> chocolate! I'm so glad we had time to experiment. So, you still have your mind intact, right? I infected you <i>oh so sloooowly</i> after all. A dripfeed of cum, none of Lisa's of course!
				t Another muffled set of noises.
				player And I bet you're so cute under there! Hmm, I wonder what strain you'll end up being? I bet... Princess. That'd be hilarious!<br>But really, I do need a second in command, not <i>just</i> another fucktoy. So give me one shout for no, and two shouts for "please oh please my queen, let me out so that I can have my first orgasm and totally submit to the virus"!
				t "MPPPH, MPPPPH!!!!"
			`);
			break;
		}
		case "typhoidFailureEnding": {
			writeHTML(`
				t You keep your head down and try not to draw attention to yourself as you make your escape along with the rest of the survivors.
				tyler !flag infectedTyler; !flag sabotageTyler; They all knew where we were at once...
				amy !flag infectedAmy; !flag sabotageAmy; Why'd they all gather like an army? Hordes only form when infected were forced to group together, or when there's a huge mass of people!
				angela !flag infectedAngela; The hounds gathered in the back, like they knew where we'd try to escape.
				lisa !flag infectedLisa; !flag sabotageLisa; Maybe it's a new type?
				keith I know what's going on. Follow me.
				t Keith guides the group towards an old warehouse, and inside is a well-maintained van.
				keith Just in case of emergencies. Alright, let me help you all inside.
				t But when it's your turn to step inside, the door is slammed shut.
				amy !flag infectedAmy; !flag sabotageAmy; Keith, what are you doing?!
				tyler !flag infectedTyler; !flag sabotageTyler; Hey, the door's locked, playerF's-
				keith I was part of another survivor group before. Smart, well-prepared, organized. None of it mattered when a queen strain arose.
				t You bite your lip and try to force the van's door open, no luck.
				keith I don't know how much of you is left in there, but out of respect for what you've done for all of us up until now...<br>I hope you enjoy the rest of your life. If you can even call it that.
				lisa !flag infectedLisa; !flag sabotageLisa; Hold on, we don't know she's infected for sure!
				keith Goodbye, playerF.
				t Keith drives off, faster than the horde you command can catch up, and you fall to your knees. Not out of regret for what you've done, but out of frustration that one small pocket of humanity has survived a bit longer.
				t But you are patient. You'll grow, improve. You'll find them again, eventually, and you look forwards to seeing what Keith turns into when his humanity finally fades, just like yours already has.
			`);
			break;
		}
		case "typhoidMonicaEpilogue": {
			writeHTML(`
				monica What the fuck... No!
				t Through a busted pair of binoculars Monica watches as the only people left for her are taken mercilessly by the infected she's struggled to survive against her entire life.
				monica I've gotta... Someone has to be... Anyone! Please!
				t She chokes back her emotions as one of the women surrounding the compound seems to turn towards her. She has to escape. To survive! For everyone who's been lost to the infection. For Tyler, Amy, Angela, Lisa, Keith...
				monica playerF... If you're still out there, I'll find you, and if you aren't...
				t She takes a breath to center herself.
				monica I'll live on for you.
				t The compound has fallen, with no survivors.
				t Except one.
			`);
			break;
		}
		//System Stuff
		default: {
			writeText("Something went wrong, and you've encountered a bug. Keep in mind where you just where and what you did, and let me know so I can fix it. <b>ERROR CODE:</b> Event Write Failure, Event "+n+" does not exist.");
			writeText("Here's a list of important details. If you message me directly with these jams, I should have a better idea of what caused the problem:");
			document.getElementById('output').innerHTML += JSON.stringify(data);
			writeText("Inventory window:" + invHidden + "");
			writeText("Browser:" + navigator.appCodeName  + "");
			writeText("OS:" + navigator.platform  + "");
			writeBig("images/butts.jpg");
			writeTransition("start", "Go back to the title.");
		}
	}
	unlockScene(n);
	if (data.player.currentScene == "gallery") {
		writeTransition("gallery", "Back to the gallery");
	}
}